<?php

namespace App\Jobs\LocalUsers;

use App\Mail\Registration\ConfirmRegistered;
use App\Models\LocalUser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailConfirmRegistered implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(protected LocalUser $localUser)
    {
    }

    public function handle(): void
    {
        Mail::to($this->localUser->email)->send(new ConfirmRegistered($this->localUser));
    }
}
