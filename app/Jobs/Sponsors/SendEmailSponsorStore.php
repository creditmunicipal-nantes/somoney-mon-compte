<?php

namespace App\Jobs\Sponsors;

use App\Mail\Sponsor;
use App\Models\CyclosUser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailSponsorStore implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(
        protected string $recipient,
        protected string $senderName,
        protected string $message,
        protected CyclosUser $user
    ) {
    }

    public function handle(): void
    {
        Mail::to($this->recipient)->send(new Sponsor($this->senderName, $this->message, $this->user));
    }
}
