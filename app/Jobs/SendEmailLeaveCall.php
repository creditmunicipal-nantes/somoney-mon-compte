<?php

namespace App\Jobs;

use App\Mail\LeaveCall;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailLeaveCall implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(protected array $user)
    {
    }

    public function handle(): void
    {
        Mail::to(config('somoney.mail.account'))->send(new LeaveCall($this->user));
    }
}
