<?php

namespace App\Jobs;

use App\Mail\Operations\Transfers\SimpleReceived;
use App\Mail\Operations\Transfers\SimpleSent;
use App\Models\CyclosUser;
use App\Services\Cyclos\TransferService;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailsTransferMobile implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(protected string $transferId)
    {
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function handle(): void
    {
        $userService = new UserService();
        $transfer = app(TransferService::class)->get($this->transferId);
        $channel = data_get($transfer, 'transaction.channel.internalName');
        if ($channel === 'mobile') {
            $applicant = CyclosUser::fill($userService->get(data_get($transfer, 'from.user.id')));
            $recipient = CyclosUser::fill($userService->get(data_get($transfer, 'to.user.id')));
            $transferDate = new Carbon($transfer['date']);
            $transferDetails = [
                'fromUser' => $applicant,
                'toUser' => $recipient,
                'amount' => $transfer['amount'],
                'label' => $transfer['type']['name'],
                'date' => $transferDate->format('d/m/Y'),
                'hour' => $transferDate->format('H:i'),
            ];
            $isScheduled = $transferDate->diffInDays(today()) !== 0;
            Mail::to($applicant->email)
                ->send(new SimpleSent(TransferService::MOBILE_TRANSFER, $transferDetails, $isScheduled));
            Mail::to($recipient->email)
                ->send(new SimpleReceived(TransferService::MOBILE_TRANSFER, $transferDetails, $isScheduled));
        }
    }
}
