<?php

namespace App\Jobs\Fidelity;

use App\Mail\Fidelity\FidelityReceive;
use App\Mail\Fidelity\FidelitySent;
use App\Models\CyclosUser;
use App\Models\Fidelity;
use App\Services\Cyclos\TransactionService;
use App\Services\Cyclos\TransferService;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class ApplyFidelity implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(protected string $transferId)
    {
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function handle(): void
    {
        $transferService = app(TransferService::class);
        $userService = app(UserService::class);
        $transfer = $transferService->get($this->transferId);
        if ($transfer) {
            $fidelity = Fidelity::where('cyclos_user_id', $transfer['to']['user']['id'])->active()->first();
            if (
                $fidelity
                && $this->canApplyFidelity(
                    data_get($transfer, 'from.user.id'),
                    data_get($transfer, 'to.user.id'),
                    $fidelity
                )
            ) {
                $fromUser = CyclosUser::fill($userService->get(data_get($transfer, 'from.user.id')));
                if ($fidelity->groups_type === 'all' || $fromUser->group === $fidelity->groups_type) {
                    $toUser = CyclosUser::fill($userService->get(data_get($transfer, 'to.user.id')));
                    $this->applyFidelity(
                        $fromUser,
                        (string) data_get($transfer, 'to.user.id'),
                        $fidelity,
                        $transfer
                    );
                    $transactionDate = new Carbon($transfer['date']);
                    $transactionDetails = [
                        'fromUser' => $fromUser,
                        'toUser' => $toUser,
                        'amount' => $transfer['amount'],
                        'label' => $transfer['type']['name'],
                        'date' => $transactionDate->format('d/m/Y'),
                        'hour' => $transactionDate->format('H:i'),
                    ];
                    Mail::to($fromUser->email)->send(new FidelityReceive($transactionDetails));
                    Mail::to($toUser->email)->send(new FidelitySent($transactionDetails));
                }
            }
        }
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    private function canApplyFidelity(int $fromUserId, int $toUserId, Fidelity $fidelityRule): bool
    {
        $transactions = app(TransactionService::class)->listOfUser([
            'userId' => $fromUserId,
            'user' => $toUserId,
            'pageSize' => 1000,
        ], true);

        if (! $transactions instanceof LengthAwarePaginator) {
            return false;
        }

        if ($fidelityRule->counter_as_price) {
            return $this->calculateFidelityIsPrice($transactions, $fidelityRule);
        }

        return $this->calculateFidelityIsNbProducts($transactions, $fidelityRule);
    }

    private function calculateFidelityIsPrice(LengthAwarePaginator $transactions, Fidelity $fidelityRule): bool
    {
        $spent = 0;
        foreach ($transactions->items() as $transaction) {
            if ($transaction['description'] !== 'Fidélité : réduction') {
                $spent += abs($transaction['amount']);
            } else {
                break;
            }
        }

        return $spent >= $fidelityRule->counter;
    }

    private function calculateFidelityIsNbProducts(LengthAwarePaginator $transactions, Fidelity $fidelityRule): bool
    {
        $nbPurchased = 0;
        foreach ($transactions->items() as $transaction) {
            if ($transaction['description'] !== 'Fidélité : réduction') {
                ++$nbPurchased;
            } else {
                break;
            }
        }

        return $nbPurchased >= $fidelityRule->counter;
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    private function applyFidelity(
        CyclosUser $fromUser,
        string $toUserId,
        Fidelity $fidelityRule,
        array $transfer
    ): void {
        $amount = $transfer['amount'];
        $discount = $fidelityRule->discount_as_percent
            ? ($amount / 100 * $fidelityRule->discount)
            : $fidelityRule->discount;
        $discount = min($discount, $amount);
        app(TransactionService::class)->performAsUser(
            $toUserId,
            $fromUser->id,
            $discount,
            'Fidélité : réduction',
            'professional.to' . ucfirst($fromUser->group)
        );
    }
}
