<?php

namespace App\Jobs\SystemPay;

use App\Jobs\ValidPaymentTransaction;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use Kblais\Uuid\Uuid;

class TransactionCallback extends ValidPaymentTransaction
{
    use Uuid;

    protected $uuidVersion = 4;

    public function __construct(public array $paymentData)
    {
        parent::__construct(Transaction::find($paymentData['orderDetails']['orderId']));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \App\Exceptions\FailCreateUserException
     * @throws \ErrorException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    public function handle(): void
    {
        $paymentStatus = config('system-pay.status')[$this->paymentData['orderStatus']];
        $this->storeResponse($paymentStatus);
        if ($paymentStatus === Transaction::STATUS_PROCESSING) {
            return;
        }
        if ($paymentStatus === Transaction::STATUS_SUCCESS) {
            $this->processSuccess();

            return;
        }
        if (
            $this->transaction->status !== Transaction::STATUS_SUCCESS
            && $paymentStatus === Transaction::STATUS_FAILED
        ) {
            $this->applyStatusTransactions(Transaction::STATUS_FAILED);

            return;
        }
    }

    /**
     * @param string $status
     *
     * @throws \JsonException
     */
    protected function storeResponse(string $status): void
    {
        DB::table('system_pay_logs')->insert([
            'id' => $this->generateUuid(),
            'transaction_id' => $this->transaction->id,
            'status' => $status,
            'response' => json_encode(request()->all(), JSON_THROW_ON_ERROR),
            'created_at' => now(),
        ]);
    }
}
