<?php

namespace App\Jobs;

use App\Mail\Registration\NeedMembership;
use App\Models\LocalUser;
use App\Models\UserToken;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailNeedMembership implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(public ?\App\Models\LocalUser $localUser)
    {
    }

    public function handle(): void
    {
        $token = UserToken::create([
            'user_id' => $this->localUser->id,
            'type' => UserToken::TYPE_REGISTRATION_NEED_MEMBERSHIP,
        ]);
        Mail::to($this->localUser->email)
            ->send(new NeedMembership($this->localUser, $token));
    }
}
