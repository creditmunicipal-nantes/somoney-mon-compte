<?php

namespace App\Jobs;

use App\Mail\Registration\Resume;
use App\Models\LocalUser;
use App\Models\UserToken;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailResumeRegistration implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected LocalUser $localUser;

    public function __construct(string $userId)
    {
        $this->localUser = LocalUser::find($userId);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $token = UserToken::create([
            'user_id' => $this->localUser->id,
            'type' => UserToken::TYPE_RESUME_REGISTRATION,
        ]);

        Mail::to($this->localUser->email)
            ->send(new Resume($this->localUser, $token));
    }
}
