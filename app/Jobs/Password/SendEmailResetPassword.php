<?php

namespace App\Jobs\Password;

use App\Mail\ResetPassword;
use App\Models\CyclosUser;
use App\Models\UserToken;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailResetPassword implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(
        protected string $recipient,
        protected CyclosUser $user,
        protected UserToken $token,
        protected string $type
    ) {
    }

    public function handle(): void
    {
        Mail::to($this->recipient)->queue(new ResetPassword($this->user, $this->token, $this->type));
    }
}
