<?php

namespace App\Jobs\Operations;

use App\Mail\Operations\PaymentRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailPaymentRequest implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(
        protected string $email,
        protected string $title,
        protected string $amount,
        protected array $userData
    ) {
    }

    public function handle(): void
    {
        Mail::to($this->email)->send(new PaymentRequest($this->title, $this->amount, $this->userData));
    }
}
