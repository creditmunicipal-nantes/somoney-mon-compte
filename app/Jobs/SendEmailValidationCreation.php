<?php

namespace App\Jobs;

use App\Mail\Registration\EmailValidation;
use App\Models\LocalUser;
use App\Models\UserToken;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailValidationCreation implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public LocalUser $localUser;

    public function __construct(string $userId, protected bool $isRemind = false)
    {
        $this->localUser = LocalUser::findOrFail($userId);
    }

    public function handle(): void
    {
        $token = UserToken::create(['user_id' => $this->localUser->id, 'type' => UserToken::TYPE_EMAIL_VALIDATION]);
        Mail::to($this->localUser->email)->send(new EmailValidation($this->localUser, $token, $this->isRemind));
    }
}
