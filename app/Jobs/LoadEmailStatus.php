<?php

namespace App\Jobs;

use App\Models\LocalUser;
use App\Models\ValidationUser;
use App\Services\Cyclos\UserService;
use App\Services\MailgunService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Bus;

class LoadEmailStatus implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public Collection $userCyclosIds;

    public Collection $localUsers;

    protected MailgunService $mailgunService;

    public function __construct(
        Collection $userCyclosIds = null,
        Collection $localUsers = null
    ) {
        $this->userCyclosIds = $userCyclosIds ?? new Collection();
        $this->localUsers = $localUsers ?? new Collection();
    }

    public function handle(): void
    {
        $cyclosUsers = app(UserService::class)->searchAll([
            'groups' => implode(',', array_values(config('cyclos.internal-names.groups'))),
        ]);
        $this->userCyclosIds->each(function (string $userCyclosId) use ($cyclosUsers) {
            $cyclosUser = $cyclosUsers->where('id', $userCyclosId)->first();
            $validationUser = ValidationUser::where('cyclos_id', $userCyclosId)->first();
            if (! $validationUser) {
                $validationUser = (new ValidationUser(['cyclos_id' => $userCyclosId]));
            }
            $validationUser->email = $cyclosUser['email'];
            $validationUser->group = $cyclosUser['group']['internalName'];
            $validationUser->save();
            Bus::dispatch(new MailgunUpdateUserEmailStatus($validationUser));
        });
        $this->localUsers->each(function (LocalUser $localUser) {
            Bus::dispatch(new MailgunUpdateUserEmailStatus($localUser->validation_user));
        });
    }
}
