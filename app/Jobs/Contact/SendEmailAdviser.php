<?php

namespace App\Jobs\Contact;

use App\Mail\ContactAdviser;
use App\Models\CyclosUser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailAdviser implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected bool $remind = false;

    public function __construct(protected string $recipient, protected string $message, protected CyclosUser $user)
    {
    }

    public function handle(): void
    {
        Mail::to($this->recipient)->send(new ContactAdviser($this->message, $this->user));
    }
}
