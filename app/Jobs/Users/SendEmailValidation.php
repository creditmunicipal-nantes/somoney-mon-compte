<?php

namespace App\Jobs\Users;

use App\Mail\EmailValidation;
use App\Models\CyclosUser;
use App\Models\UserToken;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailValidation implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(
        protected string $recipient,
        protected CyclosUser $user,
        protected UserToken $token,
        protected bool $remind = false
    ) {
    }

    public function handle(): void
    {
        Mail::to($this->recipient)->send(new EmailValidation($this->user, $this->token, $this->remind));
    }
}
