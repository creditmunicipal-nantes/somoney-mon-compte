<?php

namespace App\Jobs\Memberships;

use App\Mail\Memberships\RenewSucceed;
use App\Models\CyclosUser;
use App\Services\Cyclos\UserService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailRenewSucceed implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected CyclosUser $cyclosUser;

    public function __construct(string $userId)
    {
        $this->cyclosUser = CyclosUser::fill(app(UserService::class)->get($userId));
    }

    public function handle(): void
    {
        Mail::to($this->cyclosUser->email)->send(new RenewSucceed($this->cyclosUser->name));
    }
}
