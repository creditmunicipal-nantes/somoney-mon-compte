<?php

namespace App\Jobs\Memberships;

use App\Mail\Memberships\RenewCreated;
use App\Models\UserToken;
use App\Services\Cyclos\UserService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailRenewCreated implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(public string $cyclosUserId)
    {
    }

    public function handle(): void
    {
        $user = app(UserService::class)->get($this->cyclosUserId);
        $userToken = UserToken::create([
            'user_id' => $this->cyclosUserId,
            'type' => UserToken::TYPE_MEMBERSHIP_RENEW,
        ]);
        Mail::to($user['email'])->send(new RenewCreated($userToken, $user['display']));
    }
}
