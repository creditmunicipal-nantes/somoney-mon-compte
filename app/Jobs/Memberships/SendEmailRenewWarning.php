<?php

namespace App\Jobs\Memberships;

use App\Mail\Memberships\RenewWarning;
use App\Models\UserToken;
use App\Services\Cyclos\UserService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailRenewWarning implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(protected string $cyclosUserId)
    {
    }

    public function handle(): void
    {
        $user = app(UserService::class)->get($this->cyclosUserId);
        $userToken = UserToken::where('user_id', $this->cyclosUserId)
            ->where('type', UserToken::TYPE_MEMBERSHIP_RENEW)
            ->orderby('created_at', 'desc')
            ->first();
        Mail::to($user['email'])->send(new RenewWarning($userToken, $user['display']));
    }
}
