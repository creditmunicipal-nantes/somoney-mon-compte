<?php

namespace App\Jobs\LemonWay;

use App\Models\Wallet;
use Illuminate\Support\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use LemonWay\Lib\LwException;

class UpdateWalletJob
{
    use Dispatchable;
    use Queueable;

    /**
     * Create a new job instance.
     *
     * @param \App\Models\Wallet $wallet
     */
    public function __construct(protected Wallet $wallet)
    {
    }

    /**
     * Execute the job.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $birthday = data_get($this->wallet->user->customValues, 'birthday');
        $walletData = [
            'wallet' => $this->wallet->lw_id,
            'newEmail' => $this->wallet->user->email,
            'newTitle' => data_get($this->wallet->user->customValues, 'gender') === 'man' ? 'M' : "F",
            'newFirstName' => data_get($this->wallet->user->customValues, 'firstname'),
            'newLastName' => data_get($this->wallet->user->customValues, 'lastname'),
            'newStreet' => data_get($this->wallet->user->address, 'addressLine1'),
            'newPostCode' => data_get($this->wallet->user->address, 'zip'),
            'newCity' => data_get($this->wallet->user->address, 'city'),
            'newCtry' => 'FRA',
            'newPhoneNumber' => '',
            'newMobileNumber' => $this->wallet->user->phone,
            'newBirthDate' => $birthday ? Carbon::parse($birthday)->format('d/m/Y') : null,
            'newBirthcity' => '',
            'newBirthcountry' => '',
            'payerOrBeneficiary' => '1',
        ];
        // legacy code - commented 27/03/20 - upgrade to php7.4
        //        if ($this->wallet->user->entity_type === User::ENTITY_TYPE_COMPANY) {
        //            $walletData = array_merge($walletData, [
        //                'newIsCompany' => true,
        //                'newCompanyName' => $this->wallet->user->company->name,
        //                'newCompanyIdentificationNumber' => $this->wallet->user->company->siren,
        //            ]);
        //        }

        /** @var \LemonWay\ApiResponse $response */
        $response = app('lemonway')->UpdateWalletDetails($walletData);

        if ($response->lwError) {
            throw new LwException(
                'Error Lemonway : Code ' .
                $response->lwError->CODE .
                ' : ' .
                $response->lwError->MSG
            );
        }

        return $response;
    }
}
