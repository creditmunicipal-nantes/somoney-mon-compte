<?php

namespace App\Jobs\LemonWay;

use App\Models\Wallet;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class RegisterIBANJob
{
    use Dispatchable;
    use Queueable;

    public function __construct(protected Wallet $wallet, protected array $data)
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle(): void
    {
        /** @var \LemonWay\ApiResponse $response */
        $response = app('lemonway')->RegisterIBAN([
            'wallet' => $this->wallet->lw_id,
            'holder' => data_get($this->data, 'holder'),
            'bic' => data_get($this->data, 'bic', ''),
            'iban' => data_get($this->data, 'iban'),
            'dom1' => data_get($this->data, 'dom1', ''),
            'dom2' => data_get($this->data, 'dom2', ''),
        ]);

        if ($response->lwError) {
            throw new Exception('Error Lemonway : Code ' . $response->lwError->CODE . ' : ' . $response->lwError->MSG);
        }
    }
}
