<?php

namespace App\Jobs\LemonWay;

use App\Models\Transaction;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class SendPaymentJob
{
    use Dispatchable;
    use Queueable;

    /**
     * Create a new job instance.
     *
     * @param \App\Models\Transaction $transaction
     * @param int $amount
     */
    public function __construct(protected Transaction $transaction, protected int $amount)
    {
    }

    /**
     * Execute the job.
     *
     * @return \App\Models\Transaction
     * @throws \Exception
     */
    public function handle()
    {
        /** @var \LemonWay\ApiResponse $response */
        $response = app('lemonway')->SendPayment([
            'debitWallet' => config('lemonway.vad_account'),
            'creditWallet' => 'SC',
            'amount' => number_format($this->amount / 100, 2, '.', ''),
            'privateData' => 'Transaction #' . $this->transaction->lw_id,
        ]);

        if ($response->lwError) {
            $this->transaction->failure_reason = (string) $response->lwXml->E->Msg;
            $this->transaction->status = Transaction::STATUS_FAILED;
            $this->transaction->save();

            throw new Exception($this->transaction->failure_reason);
        }

        $this->transaction->status = Transaction::STATUS_SUCCESS;
        $this->transaction->save();

        return $this->transaction;
    }
}
