<?php

namespace App\Jobs\LemonWay;

use App\Models\LocalUser;
use App\Models\Wallet;
use Illuminate\Support\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class RegisterWalletJob
{
    use Dispatchable;
    use Queueable;

    public function __construct(protected LocalUser $localUser, protected Wallet $wallet)
    {
    }

    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        $this->wallet->lw_id = Wallet::generateLemonWayId();
        $this->wallet->save();

        $birthday = data_get($this->localUser->customValues, 'birthday');

        $walletData = [
            'wallet' => $this->wallet->lw_id,
            'walletIp' => request()->ip(),
            'clientMail' => $this->localUser->email,
            'clientTitle' => data_get($this->localUser->customValues, 'gender') === 'man' ? 'M' : "F",
            'clientFirstName' => data_get($this->localUser->customValues, 'firstname'),
            'clientLastName' => data_get($this->localUser->customValues, 'lastname'),
            'street' => data_get($this->localUser->address, 'addressLine1'),
            'postCode' => data_get($this->localUser->address, 'zip'),
            'city' => data_get($this->localUser->address, 'city'),
            'ctry' => 'FRA',
            'nationality' => 'FRA',
            'phoneNumber' => '',
            'mobileNumber' => $this->localUser->phone,
            'birthdate' => $birthday ? Carbon::parse($birthday)->format('d/m/Y') : null,
            'birthcity' => '',
            'birthcountry' => '',
            'payerOrBeneficiary' => '1',
        ];

        /** @var \LemonWay\ApiResponse $response */
        $response = app('lemonway')->RegisterWallet($walletData);
        $this->localUser->setCustomValue('lemonwayId', $this->wallet->lw_id);
        $this->localUser->save();

        if ($response->lwError) {
            throw new Exception('Error Lemonway : Code ' . $response->lwError->CODE . ' : ' . $response->lwError->MSG);
        }

        return $response;
    }
}
