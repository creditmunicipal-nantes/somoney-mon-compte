<?php

namespace App\Jobs\LemonWay;

use App\Models\Transaction;
use Illuminate\Support\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class GetMoneyInDetailsJob
{
    use Dispatchable;
    use Queueable;

    public function __construct(protected Transaction $transaction)
    {
    }

    /** @throws \Exception */
    public function handle(): array
    {
        /** @var \LemonWay\ApiResponse $response */
        $response = app('lemonway')->GetMoneyInTransDetails([
            'transactionId' => $this->transaction->lw_id,
        ]);

        if ($response->lwError) {
            throw new Exception('Error Lemonway : Code ' . $response->lwError->CODE . ' : ' . $response->lwError->MSG);
        }

        return [
            'id' => (string) $response->lwXml->TRANS->HPAY->ID,
            'date' => Carbon::createFromFormat('d/m/Y H:i:s', (string) $response->lwXml->TRANS->HPAY->DATE),
            'amount' => (int) ((float) ($response->lwXml->TRANS->HPAY->CRED ?: 0) * 100),
            'commission' => (int) ((float) ($response->lwXml->TRANS->HPAY->COM ?: 0) * 100),
            'status' => (int) $response->lwXml->TRANS->HPAY->STATUS,
            'error_message' => (string) $response->lwXml->TRANS->HPAY->INT_MSG,
            'refund' => (int) ((float) ($response->lwXml->TRANS->HPAY->REFUND ?: 0) * 100),
        ];
    }
}
