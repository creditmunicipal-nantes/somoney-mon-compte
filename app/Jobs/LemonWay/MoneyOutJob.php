<?php

namespace App\Jobs\LemonWay;

use App\Models\Wallet;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class MoneyOutJob
{
    use Dispatchable;
    use Queueable;

    public function __construct(protected Wallet $wallet, protected int $amount, protected int $commission = 0)
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Exception
     */
    public function handle(): void
    {
        $params = [
            'wallet' => $this->wallet->lw_id,
            'amountTot' => number_format(round($this->amount / 100, 2), 2, '.', ''),
            'amountCom' => number_format(round($this->commission / 100, 2), 2, '.', ''),
        ];

        if (config('somoney.available.services.lemonway-v2')) {
            $params['ibanId'] = config('lemonway.iban_id');
            $params['message'] = 'Virement hebdomadaire de Lemonway par SoMoney';
        }

        /** @var \LemonWay\ApiResponse $response */
        $response = app('lemonway')->MoneyOut($params);

        if ($response->lwError) {
            throw new Exception('Error Lemonway : Code ' . $response->lwError->CODE . ' : ' . $response->lwError->MSG);
        }
    }
}
