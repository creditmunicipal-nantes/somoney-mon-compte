<?php

namespace App\Jobs\LemonWay;

use App\Jobs\ValidPaymentTransaction;
use App\Models\Transaction;
use Illuminate\Support\Facades\Bus;
use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Arr;
use LemonWay\Lib\LwException;

class TransactionStatusUpdate extends ValidPaymentTransaction
{
    /**
     * Execute the job.
     *
     * @return void
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \App\Exceptions\FailCreateUserException
     * @throws \ErrorException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     * @throws \LemonWay\Lib\LwException
     */
    public function handle(): void
    {
        $lwTransaction = $this->getLemonwayTransaction();

        if ((int) $lwTransaction->INT_STATUS !== config('lemonway.status.pending')) {
            $transactionStatus = (int) $lwTransaction->INT_STATUS;
            if ($transactionStatus === config('lemonway.status.success')) {
                $this->processSuccess();
                if (config('somoney.available.services.lemonway-v2')) {
                    Bus::dispatch(new SendPaymentJob($this->transaction, $this->getLemonwayTransactionAmount()));
                }
            } elseif ($transactionStatus === config('lemonway.status.error')) {
                $this->applyStatusTransactions(Transaction::STATUS_FAILED);
            }
        }
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     * @throws \LemonWay\Lib\LwException
     */
    protected function getLemonwayTransaction()
    {
        $response = (new GuzzleClient())->request(
            'POST',
            config('lemonway.directkit_json2') . '/GetMoneyInTransDetails',
            [
                'json' => [
                    "p" => [
                        "wlLogin" => config('lemonway.login'),
                        "wlPass" => config('lemonway.password'),
                        "language" => app()->getLocale(),
                        "version" => "10.0",
                        "walletIp" => "82.227.211.73",
                        "walletUa" => "",
                        "transactionId" => $this->transaction->lw_id,
                        "transactionComment" => "",
                        "transactionMerchantToken" => "",
                        "startDate" => $this->transaction->created_at->subHour()->setTimezone('UTC')->timestamp,
                        "endDate" => now()->setTimezone('UTC')->timestamp,
                    ],
                ],
            ]
        );

        $jsonResponse = json_decode($response->getBody()->getContents(), false, 512, JSON_THROW_ON_ERROR);
        if ($jsonResponse->d->E) {
            throw new LwException($jsonResponse->d->E->Msg, $jsonResponse->d->E->Code);
        }

        return Arr::first($jsonResponse->d->TRANS->HPAY);
    }

    protected function getLemonwayTransactionAmount(): int
    {
        if ($this->transaction->context === Transaction::CONTEXT_REGISTRATION) {
            return $this->transaction->amount + $this->getLinkedTransactions()->filter()
                    ->sum(fn(Transaction $transaction) => $transaction->amount);
        }

        return $this->transaction->amount;
    }
}
