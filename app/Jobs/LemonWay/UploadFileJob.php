<?php

namespace App\Jobs\LemonWay;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class UploadFileJob
{
    use Dispatchable;
    use Queueable;

    /**
     * Create a new job instance.
     *
     * @param mixed $kyc
     */
    public function __construct(protected $kyc)
    {
    }

    /**
     * Execute the job.
     *
     * @throws \Exception
     */
    public function handle()
    {
        /** @var \LemonWay\ApiResponse $response */
        $response = app('lemonway')->UploadFile([
            'wallet' => $this->kyc->wallet->lw_id,
            'fileName' => basename($this->kyc->currentVersion->file->getFirstMediaPath()),
            'type' => $this->kyc->type,
            'buffer' => base64_encode(file_get_contents($this->kyc->currentVersion->file->getFirstMediaPath())),
        ]);

        if ($response->lwError->CODE) {
            throw new Exception('Error Lemonway : Code ' . $response->lwError->CODE . ' : ' . $response->lwError->MSG);
        }

        return $response;
    }
}
