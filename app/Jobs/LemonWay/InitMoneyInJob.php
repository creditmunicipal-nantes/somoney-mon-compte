<?php

namespace App\Jobs\LemonWay;

use App\Models\Transaction;
use App\Models\Wallet;
use Exception;
use Illuminate\Foundation\Bus\Dispatchable;
use Ramsey\Uuid\Uuid;

class InitMoneyInJob
{
    use Dispatchable;

    public function __construct(
        protected int $amount,
        protected ?string $userId = null,
        protected ?Wallet $wallet = null,
        protected string $context = Transaction::CONTEXT_OPERATION,
        protected int $commission = 0,
        protected ?Transaction $transaction = null
    ) {
    }

    /** @throws \Exception */
    public function handle(): string
    {
        $wkToken = bin2hex(openssl_random_pseudo_bytes(25));
        if (config('lemonway.mode') === 'vad' || config('somoney.available.services.lemonway-v2')) {
            $walletLemonwayId = config('lemonway.vad_account');
        } else {
            $walletLemonwayId = $this->wallet->lw_id;
        }
        if (! $this->transaction) {
            /** @var \App\Models\Transaction $transaction */
            $transaction = Transaction::create([
                'type' => Transaction::TYPE_LEMONWAY,
                'target_wallet_id' => ! config('somoney.available.services.lemonway-v2')
                    ? $this->wallet?->id
                    : null,
                'wk_token' => $wkToken,
                'status' => Transaction::STATUS_PROCESSING,
                'amount' => $this->amount,
                'context' => $this->context,
            ]);
        } else {
            $transaction = $this->transaction;
            $transaction->update([
                'wk_token' => $wkToken,
                'target_wallet_id' => ! config('somoney.available.services.lemonway-v2')
                    ? $this->wallet?->id
                    : null,
            ]);
        }
        if (Uuid::isValid($this->userId)) {
            $transaction->update(['local_user_id' => $this->userId]);
        } else {
            $transaction->update(['cyclos_user_id' => $this->userId]);
        }
        /** @var \LemonWay\ApiResponse $response */
        $response = app('lemonway')->MoneyInWebInit([
            'wallet' => $walletLemonwayId,
            'amountTot' => number_format(round($this->amount / 100, 2), 2, '.', ''),
            'amountCom' => number_format(round($this->commission / 100, 2), 2, '.', ''),
            'wkToken' => $wkToken,
            'returnUrl' => route('lemonway.success', ['context' => $this->context]),
            'errorUrl' => route('lemonway.error', ['context' => $this->context]),
            'cancelUrl' => route('lemonway.cancel', ['context' => $this->context]),
            'autoCommission' => 0,
        ]);
        if ($response->lwError) {
            $message = 'Error Lemonway : Code ' . $response->lwError->CODE . ' : ' . $response->lwError->MSG;
            throw new Exception($message);
        }
        $transaction->update([
            'lw_id' => $response->lwXml->MONEYINWEB->ID,
            'lw_token' => $response->lwXml->MONEYINWEB->TOKEN,
        ]);

        return config('lemonway.webkit.url') . '?' . http_build_query([
                'moneyintoken' => (string) $response->lwXml->MONEYINWEB->TOKEN,
                'p' => mix('/assets/css/somoney-iframe.css'),
                'lang' => config('lemonway.lang'),
            ]);
    }
}
