<?php

namespace App\Jobs\LemonWay;

use App\Models\Wallet;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use LemonWay\Lib\LwException;

class GetWalletDetailsJob
{
    use Dispatchable;
    use Queueable;

    public function __construct(protected Wallet $wallet)
    {
    }

    /**
     * Execute the job.
     *
     * @return array
     * @throws \LemonWay\Lib\LwException
     */
    public function handle(): array
    {
        /** @var \LemonWay\ApiResponse $response */
        $response = app('lemonway')->GetWalletDetails([
            'wallet' => $this->wallet->lw_id,
        ]);

        if ($response->lwError) {
            throw new LwException(
                'Error Lemonway : Code ' .
                $response->lwError->CODE .
                ' : ' .
                $response->lwError->MSG
            );
        }

        $walletDetails = $response->wallet;

        return [
            'id' => strval($walletDetails->ID),
            'balance' => floatval($walletDetails->BAL) * 100,
            'blocked' => boolval($walletDetails->BLOCKED),
            'status' => intval($walletDetails->STATUS),
        ];
    }
}
