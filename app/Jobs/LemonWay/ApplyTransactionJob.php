<?php

namespace App\Jobs\LemonWay;

use App\Models\Transaction;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Date;

class ApplyTransactionJob
{
    use Dispatchable;
    use Queueable;

    public function __construct(protected Transaction $transaction)
    {
    }

    /** @throws \Exception */
    public function handle(): Transaction
    {
        if ($this->transaction->lw_executed_at) {
            throw new Exception('Transaction already executed.');
        }

        /** @var \LemonWay\ApiResponse $response */
        $response = app('lemonway')->SendPayment([
            'debitWallet' => $this->transaction->sourceWallet->lw_id,
            'creditWallet' => $this->transaction->targetWallet->lw_id,
            'amount' => number_format($this->transaction->amount / 100, 2, '.', ''),
        ]);

        $this->transaction->lw_executed_at = Date::now();

        if ($response->lwError) {
            $this->transaction->failure_reason = (string) $response->lwXml->E->Msg;
            $this->transaction->status = Transaction::STATUS_FAILED;
            $this->transaction->save();

            throw new Exception($this->transaction->failure_reason);
        }

        $this->transaction->lw_id = (string) $response->lwXml->TRANS->HPAY->ID;
        $this->transaction->status = Transaction::STATUS_SUCCESS;
        $this->transaction->save();

        return $this->transaction;
    }
}
