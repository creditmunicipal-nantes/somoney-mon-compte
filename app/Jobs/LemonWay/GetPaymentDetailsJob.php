<?php

namespace App\Jobs\LemonWay;

use App\Models\Transaction;
use Illuminate\Support\Carbon;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class GetPaymentDetailsJob
{
    use Dispatchable;
    use Queueable;

    public function __construct(protected Transaction $transaction)
    {
    }

    /**
     * Execute the job.
     *
     * @return array
     * @throws \Exception
     */
    public function handle(): array
    {
        /** @var \LemonWay\ApiResponse $response */
        $response = app('lemonway')->GetPaymentDetails([
            'transactionId' => $this->transaction->lw_id,
        ]);

        if ($response->lwError) {
            throw new Exception('Error Lemonway : Code ' . $response->lwError->CODE . ' : ' . $response->lwError->MSG);
        }

        return [
            'id' => strval($response->lwXml->TRANS->HPAY->ID),
            'date' => Carbon::createFromFormat('d/m/Y H:i:s', strval($response->lwXml->TRANS->HPAY->DATE)),
            'amount' => intval(floatval($response->lwXml->TRANS->HPAY->CRED ?? 0) * 100),
            'commission' => intval(floatval($response->lwXml->TRANS->HPAY->COM ?? 0) * 100),
            'status' => intval($response->lwXml->TRANS->HPAY->STATUS),
        ];
    }
}
