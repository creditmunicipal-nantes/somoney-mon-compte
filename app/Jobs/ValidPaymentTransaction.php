<?php

namespace App\Jobs;

use App\Jobs\Memberships\SendEmailRenewSucceed;
use App\Models\CyclosUser;
use App\Models\Membership;
use App\Models\Transaction;
use App\Services\Cyclos\TransactionService;
use App\Services\Cyclos\UserService;
use App\Services\LocalUserService;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;

class ValidPaymentTransaction
{
    use Dispatchable;
    use Queueable;

    protected string $context;

    public function __construct(protected ?Transaction $transaction)
    {
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \App\Exceptions\FailCreateUserException
     * @throws \ErrorException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    public function processSuccess(): void
    {
        if ($this->transaction->context === Transaction::CONTEXT_MEMBERSHIP) {
            Membership::where('cyclos_user_id', $this->transaction->cyclos_user_id)
                ->latest()
                ->limit(1)
                ->update(['succeed_at' => now()]);
            SendEmailRenewSucceed::dispatch($this->transaction->cyclos_user_id);
            $userId = $this->transaction->cyclos_user_id;
        } else {
            $userId = $this->getTransactionUserId();
            $type = CyclosUser::fill(app(UserService::class)->get($userId))->group === 'professional'
                ? 'stableAccount.toProfessional'
                : 'stableAccount.toIndividual';
            // Avoid sending transaction from registration or with 0€ at Cyclos.
            if (
                $this->transaction->amount > 0
                && $this->transaction->context !== Transaction::CONTEXT_REGISTRATION
            ) {
                app(TransactionService::class)->storeInitialAcompte(
                    $userId,
                    $this->transaction->amount / 100,
                    'Credit Euros - ' . config('somoney.money'),
                    $type
                );
            }
        }
        $this->applyStatusTransactions(Transaction::STATUS_SUCCESS, $userId);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \App\Exceptions\FailCreateUserException
     * @throws \ErrorException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    protected function getTransactionUserId(): string
    {
        if ($this->transaction->context === Transaction::CONTEXT_REGISTRATION) {
            $userId = app(LocalUserService::class)->createCyclosUser($this->transaction->localUser)->cyclos_id;
            if (config('somoney.available.services.membership')) {
                Membership::create(['cyclos_user_id' => $userId, 'succeed_at' => now()]);
            }

            return $userId;
        }

        return $this->transaction->cyclos_user_id;
    }

    protected function applyStatusTransactions(string $status, ?string $cyclosUserId = null): void
    {
        $this->transaction->update(array_merge(
            ['status' => $status],
            $cyclosUserId ? ['cyclos_user_id' => $cyclosUserId] : []
        ));
        if ($this->transaction->context === Transaction::CONTEXT_REGISTRATION) {
            $this->getLinkedTransactions()->filter()->each(function (Transaction $transaction) use (
                $status,
                $cyclosUserId
            ) {
                $transaction->update(array_merge(
                    ['status' => $status],
                    $cyclosUserId ? ['cyclos_user_id' => $cyclosUserId] : []
                ));
            });
        }
    }

    protected function getLinkedTransactions(): Collection
    {
        if ($this->transaction->transaction_type === Transaction::REGISTER_TYPE_ADHERE) {
            return collect([
                Transaction::where('parent_id', $this->transaction->id)
                    ->where('transaction_type', Transaction::REGISTER_TYPE_DONATE)
                    ->first(),
            ]);
        }
        $membershipTransaction = Transaction::find($this->transaction->parent_id);

        return collect([
            $membershipTransaction,
            Transaction::where('parent_id', $membershipTransaction->id)
                ->whereIn('transaction_type', [Transaction::REGISTER_TYPE_DONATE])
                ->where('id', '!=', $this->transaction->id)
                ->first(),
        ]);
    }
}
