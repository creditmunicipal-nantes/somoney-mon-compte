<?php

namespace App\Jobs\NewUsers;

use App\Mail\Registration\ProfessionalUserValidation;
use App\Models\CyclosUser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailProfessionalUserValidation implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(protected CyclosUser $user)
    {
    }

    public function handle(): void
    {
        Mail::to($this->user->email)->send(new ProfessionalUserValidation($this->user));
    }
}
