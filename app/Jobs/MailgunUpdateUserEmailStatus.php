<?php

namespace App\Jobs;

use App\Models\ValidationUser;
use App\Services\MailgunService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MailgunUpdateUserEmailStatus implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(public ValidationUser $validationUser)
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \Illuminate\Http\Client\RequestException
     * @throws \JsonException
     */
    public function handle(): void
    {
        app(MailgunService::class)->updateUserEmailStatus($this->validationUser);
    }
}
