<?php

namespace App\Jobs;

use App\Mail\Operations\Transfers\RecurrentReceived;
use App\Mail\Operations\Transfers\RecurrentSent;
use App\Mail\Operations\Transfers\SimpleReceived;
use App\Mail\Operations\Transfers\SimpleSent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailsTransfer implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(
        public string $transferCanal,
        public string $applicantEmail,
        public string $recipientEmail,
        public array $transferDetails,
        public bool $isScheduled
    ) {
    }

    public function handle(): void
    {
        if ($this->transferDetails['recurrent']) {
            Mail::to($this->applicantEmail)->send(new RecurrentSent($this->transferDetails));
            Mail::to($this->recipientEmail)->send(new RecurrentReceived($this->transferDetails));
        } else {
            Mail::to($this->applicantEmail)->send(
                new SimpleSent($this->transferCanal, $this->transferDetails, $this->isScheduled)
            );
            Mail::to($this->recipientEmail)->send(
                new SimpleReceived($this->transferCanal, $this->transferDetails, $this->isScheduled)
            );
        }
    }
}
