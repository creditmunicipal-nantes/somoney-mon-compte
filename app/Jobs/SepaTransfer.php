<?php

namespace App\Jobs;

use App\Exceptions\Cyclos\ValidationException;
use App\Models\CyclosUser;
use App\Models\Sepa;
use App\Services\Cyclos\TransactionService;
use App\Services\Cyclos\UserService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SepaTransfer implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(public string $sepaId)
    {
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function handle(): void
    {
        $sepa = Sepa::find($this->sepaId);
        if ($sepa->last_executed_at && $sepa->last_executed_at->isToday()) {
            return;
        }
        $user = CyclosUser::fill(app(UserService::class)->get($sepa->user_id));
        $type = 'stableAccount.toIndividual';
        if ($user->group === config('cyclos.internal-names.groups.professional')) {
            $type = 'stableAccount.toProfessional';
        }
        try {
            app(TransactionService::class)->storeInitialAcompte(
                (string) $sepa->user_id,
                (float) $sepa->amount,
                'Credit Euros - ' . config('somoney.money'),
                $type
            );
            $sepa->update(['last_executed_at' => now()]);
        } catch (ValidationException $exception) {
            // Revert done status for non recurrent transfers
            if (! $sepa->is_recurrent) {
                $sepa->update(['done' => false]);
            }

            throw $exception;
        }
    }
}
