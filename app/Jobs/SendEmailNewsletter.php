<?php

namespace App\Jobs;

use App\Mail\Newsletter as NewsletterEmail;
use App\Models\CyclosUser;
use App\Models\Newsletter;
use App\Services\Cyclos\TransferService;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailNewsletter implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function handle()
    {
        $newsletters = Newsletter::where('sent', false)->get();
        foreach ($newsletters as $newsletter) {
            if (now()->gt(Carbon::parse($newsletter->send_date))) {
                $userService = app(UserService::class);
                $user = CyclosUser::fill($userService->get($newsletter->cyclos_user_id));
                $transferService = app(TransferService::class);
                $transfers = $transferService->list([
                    'transferKinds' => 'payment',
                    'user' => $user->id,
                    'fields' => 'from,to',
                    'pageSize' => 1000,
                ]);

                $toMail = [];

                foreach ($transfers as $transfer) {
                    // add transferts where pro is not sender but the receiver, not already in the array
                    if (
                        data_get($transfer, 'from.user.id') !== $user->id
                        && data_get($transfer, 'from.user.id') !== null
                        && (data_get($transfer, 'to.user.id') === $user->id)
                    ) {
                        $cyclosUser = CyclosUser::fill($userService->get(data_get($transfer, 'from.user.id')));
                        if ($cyclosUser->customValues['commercialinfo'] && ! in_array($cyclosUser->email, $toMail)) {
                            $toMail[] = $cyclosUser->email;
                        }
                    }
                }
                $this->sendMails($toMail, $newsletter);
            }
        }
    }

    public function sendMails(array $toMail, Newsletter $newsletter)
    {
        foreach ($toMail as $emailAddress) {
            Mail::to($emailAddress)->send(new NewsletterEmail($newsletter));
        }
        $newsletter->update(['sent' => true]);
    }
}
