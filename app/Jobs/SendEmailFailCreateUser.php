<?php

namespace App\Jobs;

use App\Mail\Registration\AdminFailCreate;
use App\Mail\Registration\UserFailCreate;
use App\Models\LocalUser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailFailCreateUser implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    protected ?LocalUser $localUser;

    public function __construct(string $localUserId, protected array $response)
    {
        $this->localUser = LocalUser::find($localUserId);
    }

    public function handle(): void
    {
        $errors = array_merge(
            array_values(data_get($this->response, 'propertyErrors', [])),
            array_values(data_get($this->response, 'customFieldErrors', []))
        );
        $errors = call_user_func_array('array_merge', $errors);

        Mail::to(config('somoney.mail.account'))
            ->send(new AdminFailCreate($this->localUser, $errors));

        Mail::to($this->localUser->email)
            ->send(new UserFailCreate($this->localUser, $errors));
    }
}
