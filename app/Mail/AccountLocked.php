<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class AccountLocked extends Mail
{
    use Queueable;
    use SerializesModels;

    /**
     * Create a new message instance.
     *
     * @param string $id
     * @param array $user
     */
    public function __construct(public string $id, public array $user)
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.templates.account-locked')
            ->subject('Un utilisateur a demandé à bloquer son compte.');
    }
}
