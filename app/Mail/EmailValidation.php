<?php

namespace App\Mail;

use App\Models\CyclosUser;
use App\Models\UserToken;
use App\Models\ValidationUser;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Queue\SerializesModels;

class EmailValidation extends Mail
{
    use Queueable;
    use SerializesModels;

    public string $username;

    public string $userType;

    public function __construct(protected CyclosUser $user, public UserToken $token, public bool $isRemind = false)
    {
        $this->username = $user->name;
        $this->userType = $user->group;
    }

    public function build(): self
    {
        $view = $this->isRemind
            ? 'emails.templates.remind.email-changed-validation'
            : 'emails.templates.email-changed-validation';
        if (config('somoney.available.services.email-status') && ! $this->isRemind) {
            try {
                ValidationUser::where('cyclos_id', $this->user->id)->firstOrFail();
            } catch (ModelNotFoundException $exception) {
                ValidationUser::create([
                    'email' => $this->user->email,
                    'group' => $this->user->group,
                    'cyclos_id' => $this->user->id,
                ]);
            }
        }

        return $this->view($view)->subject(__('mail.subject.email-validation', ['money' => config('somoney.money')]));
    }
}
