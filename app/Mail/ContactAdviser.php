<?php

namespace App\Mail;

use App\Models\CyclosUser;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class ContactAdviser extends Mail
{
    use Queueable;
    use SerializesModels;

    public string $username;

    public string $email;

    public function __construct(public string $content, CyclosUser $cyclosUser)
    {
        $this->username = $cyclosUser->name;
        $this->email = $cyclosUser->email;
    }

    public function build(): ContactAdviser
    {
        $subject = "Message envoyé par $this->username depuis l'espace mon compte " . config('somoney.money');

        return $this->view('emails.templates.contact-adviser')
            ->subject($subject);
    }
}
