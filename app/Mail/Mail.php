<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

abstract class Mail extends Mailable
{
    public const LABEL_CONTACT_INFORMATION = 'information';

    public const LABEL_CONTACT_QUESTION = 'question';

    public const CONTACT_LINK_URL = 'url';

    public const CONTACT_LINK_EMAIL = 'email';

    public const CLOSING_SIMPLE = 'simple';

    public const CLOSING_ENGAGEMENT = 'engagement';

    public const CLOSING_SUPPORT = 'support';

    public bool $showDirectoryLink = true;

    public bool $showFacebookLikePage = true;

    public bool $showSocialLinks = true;

    public string $contactType = self::LABEL_CONTACT_INFORMATION;

    public string $closingType = self::CLOSING_SIMPLE;
}
