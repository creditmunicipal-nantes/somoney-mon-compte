<?php

namespace App\Mail\Registration;

use App\Mail\Mail;
use App\Models\LocalUser;
use App\Models\UserToken;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class EmailValidation extends Mail
{
    use Queueable;
    use SerializesModels;

    public string $username;

    public string $userType;

    public bool $showDirectoryLink = false;

    public function __construct(LocalUser $localUser, public UserToken $token, public bool $isRemind = false)
    {
        $this->username = $localUser->name;
        $this->userType = $localUser->group;
    }

    public function build(): EmailValidation
    {
        $view = $this->isRemind ? 'emails.templates.registration.remind.email-validation'
            : 'emails.templates.registration.email-validation';
        $subject = config('somoney.content.email.subject.' . ($this->isRemind ? 'remind-' : '') . 'register');

        return $this->view($view)->subject($subject);
    }
}
