<?php

namespace App\Mail\Registration;

use App\Mail\Mail;
use App\Models\LocalUser;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class AdminFailCreate extends Mail
{
    use Queueable;
    use SerializesModels;

    public function __construct(public LocalUser $localUser, public array $errors)
    {
    }

    public function build(): AdminFailCreate
    {
        return $this->view('emails.templates.registration.admin-fail-create-user')
            ->subject('Echec d\'une inscription sur ' . config('somoney.money'));
    }
}
