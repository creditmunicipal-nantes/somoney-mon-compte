<?php

namespace App\Mail\Registration;

use App\Mail\Mail;
use App\Models\LocalUser;
use App\Models\UserToken;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class Resume extends Mail
{
    use Queueable;
    use SerializesModels;

    public string $userType;

    /**
     * Create a new message instance.
     *
     * @param \App\Models\LocalUser $localUser
     * @param \App\Models\UserToken $token
     */
    public function __construct(LocalUser $localUser, public UserToken $token)
    {
        $this->userType = $localUser->group;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.templates.registration.resume')
            ->subject('Reprise inscription en cours sur ' . config('somoney.money'));
    }
}
