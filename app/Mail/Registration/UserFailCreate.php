<?php

namespace App\Mail\Registration;

use App\Mail\Mail;
use App\Models\LocalUser;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class UserFailCreate extends Mail
{
    use Queueable;
    use SerializesModels;

    public function __construct(public LocalUser $localUser, public array $errors)
    {
    }

    public function build(): UserFailCreate
    {
        return $this->view('emails.templates.registration.user-fail-create')
            ->subject('Echec de votre inscription sur ' . config('somoney.money'));
    }
}
