<?php

namespace App\Mail\Registration;

use App\Mail\Mail;
use App\Models\LocalUser;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class ConfirmRegistered extends Mail
{
    use Queueable;
    use SerializesModels;

    public string $username;

    public string $closingType = self::CLOSING_ENGAGEMENT;

    public function __construct(LocalUser $localUser)
    {
        $this->username = $localUser->name;
    }

    public function build(): ConfirmRegistered
    {
        return $this->view('emails.templates.registration.confirm-registered')
            ->subject(config('somoney.content.email.subject.confirm-registered'));
    }
}
