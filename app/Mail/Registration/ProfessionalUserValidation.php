<?php

namespace App\Mail\Registration;

use App\Mail\Mail;
use App\Models\CyclosUser;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class ProfessionalUserValidation extends Mail
{
    use Queueable;
    use SerializesModels;

    public string $username;

    public function __construct(CyclosUser $user)
    {
        $this->username = $user->name;
    }

    public function build(): ProfessionalUserValidation
    {
        return $this->view('emails.templates.registration.professional-validation')
            ->subject("\xE2\x9C\x85" . ' Votre inscription à la monnaie ' . config('somoney.money') . ' est confirmée');
    }
}
