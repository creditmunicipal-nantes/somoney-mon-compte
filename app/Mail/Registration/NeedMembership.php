<?php

namespace App\Mail\Registration;

use App\Mail\Mail;
use App\Models\LocalUser;
use App\Models\UserToken;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class NeedMembership extends Mail
{
    use Queueable;
    use SerializesModels;

    public string $name;

    public function __construct(LocalUser $localUser, public UserToken $token)
    {
        $this->name = ucfirst($localUser->customValues['firstname']);
    }

    public function build(): NeedMembership
    {
        return $this->view('emails.templates.registration.need-membership')
            ->subject('Adhésion pour finaliser l\'inscription à ' . config('somoney.money'));
    }
}
