<?php

namespace App\Mail\Memberships;

use App\Mail\Mail;
use App\Models\UserToken;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class RenewCreated extends Mail
{
    use Queueable;
    use SerializesModels;

    public bool $showDirectoryLink = false;

    public bool $showFacebookLikePage = false;

    public string $contactType = self::LABEL_CONTACT_QUESTION;

    public string $closingType = self::CLOSING_SUPPORT;

    public function __construct(public UserToken $userToken, public string $username)
    {
        //
    }

    public function build(): RenewCreated
    {
        return $this->view('emails.templates.memberships.renew-created')
            ->subject(config('somoney.content.email.subject.renew-created'));
    }
}
