<?php

namespace App\Mail\Memberships;

use App\Mail\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class RenewSucceed extends Mail
{
    use Queueable;
    use SerializesModels;

    public string $closingType = self::CLOSING_ENGAGEMENT;

    public function __construct(public string $username)
    {
    }

    public function build(): RenewSucceed
    {
        return $this->view('emails.templates.memberships.renew-succeed')
            ->subject('Confirmation de votre réadhésion à ' . config('somoney.money'));
    }
}
