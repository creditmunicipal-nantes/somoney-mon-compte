<?php

namespace App\Mail\Memberships;

use App\Mail\Mail;
use App\Models\UserToken;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class RenewRemind extends Mail
{
    use Queueable;
    use SerializesModels;

    public bool $showDirectoryLink = false;

    public bool $showFacebookLikePage = false;

    public string $contactType = self::LABEL_CONTACT_QUESTION;

    public function __construct(public UserToken $userToken, public string $username)
    {
    }

    public function build(): RenewRemind
    {
        return $this->view('emails.templates.memberships.renew-remind')
            ->subject('Rappel du renouvellement de votre adhésion à ' . config('somoney.money'));
    }
}
