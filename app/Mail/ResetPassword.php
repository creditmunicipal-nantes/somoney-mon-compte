<?php

namespace App\Mail;

use App\Models\CyclosUser;
use App\Models\UserToken;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class ResetPassword extends Mail
{
    use Queueable;
    use SerializesModels;

    public function __construct(public CyclosUser $user, public UserToken $token, public string $type)
    {
    }

    public function build(): ResetPassword
    {
        return $this->view('emails.templates.reset-password')
            ->subject('Changement de mot de passe ' . config('somoney.money'));
    }
}
