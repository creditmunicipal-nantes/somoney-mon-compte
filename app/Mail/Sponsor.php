<?php

namespace App\Mail;

use App\Models\CyclosUser;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class Sponsor extends Mail
{
    use Queueable;
    use SerializesModels;

    public string $username;

    public string $email;

    public string $status;

    public function __construct(public string $senderName, public string $content, CyclosUser $user)
    {
        $this->username = $user->name;
        $this->email = $user->email;
        $this->status = $user->group;
    }

    public function build(): Sponsor
    {
        return $this->view('emails.templates.sponsor')
            ->subject('Rejoignez la communauté ' . config('somoney.money') . '!');
    }
}
