<?php

namespace App\Mail\Reconciliation;

use App\Mail\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class MonthlyErrors extends Mail
{
    use Queueable;
    use SerializesModels;

    /**
     * Create a new message instance.
     *
     * @param string $fileName
     * @param array $errors
     */
    public function __construct(protected string $fileName, protected array $errors)
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.templates.reconciliation.monthly-reconciliation-errors')
            ->with([
                'fileName' => $this->fileName,
                'errors' => $this->errors,
            ])
            ->subject('Traitement fichier réconciliation mois KO');
    }
}
