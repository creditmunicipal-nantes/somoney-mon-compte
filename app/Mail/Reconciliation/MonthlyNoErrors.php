<?php

namespace App\Mail\Reconciliation;

use App\Mail\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class MonthlyNoErrors extends Mail
{
    use Queueable;
    use SerializesModels;

    /**
     * Create a new message instance.
     *
     * @param string $fileName
     */
    public function __construct(protected string $fileName)
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.templates.reconciliation.monthly-reconciliation-no-errors')
            ->with([
                'fileName' => $this->fileName,
            ])
            ->subject('Traitement fichier réconciliation mois OK');
    }
}
