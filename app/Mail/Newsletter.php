<?php

namespace App\Mail;

use App\Models\Newsletter as NewsletterModel;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class Newsletter extends Mail
{
    use Queueable;
    use SerializesModels;

    /**
     * EmailNewsletter constructor.
     *
     * @param \App\Models\Newsletter $newsletter
     */
    public function __construct(public NewsletterModel $newsletter)
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.templates.newsletter')
            ->subject("Newsletter");
    }
}
