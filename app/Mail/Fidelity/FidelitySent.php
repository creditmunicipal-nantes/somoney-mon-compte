<?php

namespace App\Mail\Fidelity;

use App\Mail\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class FidelitySent extends Mail
{
    use Queueable;
    use SerializesModels;

    public float $amount;

    public string $toUserName;

    public string $fromUserName;

    /**
     * AppliedFidelity constructor.
     *
     * @param array $transferDetails
     */
    public function __construct(public array $transferDetails)
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->amount = $this->transferDetails['amount'];
        $this->toUserName = $this->transferDetails['toUser']->name;
        $this->fromUserName = $this->transferDetails['fromUser']->name;
        $subject =
            "Votre programme de fidélité";

        return $this->view('emails.templates.fidelity.fidelity-sent')->subject($subject);
    }
}
