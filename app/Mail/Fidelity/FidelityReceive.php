<?php

namespace App\Mail\Fidelity;

use App\Mail\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class FidelityReceive extends Mail
{
    use Queueable;
    use SerializesModels;

    public float $amount;

    public string $fromUserName;

    /**
     * FidelityReceive constructor.
     *
     * @param array $transferDetails
     */
    public function __construct(public array $transferDetails)
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->amount = $this->transferDetails['amount'];
        $this->fromUserName = $this->transferDetails['fromUser']->name;
        $subject = "Programme de fidélité de $this->fromUserName";

        return $this->view('emails.templates.fidelity.fidelity-receive')->subject($subject);
    }
}
