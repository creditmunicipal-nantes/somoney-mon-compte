<?php

namespace App\Mail;

use App\Http\Requests\RecapRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class Report extends Mail
{
    use Queueable;
    use SerializesModels;

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject = 'Objet opérations et inscriptions ' . config('somoney.money') . ' Hebdomadaires';

        return $this->view('emails.templates.report')
            ->subject($subject);
    }
}
