<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class AccountDeleted extends Mail
{
    use Queueable;
    use SerializesModels;

    public function __construct(public array $user)
    {
    }

    public function build(): AccountDeleted
    {
        return $this->view('emails.templates.account-deleted')
            ->subject('Votre compte ' . config('somoney.money') . ' a été supprimé');
    }
}
