<?php

namespace App\Mail\Commands;

use App\Mail\Mail;
use App\Models\Sepa;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class RemindStopSepa extends Mail
{
    use Queueable;
    use SerializesModels;

    public function __construct(public Sepa $sepa, public string $username)
    {
    }

    public function build(): RemindStopSepa
    {
        return $this->view('emails.templates.commands.remind-stop-sepa')
            ->subject('[' . config('app.name') . '] Fin du crédit automatique de '
                . $this->username . ' - ' . config('somoney.money'));
    }
}
