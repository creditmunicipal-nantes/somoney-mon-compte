<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LeaveCall extends Mail
{
    use Queueable;
    use SerializesModels;

    public function __construct(public array $user)
    {
    }

    public function build(): LeaveCall
    {
        return $this->view('emails.templates.leave-call')
            ->subject('Un utilisateur a demandé à quitter ' . config('somoney.money') . '.');
    }
}
