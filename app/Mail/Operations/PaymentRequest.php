<?php

namespace App\Mail\Operations;

use App\Mail\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class PaymentRequest extends Mail
{
    use Queueable;
    use SerializesModels;

    public string $username;

    public string $email;

    public string $status;

    /**
     * Create a new message instance.
     *
     * @param string $title
     * @param string $amount
     * @param array $user
     */
    public function __construct(public string $title, public string $amount, array $user)
    {
        $this->username = data_get($user, 'name');
        $this->email = data_get($user, 'email');
        $this->status = data_get($user, 'group.internalName');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): PaymentRequest
    {
        return $this->view('emails.templates.operations.payment-request')
            ->subject("Vous avez reçu une demande de paiement de la part de $this->username");
    }
}
