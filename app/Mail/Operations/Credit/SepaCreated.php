<?php

namespace App\Mail\Operations\Credit;

use App\Mail\Mail;
use App\Models\Sepa;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class SepaCreated extends Mail
{
    use Queueable;
    use SerializesModels;

    /**
     * Create a new message instance.
     *
     * @param \App\Models\Sepa $sepa
     * @param string $username
     */
    public function __construct(public Sepa $sepa, public string $username)
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.templates.operations.credit.sepa-created')
            ->subject('[' . config('app.name') . '] Nouveau prélèvement SEPA ' . config('somoney.money') . '.');
    }
}
