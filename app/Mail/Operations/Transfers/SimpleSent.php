<?php

namespace App\Mail\Operations\Transfers;

use App\Mail\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class SimpleSent extends Mail
{
    use Queueable;
    use SerializesModels;

    public float $amount;

    public string $toUserName;

    public function __construct(
        protected string $transferCanal,
        public array $transferDetails,
        public bool $isScheduled
    ) {
    }

    public function build(): self
    {
        $this->amount = $this->transferDetails['amount'];
        $this->toUserName = $this->transferDetails['toUser']->name;
        $money = config('somoney.money');
        if ($this->isScheduled) {
            $subject =
                "Vos $this->amount $money seront envoyés le {$this->transferDetails['date']} à  $this->toUserName";
        } else {
            $subject = "Vos $this->amount $money ont été envoyés à $this->toUserName";
        }

        return $this->view('emails.templates.operations.transfer.simple-sent')->subject($subject);
    }
}
