<?php

namespace App\Mail\Operations\Transfers;

use App\Mail\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class RecurrentReceived extends Mail
{
    use Queueable;
    use SerializesModels;

    public float $amount;

    public string $fromUserName;

    public string $toUserName;

    /**
     * Create a new message instance.
     *
     * @param array $transferDetails
     */
    public function __construct(public array $transferDetails)
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->amount = $this->transferDetails['amount'];
        $this->fromUserName = $this->transferDetails['fromUser']->name;
        $this->toUserName = $this->transferDetails['toUser']->name;

        $subject = "Vous allez recevoir un virement de $this->amount " . config('somoney.money')
            . " de la part de $this->fromUserName";

        return $this->view('emails.templates.operations.transfer.recurrent-received')->subject($subject);
    }
}
