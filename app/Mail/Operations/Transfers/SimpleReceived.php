<?php

namespace App\Mail\Operations\Transfers;

use App\Mail\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class SimpleReceived extends Mail
{
    use Queueable;
    use SerializesModels;

    public float $amount;

    public string $fromUserName;

    public function __construct(
        protected string $transferCanal,
        public array $transferDetails,
        public bool $isScheduled
    ) {
    }

    public function build(): self
    {
        $this->amount = $this->transferDetails['amount'];
        $this->fromUserName = $this->transferDetails['fromUser']->name;
        $money = config('somoney.money');
        if ($this->isScheduled) {
            $subject = "Vous allez recevoir un virement de $this->amount $money le {$this->transferDetails['date']}"
                . " de la part de $this->fromUserName";
        } else {
            $subject = "Vous avez reçu un virement de $this->amount " . config('somoney.money')
                . " de la part de $this->fromUserName";
        }

        return $this->view('emails.templates.operations.transfer.simple-received')->subject($subject);
    }
}
