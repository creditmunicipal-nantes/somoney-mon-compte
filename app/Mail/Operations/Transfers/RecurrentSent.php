<?php

namespace App\Mail\Operations\Transfers;

use App\Mail\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class RecurrentSent extends Mail
{
    use Queueable;
    use SerializesModels;

    public float $amount;

    public string $fromUserName;

    public string $toUserName;

    /**
     * Create a new message instance.
     *
     * @param array $transferDetails
     */
    public function __construct(public array $transferDetails)
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->amount = $this->transferDetails['amount'];
        $this->fromUserName = $this->transferDetails['fromUser']->name;
        $this->toUserName = $this->transferDetails['toUser']->name;

        $subject = 'Votre virement récurrent a été planifié';

        return $this->view('emails.templates.operations.transfer.recurrent-sent')->subject($subject);
    }
}
