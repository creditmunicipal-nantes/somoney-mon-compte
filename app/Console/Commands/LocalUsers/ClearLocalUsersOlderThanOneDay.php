<?php

namespace App\Console\Commands\LocalUsers;

use App\Models\LocalUser;
use App\Models\ValidationUser;
use App\Services\LocalUserService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ClearLocalUsersOlderThanOneDay extends Command
{
    /** @var string */
    protected $signature = 'local-users:clear:older-than-one-day';

    /** @var string */
    protected $description = 'Clear local users older than one day.';

    public function handle(): void
    {
        $this->output->title('Starting clearing local users older than one day...');
        app(LocalUserService::class)->getOldCreated(today()->subDay())->each(function (LocalUser $localUser) {
            $localUser->delete();
            ValidationUser::where('local_user_id', $localUser->id)->delete();
            $message = "Removed local user {$localUser->name} with cyclos id {$localUser->cyclos_id}";
            $this->output->comment($message);
            Log::info($message);
        });
        $this->output->success('Finished clearing local users older than one day.');
    }
}
