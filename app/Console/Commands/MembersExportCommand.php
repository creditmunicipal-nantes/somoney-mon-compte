<?php

namespace App\Console\Commands;

use App\Exports\MembersExport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Excel;

class MembersExportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:members';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export members list in a xls file';

    /**
     * Execute the console command.
     *
     * @param \Maatwebsite\Excel\Excel $excel
     * @param \App\Exports\MembersExport $export
     *
     * @return mixed
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function handle(Excel $excel, MembersExport $export)
    {
        return $excel->store($export, 'exports/members.xls');
    }
}
