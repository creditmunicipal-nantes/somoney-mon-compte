<?php

namespace App\Console\Commands\Mail;

use App\Jobs\SendEmailNewsletter;
use Illuminate\Console\Command;

class NewslettersSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newsletters:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Launch SendEmailNewsletterJob';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new SendEmailNewsletter());
    }
}
