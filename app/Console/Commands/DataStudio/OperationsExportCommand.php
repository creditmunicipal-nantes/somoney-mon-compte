<?php

namespace App\Console\Commands\DataStudio;

use App\Services\Cyclos\TransferService;
use Illuminate\Support\Carbon;
use Exception;
use Illuminate\Console\Command;

class OperationsExportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:operations:data-studio';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export operations list in a json file for Data Studio';

    protected TransferService $transactionService;

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $this->transactionService = new TransferService();
        $json = json_encode($this->transactionService->listAll()->reduce(function (array $json, array $transfer) {
            $transferData = $this->transactionService->get($transfer['id']);
            if (data_get($transfer, 'from.kind') === 'system') {
                $fromUser = data_get($transfer, 'from.type.name');
                $fromType = 'Système';
            } else {
                $fromUser = data_get($transfer, 'from.user.display');
                $fromType =
                    data_get($transfer, 'from.type.internalName') === 'individual' ? 'Particulier' : 'Professionnel';
            }

            if (data_get($transfer, 'to.kind') === 'system') {
                $toUser = data_get($transfer, 'to.type.name');
                $toType = 'Système';
            } else {
                $toUser = data_get($transfer, 'to.user.display');
                $toType =
                    data_get($transfer, 'to.type.internalName') === 'individual' ? 'Particulier' : 'Professionnel';
            }

            $date = Carbon::parse(data_get($transfer, 'date'));

            $json[] = [
                "IDTRANSFERT" => (int) data_get($transfer, 'id'),
                "LBTRANSFERT" => preg_replace(
                    '/[[:^print:]]/',
                    "",
                    data_get($transferData, 'transaction.description')
                ),
                "MONTANTTRANSFERT" => (float) data_get($transfer, 'amount'),
                "FROMIDUSER" => (int) data_get($transfer, 'from.id'),
                "FROMUSER" => $fromUser,
                "FROMCATUSER" => $fromType,
                "TOIDUSER" => (int) data_get($transfer, 'to.id'),
                "TONAMEUSER" => $toUser,
                "TOCATUSER" => $toType,
                "DATE" => $date->format('Ymd'),
                "HOUR" => $date->format('g'),
                "MINUTE" => $date->format('i'),
                "TRANSFERTYPE" => $this->getType($transfer),
            ];

            return $json;
        }, []), JSON_THROW_ON_ERROR | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

        if (! file_put_contents(storage_path('app/exports/data-studio/operations.json'), $json)) {
            throw new Exception('Export Operations commande has failed');
        }
    }

    protected function getType(array $transaction): string
    {
        if (data_get($transaction, 'from.type.internalName') === 'individual') {
            if (data_get($transaction, 'to.type.internalName') === 'professional') {
                return 'CTOB';
            }

            if (data_get($transaction, 'to.type.internalName') === 'individual') {
                return 'CTOC';
            }
        } elseif (data_get($transaction, 'from.type.internalName') === 'professional') {
            if (data_get($transaction, 'to.type.internalName') === 'professional') {
                return 'BTOB';
            }

            if (data_get($transaction, 'to.type.internalName') === 'individual') {
                return 'BTOC';
            }
        }

        return '';
    }
}
