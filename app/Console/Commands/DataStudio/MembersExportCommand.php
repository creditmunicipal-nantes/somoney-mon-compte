<?php

namespace App\Console\Commands\DataStudio;

use App\Exceptions\QrCodeTokenNotFoundException;
use App\Models\CyclosUser;
use App\Services\Cyclos\UserService;
use App\Services\QrCodeService;
use Illuminate\Support\Carbon;
use Exception;
use Illuminate\Console\Command;

class MembersExportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:members:data-studio';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export members list in a json file for Data Studio';

    protected UserService $userService;

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $this->userService = new UserService();
        $json = json_encode((new UserService())->searchAll([
            'groups' => implode(',', array_values(config('cyclos.internal-names.groups'))),
        ])->reduce(function (array $json, array $user) {
            /** @var CyclosUser $cyclosUser */
            $cyclosUser = CyclosUser::fill($this->userService->get(data_get($user, 'id')));
            $json[] = $this->makeJson($cyclosUser);

            return $json;
        }, []), JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

        if (! file_put_contents(storage_path('app/exports/data-studio/members.json'), $json)) {
            throw new Exception('Export members command has failed');
        }
    }

    /**
     * @param \App\Models\CyclosUser $user
     *
     * @return int
     */
    protected function getAge(CyclosUser $user): int
    {
        $age = 0;
        if (data_get($user->customValues, 'birthday')) {
            try {
                $age = now()->diffInYears(Carbon::parse(
                    data_get($user->customValues, 'birthday')
                ));
            } catch (Exception $exception) {
                app('sentry')->captureException($exception);
            }
        }

        return $age;
    }

    /**
     * @param int $age
     *
     * @return string
     */
    protected function getAgeCategory(int $age): string
    {
        if ($age < 25) {
            return 'Moins de 25 ans';
        }
        if ($age <= 49) {
            return '25 - 49 ans';
        }
        if ($age <= 75) {
            return '50 - 75 ans';
        }

        return 'Plus de 75 ans';
    }

    /**
     * @param \App\Models\CyclosUser $cyclosUser
     *
     * @return array
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function makeJson(CyclosUser $cyclosUser)
    {
        $age = $this->getAge($cyclosUser);
        $account = $this->userService->getUserAccount($cyclosUser->id);
        try {
            $token = config('somoney.available.services.qr_code')
                ? data_get((new QrCodeService())->getCyclosUserQrCodeToken($cyclosUser->id), 'value')
                : null;
        } catch (QrCodeTokenNotFoundException $exception) {
            $token = null;
        }

        return array_merge([
            "IDUSER" => intval($cyclosUser->id),
            "NOM" => $cyclosUser->name,
            "GROUP" => __('validation.attributes.' . $cyclosUser->group),
            "ADDRESS" => data_get($cyclosUser->address, 'addressLine1'),
            "ADDRESSGEO" => $cyclosUser->getAddressFull(),
            "ZIPCODE" => data_get($cyclosUser->address, 'zip'),
            "CITY" => data_get($cyclosUser->address, 'city'),
            "COORDONATES" => join(',', [
                data_get($cyclosUser->address, 'location.latitude'),
                data_get($cyclosUser->address, 'location.longitude'),
            ]),
            "CATEGORY" => data_get($cyclosUser->customValues, 'catpro.0.value', ''),
            "REGISTERDATE" => $cyclosUser->registerDate->format('Ymd'),
            "REGISTERHOUR" => $cyclosUser->registerDate->format('g'),
            "REGISTERMINUTE" => $cyclosUser->registerDate->format('i'),
            "LASTLOGINDATE" => $cyclosUser->lastLogin ? Carbon::parse($cyclosUser->lastLogin)->format('Ymd') : '',
            "LASTLOGINHOUR" => $cyclosUser->lastLogin ? Carbon::parse($cyclosUser->lastLogin)->format('g') : '',
            "LASTLOGINMINUTE" => $cyclosUser->lastLogin ? Carbon::parse($cyclosUser->lastLogin)->format('i') : '',
            "AGE" => $age,
            "AGECAT" => $this->getAgeCategory($age),
            "GENDER" => data_get($cyclosUser->customValues, 'gender') === 'man' ? 'M' : 'F',
            "OPTINMAIL" => data_get($cyclosUser->customValues, 'optinnewsletter') === 'true' ? 'Oui' : 'Non',
            "BALANCE" => doubleval(data_get($account, 'status.balance', '0')),
            "MAIL" => $cyclosUser->email,
            "PHONE" => $cyclosUser->phone,
        ], config('somoney.available.services.qr_code') ? ['TOKEN' => $token] : []);
    }
}
