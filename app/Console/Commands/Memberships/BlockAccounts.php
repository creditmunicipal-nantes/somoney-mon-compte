<?php

namespace App\Console\Commands\Memberships;

use App\Models\CyclosUser;
use App\Models\Membership;
use App\Services\Cyclos\UserService;
use Illuminate\Console\Command;

class BlockAccounts extends Command
{
    protected $signature = 'memberships:account:block
                        {--force : create memberships renew on other date than 15th of january}';

    protected $description = 'Block users with invalid membership after 31/06';

    protected UserService $userService;

    public function handle(): void
    {
        if (! config('somoney.available.services.membership')) {
            $this->output->error('The service "membership" is required for this command '
                . '(cf. "custom-data/current/config/somoney.php")');

            return;
        }
        if (! config('somoney.available.services.membership_block_expired_accounts')) {
            $this->output->error('The service "membership_block_expired_accounts" is required for this command '
                . '(cf. "custom-data/current/config/somoney.php")');

            return;
        }
        if ($this->option('force') || today()->format('d-m') === '01-07') {
            $memberships = Membership::where('succeed_at', null)->get();
            $memberships->each(function (Membership $membership) {
                app(UserService::class)->setStatus(
                    $membership->cyclos_user_id,
                    CyclosUser::STATUS_DISABLED,
                    'Adhésion invalide le 31-06 malgré les relances'
                );
            });
        }
    }
}
