<?php

namespace App\Console\Commands\Memberships;

use App\Jobs\Memberships\SendEmailRenewRemind;
use App\Jobs\Memberships\SendEmailRenewWarning;
use App\Models\Membership;
use Illuminate\Console\Command;

class NotifyRenewals extends Command
{
    protected $signature = 'memberships:notify:renewals
                        {--force= : Force to notify with "remind" or "warning" method }';

    protected $description = 'Notify memberships renewals';

    public function handle(): void
    {
        $forceNotify = $this->option('force');
        if (! $this->preconditionCheck($forceNotify)) {
            return;
        }

        if ($forceNotify || in_array(today()->format('d-m'), ['05-04', '05-05', '05-06'])) {
            $beginPeriod = today()->month(1)->day(15)->hour(9)->startOfHour();
            $endPeriod = today()->month(9)->endOfMonth();
            if ($endPeriod->gt(now())) {
                $beginPeriod->subYear();
            }
            Membership::whereBetween('created_at', [$beginPeriod, $endPeriod])
                ->whereNull('succeed_at')
                ->get()
                ->each(function (Membership $membership) use ($forceNotify) {
                    if ($forceNotify) {
                        switch ($forceNotify) {
                            case 'remind':
                                dispatch(new SendEmailRenewRemind($membership->cyclos_user_id));
                                break;
                            case 'warning':
                                dispatch(new SendEmailRenewWarning($membership->cyclos_user_id));
                                break;
                            default:
                                $this->output->error(
                                    'The force option allow only "remind" or "warning" as notify method'
                                );

                                return false;
                        }
                    } else {
                        $this->notifyOnDates($membership);
                    }
                });
        }
    }

    protected function preconditionCheck(string $forceNotify): bool
    {
        if (
            ! config('somoney.available.services.membership')
            || ! config('somoney.available.services.membership_notify_for_renew')
        ) {
            $this->output->error('The service "membership" and "membership_notify_for_renew" is required' .
                'for this command (cf. "custom-data/current/config/somoney.php")');

            return false;
        }
        if (! in_array($forceNotify, ['remind', 'warning'])) {
            $this->output->error('the force option accept only "remind" and "warning" values.');

            return false;
        }

        return true;
    }

    protected function notifyOnDates(Membership $membership): void
    {
        switch (today()->format('d-m')) {
            case '05-04':
            case '05-05':
                dispatch(new SendEmailRenewRemind($membership->cyclos_user_id));
                break;
            case '05-06':
                dispatch(new SendEmailRenewWarning($membership->cyclos_user_id));
                break;
        }
    }
}
