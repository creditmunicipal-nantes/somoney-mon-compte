<?php

namespace App\Console\Commands\Memberships;

use App\Jobs\Memberships\SendEmailRenewCreated;
use App\Models\Membership;
use App\Services\Cyclos\UserService;
use Illuminate\Console\Command;

class MakeRenewals extends Command
{
    protected $signature = 'memberships:make:renewals
                        {--force : create memberships renew on other date than 15th of january}';

    protected $description = 'Create memberships renewals entries';

    protected UserService $userService;

    public function handle(): void
    {
        if (! config('somoney.available.services.membership')) {
            $this->output->error(
                'The service "membership" is required for this command (cf. "custom-data/current/config/somoney.php")'
            );

            return;
        }
        if ($this->option('force') || today()->format('d-m') === '15-01') {
            $membershipsToRenew = Membership::whereBetween('succeed_at', [
                today()->subYear()->month(1)->day(15)->hour(9)->minute(0)->second(0),
                today()->subYear()->month(9)->endOfMonth(),
            ])->latest()->get()->unique('cyclos_user_id');
            $membershipsToRenew->each(function (Membership $membership) {
                Membership::create(['cyclos_user_id' => $membership->cyclos_user_id]);
                dispatch(new SendEmailRenewCreated($membership->cyclos_user_id));
            });
            $membershipsUsersIds = Membership::pluck('cyclos_user_id')->toArray();
            app(UserService::class)->searchAll([
                'groups' => implode(',', array_values(config('cyclos.internal-names.groups'))),
                'statuses' => ['active'],
            ])
                ->filter(fn(array $cyclosUser) => ! in_array($cyclosUser['id'], $membershipsUsersIds, true))
                ->each(function (array $cyclosUser) {
                    Membership::create(['cyclos_user_id' => $cyclosUser['id']]);
                    dispatch(new SendEmailRenewCreated($cyclosUser['id']));
                });
        }
    }
}
