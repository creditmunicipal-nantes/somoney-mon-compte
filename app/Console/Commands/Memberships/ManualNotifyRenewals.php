<?php

namespace App\Console\Commands\Memberships;

use App\Jobs\Memberships\SendEmailManualRenewCreated;
use App\Models\Membership;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;

class ManualNotifyRenewals extends Command
{
    protected $signature = 'memberships:manual:notify:renewals
                            {--dry : Simulate execution.}';

    protected $description = 'Manually resend memberships renewals emails to eligible users.';

    protected UserService $userService;

    public function handle(): void
    {
        $this->output->title('Starting sending expired membership reminder email to users...');
        $dryRun = (bool) $this->option('dry');
        $rangeStart = Carbon::create(today()->year, 1, 15, 9)->startOfHour();
        $rangeEnd = Carbon::create(today()->year, 5, 3)->endOfDay();
        // Membership feature has to be activated and this command is only allowed to be triggered between
        // 15/01 09:00:00 and 03/05 23:59:59 for the current year.
        // The reason is that renewal reminder and warning emails are triggered at 04/05.
        if (! config('somoney.available.services.membership')) {
            $this->output->error(
                'The "membership" service has to be activated to launch this command ' .
                '(cf. "custom-data/current/config/somoney.php")'
            );

            return;
        }
        if (! now()->isBetween($rangeStart, $rangeEnd, true)) {
            $this->output->error('This command can only be launched between 15/01 09:00:00 and '
                . '03/05/23:59:59 of the current year.');

            return;
        }
        // We only can manually resend membership renewal emails for memberships created
        // during the period described above.
        $membershipsToRemind = Membership::whereBetween('created_at', [$rangeStart, $rangeEnd])
            ->whereNull('succeed_at')
            ->oldest('updated_at')
            ->get();
        if ($dryRun) {
            $this->table(
                ['Reminded user (Cyclos ID)', 'Created at', 'Updated at', 'Succeeded at'],
                $membershipsToRemind->map(fn(Membership $membership) => [
                    $membership->cyclos_user_id,
                    $membership->created_at,
                    $membership->updated_at,
                    $membership->succeed_at,
                ])
            );
            $this->output->success($membershipsToRemind->count()
                . ' users would have been reminded for expired membership if this commands had been really executed.');

            return;
        }
        foreach ($membershipsToRemind as $membership) {
            /** @var \App\Models\Membership $membership */
            dispatch(new SendEmailManualRenewCreated($membership->cyclos_user_id));
        }
        $this->output->success('Finished sending expired membership reminder email to '
            . $membershipsToRemind->count() . ' users.');
    }
}
