<?php

namespace App\Console\Commands\Users;

use App\Models\CyclosUser;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use ErrorException;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class ReactivateUsersFromDate extends Command
{
    protected $signature = 'users:reactivation {day : on format yyyy-mm-dd}';

    protected $description = 'Reactivate users deactivated on given day.';

    /** @throws \ErrorException */
    public function handle(): void
    {
        try {
            $searchedDate = Carbon::createFromFormat('Y-m-d', $this->argument('day'))
                ->setTimezone('UTC')
                ->startOfDay();
        } catch (InvalidFormatException $exception) {
            throw new ErrorException('The date "' . $this->argument('day')
                . '" is not in the correct format.');
        }
        $this->output->title('Starting reactivating users deactivated on ' . $searchedDate->format('l d F Y') . '...');
        $userServices = app(UserService::class);
        $userReactivatedCount = 0;
        $userServices->searchAll([
            'statuses' => [CyclosUser::STATUS_DISABLED],
        ])->each(function ($user) use (&$userReactivatedCount, $userServices, $searchedDate) {
            $statuses = $userServices->getStatus($user['id']);
            $dateDeactivated = Carbon::make($statuses['history'][0]['period']['begin']);
            if ($dateDeactivated->isSameDay($searchedDate)) {
                $userServices->setStatus(
                    $user['id'],
                    CyclosUser::STATUS_ACTIVE,
                    'Reactivated by script "users:reactivation" from "SoMoney - App"'
                );
                $message = 'User ' . $user['id'] . ' has been reactivated after being deactivated on '
                . $dateDeactivated->toAtomString() . '.';
                $this->line($message);
                Log::info($message);
                $userReactivatedCount++;
            }
        });
        $this->output->success('Finished reactivating ' . $userReactivatedCount . ' ' . Str::plural(
            'user',
            $userReactivatedCount
        ) . ' deactivated on ' . $searchedDate->format('l d F Y') . '.');
    }
}
