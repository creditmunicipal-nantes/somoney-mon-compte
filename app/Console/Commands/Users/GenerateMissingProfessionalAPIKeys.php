<?php

namespace App\Console\Commands\Users;

use App\Models\LocalUser;
use App\Services\Cyclos\UserService;
use App\Services\LocalUserService;
use Illuminate\Console\Command;

class GenerateMissingProfessionalAPIKeys extends Command
{
    /** @var string */
    protected $signature = 'professionals:api-keys:generate-missing';

    /** @var string */
    protected $description = 'Generate missing professional users API keys.';

    /**
     * @return void
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    public function handle(): void
    {
        $this->output->title('Starting generating missing professional users API keys...');
        LocalUser::where('group', 'professional')
            ->get()
            ->each(function (LocalUser $localUser) {
                if (! $localUser->customValues['apikey']) {
                    $localUser->setCustomValue('apikey', app(LocalUserService::class)->createUniqueApiKey());
                    $localUser->save();
                }
            });
        app(UserService::class)->searchAll([
            'profileFields' => ['apikey' => ''],
            'groups' => config('cyclos.internal-names.groups.professional'),
        ])->each(function (array $cyclosUser) {
            $cyclosUserEdit = app(UserService::class)->getDataForEdit($cyclosUser['id']);
            $cyclosUserEdit['user']['customValues']['apikey'] = app(LocalUserService::class)->createUniqueApiKey();
            app(UserService::class)->update($cyclosUser['id'], $cyclosUserEdit['user']);
        });
        $this->output->success('Finished generating missing professional users API keys.');
    }
}
