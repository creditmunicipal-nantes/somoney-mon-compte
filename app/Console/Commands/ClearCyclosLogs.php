<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Date;

class ClearCyclosLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cyclos:logs:clear
    {--force : truncate cyclos logs table}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove cyclos logs from the cyclos_logs table old than two weeks or all if force';

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        if ($this->option('force')) {
            DB::table('cyclos_logs')->truncate();
            $this->line('All cyclos logs cleared ✓');
        } else {
            DB::table('cyclos_logs')->whereDate('created_at', '<=', Date::today()->subWeekdays(2))->delete();
            $this->line('All cyclos logs older than two weeks cleared ✓');
        }
    }
}
