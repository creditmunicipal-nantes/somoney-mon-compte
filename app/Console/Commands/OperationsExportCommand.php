<?php

namespace App\Console\Commands;

use App\Exports\OperationsExport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Excel;

class OperationsExportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:operations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export operations list in a xls file';

    /**
     * Execute the console command.
     *
     * @param \Maatwebsite\Excel\Excel $excel
     * @param \App\Exports\OperationsExport $export
     *
     * @return mixed
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function handle(Excel $excel, OperationsExport $export)
    {
        return $excel->store($export, 'exports/operations.xls');
    }
}
