<?php

namespace App\Console\Commands;

use App\Exports\SepaExport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Excel;

class SepaExportGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:sepa';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export sepa list in a xls file';

    /**
     * Execute the console command.
     *
     * @param \Maatwebsite\Excel\Excel $excel
     * @param \App\Exports\SepaExport $export
     *
     * @return mixed
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function handle(Excel $excel, SepaExport $export)
    {
        return $excel->store($export, 'exports/sepa.xls');
    }
}
