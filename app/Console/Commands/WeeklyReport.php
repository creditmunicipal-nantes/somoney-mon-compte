<?php

namespace App\Console\Commands;

use App\Jobs\Report\SendEmailWeeklyReport;
use Illuminate\Support\Facades\Bus;
use Illuminate\Console\Command;

class WeeklyReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:weekly-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Weekly report mail sender';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (config('somoney.mail.weekly-report-contact')) {
            Bus::dispatch(new SendEmailWeeklyReport(config('somoney.mail.weekly-report-contact')));
            $this->info('mail envoyé au CM avec succès le ' . now());
        }
    }
}
