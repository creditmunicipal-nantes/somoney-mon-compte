<?php

namespace App\Console\Commands\Lemonway;

use App\Jobs\LemonWay\TransactionStatusUpdate;
use App\Models\Transaction;
use Illuminate\Support\Facades\Bus;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class WatchTransactionStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lemonway:transaction:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update status of transaction, and process depends on status';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        if (config('somoney.payment-gateway') !== Transaction::TYPE_LEMONWAY) {
            $this->error('The payment gateway on this instance is not Lemonway.');

            return Command::INVALID;
        }
        Transaction::where('status', Transaction::STATUS_PROCESSING)
            ->where('created_at', '>=', now()->subHours(12))
            ->where(function (Builder $query) {
                $query->where('transaction_type', Transaction::REGISTER_TYPE_ADHERE)
                    ->orWhere('transaction_type', null);
            })
            ->get()
            ->each(function (Transaction $transaction) {
                Bus::dispatch(new TransactionStatusUpdate($transaction));
            });
        Transaction::where('status', Transaction::STATUS_PROCESSING)
            ->where('created_at', '<=', now()->subHours(12))
            ->get()
            ->each(function (Transaction $transaction) {
                $transaction->update(['status' => Transaction::STATUS_CANCELED]);
            });
    }
}
