<?php

namespace App\Console\Commands\Lemonway;

use App\Jobs\LemonWay\MoneyOutJob;
use App\Models\Transaction;
use App\Models\Wallet;
use Illuminate\Support\Facades\Bus;
use Illuminate\Console\Command;

class MoneyOutTransfer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lemonway:money-out:transfer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make an external transfer with the maximum amount possible';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (config('somoney.payment-gateway') !== Transaction::TYPE_LEMONWAY) {
            $this->error('The payment gateway on this instance is not Lemonway.');

            return Command::INVALID;
        }
        if (! config('lemonway.iban_id')) {
            $this->error('The Lemonway iban_id for money out is needed.');

            return Command::INVALID;
        }
        if (config('somoney.available.services.lemonway-v2')) {
            $scBalance = (float) app('lemonway')->GetWalletDetails([
                'wallet' => 'SC',
            ])->lwXml->WALLET->BAL;
            if ($scBalance > config('lemonway.balance_min')) {
                $wallet = Wallet::where('lw_id', 'SC')->first();
                $maxAmount = $scBalance - config('lemonway.balance_min');
                // take only the max possible
                if ($maxAmount > (int) config('lemonway.money_out.max')) {
                    $maxAmount = (int) config('lemonway.money_out.max');
                }
                Bus::dispatchNow(new MoneyOutJob($wallet, $maxAmount * 100));
            }
        }
    }
}
