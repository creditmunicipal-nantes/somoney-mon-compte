<?php

namespace App\Console\Commands;

use App\Jobs\LoadEmailStatus;
use App\Models\LocalUser;
use App\Services\Cyclos\UserService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Bus;

class RefreshEmailStatus extends Command
{
    /** @var string */
    protected $signature = 'mail:status:refresh';

    /** @var string */
    protected $description = 'Refresh email for address mail validation status';

    public function handle(): int
    {
        $this->output->title('Starting refreshing mailgun email adresse status...');
        if (! config('services.mailgun.secret')) {
            $this->error('You cannot execute this command if the env var "MAILGUN_SECRET" is not defined');

            return 1;
        }

        $users = app(UserService::class)->searchAll([
            'groups' => implode(',', array_values(config('cyclos.internal-names.groups'))),
        ]);
        Bus::dispatch(new LoadEmailStatus($users->pluck('id'), LocalUser::all()));
        $this->output->success('Finished refreshing mailgun email adresse status.');
        return 0;
    }
}
