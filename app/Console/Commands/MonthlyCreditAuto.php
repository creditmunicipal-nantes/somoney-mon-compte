<?php

namespace App\Console\Commands;

use App\Jobs\SendEmailRemindStopSepa;
use App\Jobs\SepaTransfer;
use App\Models\Sepa;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Facades\Bus;
use Illuminate\Console\Command;

class MonthlyCreditAuto extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'credit:auto';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute planned SEPA transfers.';

    protected UserService $userService;

    public function handle(): void
    {
        // Only get recurrent sepa transfers.
        $now = now()->endOfDay();
        $sepaTransfers = Sepa::where('done', true)
            ->where('canceled', false)
            ->where('stopped', false)
            ->where('confirmed', true)
            ->whereNotNull('start_date')
            ->whereNotNull('duration_month')
            ->get()
            ->filter(function (Sepa $sepaTransfer) use ($now) {
                $shouldBeExecutedToday = $this->getNormalizedDayInMonth($sepaTransfer->start_date->day) === $now->day;
                $exceedDurationLimit = $sepaTransfer->start_date->diffInMonths($now) >= $sepaTransfer->duration_month;

                return $shouldBeExecutedToday && ! $exceedDurationLimit;
            });
        $sepaTransfers->each(function (Sepa $sepaTransfer) use ($now) {
            Bus::dispatchSync(new SepaTransfer($sepaTransfer->id));
            // Send email reminder to managers when the last SEPA transfer has been executed.
            if ($sepaTransfer->start_date->diffInMonths($now) === ($sepaTransfer->duration_month - 1)) {
                Bus::dispatch(new SendEmailRemindStopSepa($sepaTransfer->id));
            }
        });
    }

    /**
     * Convert 29, 30, 31 month days to 28 when
     *
     * @param int $day
     *
     * @return int
     */
    protected function getNormalizedDayInMonth(int $day): int
    {
        $nbExceedDay = $day - now()->daysInMonth;
        if ($nbExceedDay > 0) {
            return $day - $nbExceedDay;
        }

        return $day;
    }
}
