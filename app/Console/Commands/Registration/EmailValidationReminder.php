<?php

namespace App\Console\Commands\Registration;

use App\Jobs\SendEmailValidationCreation;
use App\Models\LocalUser;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Facades\Bus;
use Illuminate\Console\Command;

class EmailValidationReminder extends Command
{
    /** @var string */
    protected $signature = 'registration:email-validation:reminder';

    /** @var string */
    protected $description = 'Remind 1 week after register validation email sent';

    protected UserService $userService;

    public function handle(): void
    {
        $localUsers = app(LocalUser::class)
            ->where('updated_at', '<', today()->startOfDay()->subDays(7))
            ->whereIn('step', [
                config('registration.steps.professional.by_slug.success')['id'],
                config('registration.steps.individual.by_slug.success')['id'],
            ])
            ->whereNull('reminded')
            ->get();
        $localUsers->each(function (LocalUser $localUser) {
            $localUser->reminded = now();
            $localUser->save();
            Bus::dispatch(new SendEmailValidationCreation($localUser->id, true));
        });
    }
}
