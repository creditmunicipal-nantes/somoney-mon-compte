<?php

namespace App\Console;

use Carbon\CarbonInterface;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Carbon;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    protected function schedule(Schedule $schedule): void
    {
        if ($this->shouldRun()) {
            // Change transaction status by get it on Lemonway - every five minutes
            $schedule->command('lemonway:transaction:update')->everyFiveMinutes();

            // Generate the members list xls file every day at 00:00 AM
            $schedule->command('export:members')->daily()->at('00:00');
            // Generate the operations list xls file every day at 00:10 AM
            $schedule->command('export:operations')->daily()->at('00:10');
            if (config('somoney.available.services.sepa-payment')) {
                // Generate the sepa operations list xls file every day at 00:15 AM
                $schedule->command('export:sepa')->daily()->at('00:15');
            }
            // Generate the members list json file for data studio every day at 00:20 AM
            $schedule->command('export:members:data-studio')->daily()->at('00:20');
            // Generate the operations list json file for data studio every day at 00:20 AM
            $schedule->command('export:operations:data-studio')->daily()->at('00:30');
            // Remove local user created a month ago every day at 00:30 AM
            $schedule->command('local-users:clear:older-than-one-day')->daily()->at('00:40');
            // Do the auto credit(s) of the day every day at 02:00 AM
            $schedule->command('credit:auto')->daily()->at('02:00');
            // send mail to CM every friday at 09:00 AM
            $schedule->command('mail:weekly-report')->weekly()->fridays()->at('09:00');
            if (config('somoney.available.services.lemonway-v2')) {
                // make transfer every wednesdays at 10:00 AM
                $schedule->command('lemonway:money-out:transfer')->weekly()->wednesdays()->at('10:00');
            }
            // Remind user that don't validated their email address after 7 days every day at 09:15 AM
            $schedule->command('registration:email-validation:reminder')->daily()->at('09:15');
            if (config('somoney.available.services.newsletters')) {
                // Dispatch SendEmailNewsletter job, check and send newsletters if needed
                $schedule->command('newsletter:send')->everyFifteenMinutes();
            }
            if (config('somoney.available.services.membership')) {
                // Memberships renew create every year on the 15th of january at 09:00 AM
                $schedule->command('memberships:make:renewal')->daily()->at('09:00');
                if (config('somoney.available.services.membership_notify_for_renew')) {
                    // Memberships renew notify every year on the 5th of April/Mai/June at 09:00 AM
                    $schedule->command('memberships:notify:renewals')->daily()->at('09:00');
                }
                if (config('somoney.available.services.membership_block_expired_accounts')) {
                    // Memberships block non members every year on the 31th of June at 00:00 PM
                    $schedule->command('memberships:account:block')->daily()->at('00:00');
                }
            }
            $schedule->command('queue:stuck:notify')->twiceDaily(10, 16);
            if (config('somoney.available.services.email-status')) {
                $schedule->command('mail:status:refresh')->twiceDaily(10, 16);
            }
            $schedule->command('cyclos:logs:clear')->weeklyOn(CarbonInterface::MONDAY)->at('07:00');
        }
    }

    protected function shouldRun(): bool
    {
        $shouldRun = ! $this->onScheduledDailyMaintenance();

        // $shouldRun = $shouldRun ?: // other condition;

        return $shouldRun;
    }

    protected function onScheduledDailyMaintenance(): bool
    {
        $dailyMaintenanceStart = $this->carbonDateFromStringTime('03:30');
        $dailyMaintenanceEnd = $this->carbonDateFromStringTime('05:30');

        return now($this->scheduleTimezone())->between($dailyMaintenanceStart, $dailyMaintenanceEnd);
    }

    protected function carbonDateFromStringTime(string $time): Carbon
    {
        $segments = explode(':', $time);

        return now($this->scheduleTimezone())
            ->startOfDay()
            ->hours((int) $segments[0])
            ->minutes((int) $segments[1]);
    }

    /**
     * Get the timezone that should be used by default for scheduled events.
     *
     * @return \DateTimeZone|string|null
     */
    protected function scheduleTimezone()
    {
        return 'Europe/Paris';
    }

    protected function commands(): void
    {
        $this->load(__DIR__ . '/Commands');
    }
}
