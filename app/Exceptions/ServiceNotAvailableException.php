<?php

namespace App\Exceptions;

use Exception;

class ServiceNotAvailableException extends Exception
{
}
