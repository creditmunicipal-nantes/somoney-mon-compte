<?php

namespace App\Exceptions\Cyclos;

use Exception;
use Illuminate\Contracts\Support\MessageBag;

class ValidationException extends Exception
{
    public function __construct(protected MessageBag $messageBag)
    {
        parent::__construct('Data failed to pass validation');
    }

    public function getMessageBag(): MessageBag
    {
        return $this->messageBag;
    }
}
