<?php

namespace App\Exceptions\Cyclos;

use Exception;
use Throwable;

class PasswordExpiredException extends Exception
{
    public function __construct(
        public bool $isAdminClient = false,
        $message = "",
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
