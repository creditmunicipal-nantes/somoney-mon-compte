<?php

namespace App\Exceptions\Cyclos;

use Exception;
use Throwable;

class ValidationSentryException extends Exception
{
    public array $cyclosRequestParams = [];

    /**
     * ValidationSentryException constructor.
     *
     * @param string $message
     * @param array $params
     * @param bool $bodyJson
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct(
        $message = "",
        array $params = [],
        bool $bodyJson = false,
        $code = 0,
        Throwable $previous = null
    ) {
        $this->cyclosRequestParams = $params;

        if ($bodyJson) {
            data_set(
                $this->cyclosRequestParams,
                'body',
                json_decode(data_get($this->cyclosRequestParams, 'body', "[]"), true)
            );
        }

        parent::__construct($message, $code, $previous);
    }
}
