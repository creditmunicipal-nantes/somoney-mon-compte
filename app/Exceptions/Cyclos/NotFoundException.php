<?php

namespace App\Exceptions\Cyclos;

use Exception;

class NotFoundException extends Exception
{
}
