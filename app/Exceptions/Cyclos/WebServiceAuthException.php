<?php

namespace App\Exceptions\Cyclos;

use Exception;

class WebServiceAuthException extends Exception
{
    /**
     * WebServiceAuthException constructor.
     *
     * @param string $body
     */
    public function __construct($body = "")
    {
        parent::__construct($body);
    }
}
