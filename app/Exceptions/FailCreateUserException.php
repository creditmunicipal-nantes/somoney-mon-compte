<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class FailCreateUserException extends Exception
{
    public function __construct(
        public string $userType,
        string $message,
        $code = 0,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
