<?php

namespace App\Exceptions;

use App\Exceptions\Cyclos\ValidationException as CyclosValidationException;
use App\Exceptions\Cyclos\ValidationSentryException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Sentry\State\Scope;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of exception types with their corresponding custom log levels.
     *
     * @var array<class-string<\Throwable>, \Psr\Log\LogLevel::*>
     */
    protected $levels = [
        //
    ];

    /**
     * A list of the exception types that should not be reported.
     *
     * @var array<int, class-string<\Throwable>>
     */
    protected $dontReport = [
        AuthenticationException::class,
        CyclosValidationException::class,
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        TokenMismatchException::class,
        ValidationException::class,
    ];

    /**
     * A list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = ['password', 'password_confirmation'];

    /**
     * @param \Throwable $exception
     *
     * @throws \Throwable
     */
    public function report(Throwable $exception): void
    {
        if ($this->shouldReport($exception) && app()->bound('sentry')) {
            if ($exception instanceof ValidationSentryException) {
                /** @var ValidationSentryException $exception */
                app('sentry')->configureScope(function (Scope $scope) use ($exception): void {
                    $scope->setExtras($exception->cyclosRequestParams);
                });
            }
            app('sentry')->captureException($exception);
        }

        parent::report($exception);
    }

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register(): void
    {
        //
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Throwable $throwable
     *
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response
     * @throws \Throwable
     */
    public function render(
        $request,
        Throwable $throwable
    ): \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response {
        if ($throwable instanceof ValidationException) {
            return redirect()->back()->withInput()->withErrors($throwable->errors());
        }

        if ($throwable instanceof FailCreateUserException) {
            return redirect()->route('register.' . $throwable->userType . '.fail-create');
        }

        return parent::render($request, $throwable);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Auth\AuthenticationException $authenticationException
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function unauthenticated(
        $request,
        AuthenticationException $authenticationException
    ): \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }
        session()->remove('auth');

        // clean before set current after login redirect
        session()->remove('login_redirect');
        session()->push('login_redirect', $request->url());

        return redirect()->guest(route('auth.login'));
    }
}
