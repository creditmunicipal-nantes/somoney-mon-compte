<?php

namespace App\Exceptions;

use Exception;

class QrCodeTokenNotFoundException extends Exception
{
}
