<?php

namespace App\Models;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kblais\QueryFilter\Filterable;
use Kblais\Uuid\Uuid;

class Sepa extends Model
{
    use Uuid;
    use Filterable;

    protected $table = 'sepas';

    protected $fillable = [
        'user_id',
        'amount',
        'gender',
        'last_name',
        'first_name',
        'email',
        'company',
        'iban',
        'done',
        'stopped',
        'canceled',
        'confirmed',
        'duration_month',
        'start_date',
        'last_executed_at',
    ];
    protected $casts = [
        'start_date' => 'datetime',
        'last_executed_at' => 'datetime',
    ];

    public function getIsRecurrentAttribute(): bool
    {
        return ! is_null($this->duration_month);
    }

    public function getEndDateAttribute(): ?Carbon
    {
        return $this->is_recurrent
            ? Carbon::parse($this->start_date_txt)->addMonths($this->duration_month)
            : null;
    }

    public function getIsDurationExceededAttribute(): ?bool
    {
        return $this->is_recurrent
            ? now()->gte($this->end_date)
            : null;
    }

    public function getStatusAttribute(): string
    {
        if ($this->canceled) {
            return 'canceled';
        } else {
            if ($this->done) {
                if ($this->is_recurrent) {
                    return $this->getRecurrentDoneStatus();
                } else {
                    return 'done';
                }
            } else {
                if (! $this->is_recurrent || $this->confirmed) {
                    return 'validation';
                } else {
                    return 'not-confirmed';
                }
            }
        }
    }

    public function getStatusIconAttribute(): string
    {
        return config('sepas.status.icon')[$this->status];
    }

    public function getStatusLabelAttribute(): string
    {
        return __('sepas.status.label')[$this->status];
    }

    public function getStartDateTxtAttribute(): string
    {
        return (data_get($this->attributes, 'start_date')
            ? Carbon::parse($this->attributes['start_date'])
            : (
                today()->setDay(config('somoney.credit.sepa.sampling_month_day'))->gt(today())
                    ? today()->setDay(config('somoney.credit.sepa.sampling_month_day'))
                    : today()->addMonth()->setDay(config('somoney.credit.sepa.sampling_month_day'))
            )
        )->format('Y-m-d');
    }

    private function getRecurrentDoneStatus(): string
    {
        if ($this->done) {
            if ($this->is_duration_exceeded) {
                return 'ended';
            }

            return 'in-progress';
        }

        return 'validation';
    }
}
