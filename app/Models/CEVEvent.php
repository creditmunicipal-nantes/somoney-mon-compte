<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class CEVEvent extends Model
{
    use Uuid;

    protected $table = 'cev_event';

    protected $fillable = [
        'file_name', 'label', 'line',
    ];
}
