<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class Membership extends Model
{
    use Uuid;

    protected $table = 'memberships';

    protected $fillable = ['cyclos_user_id', 'succeed_at'];

    protected $casts = ['succeed_at' => 'datetime'];
}
