<?php

namespace App\Models;

use App\Services\Cyclos\UserService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Kblais\Uuid\Uuid;

class SdgProfessional extends Model
{
    use Uuid;

    protected $table = 'sdg_professionals';

    protected $fillable = [
        'sdg_id',
        'cyclos_user_id',
        'ended_at',
    ];

    protected $casts = [
        'ended_at' => 'datetime',
    ];

    public function sdg(): BelongsTo
    {
        return $this->belongsTo(Sdg::class, 'sdg_id');
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getCyclosUserAttribute(): array
    {
        return app(UserService::class)->get($this->cyclos_user_id);
    }
}
