<?php

namespace App\Models;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Arr;
use Kblais\QueryFilter\Filterable;
use Kblais\Uuid\Uuid;

class LocalUser extends Model
{
    use Uuid;
    use Filterable;

    protected $table = 'local_users';

    protected $fillable = [
        'cyclos_id',
        'name',
        'email',
        'step',
        'group',
        'addresses',
        'mobilePhones',
        'landLinePhones',
        'contactInfos',
        'siren',
        'passwords',
        'images',
        'customValues',
        'acceptAgreement',
        'reminded',
        'email_status',
    ];

    protected $casts = [
        "addresses" => 'json',
        "mobilePhones" => 'json',
        "landLinePhones" => 'json',
        "contactInfos" => 'json',
        "passwords" => 'json',
        "images" => 'json',
        "customValues" => 'json',
    ];

    public function getIdAttribute(): string
    {
        return data_get($this->attributes, 'id', $this->generateUuid());
    }

    public function getMailProAttribute(): string
    {
        return data_get($this->attributes, 'customValues.mailpro', $this->email);
    }

    /**
     * @return string|null
     * @throws \JsonException
     */
    public function getPhoneAttribute(): ?string
    {
        $phone = data_get($this->attributes, 'mobilePhones');
        if ($phone !== '[]') {
            return Arr::last(json_decode($phone, true, 512, JSON_THROW_ON_ERROR))['number'];
        }
        $phone = data_get($this->attributes, 'landLinePhones');
        if ($phone !== '[]') {
            return Arr::last(json_decode($phone, true, 512, JSON_THROW_ON_ERROR))['number'];
        }

        return null;
    }

    /**@throws \JsonException */
    public function getAddressAttribute(): ?array
    {
        return Arr::last(json_decode(data_get($this->attributes, 'addresses'), true, 512, JSON_THROW_ON_ERROR));
    }

    public function getAddressFullAttribute(): ?string
    {
        return $this->address
            ? data_get($this->address, 'addressLine1') . ' ' . data_get($this->address, 'zip') . ' '
            . data_get($this->address, 'city')
            : null;
    }

    public function getIdattachmentPathAttribute(): string
    {
        return str_replace(url('/'), '', data_get('idattachment', $this->customValues));
    }

    public function getRibattachmentPathAttribute(): string
    {
        return str_replace(url('/'), '', data_get('ribattachment', $this->customValues));
    }

    public function getOrganisationattachmentPathAttribute(): string
    {
        return str_replace(url('/'), '', data_get('organisationattachment', $this->customValues));
    }

    public function getValidationUserAttribute(): ValidationUser
    {
        $validationUser = ValidationUser::where('local_user_id', $this->id)->first();
        if (! $validationUser) {
            $validationUser = ValidationUser::create([
                'local_user_id' => $this->id,
                'group' => $this->group,
                'email' => $this->email,
            ]);
        }

        return $validationUser;
    }

    public function getCreditStepReachedAttribute(): bool
    {
        return $this->group === 'individual'
            && $this->step === data_get(config("registration.steps.individual.by_slug.credit"), 'id');
    }

    public function getEmailStepReachedAttribute(): bool
    {
        $step = config("registration.steps.{$this->group}.by_slug.success")['step'];

        return data_get(config("registration.steps.{$this->group}.by_id")[$this->step], 'step', $step) >= $step;
    }

    /** @throws \JsonException */
    public function setCustomValue(string $key, mixed $value): void
    {
        $currentCustomValues = json_decode(
            data_get($this->attributes, 'customValues', []),
            true,
            512,
            JSON_THROW_ON_ERROR
        );
        $this->attributes['customValues'] = json_encode(
            array_merge($currentCustomValues, [$key => $value]),
            JSON_THROW_ON_ERROR
        );
    }

    public function toCyclosArray(): array
    {
        $data = collect($this->toArray())->filter(fn($attribute, $key) => $attribute
            && in_array($key, config('cyclos.user.attributes'), true))->toArray();
        if (array_key_exists('addresses', $data)) {
            $data['addresses'] = Arr::last($data['addresses']);
        }
        $data['hiddenFields'] = ['email'];
        $data['passwords'][0]['value'] = Crypt::decryptString($data['passwords'][0]['value']);

        return $data;
    }
}
