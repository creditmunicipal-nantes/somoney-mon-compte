<?php

namespace App\Models;

use App\Services\Cyclos\TransferService;
use App\Services\Cyclos\UserService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Arr;
use Kblais\Uuid\Uuid;

class BanknotesTransaction extends Model
{
    use Uuid;

    protected $table = 'banknotes_transactions';

    protected $fillable = [
        'operator_cyclos_id',
        'user_cyclos_id',
        'type',
        'details',
        'counter_id',
    ];

    protected $appends = [
        'amount',
    ];

    protected $casts = [
        'details' => 'array',
    ];

    // Relations #######################################################################################################

    public function counter(): BelongsTo
    {
        return $this->belongsTo(Counter::class);
    }

    // Mutators ########################################################################################################

    public function getAmountAttribute(): float
    {
        $amount = 0;
        foreach ($this->details as $value => $quantity) {
            $amount += $value * $quantity;
        }

        return $amount;
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getCyclosIdAttribute(): string
    {
        return data_get(Arr::first((app(TransferService::class)->list([
            'user' => config('somoney.counters-cyclos-account'),
            'datePeriod' => $this->created_at->format('Y-m-d\TH:i:s'),
        ]))), 'id');
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getCyclosUserAttribute(): CyclosUser
    {
        return CyclosUser::fill(app(UserService::class)->get($this->user_cyclos_id));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getOperatorUserAttribute(): CyclosUser
    {
        if ($this->operator_cyclos_id) {
            return CyclosUser::fill(app(UserService::class)->get($this->operator_cyclos_id));
        }

        return new CyclosUser([
            'name' => '-',
        ]);
    }

    public function getDescriptionAttribute(): string
    {
        return '💶 ' . $this->counter->name . ' - ' . $this->cyclos_user->name . ' [' . $this->id . ']';
    }

    public function getBanknotesDetailsAttribute(): string
    {
        $template = <<<EOT
<div class="c-h4 u-mg-bottom">
    {$this->txt_type} de {$this->cyclos_user->name} le {$this->created_at->format('d/m/Y')}
</div>
<table class="c-table u-mg-bottom">
<tr><th class="u-inline-center">Billet</th><th class="u-inline-center">Quantité</th></tr>
EOT;

        foreach ($this->details as $value => $quantity) {
            $template .= '<tr>';

            $template .= '<th class="u-inline-center">' . $value . ' <i class="c-icon icon-money"></i></th>';
            $template .= '<td class="u-inline-center">' . $quantity . '</td>';

            $template .= '</tr>';
        }
        $template .= <<<EOT
</table>
<div class="c-h4 u-mg-bottom">Total {$this->amount} <i class="c-icon icon-money"></i></div>
EOT;

        return $template;
    }

    public function getTxtTypeAttribute(): ?string
    {
        return match ($this->type) {
            'out' => config('somoney.counters-cash-out', 'Sortie'),
            'in' => config('somoney.counters-cash-in', 'Retour'),
            default => null,
        };
    }

    // QUERIES #########################################################################################################

    public static function getUserBalance(string $userCyclosId): float
    {
        return BanknotesTransaction::where('user_cyclos_id', $userCyclosId)
            ->get()
            ->reduce(
                fn(float $balance, BanknotesTransaction $transaction) => $balance + ($transaction->type === 'in'
                        ? -$transaction->amount
                        : $transaction->amount),
                0
            );
    }
}
