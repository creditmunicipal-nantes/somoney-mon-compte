<?php

namespace App\Models;

use App\Services\Cyclos\UserService;
use Illuminate\Database\Eloquent\Model;
use Kblais\QueryFilter\Filterable;
use Kblais\Uuid\Uuid;

class LeavingUser extends Model
{
    use Uuid;
    use Filterable;

    protected $table = 'leaving_users';

    protected $fillable = [
        'cyclos_user_id',
        'is_reconverted',
    ];

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getCyclosUserAttribute(): CyclosUser
    {
        return CyclosUser::fill(app(UserService::class)->get($this->cyclos_user_id));
    }
}
