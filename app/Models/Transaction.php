<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kblais\Uuid\Uuid;

class Transaction extends Model
{
    use SoftDeletes;
    use Uuid;
    use HasFactory;

    public const STATUS_PENDING = 'pending';

    public const STATUS_PROCESSING = 'processing';

    public const STATUS_SUCCESS = 'success';

    public const STATUS_CANCELED = 'canceled';

    public const STATUS_FAILED = 'failed';

    public const TYPE_LEMONWAY = 'lemonway';

    public const TYPE_SYSTEM_PAY = 'system-pay';

    public const CONTEXT_OPERATION = 'operation';

    public const CONTEXT_REGISTRATION = 'registration';

    public const CONTEXT_MEMBERSHIP = 'membership';

    public const REGISTER_TYPE_ADHERE = 'register-adhere';

    public const REGISTER_TYPE_CREDIT = 'register-credit';

    public const REGISTER_TYPE_DONATE = 'register-donate';

    protected $table = 'transaction';

    protected $fillable = [
        'type',
        'amount',
        'source_wallet',
        'target_wallet',
        'source_wallet_id',
        'target_wallet_id',
        'cyclos_user_id',
        'local_user_id',
        'lw_id',
        'lw_executed_at',
        'status',
        'failure_reason',
        'lw_token',
        'wk_token',
        'depends_on',
        'context',
        'transaction_type',
        'parent_id',
    ];
    protected $casts = [
        'lw_executed_at' => 'datetime',
    ];

    public static function getStatuses(): array
    {
        return [
            self::STATUS_PENDING,
            self::STATUS_PROCESSING,
            self::STATUS_SUCCESS,
            self::STATUS_FAILED,
        ];
    }

    public function sourceWallet(): BelongsTo
    {
        return $this->belongsTo(Wallet::class);
    }

    public function targetWallet(): BelongsTo
    {
        return $this->belongsTo(Wallet::class);
    }

    public function localUser(): BelongsTo
    {
        return $this->belongsTo(LocalUser::class, 'local_user_id');
    }

    public function childs(): HasMany
    {
        return $this->hasMany(Transaction::class, 'parent_id');
    }

    // mutators

    public function parent(): BelongsTo
    {
        return $this->belongsTo(Transaction::class, 'parent_id');
    }

    public function setTargetWalletAttribute(array $targetWallet): void
    {
        $this->attributes['target_wallet_id'] = $targetWallet['id'];
    }

    public function setSourceWalletAttribute(array $sourceWallet): void
    {
        $this->attributes['source_wallet_id'] = $sourceWallet['id'];
    }

    // macros queries

    public function getLemonwayStatusAttribute(): ?int
    {
        return config('lemonway.status.' . $this->status);
    }

    public function scopePending(Builder $query): void
    {
        $query->where('status', self::STATUS_PENDING);
    }

    public function scopeProcessing(Builder $query): void
    {
        $query->where('status', self::STATUS_PROCESSING);
    }

    public function scopeCanceled(Builder $query): void
    {
        $query->where('status', self::STATUS_CANCELED);
    }

    public function scopeFailed(Builder $query): void
    {
        $query->where('status', self::STATUS_FAILED);
    }

    // helper

    public function scopeSuccess(Builder $query): void
    {
        $query->where('status', self::STATUS_SUCCESS);
    }

    public function getTransactionTypeLabelAttribute(): string
    {
        if ($this->context === self::CONTEXT_REGISTRATION) {
            switch ($this->transaction_type) {
                case self::REGISTER_TYPE_CREDIT:
                    return 'Crédit';
                case self::REGISTER_TYPE_ADHERE:
                    return 'Adhésion';
                case self::REGISTER_TYPE_DONATE:
                    return 'Don';
            }
        }

        return $this->context === self::CONTEXT_MEMBERSHIP ? 'Adhésion' : 'Crédit';
    }
}
