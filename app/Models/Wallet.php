<?php

namespace App\Models;

use App\Services\Cyclos\UserService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kblais\Uuid\Uuid;

class Wallet extends Model
{
    use SoftDeletes;
    use Uuid;

    /**
     * Status given right after the call to RegisterWallet
     */
    public const STATUS_WALLET_KYC_1 = '5';

    /**
     * Documents received and approved
     */
    public const STATUS_WALLET_KYC_2 = '6';

    /**
     * Warning : you cannot reopen a closed wallet.
     * Only wallets with balance = 0 can be closed.
     */
    public const STATUS_WALLET_CLOSED = '12';

    public const DEFAULT_STATUS = self::STATUS_WALLET_KYC_1;

    protected $table = 'wallet';

    protected $fillable = [
        'unique_id',
        'lw_id',
        'status',
        'cyclos_user_id',
    ];

    /**
     * @return string
     */
    public static function generateLemonWayId(): string
    {
        $hex = bin2hex(openssl_random_pseudo_bytes(8));

        return implode('-', str_split($hex, 4));
    }

    /**
     * @return \App\Models\CyclosUser|null
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getUserAttribute(): ?CyclosUser
    {
        return CyclosUser::fill((new UserService())->get($this->cyclos_user_id));
    }
}
