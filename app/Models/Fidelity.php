<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fidelity extends Model
{
    protected $table = 'fidelities';

    protected $fillable = [
        'cyclos_user_id',
        'counter',
        'counter_as_price',
        'discount',
        'discount_as_percent',
        'groups_type',
        'active',
    ];

    /**
     * @param \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', true);
    }
}
