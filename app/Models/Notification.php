<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class Notification extends Model
{
    use Uuid;

    public const TOPRELOGIN = 'prelogin';

    public const TOIND = 'individual';

    public const TOPRO = 'professional';

    public const TOALL = 'all';

    protected $table = 'notification';

    protected $fillable = ['message', 'groups', 'display'];
}
