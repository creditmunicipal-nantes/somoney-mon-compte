<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kblais\QueryFilter\Filterable;
use Kblais\Uuid\Uuid;

class City extends Model
{
    use Uuid;
    use Filterable;

    protected $table = 'city';

    protected $fillable = [
        'name', 'insee_code', 'zipcode', 'lat', 'lon',
    ];
}
