<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Okipa\MediaLibraryExt\ExtendsMediaAbilities;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Newsletter extends Model implements HasMedia
{
    use InteractsWithMedia;
    use ExtendsMediaAbilities;

    /** @var string */
    protected $table = 'newsletters';

    protected $fillable = ['cyclos_user_id', 'title', 'content', 'send_date', 'sent'];

    /**
     * Register the media collections.
     * @SuppressWarnings(PHPMD.UnusedFormalParameter))
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('newsletterImage')
            ->singleFile()
            ->acceptsMimeTypes(['image/jpeg', 'image/png'])
            ->registerMediaConversions(function (Media $media = null) {
                $this->addMediaConversion('thumb')
                    ->keepOriginalImageFormat()
                    ->fit(Manipulations::FIT_CROP, 40, 40);
            });
    }
}
