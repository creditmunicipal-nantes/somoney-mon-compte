<?php

namespace App\Models;

use App\Services\Cyclos\UserService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Kblais\Uuid\Uuid;

class Sdg extends Model
{
    use Uuid;

    protected $table = 'sdgs';

    protected $fillable = [
        'title',
        'description',
        'category_id',
        'active',
    ];

    // Relations #######################################################################################################

    public function professionals(): HasMany
    {
        return $this->hasMany(SdgProfessional::class);
    }

    public function getProfessionalsAttribute(): Collection
    {
        $sdgProfessionals = $this->professionals()->get();

        $professionalIds = $sdgProfessionals->pluck('cyclos_user_id');

        $professionalUsers = app(UserService::class)->searchAll([
            'usersIdsToInclude' => $professionalIds->implode(','),
        ])->reduce(
            fn(Collection $professionals, array $professional) => $professionals->put(
                data_get($professional, 'id'),
                $professional
            ),
            collect()
        );

        return $sdgProfessionals->map(function (SdgProfessional $sdgProfessional) use ($professionalUsers) {
            $sdgProfessional->cyclosUser = $professionalUsers->get($sdgProfessional->cyclos_user_id);

            return $sdgProfessional;
        });
    }

    public function getConnectedProfessionalAttribute(): ?SdgProfessional
    {
        /** @var \App\Models\SdgProfessional|null $sdgProfessional */
        $sdgProfessional = $this->professionals()->where('cyclos_user_id', session('auth.user.id'))->first();

        return $sdgProfessional;
    }

    public function getProfessionalsEndedAttribute(): Collection
    {
        return $this->professionals->filter(fn(SdgProfessional $professional) => (bool) $professional->ended_at);
    }

    public function getCategoryAttribute(): array
    {
        return config('sdg.categories.id_raw')[$this->category_id];
    }
}
