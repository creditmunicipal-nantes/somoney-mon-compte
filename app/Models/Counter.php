<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Kblais\Uuid\Uuid;

class Counter extends Model
{
    use Uuid;

    protected $table = 'counters';

    protected $fillable = [
        'name',
        'zipcode',
        'city',
        'operators',
    ];

    protected $casts = [
        'operators' => 'array',
    ];

    // Relations #######################################################################################################

    public function transactions(): HasMany
    {
        return $this->hasMany(BanknotesTransaction::class);
    }

    public function stocks(): HasMany
    {
        return $this->hasMany(BanknotesStock::class);
    }

    // Mutators ########################################################################################################

    public function getLastStockAttribute(): ?BanknotesStock
    {
        /** @var \App\Models\BanknotesStock|null $bankNotesStock */
        $bankNotesStock = $this->stocks()->where('counter_id', $this->id)
            ->latest()
            ->first();

        return $bankNotesStock;
    }

    public function getStockBalanceAttribute(): float
    {
        $lastStock = $this->last_stock;
        if ($this->stocks->isNotEmpty()) {
            return $lastStock->stock - $this->sum_stock;
        }
        return $this->sum_stock === 0.0 ? 0 : -$this->sum_stock;
    }

    public function getSumStockAttribute(): float
    {
        if ($this->last_stock) {
            return $this->transactions()
                ->where('created_at', '>=', $this->last_stock->created_at)
                ->get()
                ->reduce(fn(int $sum, BanknotesTransaction $transaction) => $sum + $transaction->amount, 0);
        }

        return $this->transactions()->get()
            ->reduce(fn(int $sum, BanknotesTransaction $transaction) => $sum + $transaction->amount, 0);
    }
}
