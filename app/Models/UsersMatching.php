<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kblais\Uuid\Uuid;

class UsersMatching extends Model
{
    use Uuid;
    use SoftDeletes;

    protected $fillable = ['user_id', 'cev_id', 'cev_account_id'];
}
