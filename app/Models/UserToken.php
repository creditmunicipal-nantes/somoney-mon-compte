<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class UserToken extends Model
{
    use Uuid;

    public const TYPE_RESET_PASSWORD = 'reset-password';

    public const TYPE_EMAIL_VALIDATION = 'email-validation';

    public const TYPE_REGISTRATION_NEED_MEMBERSHIP = 'email-need-credit';

    public const TYPE_RESUME_REGISTRATION = 'resume-registration';

    public const TYPE_NEW_PASSWORD_CGU = 'new-password-cgu';

    public const TYPE_MEMBERSHIP_RENEW = 'membership-renew';

    protected $table = 'user_token';

    protected $fillable = ['user_id', 'type'];

    public function scopeIsResetToken(Builder $query): Builder
    {
        return $query->where('type', self::TYPE_RESET_PASSWORD);
    }

    public function scopeIsEmailValidationToken(Builder $query): Builder
    {
        return $query->where('type', self::TYPE_EMAIL_VALIDATION);
    }

    public function scopeIsResumeRegistrationToken(Builder $query): Builder
    {
        return $query->where('type', self::TYPE_RESUME_REGISTRATION);
    }

    public function scopeIsRegistrationNeedMembership(Builder $query): Builder
    {
        return $query->where('type', self::TYPE_REGISTRATION_NEED_MEMBERSHIP);
    }

    public function scopeIsNewPasswordCgu(Builder $query): Builder
    {
        return $query->where('type', self::TYPE_NEW_PASSWORD_CGU);
    }

    public function scopeIsMembershipRenewToken(Builder $query): Builder
    {
        return $query->where('type', self::TYPE_MEMBERSHIP_RENEW);
    }
}
