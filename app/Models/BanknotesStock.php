<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Kblais\Uuid\Uuid;

class BanknotesStock extends Model
{
    use Uuid;

    protected $table = 'banknotes_stocks';

    protected $fillable = [
        'stock',
        'counter_id',
    ];

    // Relations #######################################################################################################

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function counter()
    {
        return $this->belongsTo(Counter::class);
    }
}
