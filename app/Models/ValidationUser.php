<?php

namespace App\Models;

use App\Services\Cyclos\UserService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Kblais\QueryFilter\Filterable;
use Kblais\Uuid\Uuid;

class ValidationUser extends Model
{
    use Uuid;
    use Filterable;

    // Email status
    public const NO_EMAIL = 'no-email';

    public const SPAM = 'spam';

    public const NOT_OPENED = 'not-opened';

    public const OPENED = 'opened';

    public const CLICKED = 'clicked';

    protected $table = 'validation_users';

    protected $fillable = [
        'cyclos_id',
        'local_user_id',
        'group',
        'email',
        'email_status',
    ];

    protected $attributes = ['email_status' => self::NO_EMAIL];

    public static function getStatusForResendEmail(): array
    {
        return [
            self::NO_EMAIL,
            self::SPAM,
            self::NOT_OPENED,
            self::OPENED,
        ];
    }

    public function localUser(): BelongsTo
    {
        return $this->belongsTo(LocalUser::class, 'local_user_id');
    }

    public function getEmailStatusClassAttribute(): string
    {
        return match ($this->email_status) {
            self::SPAM => 'u-orange',
            self::NOT_OPENED, self::OPENED => 'u-green',
            self::CLICKED => 'u-blue',
            default => 'u-red',
        };
    }

    public function getEmailStatusLabelAttribute(): string
    {
        return match ($this->email_status) {
            self::SPAM => 'Email en Spam',
            self::NOT_OPENED => 'Email non ouvert',
            self::OPENED => 'Email ouvert, lien non cliqué',
            self::CLICKED => 'Email ouvert, lien cliqué',
            default => 'Email non trouvé',
        };
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getCyclosUserAttribute(): CyclosUser
    {
        return CyclosUser::fill(app(UserService::class)->get($this->cyclos_id));
    }
}
