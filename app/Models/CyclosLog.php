<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CyclosLog extends Model
{
    protected $table = 'cyclos_logs';

    protected $fillable = [
        'auth',
        'method',
        'url',
        'query_strings',
        'body',
        'status',
        'response',
    ];
}
