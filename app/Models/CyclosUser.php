<?php

namespace App\Models;

use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class CyclosUser
{
    public const STATUS_ACTIVE = 'active';

    public const STATUS_BLOCKED = 'blocked';

    public const STATUS_DISABLED = 'disabled';

    public const STATUS_PENDING = 'pending';

    public const STATUS_REMOVED = 'removed';

    public const STATUS_PURGED = 'purged';

    public ?array $address;

    public Collection $customValues;

    public string $email;

    public string $group;

    public string $id;

    public ?string $name;

    public ?string $phone;

    public ?string $profile;

    public ?string $profileName;

    public ?string $comment;

    public ?string $lastLogin;

    public Carbon $registerDate;

    public string $emailStatusClass = '';

    public string $emailStatusLabel = '';

    public function __construct(array $data = [])
    {
        if (! empty($data)) {
            $this->id = data_get($data, 'id');
            $this->name = data_get($data, 'name');
            $this->profileName = data_get($data, 'image.name');
            $this->group = data_get($data, 'group.internalName');
            $this->email = data_get($data, 'email');
            $this->address = data_get($data, 'addresses.0');
            $this->phone = data_get($data, 'phones.0.number');
            $this->profile = data_get($data, 'image.url');
            $this->customValues = $this->prepareCustomValues(collect(data_get($data, 'customValues', [])));
            $this->lastLogin = data_get($data, 'lastLogin');
            $this->registerDate = Carbon::parse(data_get($data, 'activationDate'));
            $this->comment = data_get($data, 'comment');
        }
    }

    private function prepareCustomValues(Collection $customValuesCollection): Collection
    {
        return $customValuesCollection->keyBy('field.internalName')
            ->map(function ($customValue) {
                $fieldType = data_get($customValue, 'field.type');
                if ($fieldType === 'multiSelection') {
                    return data_get($customValue, 'enumeratedValues');
                }
                if ($fieldType === 'singleSelection') {
                    return data_get($customValue, 'enumeratedValues.0');
                }
                if ($fieldType === 'date') {
                    return substr(data_get($customValue, 'dateValue'), 0, 10);
                }
                if (in_array($fieldType, ['url', 'text'])) {
                    $fieldType = 'string';
                }

                return data_get($customValue, $fieldType . 'Value');
            });
    }

    public static function fill(array $data = []): self
    {
        return new self($data);
    }

    public function getFavorites(): array
    {
        $favorites = data_get($this->customValues, 'favorites');
        if ($favorites) {
            return explode(',', $favorites);
        }

        return [];
    }

    public function hasAutoCreditInProgress(): bool
    {
        return app(Sepa::class)
                ->where('user_id', $this->id)
                ->where('stopped', false)
                ->where('canceled', false)
                ->whereNotNull('duration_month')->count() > 0;
    }

    public function getAutoCredits(): \Illuminate\Database\Eloquent\Collection
    {
        return app(Sepa::class)
            ->where('user_id', $this->id)
            ->whereNotNull('duration_month')->get();
    }

    public function getAddressFull(): ?string
    {
        return $this->address
            ? data_get($this->address, 'addressLine1') . ' ' . data_get($this->address, 'zip') . ' '
            . data_get($this->address, 'city')
            : null;
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'profileName' => $this->profileName,
            'group' => $this->group,
            'email' => $this->email,
            'address' => $this->address,
            'phone' => $this->phone,
            'profile' => $this->profile,
            'customValues' => $this->customValues,
            'lastLogin' => $this->lastLogin,
            'registerDate' => $this->registerDate,
            'comment' => $this->comment,
        ];
    }
}
