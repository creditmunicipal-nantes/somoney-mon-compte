<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class SepaExport implements WithMultipleSheets
{
    use Exportable;

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets['Crédits automatique'] = new SepaRecurrentExport();
        $sheets['Crédits simple'] = new SepaSimpleExport();

        return $sheets;
    }
}
