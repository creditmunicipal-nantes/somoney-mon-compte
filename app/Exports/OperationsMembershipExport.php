<?php

namespace App\Exports;

use App\Models\Transaction;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class OperationsMembershipExport implements FromCollection, WithMapping, WithHeadings
{
    use Exportable;

    protected Collection $cyclosUsers;

    public function __construct()
    {
        $this->cyclosUsers = app(UserService::class)->searchAll([])->groupBy('id');
    }

    public function collection(): Collection
    {
        return Transaction::where('status', Transaction::STATUS_SUCCESS)
            ->where(function ($query) {
                $query->where('context', Transaction::CONTEXT_MEMBERSHIP);
                $query->orWhere(function ($query) {
                    $query->where('context', Transaction::CONTEXT_REGISTRATION)
                        ->where('transaction_type', Transaction::REGISTER_TYPE_ADHERE)
                        ->orWhere('transaction_type', Transaction::REGISTER_TYPE_DONATE);
                });
            })
            ->oldest()
            ->get();
    }

    /**
     * @param \App\Models\Transaction $transactionData
     *
     * @return array
     */
    public function map($transactionData): array
    {
        $user = optional($this->cyclosUsers->get($transactionData->cyclos_user_id))->first() ?? [];

        return [
            $transactionData->id,
            $transactionData->cyclos_user_id ? '#' . $transactionData->cyclos_user_id : '',
            data_get($user, 'name'),
            number_format($transactionData->amount / 100, 2, ',', ' '),
            $transactionData->created_at->format('d/m/Y H:i'),
            $transactionData->transaction_type_label
        ];
    }

    public function headings(): array
    {
        return ['ID Transaction', 'ID Utilisateur', 'Nom Utilisateur', 'Montant', 'Date de paiement', 'Type'];
    }
}
