<?php

namespace App\Exports;

use App\Models\CyclosUser;
use App\Models\Sepa;
use App\Services\Cyclos\UserService;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class SepaSimpleExport implements FromCollection, WithMapping, WithHeadings, WithColumnFormatting, WithTitle
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Sepa::whereNull('duration_month')->get();
    }

    /**
     * @param mixed $transaction
     *
     * @return array
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function map($transaction): array
    {
        $user = CyclosUser::fill(app(UserService::class)->get($transaction->user_id));

        return [
            $transaction->id,//            'ID',//A
            $transaction->user_id, //            'ID User',//B
            $transaction->amount,//            'Montant',//C
            $transaction->gender,//            'Civilité',//D
            $transaction->last_name,//            'Nom',//E
            $transaction->first_name,//            'Prénom',//F
            $transaction->email,//            'Email',//G
            $transaction->company,//            'Raison sociale',//H
            $transaction->iban,//            'IBAN',//I
            __('users.' . $user->group),//            'Type Utilisateur',//J
            $transaction->status_label,//            'Statut',//K
        ];
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'ID',//A
            'ID User',//B
            'Montant',//C
            'Civilité',//D
            'Nom',//E
            'Prénom',//F
            'Email',//G
            'Raison sociale',//H
            'IBAN',//I
            'Type Utilisateur',//J
            'Statut',//K
        ];
    }

    /**
     * @return array
     */
    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_TEXT,
            'B' => NumberFormat::FORMAT_TEXT,
            'C' => NumberFormat::FORMAT_NUMBER,
            'D' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_TEXT,
            'F' => NumberFormat::FORMAT_TEXT,
            'G' => NumberFormat::FORMAT_TEXT,
            'H' => NumberFormat::FORMAT_TEXT,
            'I' => NumberFormat::FORMAT_TEXT,
            'J' => NumberFormat::FORMAT_TEXT,
            'K' => NumberFormat::FORMAT_TEXT,
        ];
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Crédits simple';
    }
}
