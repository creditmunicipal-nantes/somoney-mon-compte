<?php

namespace App\Exports;

use App\Services\Cyclos\AccountService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class UserOperationsExport implements FromCollection, WithMapping, WithHeadings
{
    use Exportable;

    public function __construct(protected AccountService $accountService)
    {
    }

    /**
     * @return \Illuminate\Support\Collection
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function collection(): Collection
    {
        return $this->accountService->fullHistory(
            session('auth.user.id'),
            session('auth.user.account.type.internalName'),
            []
        );
    }

    /**
     * @param array $transactionData
     *
     * @return array
     */
    public function map($transactionData): array
    {
        return [
            Carbon::parse(Arr::get($transactionData, 'date'))->toDateTimeString(),
            Arr::get($transactionData, 'type.name'),
            Arr::get($transactionData, 'relatedAccount.kind') === 'system'
                ? Arr::get($transactionData, 'relatedAccount.type.name')
                : Arr::get($transactionData, 'relatedAccount.user.display'),
            Arr::get($transactionData, 'amount'),
        ];
    }

    public function headings(): array
    {
        return ['Date', 'Description', 'Débiteur / Créditeur', 'Montant'];
    }
}
