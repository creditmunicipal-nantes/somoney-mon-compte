<?php

namespace App\Exports;

use App\Models\Sdg;
use App\Models\SdgProfessional;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Illuminate\Support\Collection;

class SdgsExport implements FromCollection, WithMapping, WithHeadings, WithColumnFormatting
{
    use Exportable;

    public function collection(): Collection
    {
        return Sdg::with('professionals')->where('active', true)->get()->reduce(
            fn(Collection $collection, Sdg $sdg) => $collection->merge($sdg->professionals_ended),
            collect()
        );
    }

    public function map($row): array
    {
        /** @var SdgProfessional $sdgProfessional */
        $sdgProfessional = $row;
        $sdgProfessional->load('sdg');
        $sdgProfessional->append('cyclos_user');

        return [
            config('sdg.categories.id_raw')[$sdgProfessional->sdg->category_id]['label'],
            $sdgProfessional->sdg->title,
            $sdgProfessional->cyclos_user_id,
            $sdgProfessional->cyclos_user['name'],
            $sdgProfessional->created_at->format('d/m/Y H:i'),
            $sdgProfessional->ended_at->format('d/m/Y H:i'),
        ];
    }

    public function headings(): array
    {
        return [
            'CATEGORIE',// A
            'OBJECTIF',// B
            'ID_USER',// C
            'NAME_USER',// D
            'STARTED_AT',// E
            'ENDED_AT',// F
        ];
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_TEXT,
            'E' => NumberFormat::FORMAT_DATE_DATETIME,
            'F' => NumberFormat::FORMAT_DATE_DATETIME,
        ];
    }
}
