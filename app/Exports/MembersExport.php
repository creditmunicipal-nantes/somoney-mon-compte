<?php

namespace App\Exports;

use App\Exceptions\QrCodeTokenNotFoundException;
use App\Models\CyclosUser;
use App\Services\Cyclos\AccountService;
use App\Services\Cyclos\AdService;
use App\Services\Cyclos\UserService;
use App\Services\QrCodeService;
use Illuminate\Support\Carbon;
use Exception;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class MembersExport implements FromCollection, WithMapping, WithHeadings, WithColumnFormatting
{
    use Exportable;

    public function __construct(
        protected UserService $userService,
        protected AccountService $accountService,
        protected AdService $adService
    ) {
    }

    public function collection()
    {
        return $this->userService->searchAll([
            'groups' => implode(',', array_values(config('cyclos.internal-names.groups'))),
        ]);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function map($userData): array
    {
        $userId = data_get($userData, 'id');
        $account = $this->userService->getUserAccount($userId);
        $user = CyclosUser::fill($this->userService->get($userId));
        //$accountType = data_get($account, 'type.internalName');
        //$operations = $this->accountService->fullHistory($userId, $accountType);
        /*$paginatedAdsList = $this->adService->list([
            'owner' => $userId,
        ]);
        $totalCredit = 0;
        if ($operations->count()) {
            $operations->each(function ($operation, $key) use (&$totalCredit) {
                $amount = (float) data_get($operation, 'amount');
                $kind = data_get($operation, 'transaction.kind');
                $relatedaccount = data_get($operation, 'relatedAccount.kind');
                if ($amount > 0 && $kind === 'import' && $relatedaccount === 'system') {
                    $totalCredit += $amount;
                }
            });
        }*/

        $birthday = $this->getBirthday($user);
        try {
            $token = config('somoney.available.services.qr_code')
                ? data_get((new QrCodeService())->getCyclosUserQrCodeToken($userId), 'value')
                : null;
        } catch (QrCodeTokenNotFoundException) {
            $token = null;
        }

        return array_merge([
            (string) $userId,
            data_get($userData, 'name'),
            data_get($userData, 'group.internalName') === 'individual' ? 'Particulier' : 'Professionnel',
            data_get($user->address, 'addressLine1'),
            data_get($user->address, 'zip'),
            data_get($user->address, 'city'),
            data_get($user->address, 'location.latitude'),
            data_get($user->address, 'location.longitude'),
            data_get($user->customValues, 'catpro.0.value'),
            $user->registerDate->format('d/m/Y'),
            $user->lastLogin ? Carbon::parse($user->lastLogin)->format('d/m/Y') : '',
            $birthday,
            data_get($userData, 'customValues.gender') === 'man' ? 'M' : 'F',
            data_get($userData, 'customValues.optinnewsletter') === 'true' ? 'Oui' : 'Non',
            (float) data_get($account, 'status.balance'),
            data_get($userData, 'email'),
            data_get($userData, 'phone'),

            //data_get($item, 'customValues.proname'),
            //data_get($item, 'firstname'),
            //$paginatedAdsList->total(),
            //data_get($item, 'customValues.validatedemail') === 'true' ? 'Valide' : 'Nouveau',
            //(string) $operations->count(),
            //(string) number_format($totalCredit),
        ], config('somoney.available.services.qr_code') ? [$token] : []);
    }

    public function headings(): array
    {
        return array_merge([
            'IDUSER',
            'NOM',
            'GROUP',
            'ADDRESS',
            'ZIPCODE',
            'CITY',
            'LATITUDE',
            'LONGITUDE',
            'CATEGORY',
            'REGISTERDATE',
            'LASTLOGINDATE',
            'BIRTHDATE',
            'GENDER',
            'OPTINMAIL',
            'BALANCE',
            'MAIL',
            'PHONE',
        ], config('somoney.available.services.qr_code') ? ['TOKEN'] : []);
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_NUMBER,
            'J' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'K' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'L' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'O' => NumberFormat::FORMAT_NUMBER_00,
        ];
    }

    protected function getBirthday(CyclosUser $user): string
    {
        $birthday = 'Aucune date';
        if ($user->customValues->get('birthday')) {
            try {
                $birthday = Carbon::parse($user->customValues->get('birthday'))->format('d/m/Y');
            } catch (Exception) {
                $birthday = 'Format date invalide.';
            }
        }

        return $birthday;
    }
}
