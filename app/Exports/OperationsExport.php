<?php

namespace App\Exports;

use App\Services\Cyclos\TransferService;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class OperationsExport implements FromCollection, WithMapping, WithHeadings, WithColumnFormatting
{
    use Exportable;

    public function __construct(protected TransferService $transferService)
    {
        //
    }

    /** @return \Illuminate\Support\Collection|mixed */
    public function collection()
    {
        return $this->transferService->listAll();
    }

    /**
     * @param array $transaction
     *
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function map($transaction): array
    {
        $transactionData = $this->transferService->get($transaction['id']);
        if (data_get($transaction, 'from.kind') === 'system') {
            $fromUser = data_get($transaction, 'from.type.name');
            $fromType = 'Système';
        } else {
            $fromUser = data_get($transaction, 'from.user.display');
            $fromType =
                data_get($transaction, 'from.type.internalName') === 'individual' ? 'Particulier' : 'Professionnel';
        }
        if (data_get($transaction, 'to.kind') === 'system') {
            $toUser = data_get($transaction, 'to.type.name');
            $toType = 'Système';
        } else {
            $toUser = data_get($transaction, 'to.user.display');
            $toType = data_get($transaction, 'to.type.internalName') === 'individual' ? 'Particulier' : 'Professionnel';
        }

        return [
            data_get($transaction, 'id'),
            preg_replace('/[[:^print:]]/', "", data_get($transactionData, 'transaction.description')),
            data_get($transaction, 'amount'),
            data_get($transaction, 'from.id'),
            $fromUser,
            $fromType,
            data_get($transaction, 'to.id'),
            $toUser,
            $toType,
            Carbon::parse(data_get($transaction, 'date'))->format('d/m/Y H:i'),
            //data_get($item, 'type.name'),
            //data_get($item, 'currency.symbol'),
        ];
    }

    public function headings(): array
    {
        return [
            'IDTRANSFERT',
            'LBTRANSFERT',
            'MONTANTTRANSFERT',
            'FROMIDUSER',
            'FROMUSER',
            'FROMCATUSER',
            'TOIDUSER',
            'TONAMEUSER',
            'TOCATUSER',
            'DATETIME',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'A' => NumberFormat::FORMAT_NUMBER,
            'C' => NumberFormat::FORMAT_NUMBER_00,
            'D' => NumberFormat::FORMAT_NUMBER,
            'G' => NumberFormat::FORMAT_NUMBER,
            'L' => NumberFormat::FORMAT_DATE_DATETIME,
        ];
    }
}
