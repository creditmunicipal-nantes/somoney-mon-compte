<?php

namespace App\Exports;

use App\Models\BanknotesTransaction;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class CountersOperationsExport implements FromCollection, WithMapping, WithHeadings, WithColumnFormatting
{
    use Exportable;

    public function collection(): Collection
    {
        return BanknotesTransaction::all();
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($row): array
    {
        /** @var BanknotesTransaction $transaction */
        $transaction = $row;
        $transaction->load('counter');

        $operatorUser = $transaction->operator_user;
        $cyclosUser = $transaction->cyclos_user;

        return [
            $transaction->id,
            $transaction->cyclos_id,
            $transaction->txt_type,
            $operatorUser->id,
            $operatorUser->name,
            $transaction->counter->id,
            $transaction->counter->name,
            $transaction->amount,
            $cyclosUser->id,
            $cyclosUser->name,
            $transaction->created_at->format('d/m/Y H:i'),
        ];
    }

    public function headings(): array
    {
        return [
            'IDTRANSFERT',
            'IDCYCLOSTRANSFERT',
            'TYPETRANSFERT',
            'IDOPERATOR',
            'NAMEOPERATOR',
            'IDCOUNTER',
            'NAMECOUNTER',
            'AMOUNT',
            'IDUSER',
            'NAMEUSER',
            'DATETIME',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_NUMBER,
            'D' => NumberFormat::FORMAT_NUMBER,
            'H' => NumberFormat::FORMAT_NUMBER_00,
            'I' => NumberFormat::FORMAT_NUMBER,
            'K' => NumberFormat::FORMAT_DATE_DATETIME,

        ];
    }
}
