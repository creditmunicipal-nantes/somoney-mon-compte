<?php

namespace App\Exports;

use App\Models\Membership;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class MembershipsExport implements FromCollection, WithMapping, WithHeadings, WithColumnFormatting
{
    use Exportable;

    protected Collection $cyclosUsers;

    public function __construct()
    {
        $this->cyclosUsers = app(UserService::class)->searchAll([])->groupBy('id');
    }

    public function collection()
    {
        return Membership::latest()->get()->unique('cyclos_user_id');
    }

    /**
     * @param \App\Models\Membership $membershipData
     *
     * @return array
     */
    public function map($membershipData): array
    {
        $user = optional($this->cyclosUsers->get($membershipData->cyclos_user_id))->first() ?? [];

        return [
            '#' . $membershipData->cyclos_user_id,
            data_get($user, 'name'),
            $membershipData->succeed_at ? 'Valide' : 'Expiré',
        ];
    }

    public function headings(): array
    {
        return ['ID Utilisateur', 'Nom', 'Valide / Expiré'];
    }

    public function columnFormats(): array
    {
        return ['A' => NumberFormat::FORMAT_TEXT];
    }
}
