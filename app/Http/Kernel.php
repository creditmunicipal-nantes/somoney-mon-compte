<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    protected $middleware = [
        // \App\Http\Middleware\TrustHosts::class,
        \App\Http\Middleware\TrustProxies::class,
        \Illuminate\Http\Middleware\HandleCors::class,
        \Illuminate\Foundation\Http\Middleware\PreventRequestsDuringMaintenance::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\HttpsProtocol::class,
        \App\Http\Middleware\SentryContext::class,
        \Spatie\HttpLogger\Middlewares\HttpLogger::class,
    ];

    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \App\Http\Middleware\IeFix::class,
        ],

        'api' => [
            'throttle:api',
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],
    ];

    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'cache.headers' => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'password.confirm' => \Illuminate\Auth\Middleware\RequirePassword::class,
        'signed' => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'verified' => \Illuminate\Auth\Middleware\EnsureEmailIsVerified::class,
        // Custom
        'isLock' => \App\Http\Middleware\IsLock::class,
        'onlyMembers' => \App\Http\Middleware\OnlyMembers::class,
        'restrictToAdmin' => \App\Http\Middleware\RestrictToAdmin::class,
        'restrictToProfessional' => \App\Http\Middleware\RestrictToProfessional::class,
        'restrictToProfessionalAndAdmin' => \App\Http\Middleware\RestrictToProfessionalAndAdmin::class,
        'professionalRegisterRedirect' => \App\Http\Middleware\ProfessionalRegisterRedirect::class,
        'individualRegisterRedirect' => \App\Http\Middleware\IndividualRegisterRedirect::class,
        'cyclosAuth' => \App\Http\Middleware\CyclosApiAuthorize::class,
        'individualNeedIdDocument' => \App\Http\Middleware\IndividualNeedIdDocument::class,
        'cors' => \App\Http\Middleware\Cors::class,
        'serviceAvailable' => \App\Http\Middleware\ServiceAvailable::class,
        'registerAvailable' => \App\Http\Middleware\RegisterAvailable::class,
        'dataStudioAuth' => \App\Http\Middleware\DataStudioApiAuthorize::class,
        'sentryContext' => \App\Http\Middleware\SentryContext::class,
        'isOperatorOfCounter' => \App\Http\Middleware\IsOperatorOfCounter::class,
        'individualMembershipRegisterRedirect' => \App\Http\Middleware\IndividualMembershipRegisterRedirect::class,
        'professionalMembershipRegisterRedirect' => \App\Http\Middleware\ProfessionalMembershipRegisterRedirect::class,
    ];
}
