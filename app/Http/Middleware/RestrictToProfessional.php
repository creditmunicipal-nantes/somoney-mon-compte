<?php

namespace App\Http\Middleware;

use App\Models\CyclosUser;
use Closure;
use Illuminate\Http\Request;

class RestrictToProfessional
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $cyclosUser = CyclosUser::fill(session()->get('auth.user'));
        if ($cyclosUser->group !== config('cyclos.internal-names.groups.professional')) {
            $route = session('auth.user.is_admin') ? 'admin.home' : 'home';

            return redirect()->route($route)
                ->with('error', 'Seul les professionnels peuvent accéder à cette partie.');
        }

        return $next($request);
    }
}
