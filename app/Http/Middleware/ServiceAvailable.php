<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ServiceAvailable
 *
 * @package App\Http\Middleware
 */
class ServiceAvailable
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string $serviceSlug
     *
     * @return mixed
     */
    public function handle($request, Closure $next, string $serviceSlug)
    {
        if (! config('somoney.available.services.' . $serviceSlug)) {
            abort(Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
