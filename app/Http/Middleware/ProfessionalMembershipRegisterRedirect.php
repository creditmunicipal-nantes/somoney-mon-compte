<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use App\Models\Transaction;
use Closure;

class ProfessionalMembershipRegisterRedirect
{
    public function handle(Request $request, Closure $next): mixed
    {
        $membershipTransaction = Transaction::where('id', session('registration.parent_transaction_id'))
            ->where('transaction_type', Transaction::REGISTER_TYPE_ADHERE)
            ->exists();
        if (! $membershipTransaction && $request->route()->getName() === 'register.professional.credit.donate') {
            return redirect()->route('register.professional.credit.membership');
        }
        if ($request->route()->getName() === 'register.professional.credit.payment') {
            $donateTransaction = Transaction::where('parent_id', session('registration.parent_transaction_id'))
                ->where('transaction_type', Transaction::REGISTER_TYPE_DONATE)
                ->exists();
            if (config('registration.available.professional.donate') && ! $donateTransaction) {
                return redirect()->route('register.professional.credit.donate');
            }
            if (! $membershipTransaction) {
                return redirect()->route('register.professional.credit.membership');
            }
        }

        return $next($request);
    }
}
