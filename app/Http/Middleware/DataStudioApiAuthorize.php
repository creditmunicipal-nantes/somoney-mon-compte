<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class DataStudioApiAuthorize
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function handle(Request $request, Closure $next)
    {
        if (config('services.google.data_studio.api.key') !== $request->header('Authorization', '1')) {
            throw new AuthorizationException();
        }

        return $next($request);
    }
}
