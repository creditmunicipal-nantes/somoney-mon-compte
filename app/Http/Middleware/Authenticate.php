<?php

namespace App\Http\Middleware;

use App\Services\Cyclos\UserService;
use Closure;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Sentry\State\Scope;

use function Sentry\configureScope;

class Authenticate
{
    public function __construct(protected UserService $userService)
    {
        //
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if (! session()->has('auth')) {
            throw new AuthenticationException();
        }
        try {
            $user = $this->userService->get(data_get(session()->get('auth.user'), 'id'));
            $user['account'] = $this->userService->getUserAccount($user['id']);
            $user['is_admin'] = Str::startsWith(
                session()->get('auth.user.group.internalName'),
                config('cyclos.internal-names.admin')
            );
            session()->put('auth.user', $user);
            if (app()->bound('sentry')) {
                configureScope(function (Scope $scope) use ($user): void {
                    $scope->setUser([
                        'id' => $user['id'],
                        'email' => $user['email'],
                        'name' => $user['name'],
                    ]);
                });
            }
        } catch (AuthenticationException | Exception $exception) {
            session()->forget('auth');
            throw $exception;
        }

        return $next($request);
    }
}
