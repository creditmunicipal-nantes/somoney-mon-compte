<?php

namespace App\Http\Middleware;

use App\Models\LocalUser;
use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

abstract class RegisterTunnelRedirect
{
    protected function registerRedirect(
        Request $request,
        Closure $next,
        string $stepConfigPath,
        string $firstStepRoute
    ): mixed {
        if (session('registration')) {
            $steps = config($stepConfigPath . 'step_route');
            if (
                session('registration.currentStep')
                && array_key_exists(session('registration.currentStep'), config($stepConfigPath . 'by_step'))
                && array_search(Route::current()->getName(), $steps)
            ) {
                $currentStep = config($stepConfigPath . 'by_step')[session('registration.currentStep')];
                $currentRouteIndex = array_search(Route::current()->getName(), $steps);
                $currentRoute = config($stepConfigPath . 'by_step')[$currentRouteIndex];
                if ($currentRoute && $currentRoute['step'] > $currentStep['step']) {
                    return redirect()->route($currentStep['route']);
                }
            }

            $redirect = $this->getRedirectToHome($firstStepRoute, true, $this->getLocalUser());
        } else {
            $redirect = $this->getRedirectToHome($firstStepRoute, false, $this->getLocalUser());
        }

        return $redirect ?? $next($request);
    }

    protected function getLocalUser(): ?LocalUser
    {
        return session('registration.user_id')
            ? LocalUser::find(session('registration.user_id'))
            : null;
    }

    protected function getRedirectToHome(
        string $firstStepRoute,
        bool $withUser = false,
        ?LocalUser $localUser = null
    ): ?RedirectResponse {
        if ($withUser) {
            if ($localUser === null && Route::current()->getName() !== $firstStepRoute) {
                return redirect()->route($firstStepRoute);
            }
        } elseif (Route::current()->getName() !== $firstStepRoute) {
            return redirect()->route($firstStepRoute);
        }

        return null;
    }
}
