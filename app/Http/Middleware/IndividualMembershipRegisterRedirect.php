<?php

namespace App\Http\Middleware;

use App\Models\Transaction;
use Closure;
use Illuminate\Http\Request;

class IndividualMembershipRegisterRedirect
{
    public function handle(Request $request, Closure $next): mixed
    {
        $membershipTransaction = Transaction::where('id', session('registration.parent_transaction_id'))
            ->where('transaction_type', Transaction::REGISTER_TYPE_ADHERE)
            ->exists();
        if (! $membershipTransaction && $request->route()->getName() === 'register.individual.credit.donate') {
            return redirect()->route('register.individual.credit.membership');
        }
        if ($request->route()->getName() === 'register.individual.credit.payment') {
            $donateTransaction = Transaction::where('parent_id', session('registration.parent_transaction_id'))
                ->where('transaction_type', Transaction::REGISTER_TYPE_DONATE)
                ->exists();
            if (config('registration.available.individual.donate') && ! $donateTransaction) {
                return redirect()->route('register.individual.credit.donate');
            }
            if (! $membershipTransaction) {
                return redirect()->route('register.individual.credit.membership');
            }
        }

        return $next($request);
    }
}
