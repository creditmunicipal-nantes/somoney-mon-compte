<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RegisterElementAvailable
 *
 * @package App\Http\Middleware
 */
class RegisterAvailable
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @param string $elementSlug
     *
     * @return mixed
     */
    public function handle($request, Closure $next, string $elementSlug)
    {
        if (! config('registration.available.' . $elementSlug)) {
            abort(Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
