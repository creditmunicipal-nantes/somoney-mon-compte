<?php

namespace App\Http\Middleware;

use App\Models\CyclosUser;
use Closure;
use Illuminate\Http\Request;

class IsLock
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $cyclosUser = CyclosUser::fill(session()->get('auth.user'));
        $lock = $cyclosUser->customValues->get('accountlock');

        if ($lock) {
            return redirect()->route('home')
                ->with('success', 'Votre compte est bloqué,
                 veuillez contacter un administrateur ' . config('somoney.money') . ' pour le réactiver.');
        }

        return $next($request);
    }
}
