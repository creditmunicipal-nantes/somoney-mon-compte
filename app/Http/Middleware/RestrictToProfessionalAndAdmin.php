<?php

namespace App\Http\Middleware;

use App\Models\CyclosUser;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RestrictToProfessionalAndAdmin
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $cyclosUser = CyclosUser::fill(session()->get('auth.user'));
        if (
            $cyclosUser->group !== config('cyclos.internal-names.groups.professional')
            && ! Str::startsWith(
                session()->get('auth.user.group.internalName'),
                config('cyclos.internal-names.admin')
            )
        ) {
            return redirect()->route('home')
                ->with('error', 'Seul les professionnels ou les administrateurs peuvent accéder à cette partie.');
        }

        return $next($request);
    }
}
