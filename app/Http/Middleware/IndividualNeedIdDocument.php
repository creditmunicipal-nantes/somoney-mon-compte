<?php

namespace App\Http\Middleware;

use App\Models\CyclosUser;
use Closure;

class IndividualNeedIdDocument
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session('auth.user')) {
            $cyclosUser = CyclosUser::fill(session('auth.user'));
            if ($cyclosUser->group === config('somoney.cyclos-internal-names.groups.individual')) {
                if (! data_get($cyclosUser->customValues, 'idattachment')) {
                    session()->flash('notification.need_id_document', true);

                    return redirect()->route('account.information');
                }
            }
        }

        return $next($request);
    }
}
