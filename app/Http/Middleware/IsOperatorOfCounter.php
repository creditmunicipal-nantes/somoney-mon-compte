<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class IsOperatorOfCounter
{
    public function handle(Request $request, Closure $next): mixed
    {
        /** @var \App\Models\Counter $counter */
        $counter = $request->route('counter');
        if (
            ! Str::startsWith(session('auth.user.group.internalName'), config('cyclos.internal-names.admin'))
            && ! in_array(session('auth.user.id'), $counter->operators)
        ) {
            throw new HttpException(Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
