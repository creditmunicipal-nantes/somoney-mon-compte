<?php

namespace App\Http\Middleware;

use Closure;

class ProfessionalRegisterRedirect extends RegisterTunnelRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $stepConfigPath = 'registration.steps.professional.';
        $firstStepRoute = 'register.professional.informations.account';

        return $this->registerRedirect($request, $next, $stepConfigPath, $firstStepRoute);
    }
}
