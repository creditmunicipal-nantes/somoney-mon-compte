<?php

namespace App\Http\Filters;

use Kblais\QueryFilter\QueryFilter;

class CityFilter extends QueryFilter
{
    /**
     * @param string $zipcode
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function zipcode(string $zipcode)
    {
        return $this->builder->where('zipcode', $zipcode);
    }

    /**
     * @param string $name
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function name(string $name)
    {
        return $this->builder->where('name', 'ilike', '%' . $name . '%');
    }
}
