<?php

namespace App\Http\Controllers\Admin;

use App\Exports\SdgsExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Sdgs\SdgsIndexRequest;
use App\Http\Requests\Admin\Sdgs\SdgsProfessionalsRequest;
use App\Http\Requests\Admin\Sdgs\SdgStoreOrUpdateRequest;
use App\Models\Sdg;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Maatwebsite\Excel\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Barryvdh\DomPDF\Facade\Pdf;

class SdgsController extends Controller
{
    public function index(SdgsIndexRequest $request): View
    {
        $title = 'Objectifs DD - ' . config('somoney.money');
        $categorySelected = $request->categorie;

        $sdgs = null;
        if ($request->categorie) {
            $sdgs = Sdg::where('category_id', config('sdg.categories.slug_raw.' . $request->categorie)['id'])
                ->get();
        }

        return view('main.templates.admin.sdg.index', compact('title', 'sdgs', 'categorySelected'));
    }

    public function exportPdf(): Response
    {
        $sdgCategories = Sdg::with('professionals')->get()->groupBy('category_id');

        setlocale(LC_TIME, 'fr_FR.UTF-8');
        $filename = Str::slug(config('somoney.money')) . '-ODD-professionnels-' . today()->format('d-m-Y');

        $pdf = PDF::loadView('pdf.sdgs.admin', compact('sdgCategories'));

        return $pdf->download($filename . '.pdf');
    }

    public function exportXls(): BinaryFileResponse
    {
        setlocale(LC_TIME, 'fr_FR.UTF-8');
        $filename = Str::slug(config('somoney.money')) . '-ODD-professionnels-' . today()->format('d-m-Y');

        app(Excel::class)->store(new SdgsExport(), 'exports/' . $filename . '.xls');

        return response()->download(storage_path('app/exports/' . $filename . '.xls'))
            ->deleteFileAfterSend(true);
    }

    public function professionals(Sdg $sdg, SdgsProfessionalsRequest $request): View
    {
        $title = 'Objectifs DD - ' . $sdg->title . ' - ' . config('somoney.money');
        $professionals = $sdg->professionals;

        if ($request->status) {
            if ($request->status === 'ended') {
                $professionals = $professionals->filter(fn($professional) => ! is_null($professional->ended_at));
            } elseif ($request->status === 'ongoing') {
                $professionals = $professionals->filter(fn($professional) => is_null($professional->ended_at));
            }
        }

        return view('main.templates.admin.sdg.professionals', compact('title', 'sdg', 'professionals'));
    }

    public function create(string $sdgCategory): View
    {
        $sdg = null;
        $sdgCategory = config('sdg.categories.slug_raw.' . $sdgCategory);

        return view('main.templates.admin.sdg.edit', compact('sdg', 'sdgCategory'));
    }

    public function store(string $sdgCategory, SdgStoreOrUpdateRequest $request): RedirectResponse
    {
        $sdg = Sdg::create(array_merge(
            $request->validated(),
            [
                'category_id' => data_get(config('sdg.categories.slug_raw.' . $sdgCategory), 'id'),
            ]
        ));

        return redirect()
            ->route('admin.sdgs.index', ['categorie' => $sdgCategory])
            ->with('success', 'L\'objectif "' . $sdg->title . '" a été créé.');
    }

    public function edit(Sdg $sdg): View
    {
        $sdgCategory = config('sdg.categories.id_raw.' . $sdg->category_id);

        return view('main.templates.admin.sdg.edit', compact('sdg', 'sdgCategory'));
    }

    public function update(Sdg $sdg, SdgStoreOrUpdateRequest $request): RedirectResponse
    {
        $sdg->update($request->validated());

        $sdgCategory = config('sdg.categories.id_raw.' . $sdg->category_id);

        return redirect()->route('admin.sdgs.index', ['categorie' => $sdgCategory['slug']])
            ->with('success', 'L\'objectif "' . $sdg->title . '" a été mis-à-jour.');
    }

    public function destroy(Sdg $sdg): RedirectResponse
    {
        $name = $sdg->title;
        $sdgCategory = config('sdg.categories.id_raw.' . $sdg->category_id);
        $sdg->delete();

        return redirect()->route('admin.sdgs.index', ['categorie' => $sdgCategory['slug']])
            ->with('success', 'L\'objectif "' . $name . '" a été supprimé.');
    }
}
