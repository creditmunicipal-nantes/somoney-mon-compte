<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\Cyclos\ValidationSentryException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\NewUsers\EmailResendRequest;
use App\Jobs\LoadEmailStatus;
use App\Jobs\Memberships\SendEmailNewCreated;
use App\Jobs\NewUsers\SendEmailProfessionalUserValidation;
use App\Jobs\SendEmailValidationCreation;
use App\Models\CyclosUser;
use App\Models\LocalUser;
use App\Models\Membership;
use App\Models\ValidationUser;
use App\Services\Cyclos\UserService;
use App\Services\LocalUserService;
use Illuminate\Support\Facades\Bus;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class NewUsersController extends Controller
{
    public const FORCE_MEMBERSHIPS = 'membership';

    public const FORCE_SIMPLE = 'simple';

    public function __construct(protected UserService $userService)
    {
        //
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function index(string $userType, Request $request): View
    {
        $localUsers = null;
        $userType = $userType === 'professionnel' ? 'professional' : 'individual';
        $users = null;
        if ($userType === 'professional') {
            $params = ['page' => $request->get('page'), 'pageSize' => 40];
            $users = $this->getCyclosUsers($userType, $params);
        }
        if (config('somoney.available.services.email-status')) {
            if ($userType === 'professional') {
                $users = collect($users->items())->map(function (array $user) {
                    $validationUser = ValidationUser::where('cyclos_id', $user['id'])->first();
                    if (! $validationUser) {
                        $validationUser = ValidationUser::create([
                            'cyclos_id' => $user['id'],
                            'group' => $user['group']['internalName'],
                            'email' => $user['email'],
                        ]);
                    }
                    $user['emailStatusClass'] = $validationUser->email_status_class;
                    $user['emailStatusLabel'] = $validationUser->email_status_label;

                    return $user;
                });
                $users = new Paginator($users->toArray(), 40, $request->get('page'));
            }
            $localUsers = LocalUser::where('group', config('cyclos.internal-names.groups.' . $userType))
                ->get([
                    'id',
                    'cyclos_id',
                    'name',
                    'email',
                    'step',
                    'group',
                    'updated_at',
                ]);
        }

        return view('main.templates.admin.users.new', compact('users', 'userType', 'localUsers'));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function getCyclosUsers(string $userType, array $params = []): LengthAwarePaginator
    {
        $filters = [
            'page' => data_get('page', $params, 0),
            'pgeSize' => data_get('pageSize', $params, 9999),
            'profileFields' => [
                'needsValidation' => 'true',
            ],
            'groups' => config('cyclos.internal-names.groups.' . $userType),
        ];

        return $this->userService->search($filters);
    }

    public function show(LocalUser $localUser): View
    {
        return view('main.templates.admin.users.show', [
            'user' => $localUser,
            'isLocalUser' => true,
        ]);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function emailRefresh(string $userType): RedirectResponse
    {
        if (! config('services.mailgun.secret')) {
            return redirect()
                ->back()
                ->with('error', 'Mailgun n\'est pas configuré pour permettre de rafraichir le statut des emails.');
        }
        $userType = $userType === 'professionnel' ? 'professional' : 'individual';
        $users = $this->getCyclosUsers($userType);
        $this->dispatchSync(new LoadEmailStatus(
            collect(Arr::pluck($users->items(), 'id')),
            LocalUser::where('group', config('cyclos.internal-names.groups.' . $userType))->get()
        ));

        return back();
    }

    public function emailResend(LocalUser $localUser, EmailResendRequest $request): RedirectResponse
    {
        $localUser->email = $request->email;
        $localUser->save();
        $this->dispatch(new SendEmailValidationCreation($localUser->id));
        session()->flash(
            'email-resend-succeed',
            "L'e-mail de validation a été renvoyé à <b>{$localUser->name}</b> (<i>{$localUser->email}</i>)"
        );

        return back();
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function valid(string $userId): RedirectResponse
    {
        $dataForEdit = $this->userService->getDataForEdit($userId)['user'];
        $dataForEdit['customValues']['needsValidation'] = false;
        $this->userService->update($userId, $dataForEdit);
        $user = CyclosUser::fill($this->userService->get($userId));
        $this->dispatch(new SendEmailProfessionalUserValidation($user));

        return back();
    }

    public function destroy(LocalUser $localUser, string $confirm = null): RedirectResponse
    {
        if ($confirm && $confirm === session('registration-delete-token')) {
            $localUser->delete();
            ValidationUser::where('local_user_id', $localUser->id)->delete();
            session()->flash(
                'registration-deleted',
                "L'inscription de <b>{$localUser->name}</b> a été supprimé."
            );
            session()->forget('registration-delete-token');
        } else {
            session()->put('registration-delete-token', Str::random(64));
            session()->flash('registration-delete', $localUser);
        }

        return back();
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \App\Exceptions\FailCreateUserException
     * @throws \ErrorException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    public function forceValidation(string $userType, LocalUser $localUser, string $type): RedirectResponse
    {
        $userType = $userType === 'professionnel' ? 'professional' : 'individual';
        if (
            $userType !== 'individual'
            || ! config('somoney.available.services.membership')
            || $localUser->step !== config('registration.steps.individual.slug_id.credit')
        ) {
            $localUser->setCustomValue('validatedemail', true);
            $localUser->setCustomValue('isactive', true);
            $localUser->step = config("registration.steps.$userType.slug_id.validation");
            $localUser->save();
        }
        try {
            $localUser = app(LocalUserService::class)->createCyclosUser($localUser);
            if (config('somoney.available.services.membership')) {
                Membership::create([
                    'cyclos_user_id' => $localUser->cyclos_id,
                    'succeed_at' => $type === self::FORCE_MEMBERSHIPS ? now() : null,
                ]);

                if ($type === self::FORCE_SIMPLE) {
                    Bus::dispatch(new SendEmailNewCreated($localUser->cyclos_id));
                }
            }
            session()->flash(
                'user-validated',
                'Le compte ' . __('validation.attributes.' . $userType) . ' de <b>' . $localUser->name .
                '</b> a été validé.'
            );
        } catch (ValidationSentryException $exception) {
            $errors = array_reduce(
                data_get(json_decode(
                    $exception->getMessage(),
                    true,
                    512,
                    JSON_THROW_ON_ERROR
                ), 'propertyErrors', []),
                static fn(string $errors, array $messages) => $errors . implode('<br/>', $messages) . '<br/>',
                ''
            );
            session()->flash(
                'user-error',
                'L\'inscription de  ' . __('validation.attributes.' . $userType) . ' de <b>' . $localUser->name
                . '</b> n\'est pas possible.<hr/>' . $errors . '<hr/><br/>'
            );
        }

        return back();
    }
}
