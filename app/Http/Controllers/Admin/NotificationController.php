<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Notification\StoreNotificationRequest;
use App\Models\Notification;

class NotificationController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $notifications = Notification::latest('updated_at')->get();

        return view('main.templates.admin.notification.index', compact('notifications'));
    }

    /**
     * @param \App\Http\Requests\Notification\StoreNotificationRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreNotificationRequest $request)
    {
        Notification::create([
            'message' => $request->message,
            'groups' => $request->groups,
            'display' => true,
        ]);

        return redirect()->route('admin.notification.index');
    }

    /**
     * @param string $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(string $id)
    {
        Notification::where('id', $id)->delete();

        return redirect()->route('admin.notification.index');
    }
}
