<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserUpdateRequest;
use App\Jobs\LoadEmailStatus;
use App\Jobs\Users\SendEmailValidation;
use App\Models\CyclosUser;
use App\Models\UserToken;
use App\Models\ValidationUser;
use App\Services\Cyclos\AccountService;
use App\Services\Cyclos\User\AddressService;
use App\Services\Cyclos\User\ImageService;
use App\Services\Cyclos\User\PhoneService;
use App\Services\Cyclos\UserService;
use App\Services\QrCodeService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class UserController extends Controller
{
    protected UserService $userService;

    public function __construct()
    {
        $this->userService = app(UserService::class);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\View\View
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function index(Request $request): View
    {
        $params = [
            'page' => $request->get('page'),
            'pageSize' => $request->get('pageSize', 40),
        ];

        if (in_array($request->get('status'), ['accountlock', 'leavecall'])) {
            $params['profileFields'][$request->get('status')] = 'true';
        }

        $filters = array_replace_recursive(
            $request->get('filters', [
                'groups' => implode(',', config('cyclos.internal-names.groups')),
            ]),
            $params
        );

        $users = $this->userService->search($filters);

        return view('main.templates.admin.users.index', compact('users', 'filters'));
    }

    public function membersExport(): BinaryFileResponse
    {
        return response()->download(storage_path('app/exports/members.xls'));
    }

    /**
     * @param string $userId
     *
     * @return \Illuminate\View\View
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \App\Exceptions\QrCodeTokenNotFoundException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function show(string $userId): View
    {
        $accountService = app(AccountService::class);
        $user = CyclosUser::fill($this->userService->get($userId));
        if (config('somoney.available.services.email-status')) {
            $validationUser = ValidationUser::where('cyclos_id', $user->id)->first();
            if (! $validationUser) {
                $validationUser = new ValidationUser([
                    'email' => $user->email,
                    'group' => $user->group,
                ]);
            }
            $user->emailStatusClass = $validationUser->email_status_class;
            $user->emailStatusLabel = $validationUser->email_status_label;
        }

        $dataForEdit = $this->userService->getDataForEdit($userId);
        $balance = data_get($accountService->getUsersAccountsBalance(
            $user->group,
            [
                'usersToInclude' => $userId,
                'fields' => 'balance',
            ]
        )->items(), '0.balance', '0.00');

        $genders = data_get(Arr::first(
            $dataForEdit['customFields'],
            fn($value) => $value['internalName'] === 'gender'
        ), 'possibleValues');

        $professionalCategories = data_get(Arr::first(
            $dataForEdit['customFields'],
            fn($value) => $value['internalName'] === 'catpro'
        ), 'possibleValues');
        $qrCodeImage = config('somoney.available.services.qr_code')
            ? app(QrCodeService::class)->getCyclosUserQrCodeTokenImage($user)
            : null;
        $qrCodePoster = config('somoney.available.services.qr_code')
            ? app(QrCodeService::class)->getCyclosUserQrCodeTokenPoster($user)
            : null;

        return view(
            'main.templates.admin.users.show',
            compact('user', 'genders', 'balance', 'professionalCategories', 'qrCodeImage', 'qrCodePoster')
        );
    }

    /**
     * @param string $userId
     *
     * @return \Illuminate\View\View
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function edit(string $userId): View
    {
        $user = CyclosUser::fill($this->userService->get($userId));
        $dataForEdit = $this->userService->getDataForEdit($userId);
        $genders = data_get(Arr::first(
            $dataForEdit['customFields'],
            fn($value) => $value['internalName'] === 'gender'
        ), 'possibleValues');

        $professionalCategories = data_get(Arr::first(
            $dataForEdit['customFields'],
            fn($value) => $value['internalName'] === 'catpro'
        ), 'possibleValues');

        $organisationAttachmentTypes = data_get(Arr::first(
            $dataForEdit['customFields'],
            fn($value) => $value['internalName'] === 'organisationattachmenttype'
        ), 'possibleValues');

        return view(
            'main.templates.admin.users.edit',
            compact('user', 'genders', 'professionalCategories', 'organisationAttachmentTypes')
        );
    }

    /**
     * @param \App\Http\Requests\Admin\UserUpdateRequest $updateRequest
     * @param \App\Services\Cyclos\User\PhoneService $phoneService
     * @param \App\Services\Cyclos\User\ImageService $imageService
     * @param string $userId
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \App\Exceptions\QrCodeTokenNotFoundException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function update(
        UserUpdateRequest $updateRequest,
        PhoneService $phoneService,
        ImageService $imageService,
        string $userId
    ): RedirectResponse {
        $user = CyclosUser::fill($this->userService->get($userId));
        $userId = $user->id;
        $requestUser = $updateRequest->get('user');
        $customValues = $requestUser['customValues'];
        $requestUser['customValues']['birthday'] = $updateRequest->birthday->toDateString();
        $requestUser['customValues']['commercialinfo'] = $customValues['commercialinfo'] ? 'true' : 'false';
        $requestUser['customValues']['optinnewsletter'] = $customValues['optinnewsletter'] ? 'true' : 'false';
        if ($user->group === 'professional') {
            $requestUser['customValues']['visible'] = $customValues['visible'] ? 'true' : 'false';
        } else {
            $requestUser['name'] = $customValues['firstname'] . ' ' . $customValues['lastname'];
        }
        if ($updateRequest->hasFile('user.profile')) {
            $imageService->setUserImage($userId, $updateRequest->file('user.profile'));
        }
        $phoneService->setUserPhone($userId, $updateRequest->phone);
        $this->setAddress($user, $updateRequest);
        $requestUser = $this->setFiles($userId, $updateRequest, $requestUser);
        $this->userService->setUserData($userId, $requestUser);
        if (config('somoney.available.services.qr_code')) {
            // Remove qr code images
            File::delete(app(QrCodeService::class)->getCyclosUserQrCodeTokenImage($user, false));
            File::delete(app(QrCodeService::class)->getCyclosUserQrCodeTokenPoster($user, false));
        }
        if (session('newEmail')) {
            $token = UserToken::create([
                'user_id' => $userId,
                'type' => UserToken::TYPE_EMAIL_VALIDATION,
            ]);
            $this->dispatch(new SendEmailValidation($requestUser['email'], $user, $token));
        }

        return redirect()->route('admin.users.edit', ['id' => $userId])
            ->with('success', 'Les modifications ont été enregistré avec succès');
    }

    public function emailStatusRefresh(string $userId): RedirectResponse
    {
        $this->dispatchSync(new LoadEmailStatus(collect([$userId]), null));

        return redirect()->back();
    }

    /**
     * @param string $userId
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function delete(string $userId): RedirectResponse
    {
        $user = $this->userService->getDataForEdit($userId)['user'];
        $user['customValues']['leavecall'] = "true";
        $this->userService->update($userId, $user);

        return redirect()
            ->back()
            ->with('success', 'Vous avez lancé la procédure pour supprimer ce compte.');
    }

    protected function storeRequestFile(Request $request, string $userId, string $fileName): ?string
    {
        if (! $request->hasFile($fileName)) {
            return null;
        }
        $file = $request->file($fileName);
        $fileExtension = $file->getClientOriginalExtension();
        $path = $file->storePubliclyAs('legal_documents/' . $userId, $fileName . '.' . $fileExtension);

        return url($path);
    }

    /**
     * @param \App\Models\CyclosUser $user
     * @param \App\Http\Requests\Admin\UserUpdateRequest $updateRequest
     *
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function setAddress(CyclosUser $user, UserUpdateRequest $updateRequest): void
    {
        $manualAddress = [
            'name' => 'address',
            'addressLine1' => $updateRequest->addressLine1,
            'zip' => $updateRequest->zip,
            'city' => $updateRequest->city,
            'addressLine2' => $updateRequest->get('addressLine2', ''),
        ];
        $gmapAddress = [];

        if ($updateRequest->gmap) {
            $gmapAddress = [
                'name' => 'address',
                'addressLine1' => data_get($updateRequest->gmap, 'addressLine1'),
                'zip' => data_get($updateRequest->gmap, 'zip'),
                'city' => data_get($updateRequest->gmap, 'city'),
                'location' => Arr::only($updateRequest->gmap, ['latitude', 'longitude']),
            ];
        }

        if (! $gmapAddress['addressLine1']) {
            $newAddress = array_merge($gmapAddress, array_filter($manualAddress));
        } else {
            $newAddress = array_merge($manualAddress, array_filter($gmapAddress));
        }
        $elemOfAddressChanged = (bool) count(
            array_diff_assoc(
                Arr::only($newAddress, ['addressLine1', 'zip', 'city']),
                Arr::only($user->address, ['addressLine1', 'zip', 'city'])
            )
        );
        $locationChanged = (bool) count(
            array_diff_assoc(
                data_get($newAddress, 'location'),
                data_get($user->address, 'location')
            )
        );
        if ($elemOfAddressChanged || $locationChanged) {
            app(AddressService::class)->setUserAddress($user->id, $newAddress);
        }
    }

    protected function setFiles(string $userId, UserUpdateRequest $updateRequest, array $requestUser): array
    {
        if ($updateRequest->hasFile('idattachment')) {
            $requestUser['customValues']['idattachment'] = $this->storeRequestFile(
                $updateRequest,
                $userId,
                'idattachment'
            );
        }

        if ($updateRequest->hasFile('organisationattachment')) {
            $requestUser['customValues']['organisationattachment'] = $this->storeRequestFile(
                $updateRequest,
                $userId,
                'organisationattachment'
            );
        }

        if ($updateRequest->hasFile('ribattachment')) {
            $requestUser['customValues']['ribattachment'] = $this->storeRequestFile(
                $updateRequest,
                $userId,
                'ribattachment'
            );
        }

        return $requestUser;
    }
}
