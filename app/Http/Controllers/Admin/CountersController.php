<?php

namespace App\Http\Controllers\Admin;

use App\Exports\CountersOperationsExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Counter\StoreCounterRequest;
use App\Http\Requests\Admin\Counter\UpdateCounterRequest;
use App\Models\BanknotesTransaction;
use App\Models\Counter;
use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\Support\Facades\Request;

class CountersController extends Controller
{
    public function index(): View
    {
        $counters = Counter::with('transactions')->get();
        $cities = $counters->pluck('city')->unique();
        $transactions = $counters->reduce(function (Collection $transactions, Counter $counter) {
            if (
                $counter->city === request('city', $counter->city)
                && $counter->id === request('counter', $counter->id)
            ) {
                return $transactions
                    ->merge($counter->transactions->filter(
                        function (BanknotesTransaction $transaction) {
                            $begin = Carbon::createFromFormat(
                                'd/m/Y',
                                request('begin', $transaction->created_at->format('d/m/Y'))
                            )->startOfDay();
                            $end = Carbon::createFromFormat(
                                'd/m/Y',
                                request('end', $transaction->created_at->format('d/m/Y'))
                            )->endOfDay();

                            return $transaction->created_at->gte($begin) && $transaction->created_at->lte($end);
                        }
                    ));
            }

            return $transactions;
        }, new Collection());
        $transactions->load('counter');

        $total = $transactions->reduce(
            fn(float $total, BanknotesTransaction $transaction) => $total + ($transaction->type === 'out'
                    ? -$transaction->amount
                    : $transaction->amount),
            0
        );
        $transactions = paginateCollection($transactions, request('pageSize', 1));

        return view(
            'main.templates.admin.counter.index',
            array_merge(compact('counters', 'cities', 'transactions', 'total'), Request::all())
        );
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function export(): BinaryFileResponse
    {
        app(Excel::class)->store(new CountersOperationsExport(), 'exports/counters-operations.xls');

        return response()->download(storage_path('app/exports/counters-operations.xls'));
    }

    public function create(): View
    {
        $counter = new Counter();

        return view('main.templates.admin.counter.edit', compact('counter'));
    }

    public function store(StoreCounterRequest $request): RedirectResponse
    {
        Counter::create($request->except('_token'));

        return redirect()->route('admin.counters');
    }

    public function edit(Counter $counter): View
    {
        $counter->load('transactions');

        return view('main.templates.admin.counter.edit', compact('counter'));
    }

    public function update(Counter $counter, UpdateCounterRequest $request): RedirectResponse
    {
        $counter->update($request->except('_token'));

        return redirect()->route('admin.counters');
    }

    /** @throws \Exception */
    public function destroy(Counter $counter): RedirectResponse
    {
        $counter->delete();

        return redirect()->route('admin.counters');
    }
}
