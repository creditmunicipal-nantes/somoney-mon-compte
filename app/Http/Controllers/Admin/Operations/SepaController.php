<?php

namespace App\Http\Controllers\Admin\Operations;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SepaStartDateUpdateRequest;
use App\Jobs\SepaTransfer;
use App\Models\Sepa;
use App\Services\Cyclos\UserService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Carbon;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class SepaController extends Controller
{
    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function sepas(): View
    {
        $users = collect(app(UserService::class)->search(['pageSize' => 100000])->items())->reduce(function (
            array $users,
            array $user
        ) {
            $users[$user['id']] = $user;

            return $users;
        }, []);
        $sepasAwaiting = app(Sepa::class)
            ->where('done', false)
            ->where('canceled', false)
            ->where('confirmed', true)
            ->oldest('updated_at')
            ->get();
        $sepasDone = app(Sepa::class)
            ->where('done', true)
            ->where('stopped', false)
            ->oldest('updated_at')
            ->get();
        $sepasActive = collect([]);
        $sepasDone = $sepasDone->filter(function (Sepa $sepa) use (&$sepasActive) {
            if ($sepa->is_recurrent) {
                $sepasActive->push($sepa);

                return false;
            }

            return true;
        });
        $sepasCanceled = app(Sepa::class)
            ->where('canceled', true)
            ->oldest('updated_at')
            ->get();
        $sepasStopped = app(Sepa::class)
            ->where('stopped', true)
            ->oldest('updated_at')
            ->get();

        return view(
            'main.templates.admin.operations.sepas',
            compact('sepasAwaiting', 'sepasDone', 'sepasActive', 'sepasStopped', 'sepasCanceled', 'users')
        );
    }

    public function updateStartDate(Sepa $sepa, SepaStartDateUpdateRequest $request): RedirectResponse
    {
        $sepa->update(['start_date' => $request->start_date]);

        return redirect()->route('admin.operations.sepas')
            ->with('success', 'Le date du premier prélèvement a été modifié !');
    }

    public function done(Sepa $sepa): RedirectResponse
    {
        $sepa->update([
            'done' => true,
            'start_date' => Carbon::parse($sepa->start_date_txt),
        ]);

        $this->dispatch((new SepaTransfer($sepa->id))->onConnection('sync'));

        return redirect()->route('admin.operations.sepas');
    }

    public function active(Sepa $sepa): RedirectResponse
    {
        $sepa->update([
            'done' => true,
            'start_date' => Carbon::parse($sepa->start_date_txt),
        ]);

        return redirect()->route('admin.operations.sepas')
            ->with('success', 'Le crédit automatique a bien été activé !');
    }

    public function canceled(Sepa $sepa): RedirectResponse
    {
        $sepa->update(['canceled' => true]);

        return redirect()->route('admin.operations.sepas');
    }

    public function stopped(Sepa $sepa): RedirectResponse
    {
        $sepa->update(['stopped' => true]);

        return redirect()->route('admin.operations.sepas');
    }

    public function exportXls(): BinaryFileResponse
    {
        return response()->download(storage_path('app/exports/sepa.xls'));
    }
}
