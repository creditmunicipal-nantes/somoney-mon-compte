<?php

namespace App\Http\Controllers\Admin\Operations;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ManuelCreditStoreRequest;
use App\Models\CyclosUser;
use App\Services\Cyclos\TransactionService;
use App\Services\Cyclos\UserService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ManualCreditController extends Controller
{
    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function index(): View
    {
        $user = $maxAmount = null;
        if (request()->has('user')) {
            $user = app(UserService::class)->get(request()->user);
            $account = app(UserService::class)->getUserAccount($user['id']);
            $maxAmount = $account['status']['upperCreditLimit'] - $account['status']['balance'];
        }

        return view(
            'main.templates.admin.operations.user-credit',
            compact('user', 'maxAmount')
        );
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function store(string $userId, ManuelCreditStoreRequest $request): RedirectResponse
    {
        $user = CyclosUser::fill(app(UserService::class)->get($userId));
        $type = 'stableAccount.toIndividual';
        if ($user->group === 'professional') {
            $type = 'stableAccount.toProfessional';
        }

        app(TransactionService::class)->storeInitialAcompte(
            $userId,
            $request->amount,
            'Credit Euros (manuel) - ' . config('somoney.money'),
            $type
        );

        $money = config('somoney.money');

        session()->flash(
            'admin-user-credit-succeed',
            "Vous avez crédité manuellement <br/>
            <b>{$user->name}</b> de <b>{$request->amount}</b> $money<hr/>"
        );

        return redirect()->route('admin.operations.user-credit.index');
    }
}
