<?php

namespace App\Http\Controllers\Admin\Operations;

use App\Http\Controllers\Controller;
use App\Services\Cyclos\AccountService;
use App\Services\Cyclos\TransferService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class HistoricController extends Controller
{
    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function index(Request $request, TransferService $transferService, AccountService $accountService): View
    {
        $filters = array_replace_recursive(
            $request->get('filters', []),
            [
                'page' => $request->get('page'),
                'pageSize' => $request->get('pageSize', 12),
            ]
        );
        $operations = $transferService->list($filters);
        $months = array_map(function ($month) {
            $date = now()->day(1)->subMonths($month);

            return [
                'date' => $date,
                'interval' => $date->format('Y-m-d') . ',' . $date->addMonth()->subDay()->format('Y-m-d'),
            ];
        }, range(0, 11));

        $equation = $accountService->getEquation();

        return view(
            'main.templates.admin.operations.historic',
            compact('operations', 'filters', 'equation', 'months')
        );
    }

    public function export(): BinaryFileResponse
    {
        return response()->download(storage_path('app/exports/operations.xls'));
    }
}
