<?php

namespace App\Http\Controllers\Admin\Operations;

use App\Exceptions\Cyclos\ValidationSentryException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ReconversionStoreRequest;
use App\Models\CyclosUser;
use App\Services\Cyclos\TransactionService;
use App\Services\Cyclos\UserService;
use Coderello\SharedData\Facades\SharedData;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ReconversionController extends Controller
{
    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function index(): View
    {
        $user = $maxAmount = null;
        if (request()->has('user')) {
            $user = app(UserService::class)->get(request()->user);
            $maxAmount = app(UserService::class)->getUserAccount(request()->user)['status']['balance'];
        }

        SharedData::put([
            'conversionFeeRate' => config('somoney.conversion-fee-rate'),
        ]);

        return view('main.templates.admin.operations.reconversion-user', compact('user', 'maxAmount'));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function store(string $user, ReconversionStoreRequest $request): RedirectResponse
    {
        $money = config('somoney.money');
        try {
            $user = CyclosUser::fill(app(UserService::class)->get($user));
            $conversionFee = $request->amount * config('somoney.conversion-fee-rate');
            $finalAmount = $request->amount - $conversionFee;
            app(TransactionService::class)->storeUserReconversion($user->id, $request->amount, $user->group);

            session()->flash(
                'reconversion-user-succeed',
                "Vous avez reconverti <br/>
                <b>{$user->name}</b> de <b>{$request->amount}</b> $money <br>
                 <b>$conversionFee</b> $money de frais reconversion ont étés appliqués </br>
                Seront donc rendus <b>$finalAmount</b> $money
                <hr>"
            );
        } catch (ValidationSentryException $exception) {
            $errorMessage = json_decode($exception->getMessage(), true);
            if (
                array_key_exists('propertyErrors', $errorMessage)
                && array_key_exists('amount', $errorMessage['propertyErrors'])
            ) {
                session()->flash(
                    'reconversion-user-failed',
                    $errorMessage['propertyErrors']['amount'][0]
                );
            } else {
                throw $exception;
            }
        }

        return redirect()->route('admin.operations.reconversion-user.index', ['user' => $user->id]);
    }
}
