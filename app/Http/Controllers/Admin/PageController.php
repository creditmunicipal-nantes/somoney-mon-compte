<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DashboardRequest;
use App\Services\Cyclos\TransactionService;
use Illuminate\Support\Carbon;
use Illuminate\View\View;

class PageController extends Controller
{
    public function home(DashboardRequest $request): View
    {
        $transactionService = app(TransactionService::class);
        $historic = $transactionService->listAsAdmin([
            'pageSize' => 25,
        ])
            ->filter(fn(array $transaction) => $transaction['from']['kind'] === 'system'
                || $transaction['to']['kind'] === 'system')
            ->take(25);

        $cachedAt = Carbon::parse(data_get(cache()->get('admin_dashboard'), 'cached_at', now()->subDays(2)));
        if ($cachedAt->lt(now()->subDay()) || ! empty($request->all())) {
            $dateBegin = Carbon::parse($request->get('transactions_date_begin', today()));
            $dateEnd = Carbon::parse($request->get('transactions_date_end', today()));
            $allTransactions = $transactionService->allAsAdmin([
                'datePeriod' => $dateBegin->toDateString() . ',' . $dateEnd->toDateString(),
            ]);

            $totalAmounts = ['CtoC' => 0, 'CtoB' => 0, 'BtoB' => 0, 'BtoC' => 0];
            $nbTransactions = ['CtoC' => 0, 'CtoB' => 0, 'BtoB' => 0, 'BtoC' => 0];
            $creditAmounts = ['individual' => 0, 'professional' => 0];
            $reconversionAmounts = ['individual' => 0, 'professional' => 0];
            if ($request->user_type !== 'professional') {
                $stableToIndividual = $transactionService->filterFromTo(
                    $allTransactions,
                    'stableAccount',
                    'individual'
                );
                $creditAmounts['individual'] = $transactionService->sumOfTransactions($stableToIndividual);
                $individualToStable = $transactionService->filterFromTo(
                    $allTransactions,
                    'individual',
                    'stableAccount'
                );
                $reconversionAmounts['individual'] = $transactionService->sumOfTransactions($individualToStable);
                $cToC = $transactionService->filterFromTo($allTransactions, 'individual', 'individual');
                $nbTransactions['CtoC'] = $cToC->count();
                $totalAmounts['CtoC'] = $transactionService->sumOfTransactions($cToC);
                $cToB = $transactionService->filterFromTo($allTransactions, 'individual', 'professional');
                $nbTransactions['CtoB'] = $cToB->count();
                $totalAmounts['CtoB'] = $transactionService->sumOfTransactions($cToB);
            }
            if ($request->user_type !== 'individual') {
                $stableToProfessional =
                    $transactionService->filterFromTo($allTransactions, 'stableAccount', 'professional');
                $creditAmounts['professional'] = $transactionService->sumOfTransactions($stableToProfessional);
                $professionalToStable =
                    $transactionService->filterFromTo(
                        $allTransactions,
                        'professional',
                        'stableAccount'
                    );
                $reconversionAmounts['professional'] = $transactionService->sumOfTransactions($professionalToStable);
                $bToB = $transactionService->filterFromTo(
                    $allTransactions,
                    'professional',
                    'professional'
                );
                $nbTransactions['BtoB'] = $bToB->count();
                $totalAmounts['BtoB'] = $transactionService->sumOfTransactions($bToB);
                $bToC = $transactionService->filterFromTo($allTransactions, 'professional', 'individual');
                $nbTransactions['BtoC'] = $bToC->count();
                $totalAmounts['BtoC'] = $transactionService->sumOfTransactions($bToC);
            }

            cache()->put('admin_dashboard', array_merge(
                ['cached_at' => now()],
                compact(
                    'historic',
                    'dateBegin',
                    'dateEnd',
                    'totalAmounts',
                    'nbTransactions',
                    'creditAmounts',
                    'reconversionAmounts',
                )
            ));
        }

        return view(
            'main.templates.admin.dashboard',
            cache()->get('admin_dashboard')
        );
    }
}
