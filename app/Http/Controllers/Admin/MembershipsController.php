<?php

namespace App\Http\Controllers\Admin;

use App\Exports\MembershipsExport;
use App\Exports\OperationsMembershipExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Membership\MembershipsIndexRequest;
use App\Http\Requests\Membership\MembershipsOperationsRequest;
use App\Models\CyclosUser;
use App\Models\Membership;
use App\Models\Transaction;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Maatwebsite\Excel\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class MembershipsController extends Controller
{
    public function index(MembershipsIndexRequest $request): View
    {
        /** @var \Illuminate\Database\Eloquent\Collection $memberships */
        $memberships = Membership::latest()->get()->unique('cyclos_user_id');
        $isLastPage = true;
        if ($request->get('status', 'all') !== 'all') {
            $memberships = $memberships->filter(
                fn(Membership $membership) => ($request->status === 'valid' && $membership->succeed_at !== null)
                    || ($request->status === 'expired' && $membership->succeed_at === null)
            );
        }
        if ($memberships->isNotEmpty()) {
            $users = app(UserService::class)->searchAll([
                'groups' => implode(',', array_values(config('cyclos.internal-names.groups'))),
                'statuses' => [
                    'active',
                ],
            ]);
            // keep only user needed
            $users = $users->whereIn('id', $memberships->pluck('cyclos_user_id'));
            $isLastPage = $users->skip(40 * ($request->get('page', 1) - 1))->count() <= 40;
            // paginate users
            $users = $users->slice(40 * ($request->get('page', 1) - 1), 40);
        } else {
            $users = [];
        }
        $usersMemberships = $memberships->keyBy('cyclos_user_id');

        return view('main.templates.admin.memberships.index', compact('users', 'usersMemberships', 'isLastPage'));
    }

    public function operations(MembershipsOperationsRequest $request): View
    {
        $transactionsQuery = Transaction::where(function ($query) use ($request) {
            if ($request->type === Transaction::CONTEXT_MEMBERSHIP || $request->get('type', 'all') === 'all') {
                $query->where('context', Transaction::CONTEXT_MEMBERSHIP)
                    ->orWhere('transaction_type', Transaction::REGISTER_TYPE_ADHERE);
            }
            if ($request->type === Transaction::REGISTER_TYPE_DONATE || $request->get('type', 'all') === 'all') {
                $query->orWhere('transaction_type', Transaction::REGISTER_TYPE_DONATE);
            }
        })
            ->where('status', Transaction::STATUS_SUCCESS)
            ->where('amount', '>', 0);
        $users = app(UserService::class)->searchAll([
            'groups' => implode(',', array_values(config('cyclos.internal-names.groups'))),
        ])->groupBy('id');
        $monthOptions = $transactionsQuery->distinct()
            ->get(DB::raw('to_char(created_at,\'MM_YYYY\') as created_at_month'))
            ->reduce(function (array $options, Transaction $transaction) {
                $options[$transaction->getAttribute('created_at_month')] = Carbon::createFromFormat(
                    'm_Y',
                    $transaction->getAttribute('created_at_month')
                )->formatLocalized('%B %Y');

                return $options;
            }, []);

        if ($request->month) {
            $date = Carbon::createFromFormat('m_Y', $request->month);
            $transactionsQuery->whereDate('created_at', '>=', $date->startOfMonth())
                ->whereDate('created_at', '<=', $date->endOfMonth());
        }
        $transactions = $transactionsQuery->latest()->get();
        $total = $transactions->sum(fn(Transaction $transaction) => $transaction->amount);
        $isLastPage = $transactions->skip(40 * ($request->get('page', 1) - 1))->count() <= 40;
        $transactions = $transactions->slice(40 * ($request->get('page', 1) - 1), 40);

        return view(
            'main.templates.admin.memberships.operations',
            compact('transactions', 'total', 'users', 'isLastPage', 'monthOptions')
        );
    }

    public function show(string $userId): View
    {
        $user = CyclosUser::fill(app(UserService::class)->get($userId));
        $memberships = Membership::where('cyclos_user_id', $userId)->get();

        return view('main.templates.admin.memberships.show', compact('user', 'memberships'));
    }

    public function validateMembership(string $membershipId): RedirectResponse
    {
        $membership = Membership::find($membershipId);
        $membership->update(['succeed_at' => now()]);

        return redirect()
            ->route('admin.memberships.show', ['userId' => $membership->cyclos_user_id])
            ->with('success', 'Vous venez de valider cette adhésion!');
    }

    public function exportMemberships(): BinaryFileResponse
    {
        $filename = Str::slug(config('somoney.money')) . '-adhesions-membres-export-' . today()->format('d-m-Y');
        app(Excel::class)->store(new MembershipsExport(), 'exports/' . $filename . '.xls');

        return response()->download(storage_path('app/exports/' . $filename . '.xls'))->deleteFileAfterSend(true);
    }

    public function exportOperations(): BinaryFileResponse
    {
        $filename = Str::slug(config('somoney.money')) . '-adhesions-operations-export-' . today()->format('d-m-Y');
        app(Excel::class)->store(new OperationsMembershipExport(), 'exports/' . $filename . '.xls');

        return response()->download(storage_path('app/exports/' . $filename . '.xls'))->deleteFileAfterSend(true);
    }
}
