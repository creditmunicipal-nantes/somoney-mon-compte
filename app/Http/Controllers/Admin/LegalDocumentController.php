<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Filesystem\FileNotFoundException;

class LegalDocumentController extends Controller
{
    /**
     * @param string $filename
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function show(string $filename = null)
    {
        try {
            return response()->download(storage_path("app/legal_documents/$filename"));
        } catch (FileNotFoundException $exception) {
            return response('', 404);
        }
    }
}
