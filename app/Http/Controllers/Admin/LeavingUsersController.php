<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmailDeleteAccount;
use App\Models\CyclosUser;
use App\Services\Cyclos\AccountService;
use App\Services\Cyclos\UserService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Pagination\Paginator;
use Illuminate\View\View;

class LeavingUsersController extends Controller
{
    public function __construct(protected UserService $userService, protected AccountService $accountService)
    {
    }

    /**
     * @return \Illuminate\View\View
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function index(): View
    {
        $filters = [
            'page' => request('page', 0),
            'pageSize' => 40,
            'profileFields' => [
                'leavecall' => 'true',
            ],
        ];

        $users = $this->userService->search($filters);

        $users = collect($users->items())->map(function (array $user) {
            $balance = $this->accountService->getUserAccounts($user['id']);
            $user['need_reconversion'] = floatval(data_get($balance[0], 'status.balance', 0)) > 0.0;

            return $user;
        });
        $users = new Paginator($users->toArray(), $filters['pageSize'], $filters['page']);

        return view('main.templates.admin.users.leaving', compact('users'));
    }

    /**
     * @param string $userId
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function destroy(string $userId): RedirectResponse
    {
        $user = $this->userService->get($userId);
        $this->userService->setStatus($userId, CyclosUser::STATUS_DISABLED, 'Confirmation du souhait de quitter');

        $this->dispatch(new SendEmailDeleteAccount($user));

        return redirect()
            ->back()
            ->with('success', 'La demande de suppression du compte de ' . $user['name'] . ' a été validé');
    }

    /**
     * @param string $userId
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function cancel(string $userId): RedirectResponse
    {
        $user = $this->userService->getDataForEdit($userId)['user'];
        $user['customValues']['leavecall'] = "false";
        $this->userService->update($userId, $user);

        return redirect()
            ->back()
            ->with('success', 'La demande de suppression du compte de ' . $user['name'] . ' a été annulé');
    }
}
