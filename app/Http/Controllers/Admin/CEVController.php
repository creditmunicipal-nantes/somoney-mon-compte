<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CEVEvent;
use Illuminate\View\View;

class CEVController extends Controller
{
    public function index(): View
    {
        $cevEvents = CEVEvent::latest()->orderByDesc('file_name')->paginate();

        return view('main.templates.admin.cev.index', compact('cevEvents'));
    }
}
