<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Http\Requests\Membership\StoreMembershipRenewalPaymentRequest;
use App\Jobs\LemonWay\InitMoneyInJob;
use App\Models\CyclosUser;
use App\Models\Transaction;
use App\Models\UserToken;
use App\Models\Wallet;
use App\Services\Cyclos\UserService;
use App\Services\SystemPayService;
use ErrorException;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Bus;

class MembershipsRenewController extends Controller
{
    public function index(UserToken $membershipRenewToken): View
    {
        if (session('auth.user.id') !== $membershipRenewToken->user_id) {
            abort(403);
        }
        $userType = CyclosUser::fill(app(UserService::class)->get($membershipRenewToken->user_id))->group;

        return view('main.templates.memberships.index', compact('membershipRenewToken', 'userType'));
    }

    /**
     * @param \App\Models\UserToken $membershipRenewToken
     * @param \App\Http\Requests\Membership\StoreMembershipRenewalPaymentRequest $request
     *
     * @return \Illuminate\Contracts\View\View
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \ErrorException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Lyra\Exceptions\LyraException
     */
    public function payment(UserToken $membershipRenewToken, StoreMembershipRenewalPaymentRequest $request): View
    {
        if (session('auth.user.id') !== $membershipRenewToken->user_id) {
            abort(403);
        }
        switch (config('somoney.payment-gateway')) {
            case Transaction::TYPE_LEMONWAY:
                $wallet = null;
                if (! config('somoney.available.services.lemonway-v2')) {
                    $wallet = Wallet::first();
                }
                $initPayment = Bus::dispatchNow(new InitMoneyInJob(
                    $request->amount * 100,
                    $membershipRenewToken->user_id,
                    $wallet,
                    Transaction::CONTEXT_MEMBERSHIP
                ));
                break;
            case Transaction::TYPE_SYSTEM_PAY:
                $initPayment = app(SystemPayService::class)->createPayment(
                    $request->amount * 100,
                    $membershipRenewToken->user_id,
                    Transaction::CONTEXT_MEMBERSHIP
                );
                break;
            default:
                throw new ErrorException('Payment Gateway not recognized "' . config('somoney.payment-gateway') . '"');
        }
        session()->put('operations.initPaymentResponse', $initPayment);

        return view('main.templates.memberships.payment', compact('initPayment'));
    }
}
