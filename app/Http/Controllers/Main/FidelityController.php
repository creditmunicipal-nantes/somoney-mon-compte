<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Http\Requests\Fidelity\FidelityUpdateRequest;
use App\Models\Fidelity;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class FidelityController extends Controller
{
    public function show(): View
    {
        $fidelity = Fidelity::where('cyclos_user_id', session('auth.user')['id'])->first();

        return view('main.templates.fidelity', compact('fidelity'));
    }

    public function update(FidelityUpdateRequest $request): RedirectResponse
    {
        $isCreated = false;
        $fidelity = Fidelity::where('cyclos_user_id', session('auth.user')['id'])->first();
        if (! $fidelity) {
            $fidelity = new Fidelity();
            $isCreated = true;
        }
        $fidelity->fill($request->all());
        $fidelity->cyclos_user_id = session('auth.user')['id'];
        $fidelity->save();
        $success = 'Règle de fidélité ' . ($isCreated ? ' créée' : 'modifiée') . ' avec succès !';

        return redirect()->route('fidelity.show')->with(compact('success', 'fidelity'));
    }

    /**
     * @param \App\Models\Fidelity $fidelity
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Fidelity $fidelity): RedirectResponse
    {
        $fidelity->delete();
        $success = 'Règle de fidélité supprimée.';

        return back()->with(compact('success'));
    }
}
