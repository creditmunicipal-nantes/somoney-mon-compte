<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\CyclosUser;
use App\Models\Notification;
use App\Services\Cyclos\AccountService;
use Illuminate\View\View;
use voku\helper\HtmlMin;

class PageController extends Controller
{
    /**
     * @param \App\Services\Cyclos\AccountService $accountService
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function home(AccountService $accountService)
    {
        if (session('auth.user.is_admin')) {
            return redirect()->route('admin.home');
        }

        $operations = [];
        $progress = 0;
        $notification = $transactions = null;
        if (in_array(session('auth.user.group.internalName'), config('cyclos.internal-names.groups'))) {
            $transactions = $accountService->history(
                session('auth.user.id'),
                session('auth.user.account.type.internalName')
            );
            $operations = array_map(fn($transaction) => $transaction, $transactions->items());
            $balance = session('auth.user.account.status.balance');
            if ($balance < 0) {
                $progress = ($balance / session('auth.user.account.status.creditLimit')) * 100;
            } else {
                $progress = ($balance / session('auth.user.account.status.upperCreditLimit')) * 100;
            }
            $notification = Notification::where([
                ['groups', session('auth.user.account.type.internalName')],
                ['display', true],
            ])
                ->orWhere([
                    ['groups', Notification::TOALL],
                    ['display', true],
                ])
                ->latest('updated_at')
                ->first();
        }

        return view('main.templates.dashboard', compact('operations', 'progress', 'notification', 'transactions'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register()
    {
        session()->forget('registration');

        return view('main.templates.register');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function favorite()
    {
        return view('main.templates.favorite');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function accountAlert()
    {
        return view('main.templates.account.alert');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function accountWidget()
    {
        $cyclosUser = CyclosUser::fill(session()->get('auth.user'));
        $apiKey = $cyclosUser->customValues->get('apikey', '#VOTREAPIKEY#');
        $directoryBaseUrl = 'http' . (request()->isSecure() ? 's' : '') . '://' . config('somoney.domain.directory');
        $urlJsFile = $directoryBaseUrl . '/assets/js/somoney-annuaire-badge.js';
        $badgeSnippetPretty = <<<EOT
<script src="$urlJsFile"></script>
<a href="$directoryBaseUrl"
   id="SoMoneyBadge"
   data-type="medium"
   data-position="left"
   data-top="400"
   data-width="100"
   data-api-key="$apiKey"></a>
EOT;
        $badgeSnippetMinify = (new HtmlMin())->minify($badgeSnippetPretty);

        return view(
            'main.templates.account.widget',
            compact('apiKey', 'badgeSnippetMinify', 'badgeSnippetPretty', 'directoryBaseUrl', 'urlJsFile')
        );
    }

    public function errorNotFound(): View
    {
        return view('errors.404');
    }
}
