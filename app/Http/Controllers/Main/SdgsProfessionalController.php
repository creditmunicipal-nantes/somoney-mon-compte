<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Http\Requests\Sdgs\SdgProfessionalBeginRequest;
use App\Models\CyclosUser;
use App\Models\Sdg;
use App\Models\SdgProfessional;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Barryvdh\DomPDF\Facade\Pdf;

class SdgsProfessionalController extends Controller
{
    public function index(): View
    {
        $title = 'Catégories des objectifs DD - ' . config('somoney.money');

        $sdgCategoryEmpty = $sdgCategories = [];
        foreach (config('sdg.categories.slug_raw') as $sdgCategory) {
            /** @var \Illuminate\Database\Eloquent\Builder $baseSdgQuery */
            $baseSdgQuery = Sdg::where('category_id', $sdgCategory['id']);
            if (
                $baseSdgQuery->doesntExist()
                || $baseSdgQuery->count() === (clone $baseSdgQuery)
                    ->where('active', false)
                    ->count()
            ) {
                $sdgCategoryEmpty[$sdgCategory['slug']] = $sdgCategory;
                continue;
            }
            $userHasBeginSdg =
                (clone $baseSdgQuery)->where('active', true)->whereHas('professionals', function ($query) {
                    $query->where('cyclos_user_id', session('auth.user.id'));
                })->exists();
            if ($userHasBeginSdg) {
                $sdgCategory['none_begin'] = false;
                $sdgCategories[$sdgCategory['slug']] = $sdgCategory;
            } else {
                $sdgCategory['none_begin'] = true;
                $sdgCategories[$sdgCategory['slug']] = $sdgCategory;
            }
        }

        return view(
            'main.templates.sdg.professional.index',
            compact('title', 'sdgCategories', 'sdgCategoryEmpty')
        );
    }

    public function show(string $sdgCategory): View
    {
        $sdgCategory = config('sdg.categories.slug_raw.' . $sdgCategory);
        $title = 'Objectifs DD ' . $sdgCategory['label'] . '- ' . config('somoney.money');
        $sdgs = Sdg::where('active', true)
            ->where('category_id', $sdgCategory['id'])
            ->whereHas('professionals', function ($query) {
                $query->where('cyclos_user_id', session('auth.user.id'));
            })->get();
        $sdgs->append('connected_professional');

        return view('main.templates.sdg.professional.show', compact('title', 'sdgCategory', 'sdgs'));
    }

    public function exportPdf(): Response
    {
        $user = CyclosUser::fill(session()->get('auth.user'));

        $sdgCategories =
            Sdg::with('professionals')->where('active', true)
                ->whereHas('professionals', function ($query) use ($user) {
                    $query->where('cyclos_user_id', $user->id)
                        ->whereNotNull('ended_at');
                })->get()->groupBy('category_id');

        setlocale(LC_TIME, 'fr_FR.UTF-8');
        $filename = Str::slug(config('somoney.money')) . '-ODD-' . $user->name . '-' . today()->format('d-m-Y');

        $pdf = PDF::loadView('pdf.sdgs.professional', compact('user', 'sdgCategories'));

        return $pdf->download($filename . '.pdf');
    }

    public function begin(): View
    {
        $title = 'Fixer un objectifs DD - ' . config('somoney.money');

        $sdgCategories = array_reduce(
            config('sdg.categories.id_raw'),
            function (array $sdgCategories, array $sdgCategory) {
                $sdgBaseQuery = Sdg::where('active', true)->where('category_id', $sdgCategory['id']);
                $haveActiveSdg = (clone $sdgBaseQuery)->exists();
                $sdgAllNotBegin = (clone $sdgBaseQuery)
                        ->whereHas('professionals', function ($query) {
                            $query->where('cyclos_user_id', session('auth.user.id'));
                        })->count() !== (clone $sdgBaseQuery)->count();
                if ($haveActiveSdg && $sdgAllNotBegin) {
                    $sdgCategories[$sdgCategory['id']] = $sdgCategory;
                }

                return $sdgCategories;
            },
            []
        );

        return view('main.templates.sdg.professional.begin', compact('title', 'sdgCategories'));
    }

    public function store(SdgProfessionalBeginRequest $request): RedirectResponse
    {
        SdgProfessional::create([
            'sdg_id' => $request->sdg_id,
            'cyclos_user_id' => session('auth.user.id'),
        ]);

        $sdg = Sdg::find($request->sdg_id);
        $category = config('sdg.categories.id_raw')[$request->category_id];

        return redirect()
            ->route('sdgs.professional.show', ['sdgCategory' => $category['slug']])
            ->with('success', 'Vous vous êtes fixé l\'objectif "' . $sdg->title . '".');
    }

    public function finish(Sdg $sdg): RedirectResponse
    {
        $sdg->professionals()->where('cyclos_user_id', session('auth.user.id'))->firstOrFail()
            ->update(['ended_at' => now()]);

        return back()->with('success', 'Vous avez atteint l\'objectif "' . $sdg->title . '".');
    }

    public function stop(Sdg $sdg): RedirectResponse
    {
        $sdg->professionals()->where('cyclos_user_id', session('auth.user.id'))->delete();

        return back()->with('success', 'Vous avez abandonné l\'objectif "' . $sdg->title . '".');
    }
}
