<?php

namespace App\Http\Controllers\Main\Register;

use App\Http\Controllers\Controller;
use App\Http\Requests\Registration\Credit\StoreDonateRequest;
use App\Jobs\LemonWay\InitMoneyInJob;
use App\Models\LocalUser;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Services\Cyclos\UserService;
use App\Services\SystemPayService;
use ErrorException;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Bus;

abstract class MembershipsController extends Controller
{
    protected ?string $userType;

    public function __construct(protected UserService $userService)
    {
    }

    public function membership(): View
    {
        return view('main.templates.register.' . $this->userType . '.credit.membership');
    }

    public function donate(): View
    {
        return view('main.templates.register.' . $this->userType . '.credit.donate');
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \ErrorException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Lyra\Exceptions\LyraException
     */
    public function postDonate(StoreDonateRequest $request): RedirectResponse
    {
        $parentTransactionId = session('registration.parent_transaction_id');
        Transaction::create([
            'type' => config('somoney.payment-gateway'),
            'target_wallet_id' => config('somoney.payment-gateway') === Transaction::TYPE_LEMONWAY
            && ! config('somoney.available.services.lemonway-v2')
                ? Wallet::first()?->id
                : null,
            'wk_token' => bin2hex(openssl_random_pseudo_bytes(25)),
            'status' => Transaction::STATUS_PROCESSING,
            'amount' => 100 * (float) str_replace(',', '.', $request->amount),
            'context' => Transaction::CONTEXT_REGISTRATION,
            'transaction_type' => Transaction::REGISTER_TYPE_DONATE,
            'parent_id' => $parentTransactionId,
            'local_user_id' => session('registration.user_id'),
        ]);
        $localUser = LocalUser::find(session('registration.user_id'));
        $membershipTransaction = Transaction::find($parentTransactionId);
        $initPaymentResponse = $this->makeInitPaymentResponse($membershipTransaction, $localUser);
        session()->put('registration.initPaymentResponse', $initPaymentResponse);

        return redirect()->route('register.' . $localUser->group . '.credit.payment');
    }

    public function payment(): View|RedirectResponse
    {
        $initPayment = session()->pull('registration.initPaymentResponse');
        if (! $initPayment) {
            return redirect()
                ->back()
                ->with('error', 'Une erreur d\'identification du paiement est survenue, veuillez réessayer.');
        }

        return view('main.templates.register.' . $this->userType . '.credit.payment', compact('initPayment'));
    }

    public function paid(): View
    {
        return view('main.templates.register.' . $this->userType . '.credit.paid');
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \ErrorException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Lyra\Exceptions\LyraException
     */
    protected function makeInitPaymentResponse(Transaction $membershipTransaction, LocalUser $localUser): string
    {
        $donateTransaction = Transaction::where('parent_id', $membershipTransaction->id)
            ->where('transaction_type', Transaction::REGISTER_TYPE_DONATE)
            ->first();
        switch (config('somoney.payment-gateway')) {
            case Transaction::TYPE_LEMONWAY:
                $lemonwayId = data_get($localUser->customValues, 'lemonwayId');
                $wallet = null;
                if (! config('somoney.available.services.lemonway-v2')) {
                    try {
                        $wallet = Wallet::where('lw_id', $lemonwayId)->firstOrFail();
                    } catch (ModelNotFoundException) {
                        throw new ErrorException(
                            'Local user "' . $localUser->id . '" has no wallet or lemonway Id'
                        );
                    }
                }
                if (! config('somoney.available.services.lemonway-v2')) {
                    $wallet = Wallet::first();
                }

                return Bus::dispatchNow(new InitMoneyInJob(
                    $membershipTransaction->amount + $donateTransaction?->amount,
                    $localUser->id,
                    $wallet,
                    Transaction::CONTEXT_REGISTRATION,
                    0,
                    $membershipTransaction
                ));
            case Transaction::TYPE_SYSTEM_PAY:
                return app(SystemPayService::class)->createPayment(
                    $membershipTransaction->amount + $donateTransaction?->amount,
                    $localUser->id,
                    Transaction::CONTEXT_REGISTRATION,
                    $membershipTransaction
                );
            default:
                throw new ErrorException(
                    'Payment Gateway not recognized "' . config('somoney.payment-gateway') . '"'
                );
        }
    }
}
