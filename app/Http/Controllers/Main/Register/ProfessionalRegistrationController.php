<?php

namespace App\Http\Controllers\Main\Register;

use App\Http\Requests\Registration\Professional\StoreFilesRequest;
use App\Http\Requests\Registration\Professional\StoreSocietyInformationsRequest;
use App\Models\LocalUser;
use App\Models\UserToken;
use App\Services\GeocodingService;
use App\Services\InseeService;
use App\Services\LocalUserService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Proengsoft\JsValidation\Facades\JsValidatorFacade as JsValidator;

class ProfessionalRegistrationController extends RegistrationController
{
    protected string $userType = 'professional';

    protected string $informationStepSlug = 'society';

    /**
     * STEP 2 of the registration - view
     *
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function informations(): View
    {
        $localUser = LocalUser::find(session('registration.user_id'));
        $genders = $this->localUserService->getDataCustomField($this->userType, 'gender');
        $structureTypes = $this->localUserService->getDataCustomField($this->userType, 'employeenumber');
        $validator = JsValidator::formRequest(
            StoreSocietyInformationsRequest::class,
            '#register-society-form'
        )->render();

        return view(
            "main.templates.register.{$this->userType}.informations.society",
            compact('structureTypes', 'genders', 'localUser', 'validator')
        );
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    public function postInformations(StoreSocietyInformationsRequest $request): RedirectResponse
    {
        if (config('registration.available.professional.siren')) {
            $localUser = LocalUser::find(session('registration.user_id'));
            if ($request->siren) {
                $localUser->siren = $request->siren;

                if (! $localUser->address) {
                    $siretAddress = app(InseeService::class)
                        ->getCompanySiretWithSiren($localUser->siren)['adresseEtablissement'];
                    $addressLine1 = data_get($siretAddress, 'numeroVoieEtablissement') . ' ';
                    $addressLine1 .= config(
                        'insee.values.way_type.' .
                        data_get($siretAddress, 'typeVoieEtablissement', ' ')
                    );
                    $addressLine1 .= ' ' . data_get($siretAddress, 'libelleVoieEtablissement');
                    $zip = data_get($siretAddress, 'codePostalEtablissement');
                    $city = data_get($siretAddress, 'libelleCommuneEtablissement');
                    $geocoding = app(GeocodingService::class)
                        ->geocodingFromAddress(implode(' ', [$addressLine1, $zip, $city]));

                    $localUser->addresses = [
                        [
                            "name" => "address",
                            "defaultAddress" => "true",
                            'addressLine1' => $addressLine1,
                            'zip' => $zip,
                            'city' => $city,
                            'location' => [
                                'latitude' => data_get($geocoding, 'lat', config('somoney.map.center.lat')),
                                'longitude' => data_get($geocoding, 'lng', config('somoney.map.center.lng')),
                            ],
                        ],
                    ];
                }
                $localUser->save();
            }
        }

        return $this->storeInformations($request);
    }

    /** @throws \JsonException */
    public function postFiles(StoreFilesRequest $request): RedirectResponse
    {
        $localUser = LocalUser::find(session('registration.user_id'));
        $localUser->setCustomValue('idattachment', $this->storeRequestFile(
            $request,
            $localUser->id,
            'idattachment'
        ));
        $localUser->setCustomValue('organisationattachment', $this->storeRequestFile(
            $request,
            $localUser->id,
            'organisationattachment'
        ));
        $localUser->setCustomValue('ribattachment', $this->storeRequestFile(
            $request,
            $localUser->id,
            'ribattachment'
        ));
        $localUser->setCustomValue('organisationattachmenttype', $request->organisationattachmenttype);

        return $this->nextStep($localUser, config('registration.steps.professional.by_slug.files')['step']);
    }

    /**
     * Validate the email and create the cyclos account
     *
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \App\Exceptions\FailCreateUserException
     * @throws \ErrorException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    public function validation(UserToken $token): View
    {
        $localUser = LocalUser::find($token->user_id);
        $localUser->setCustomValue('validatedemail', true);
        $localUser->setCustomValue('isactive', true);
        $localUser->step = config("registration.steps.professional.slug_id.validation");
        $localUser->save();
        session()->put([
            'registration.user_id' => $localUser->id,
        ]);
        if (! config('somoney.available.services.membership')) {
            app(LocalUserService::class)->createCyclosUser($localUser);
        }
        $token->delete();

        return view('main.templates.register.professional.validation');
    }

    /** @throws \Exception */
    public function resumeRegistrationFromEmail(UserToken $token): RedirectResponse
    {
        $localUser = LocalUser::find($token->user_id);
        session()->put([
            'registration.user_id' => $localUser->id,
        ]);
        if (array_key_exists('step', config("registration.steps.professional.by_id")[$localUser->step])) {
            session()->put([
                'registration.currentStep' => config("registration.steps.professional.by_id")[$localUser->step]['step'],
            ]);
        }
        $token->delete();

        return redirect()->route(config("registration.steps.professional.by_id")[$localUser->step]['route']);
    }

    protected function validateDataForCyclos(RedirectResponse $redirect): RedirectResponse
    {
        $localUser = LocalUser::find(session('registration.user_id'));
        $step = $this->validateFiles($localUser, null);
        $step = $this->validateAddress($localUser, $step);
        $step = $this->validateInformations($localUser, $step);
        $step = $this->validateAccount($localUser, $step);

        if ($step) {
            session()->put([
                'registration.currentStep' => $step['step'],
            ]);

            return redirect()->route(config("registration.steps.professional.step_route")[$step['step']])
                ->with(
                    'error',
                    'Votre inscription n\'est pas complète, certain champs n\'ont pas été remplis'
                );
        }

        return $redirect;
    }

    private function validateFiles(?LocalUser $localUser, ?array $step): ?array
    {
        if (
            config('registration.available.professional.justification_documents')
            && (
                ! data_get($localUser->customValues, 'idattachment')
                || ! data_get($localUser->customValues, 'ribattachment')
                || ! data_get($localUser->customValues, 'organisationattachment')
                || ! data_get($localUser->customValues, 'organisationattachmenttype')
            )
        ) {
            return config('registration.steps.professional.by_slug')['files'];
        }

        return $step;
    }

    private function validateAddress(?LocalUser $localUser, ?array $step): ?array
    {
        if (! config('registration.available.professional.siren') && empty($localUser->addresses)) {
            return config('registration.steps.professional.by_slug')['address'];
        }

        return $step;
    }

    private function validateInformations(?LocalUser $localUser, ?array $step): ?array
    {
        if (
            $this->conditionBasicInformations($localUser)
            || ! data_get($localUser->customValues, 'employeenumber')
            || ! $localUser->name
            || ! data_get($localUser->customValues, 'mailpro')
            || (config('registration.available.professional.siren') && ! $localUser->siren)
        ) {
            return config('registration.steps.professional.by_slug')['society'];
        }

        return $step;
    }

    private function validateAccount(?LocalUser $localUser, ?array $step): ?array
    {
        if (
            ! $localUser->email
            || empty($localUser->passwords)
            || (config('registration.available.professional.category')
                && ! data_get($localUser->customValues, 'catpro'))
        ) {
            return config('registration.steps.professional.by_slug')['account'];
        }

        return $step;
    }

    private function conditionBasicInformations(?LocalUser $localUser): bool
    {
        return ! data_get($localUser->customValues, 'firstname')
            || ! data_get($localUser->customValues, 'lastname')
            || ! data_get($localUser->customValues, 'gender')
            || (empty($localUser->mobilePhones) && empty($localUser->landLinePhones))
            || ! data_get($localUser->customValues, 'birthday');
    }
}
