<?php

namespace App\Http\Controllers\Main\Register;

use App\Http\Requests\Registration\Credit\StoreProfessionalMembershipRegistrationRequest;
use App\Models\LocalUser;
use App\Models\Transaction;
use App\Models\Wallet;
use Illuminate\Http\RedirectResponse;

class ProfessionalMembershipsController extends MembershipsController
{
    protected ?string $userType = 'professional';

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \ErrorException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Lyra\Exceptions\LyraException
     */
    public function postMembership(StoreProfessionalMembershipRegistrationRequest $request): RedirectResponse
    {
        $transaction = Transaction::create([
            'type' => config('somoney.payment-gateway'),
            'target_wallet_id' => ! config('somoney.available.services.lemonway-v2')
                ? Wallet::first()?->id
                : null,
            'wk_token' => bin2hex(openssl_random_pseudo_bytes(25)),
            'status' => Transaction::STATUS_PROCESSING,
            'amount' => 100 * (float) str_replace(',', '.', $request->amount),
            'context' => Transaction::CONTEXT_REGISTRATION,
            'transaction_type' => Transaction::REGISTER_TYPE_ADHERE,
            'local_user_id' => session('registration.user_id'),
        ]);
        session()->put('registration.parent_transaction_id', $transaction->id);
        if (config('registration.available.professional.donate')) {
            return redirect()->route('register.' . $this->userType . '.credit.donate');
        }
        $localUser = LocalUser::find(session('registration.user_id'));
        $initPaymentResponse = $this->makeInitPaymentResponse($transaction, $localUser);
        session()->put('registration.initPaymentResponse', $initPaymentResponse);

        return redirect()->route('register.' . $localUser->group . '.credit.payment');
    }
}
