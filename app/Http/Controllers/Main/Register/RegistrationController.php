<?php

namespace App\Http\Controllers\Main\Register;

use App\Http\Controllers\Controller;
use App\Http\Requests\Registration\StoreAccountRequest;
use App\Http\Requests\Registration\StoreAddressRequest;
use App\Http\Requests\Registration\StoreCguRequest;
use App\Http\Requests\Registration\StoreInformationsRequest;
use App\Jobs\LemonWay\RegisterWalletJob;
use App\Jobs\SendEmailNeedMembership;
use App\Jobs\SendEmailResumeRegistration;
use App\Jobs\SendEmailValidationCreation;
use App\Models\LocalUser;
use App\Models\Transaction;
use App\Models\UsersMatching;
use App\Models\UserToken;
use App\Models\Wallet;
use App\Services\Cyclos\User\AddressService;
use App\Services\Cyclos\UserService;
use App\Services\LocalUserService;
use Exception;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;
use Proengsoft\JsValidation\Facades\JsValidatorFacade as JsValidator;

abstract class RegistrationController extends Controller
{
    protected string $userType;

    protected string $informationStepSlug;

    public function __construct(
        protected UserService $userService,
        protected AddressService $addressService,
        protected LocalUserService $localUserService
    ) {
        //
    }

    /**
     * STEP 1 of the registration - view
     *
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function account(): View|RedirectResponse
    {
        if (! session()->has('registration.user_id')) {
            $localUser = $this->localUserService->makeNewUser($this->userType);
            $localUser->step = config('registration.steps.' . $this->userType . '.by_step')[1]['id'];
        } else {
            $localUser = LocalUser::find(session('registration.user_id'));
        }
        $professionalCategories = [];
        if ($this->userType === config('cyclos.internal-names.groups.professional')) {
            $dataForEdit = $this->userService->getDataForNew('professional');
            $professionalCategories = Arr::first(
                $dataForEdit['customFields'],
                fn(array $value) => $value['internalName'] === 'catpro'
            )['possibleValues'];
            $professionalCategories = array_map(
                fn(array $category) => array_merge($category, ['slug' => Str::slug($category['value'])]),
                $professionalCategories
            );
        }
        try {
            session()->put([
                'registration.user' => $localUser,
                'registration.currentStep' => config(
                    "registration.steps.{$this->userType}.by_id"
                )[$localUser->step]['step'],
            ]);
        } catch (Exception $exception) {
            if ($exception->getMessage() === 'Undefined index: step') {
                session()->forget('registration');

                return redirect(Request::url());
            }
        }
        $validator = JsValidator::formRequest(
            StoreAccountRequest::class,
            '#register-account-form'
        )->render();

        return view(
            "main.templates.register.{$this->userType}.informations.account",
            compact('localUser', 'professionalCategories', 'validator')
        );
    }

    public function resumeRegistration(string $email): RedirectResponse
    {
        $localUser = LocalUser::where('email', $email)->firstOrFail();
        if (
            array_key_exists(
                'step',
                config('registration.steps.' . $localUser->group . '.by_id')[$localUser->step]
            )
        ) {
            session()->forget('errors');
            session()->flash('registerAccountExist', true);
            $this->dispatch(new SendEmailResumeRegistration($localUser->id));
        } elseif (
            $this->userType === 'individual'
            && config('somoney.available.services.membership')
            && config("registration.steps.{$this->userType}.slug_id.credit") === $localUser->step
        ) {
            session()->forget('errors');
            session()->flash('registerAccountExist', true);
            $this->dispatch(new SendEmailNeedMembership($localUser));
        } elseif (config('registration.steps.' . $localUser->group . '.slug_id.validation') === $localUser->step) {
            session()->forget('errors');
            session()->flash('registerNotValidated', true);
            $this->dispatch(new SendEmailValidationCreation($localUser->id, true));
        }

        return redirect()->route('auth.login');
    }

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \JsonException
     */
    public function storeAccount(StoreAccountRequest $request): RedirectResponse
    {
        $localUser = LocalUser::find(session('registration.user_id'));
        if (! $localUser) {
            $localUser = session()->get('registration.user');
        }
        session()->forget('registration.user');
        $localUser->email = $request->get('email', $localUser->email);
        $localUser->passwords = [
            [
                'type' => 'login',
                'value' => Crypt::encryptString($request->password),
                'checkConfirmation' => false,
            ],
        ];
        if ($this->userType === config('cyclos.internal-names.groups.professional')) {
            $localUser->setCustomValue('catpro', $request->category);
        }
        $localUser->save();
        session()->put(['registration.user_id' => $localUser->id]);

        return $this->nextStep($localUser, config("registration.steps.{$this->userType}.by_slug.account")['step']);
    }

    protected function nextStep(LocalUser $localUser, int $stepNumber): RedirectResponse
    {
        $currentStep = session('registration.currentStep');
        if ($currentStep <= $stepNumber) {
            $nextStep = $currentStep + 1;
            $localUser->step = config("registration.steps.{$this->userType}.by_step")[$nextStep]['id'];
            $localUser->save();
            session()->put([
                'registration.currentStep' => config("registration.steps.{$this->userType}.by_id")
                [$localUser->step]['step'],
            ]);
        } else {
            $nextStep = $stepNumber + 1;
        }

        return redirect()->route(config("registration.steps.{$this->userType}.step_route")[$nextStep]);
    }

    /** STEP 2 of the registration - view */
    abstract public function informations(): View;

    public function address(): View
    {
        $localUser = LocalUser::find(session('registration.user_id'));

        return view(
            'main.templates.register.' . $this->userType . '.informations.address',
            compact('localUser')
        );
    }

    public function postAddress(StoreAddressRequest $request): RedirectResponse
    {
        $localUser = LocalUser::find(session('registration.user_id'));
        $manualAddress = [
            'name' => 'address',
            'defaultAddress' => true,
            'addressLine1' => $request->addressLine1,
            'zip' => $request->zip,
            'city' => $request->city,
            'addressLine2' => $request->addressLine2,
        ];
        $gmapAddress = [];
        if ($request->gmap) {
            $gmapAddress = [
                'addressLine1' => data_get($request->gmap, 'addressLine1'),
                'zip' => data_get($request->gmap, 'zip'),
                'city' => data_get($request->gmap, 'city'),
                'location' => Arr::only($request->gmap, ['latitude', 'longitude']),
            ];
        }
        // Remove null values
        $gmapAddress = array_filter($gmapAddress);
        $manualAddress = array_filter($manualAddress);
        if (! array_key_exists('addressLine1', $gmapAddress)) {
            $localUser->addresses = [array_merge($gmapAddress, $manualAddress)];
        } else {
            $localUser->addresses = [array_merge($manualAddress, $gmapAddress)];
        }
        $localUser->save();

        return $this->nextStep($localUser, config("registration.steps.{$this->userType}.by_slug.address")['step']);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function files(): View
    {
        $localUser = LocalUser::find(session('registration.user_id'));
        $organisationAttachmentTypes = $this->localUserService
            ->getDataCustomField($this->userType, 'organisationattachmenttype');

        return view(
            "main.templates.register.{$this->userType}.files",
            compact('localUser', 'organisationAttachmentTypes')
        );
    }

    public function cgu(): View
    {
        $localUser = null;
        if (config('registration.available.common.comment')) {
            $localUser = LocalUser::find(session('registration.user_id'));
        }

        return view("main.templates.register.{$this->userType}.cgu", compact('localUser'));
    }

    /** @throws \JsonException */
    public function postCgu(StoreCguRequest $request): RedirectResponse
    {
        $localUser = LocalUser::find(session('registration.user_id'));
        $localUser->acceptAgreement = $request->cgu;
        $localUser->setCustomValue('commercialinfo', $request->commercialinfo);
        $localUser->setCustomValue('optinnewsletter', $request->optinnewsletter);
        $localUser->setCustomValue('membershipsonantaise', $request->membershipsonantaise);
        if (config('registration.available.common.comment')) {
            $localUser->setCustomValue('comment', $request->comment);
        }
        $localUser->save();

        return $this->validateDataForCyclos(
            $this->nextStep($localUser, config("registration.steps.{$this->userType}.by_slug.cgu")['step'])
        );
    }

    abstract protected function validateDataForCyclos(RedirectResponse $redirect): RedirectResponse;

    public function success(): View
    {
        $localUser = LocalUser::find(session('registration.user_id'));
        $this->dispatch(new SendEmailValidationCreation($localUser->id));
        $localUser->step = config("registration.steps.{$this->userType}.slug_id.success");
        $localUser->save();
        session()->forget('registration');

        return view('main.templates.register.' . $this->userType . '.success');
    }

    abstract public function resumeRegistrationFromEmail(UserToken $token): RedirectResponse;

    /** @throws \Exception */
    public function resumeNeedMembership(UserToken $token): RedirectResponse
    {
        $localUser = LocalUser::find($token->user_id);
        session()->put([
            'registration.user_id' => $localUser->id,
        ]);
        $token->delete();

        return redirect()->route('register.' . $this->userType . '.credit.membership');
    }

    public function failCreate(): View
    {
        return view('main.templates.register.' . $this->userType . '.fail-create');
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function storeInformations(StoreInformationsRequest $request): RedirectResponse
    {
        $localUser = LocalUser::find(session('registration.user_id'));
        $phone = preg_replace('/^(33)?0?/', '+33', preg_replace('/\D/', '', $request->phone));
        if (preg_match('/^\+33[67]/', $phone)) {
            $phoneKey = 'mobilePhones';
        } else {
            $phoneKey = 'landLinePhones';
        }
        $localUser->mobilePhones = [];
        $localUser->landLinePhones = [];
        $localUser->{$phoneKey} = [
            [
                'name' => 'phone',
                'number' => $phone,
            ],
        ];
        $localUser->name = $request->name ?: "{$request->firstname} {$request->lastname}";
        $localUser->setCustomValue('firstname', $request->firstname);
        $localUser->setCustomValue('lastname', $request->lastname);
        $localUser->setCustomValue('gender', $request->gender);
        $localUser->setCustomValue('birthday', $request->birthday);
        $localUser->setCustomValue('employeenumber', $request->employeenumber);
        $localUser->setCustomValue('mailpro', $request->mailpro);
        $localUser->setCustomValue('tpeconfig', config('somoney.payment-mode') === 'cev' ? $request->tpeconfig : null);
        if (! config('registration.available.' . $this->userType . '.credit')) {
            $localUser->setCustomValue(
                'apikey',
                $this->userType === 'professional' ? app(LocalUserService::class)->createUniqueApiKey() : null
            );
        }
        $localUser->setCustomValue('needsValidation', true);
        $localUser->setCustomValue('cardexpirydate', config('somoney.payment-mode') === 'cev' ? '0000' : null);
        $localUser->save();
        if (
            config('somoney.payment-gateway') === Transaction::TYPE_LEMONWAY
            && ! config('somoney.available.services.lemonway-v2')
        ) {
            $this->createOrUpdateWallet($localUser);
        }

        return $this->nextStep(
            $localUser,
            config("registration.steps.{$this->userType}.by_slug.{$this->informationStepSlug}")['step']
        );
    }

    protected function createOrUpdateWallet(LocalUser $localUser): void
    {
        if (config('somoney.payment-mode') === 'cev') {
            UsersMatching::create([
                'user_id' => $localUser->id,
                'cev_id' => $localUser->name,
                'cev_account_id' => $localUser->name,
            ]);
        }
        $wallet = Wallet::where('cyclos_user_id', $localUser->id)->first();
        if (! $wallet) {
            $walletLwId = Wallet::generateLemonWayId();
            $wallet = Wallet::create([
                'cyclos_user_id' => $localUser->id,
                'lw_id' => $walletLwId,
            ]);
            $localUser->setCustomValue('lemonwayId', $walletLwId);
            if (config('lemonway.mode') !== 'vad' && app()->environment() !== 'testing') {
                Bus::dispatchNow(new RegisterWalletJob(
                    $localUser,
                    $wallet
                ));
            }
        }
    }

    protected function storeRequestFile(FormRequest $request, string $userId, string $fileName): ?string
    {
        if (! $request->hasFile($fileName)) {
            return null;
        }
        $file = $request->file($fileName);
        $fileExtension = $file->getClientOriginalExtension();
        $file->storePubliclyAs('legal_documents/' . $userId, $fileName . '.' . $fileExtension);

        return route('admin.legal_documents', ['filename' => "$userId/$fileName.$fileExtension"]);
    }
}
