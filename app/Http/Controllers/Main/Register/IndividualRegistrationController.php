<?php

namespace App\Http\Controllers\Main\Register;

use App\Http\Requests\Registration\Individual\StoreFilesRequest;
use App\Http\Requests\Registration\Individual\StorePersonalInformationsRequest;
use App\Models\LocalUser;
use App\Models\UserToken;
use App\Services\LocalUserService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Proengsoft\JsValidation\Facades\JsValidatorFacade as JsValidator;

class IndividualRegistrationController extends RegistrationController
{
    protected string $userType = 'individual';

    protected string $informationStepSlug = 'personal';

    /**
     * STEP 2 of the registration - view
     *
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function informations(): View
    {
        $localUser = LocalUser::find(session('registration.user_id'));
        $genders = $this->localUserService->getDataCustomField('individual', 'gender');
        $validator = JsValidator::formRequest(
            StorePersonalInformationsRequest::class,
            '#register-personal-form'
        )->render();

        return view(
            "main.templates.register.individual.informations.personal",
            compact('genders', 'localUser', 'validator')
        );
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    public function postInformations(StorePersonalInformationsRequest $request): RedirectResponse
    {
        return $this->storeInformations($request);
    }

    public function postFiles(StoreFilesRequest $request): RedirectResponse
    {
        $localUser = LocalUser::find(session('registration.user_id'));
        $localUser->setCustomValue('idattachment', $this->storeRequestFile(
            $request,
            $localUser->id,
            'idattachment'
        ));

        return $this->nextStep($localUser, config('registration.steps.individual.by_slug.files')['step']);
    }

    /** @throws \Exception */
    public function validation(UserToken $token): View|RedirectResponse
    {
        $localUser = LocalUser::find($token->user_id);
        if (! $localUser) {
            $token->delete();
            session()->flash('validationTokenExpired', true);

            return redirect()->route('auth.login');
        }
        $localUser->setCustomValue('validatedemail', true);
        $localUser->setCustomValue('needsValidation', false);
        $localUser->setCustomValue('isactive', true);
        $localUser->step = config('registration.steps.individual.slug_id.validation');
        $localUser->save();
        session()->put([
            'registration.user_id' => $localUser->id,
        ]);
        if (! config('somoney.available.services.membership')) {
            app(LocalUserService::class)->createCyclosUser($localUser);
        } else {
            $localUser->step = config('registration.steps.individual.slug_id.membership');
            $localUser->save();
        }
        $token->delete();

        return view('main.templates.register.individual.validation');
    }

    /** @throws \Exception */
    public function resumeRegistrationFromEmail(UserToken $token): RedirectResponse
    {
        $localUser = LocalUser::find($token->user_id);
        session()->put([
            'registration.user_id' => $localUser->id,
        ]);
        if (array_key_exists('step', config('registration.steps.individual.by_id')[$localUser->step])) {
            session()->put([
                'registration.currentStep' => config('registration.steps.individual.by_id')[$localUser->step]['step'],
            ]);
        }
        $token->delete();

        return redirect()->route(config('registration.steps.individual.by_id')[$localUser->step]['route']);
    }

    protected function validateDataForCyclos(RedirectResponse $redirect): RedirectResponse
    {
        $localUser = LocalUser::find(session('registration.user_id'));
        $step = $this->validateFiles($localUser, null);
        if (empty($localUser->addresses)) {
            $step = config('registration.steps.individual.by_slug')['address'];
        }
        $step = $this->validateInformations($localUser, $step);
        $step = $this->validateAccount($localUser, $step);

        if ($step) {
            session()->put([
                'registration.currentStep' => $step['step'],
            ]);

            return redirect()->route(config("registration.steps.individual.step_route")[$step['step']])
                ->with(
                    'error',
                    'Votre inscription n\'est pas complète, certain champs n\'ont pas été remplis.'
                );
        }

        return $redirect;
    }

    private function validateFiles(?LocalUser $localUser, ?array $step): ?array
    {
        if (
            config('registration.available.individual.id_document')
            && ! data_get($localUser->customValues, 'idattachment')
        ) {
            return config('registration.steps.individual.by_slug')['files'];
        }

        return $step;
    }

    private function validateInformations(?LocalUser $localUser, ?array $step): ?array
    {
        if (
            ! data_get($localUser->customValues, 'firstname') || ! data_get($localUser->customValues, 'lastname')
            || ! data_get($localUser->customValues, 'gender')
            || (empty($localUser->mobilePhones) && empty($localUser->landLinePhones))
            || ! data_get($localUser->customValues, 'birthday')
        ) {
            return config('registration.steps.individual.by_slug')['personal'];
        }

        return $step;
    }

    private function validateAccount(?LocalUser $localUser, ?array $step): ?array
    {
        if (! $localUser->email || empty($localUser->passwords)) {
            return config('registration.steps.individual.by_slug')['account'];
        }

        return $step;
    }
}
