<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Http\Requests\Ads\AdStoreRequest;
use App\Http\Requests\Ads\AdUpdateRequest;
use App\Services\Cyclos\AdService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class AdsController extends Controller
{
    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function index(AdService $adService): View
    {
        $ads = $adService->list([
            'owner' => session('auth.user.id'),
        ]);
        $ads = collect($ads->items())
            ->map(function (array $ad) use ($adService) {
                $adItem = $adService->get($ad['id']);
                if (! $adItem) {
                    return null;
                }
                data_set(
                    $adItem,
                    'image.url',
                    str_replace('ad', 'deal', data_get($adItem, 'image.url'))
                );

                return $adItem;
            })
            ->filter();

        return view('main.templates.advertisements.index', compact('ads'));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function create(AdService $adService): View
    {
        $data = $adService->dataForNew();
        $advertisement = data_get($data, 'advertisement');
        $categories = data_get($data, 'categories');
        $currency = $data['currencies'][0]['internalName'];

        return view(
            'main.templates.advertisements.create',
            compact('advertisement', 'categories', 'currency')
        );
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function store(AdStoreRequest $request, AdService $adService): RedirectResponse
    {
        $adId = $adService->store(
            $request->get('name'),
            $request->get('description'),
            $request->start_date,
            $request->end_date,
            [$request->get('category')],
            $request->currency,
            session('auth.user.id'),
            $request->get('tags')
        );
        if ($request->hasFile('images')) {
            foreach ($request->images as $image) {
                $adService->setAdImage((string) $adId, $image);
            }
        }

        return redirect()->route('ads.index')->with('success', 'Bon plan ajouté avec succès !');
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function edit(AdService $adService, string $adId): View
    {
        $data = $adService->dataForEdit($adId);
        $images = array_map(
            static function (array $item) {
                data_set(
                    $item,
                    'url',
                    str_replace('ad', 'deal', data_get($item, 'url'))
                );

                return $item;
            },
            $adService->getAdImages($adId)
        );
        $advertisement = data_get($data, 'advertisement');
        $categories = data_get($data, 'categories');

        return view(
            'main.templates.advertisements.edit',
            compact('advertisement', 'categories', 'adId', 'images')
        );
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function update(AdUpdateRequest $request, AdService $adService, string $adId): RedirectResponse
    {
        $adService->update(
            $request->get('name'),
            $request->get('description'),
            $request->start_date,
            $request->end_date,
            [$request->get('category')],
            $adId,
            $request->tags
        );
        if ($request->hasFile('images')) {
            foreach ($request->images as $image) {
                $adService->setAdImage($adId, $image);
            }
        }

        return redirect()->route('ads.index')->with('success', 'Bon plan édité avec succès !');
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function delete(AdService $adService, string $adId): RedirectResponse
    {
        $adService->delete($adId);

        return redirect()->back()->with('success', 'Bon plan supprimé avec succès !');
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function deleteImage(AdService $adService, string $imageId): RedirectResponse
    {
        $adService->deleteImage($imageId);

        return redirect()->back()->with('success', 'Image supprimée avec succès !');
    }
}
