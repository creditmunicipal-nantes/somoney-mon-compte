<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;

class DocumentsController extends Controller
{
    public function redirect(string $filename): RedirectResponse
    {
        return response()->redirectToRoute('admin.legal_documents', [$filename]);
    }
}
