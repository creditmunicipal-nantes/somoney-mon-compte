<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\CyclosUser;
use App\Services\Cyclos\UserService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class FavoritesController extends Controller
{
    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function index(UserService $userService, Request $request): View
    {
        $user = CyclosUser::fill(session()->get('auth.user'));
        $favorites = $user->getFavorites();
        $category = $request->get('category');
        if ($category === 'null') {
            $category = null;
        }
        if ($favorites) {
            $favorites = collect($favorites)->map(function ($favoriteId) use ($userService, $category) {
                $user = CyclosUser::fill($userService->get($favoriteId));
                if (! $category) {
                    return $user;
                }
                if (data_get($user->customValues, 'catpro', null)) {
                    foreach ($user->customValues['catpro'] as $value) {
                        if ($value['value'] === $category) {
                            return $user;
                        }
                    }
                }
            })->filter();
        }

        return view('main.templates.favorite', compact('favorites'));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function store(UserService $userService, Request $request): RedirectResponse
    {
        $user = CyclosUser::fill(session()->get('auth.user'));
        $favoriteId = $userService->search([
            'profileFields' => [
                'email' => $request->get('pro_email'),
            ],
        ])->first()['id'];
        $favorites = array_merge($user->getFavorites(), [$favoriteId]);
        $userService->setUserData($user->id, [
            'customValues' => [
                'favorites' => implode(',', $favorites),
            ],
        ]);

        return redirect()->route('dashboard.favorites.index');
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function delete(UserService $userService, string $favoriteId): RedirectResponse
    {
        $user = CyclosUser::fill(session()->get('auth.user'));
        $favorites = array_diff($user->getFavorites(), [$favoriteId]);
        $userService->setUserData($user->id, [
            'customValues' => [
                'favorites' => implode(',', $favorites),
            ],
        ]);

        return redirect()->route('dashboard.favorites.index');
    }
}
