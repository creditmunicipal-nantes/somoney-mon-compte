<?php

namespace App\Http\Controllers\Main\Operations;

use App\Http\Controllers\Controller;
use App\Http\Requests\Credit\Automatic\StoreRequest;
use App\Jobs\SendEmailConfirmCreditAutomatic;
use App\Jobs\SendEmailSepaCreated;
use App\Models\CyclosUser;
use App\Models\Sepa;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CreditAutomaticController extends Controller
{
    public function index(): View
    {
        $amount = session()->get('amount', config('somoney.credit.auto.minimal'));
        $genders = ['m' => 'M.', 'f' => 'Mme'];
        $user = CyclosUser::fill(session()->get('auth.user'));
        $creditsAuto = $user->getAutoCredits();
        $maxAmount = session('auth.user.account.status.upperCreditLimit') - session('auth.user.account.status.balance');
        $maxAmount = config('somoney.credit.maximal.' . $user->group, $maxAmount) < $maxAmount
            ? config('somoney.credit.maximal.' . $user->group, $maxAmount)
            : $maxAmount;

        return view(
            'main.templates.operations.credit.automatic',
            compact('amount', 'maxAmount', 'genders', 'user', 'creditsAuto')
        );
    }

    public function store(StoreRequest $request): RedirectResponse
    {
        $sepa = Sepa::create([
            'user_id' => session()->get('auth.user')['id'],
            'amount' => $request->amount,
            'gender' => $request->gender,
            'last_name' => $request->last_name,
            'first_name' => $request->first_name,
            'email' => $request->email,
            'company' => $request->company,
            'iban' => $request->iban,
            'duration_month' => $request->duration_month,
        ]);
        $this->dispatch(new SendEmailConfirmCreditAutomatic($sepa->id));

        return redirect()->route('home')
            ->with('success', 'Une demande de confirmation du crédit automatique vous a été envoyé !');
    }

    /**
     * @param \App\Models\Sepa $sepa
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function confirm(Sepa $sepa)
    {
        if (! $sepa->confirmed && session()->get('auth.user')['id'] === $sepa->user_id) {
            $sepa->update([
                'confirmed' => true,
            ]);

            $this->dispatch(new SendEmailSepaCreated($sepa->id));

            return redirect()->route('home')
                ->with('success', 'Votre demande de crédit automatique a bien été confirmé !');
        }

        throw new HttpException(Response::HTTP_FORBIDDEN);
    }

    /**
     * @param \App\Models\Sepa $sepa
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function cancel(Sepa $sepa)
    {
        if (! $sepa->confirmed && session()->get('auth.user')['id'] === $sepa->user_id) {
            $sepa->delete();

            return redirect()->route('home')
                ->with('success', 'Votre demande de crédit automatique a bien été annulé !');
        }

        throw new HttpException(Response::HTTP_FORBIDDEN);
    }
}
