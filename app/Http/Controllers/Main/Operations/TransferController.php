<?php

namespace App\Http\Controllers\Main\Operations;

use App\Http\Controllers\Controller;
use App\Http\Requests\Transfer\TransferRequest;
use App\Jobs\SendEmailsTransfer;
use App\Models\CyclosUser;
use App\Services\Cyclos\TransactionService;
use App\Services\Cyclos\TransferService;
use App\Services\Cyclos\UserService;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Http\RedirectResponse;

class TransferController extends Controller
{
    protected UserService $userService;

    public function __construct()
    {
        $this->userService = app(UserService::class);
    }

    public function index()
    {
        $transactionService = app(TransactionService::class);
        $transactions = [
            'planned' => $transactionService->listAll([
                'kinds' => 'scheduledPayment',
                'scheduledPaymentStatuses' => 'open',
            ])->map(function (array $transaction) use ($transactionService) {
                $transaction = $transactionService->get(data_get($transaction, 'id'));

                return [
                    'date' => Carbon::parse(data_get($transaction, 'installments.0.dueDate'))->format('d/m/Y'),
                    'toUser' => data_get($transaction, 'toUser.display'),
                    'description' => data_get($transaction, 'description'),
                    'amount' => data_get($transaction, 'dueAmount'),
                ];
            }),
            'recurrent' => $transactionService->listAll([
                'kinds' => 'recurringPayment',
                'recurringPaymentStatuses' => 'open',
            ])->map(function (array $transaction) use ($transactionService) {
                $transaction = $transactionService->get(data_get($transaction, 'id'));

                return [
                    'toUser' => data_get($transaction, 'toUser.display'),
                    'description' => data_get($transaction, 'description'),
                    'amount' => data_get($transaction, 'amount'),
                    'nextOccurrenceDate' => Carbon::parse(data_get(
                        $transaction,
                        'nextOccurrenceDate',
                        today()
                    ))->format('d/m/Y'),
                    'canBeCanceled' => data_get($transaction, 'recurringPaymentPermissions.cancel', false),
                ];
            }),
        ];

        return view('main.templates.operations.transfer', compact('transactions'));
    }

    /**
     * @param \App\Http\Requests\Transfer\TransferRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Exception
     */
    public function store(TransferRequest $request): RedirectResponse
    {
        $transactionService = app(TransactionService::class);
        /** @var array|null $toUser */
        $toUser = $this->userService->search([
            'profileFields' => ['email' => $request->{$request->profil . '_email'}],
        ])
            ?->first();
        if (! $toUser) {
            return redirect()->back()->withErrors(['transfer' => 'userToNotFound']);
        }
        $toUser = CyclosUser::fill($toUser);
        $fromUser = CyclosUser::fill(session('auth.user'));
        $scheduleDate = $request->date;

        $params = [
            'toUser' => $toUser->id,
            'amount' => $request->amount,
            'description' => $request->comment,
            'scheduleDate' => $scheduleDate,
            'type' => $this->getPaymentType($fromUser, $toUser),
        ];
        $isScheduled = $scheduleDate->diffInDays(today()) > 0;
        $money = config('somoney.money');
        if ($request->recurrent) {
            [$successMessage, $params] =
                $this->makeParamsRecurrent($request, $params, $toUser, $scheduleDate, $money);
        } elseif ($isScheduled) {
            $successMessage = <<<EOT
Virement planifié avec succès<br/>
Le montant est de <b>{$request->amount} $money</b> à <b>{$toUser->name}</b>
avec le commentaire suivant : "{$request->comment}", <br/>
il sera viré le <b>{$scheduleDate->format('d/m/Y')}</b> à  <b>{$scheduleDate->format('G:i')}</b>.
EOT;
        } else {
            $successMessage = <<<EOT
Virement réalisé avec succès<br/>
Le montant était de <b>{$request->amount} $money</b> à <b>{$toUser->name}</b>
avec le commentaire suivant : "{$request->comment}"
EOT;
        }
        try {
            $transactionService->store($params);
        } catch (Exception $exception) {
            if (strpos($exception->getMessage(), 'org.cyclos.model.banking.MinTimeBetweenPaymentsException')) {
                return redirect()
                    ->back()
                    ->with('error', __('validation.cyclos.timeBetweenPaymentsNotMet'));
            }
            if (strpos($exception->getMessage(), 'org.cyclos.model.banking.InsufficientBalanceException')) {
                return redirect()->back()->with('error', __('validation.cyclos.insufficientBalance'));
            }
            throw $exception;
        }
        $transferDetails = $this->generateTransferDetails($fromUser, $toUser, $request);
        $this->dispatch(new SendEmailsTransfer(
            TransferService::WEB_TRANSFER,
            $fromUser->email,
            $toUser->email,
            $transferDetails,
            $isScheduled
        ));

        return redirect()->back()->with('success', $successMessage);
    }

    protected function getPaymentType(CyclosUser $fromUser, CyclosUser $toUser): string
    {
        if ($fromUser->group === config('cyclos.internal-names.groups.professional')) {
            if ($toUser->group === config('cyclos.internal-names.groups.professional')) {
                return 'professional.toProfessional';
            }

            return 'professional.toIndividual';
        }
        if ($toUser->group === config('cyclos.internal-names.groups.professional')) {
            return 'individual.toProfessional';
        }

        return 'individual.toIndividual';
    }

    protected function addRecurrenceOnTransferInfos(
        array $paymentData,
        array $transferDetails,
        string $recurrence = null
    ): array {
        if (data_get($paymentData, 'recurrent')) {
            $transferDetails = data_set($transferDetails, 'recurrence', $recurrence);
        }

        return $transferDetails;
    }

    protected function computeNbOccurrence(string $frequency, Carbon $dateBegin, Carbon $dateEnd): int
    {
        $addFn = match ($frequency) {
            'weekly' => fn(Carbon $date) => $date->addWeek(),
            'bi-monthly' => fn(Carbon $date) => $date->addDays(15),
            default => fn(Carbon $date) => $date->addMonth(),
        };
        $count = 0;
        $pivotDate = $dateBegin->copy();
        while ($pivotDate->lte($dateEnd)) {
            $count++;
            $pivotDate = $addFn($pivotDate);
        }

        return $count;
    }

    protected function generateTransferDetails(
        CyclosUser $fromUser,
        CyclosUser $toUser,
        TransferRequest $request
    ): array {
        return array_merge(
            [
                'fromUser' => $fromUser,
                'toUser' => $toUser,
                'amount' => $request->amount,
                'label' => $request->comment,
                'date' => $request->date->format('d/m/Y'),
                'hour' => $request->date->format('H:i'),
                'recurrent' => $request->recurrent,
            ],
            ($request->recurrent
                ? [
                    'date_end' => $request->date_end->format('d/m/Y'),
                    'frequency' => __($request->frequency),
                ]
                : []
            )
        );
    }

    /**
     * @param \App\Http\Requests\Transfer\TransferRequest $request
     * @param array $params
     * @param \App\Models\CyclosUser $toUser
     * @param \Illuminate\Support\Carbon $scheduleDate
     * @param string $money
     *
     * @return array
     * @throws \Exception
     */
    protected function makeParamsRecurrent(
        TransferRequest $request,
        array $params,
        CyclosUser $toUser,
        Carbon $scheduleDate,
        string $money
    ): array {
        $dateStr = $scheduleDate->format('d/m/Y');
        $occurrenceInterval = [];
        $recurrence = '';
        switch ($request->frequency) {
            case 'weekly':
                $occurrenceInterval = [
                    'amount' => 7,
                    'field' => 'days',
                ];
                $recurrence = 'hebdomadaire';
                break;
            case 'bi-monthly':
                $occurrenceInterval = [
                    'amount' => 15,
                    'field' => 'days',
                ];
                $recurrence = 'bi-mensuelle';
                break;
            case 'monthly':
                $occurrenceInterval = [
                    'amount' => 1,
                    'field' => 'months',
                ];
                $recurrence = 'mensuelle';
                break;
        }
        $params['recurrent'] = $request->recurrent;
        $params['occurrenceInterval'] = $occurrenceInterval;
        $params['occurrencesCount'] = $this->computeNbOccurrence(
            $request->frequency,
            $scheduleDate,
            new Carbon($request->date_end)
        );
        $occurrences = $params['occurrencesCount'];
        $successMessage = <<<EOT
Virement permanent planifié avec succès<br/>
Le montant sera de <b>{$request->amount} $money</b> à <b>{$toUser->name}</b>
avec le commentaire suivant : "{$request->comment}"<br/>
Il y aura <b>$occurrences</b> virements <b>$recurrence</b> avec un premier versement le $dateStr
EOT;

        return [$successMessage, $params];
    }
}
