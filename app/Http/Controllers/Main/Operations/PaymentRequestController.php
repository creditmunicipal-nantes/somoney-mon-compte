<?php

namespace App\Http\Controllers\Main\Operations;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contact\PaymentRequestRequest;
use App\Jobs\Operations\SendEmailPaymentRequest;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Facades\Bus;
use Illuminate\View\View;

class PaymentRequestController extends Controller
{
    public function __construct(protected UserService $userService)
    {
    }

    public function index(): View
    {
        return view('main.templates.operations.payment-request');
    }

    /**
     * @param \App\Http\Requests\Contact\PaymentRequestRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function store(PaymentRequestRequest $request)
    {
        $userData = $this->userService->get(data_get(session()->get('auth.user'), 'id'));
        $validatedData = $request->validated();
        $toUser = $this->getToUser($validatedData);

        if (! $toUser) {
            return redirect()
                ->back()
                ->withErrors(['transfer' => 'userToNotFound']);
        }
        Bus::dispatch(new SendEmailPaymentRequest(
            data_get($toUser, 'email'),
            $request->title,
            $request->amount,
            $userData
        ));
        $name = data_get($toUser, 'name');

        return redirect()->back()->with('success', 'Votre message a bien été envoyé à ' . $name . ' !');
    }

    /**
     * @param array $validatedData
     *
     * @return array|null
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function getToUser(array $validatedData): ?array
    {
        $email = data_get($validatedData, data_get($validatedData, 'profil') . '_email');

        /** @var array|null $user */
        $user = $this->userService->search(['keywords' => $email])->first();

        if ($user && data_get($user, 'email') === $email) {
            return $user;
        }

        return null;
    }
}
