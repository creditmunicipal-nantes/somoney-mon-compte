<?php

namespace App\Http\Controllers\Main\Operations;

use App\Http\Controllers\Controller;
use App\Http\Requests\Credit\PaymentSepaRequest;
use App\Http\Requests\Credit\StoreRequest;
use App\Jobs\LemonWay\InitMoneyInJob;
use App\Jobs\SendEmailSepaCreated;
use App\Models\CyclosUser;
use App\Models\Notification;
use App\Models\Sepa;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Services\SystemPayService;
use ErrorException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class CreditController extends Controller
{
    public function index(): View
    {
        $amount = session('amount', config('somoney.credit.simple.minimal'));
        $cyclosUser = CyclosUser::fill(session('auth.user'));
        $isProfessional = $cyclosUser->group === config('cyclos.internal-names.groups.professional');
        $maxAmount = session('auth.user.account.status.upperCreditLimit') - session('auth.user.account.status.balance');
        $maxAmount = config('somoney.credit.maximal.' . $cyclosUser->group, $maxAmount) < $maxAmount
            ? config('somoney.credit.maximal.' . $cyclosUser->group, $maxAmount)
            : $maxAmount;
        $notification = Notification::where([
            ['groups', 'credit-page'],
            ['display', true],
        ])->orWhere([
            ['groups', Notification::TOALL],
            ['display', true],
        ])->latest('updated_at')->first();

        return view(
            'main.templates.operations.credit.index',
            compact('amount', 'maxAmount', 'isProfessional', 'notification')
        );
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \ErrorException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Lyra\Exceptions\LyraException
     */
    public function store(StoreRequest $request): RedirectResponse
    {
        $user = CyclosUser::fill(session('auth.user'));
        if (config('somoney.available.services.sepa-payment')) {
            if ($request->automatic) {
                return redirect()->route('operations.credit.automatic.index');
            }
            if ($user->group === 'professional' && $request->type === 'sepa') {
                return redirect()->route('operations.credit.payment.sepa.index');
            }
        }
        $amount = (int) (100 * (float) str_replace(',', '.', $request->amount));
        session()->put('operations.initPaymentResponse', $this->getPaymentResponse($user, $amount));

        return redirect()->route('operations.credit.payment.bank-card');
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \ErrorException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Lyra\Exceptions\LyraException
     */
    protected function getPaymentResponse(CyclosUser $user, int $amount): string
    {
        return match (config('somoney.payment-gateway')) {
            Transaction::TYPE_LEMONWAY => InitMoneyInJob::dispatchSync(
                $amount,
                $user->id,
                config('somoney.available.services.lemonway-v2') ? null : Wallet::first(),
                Transaction::CONTEXT_OPERATION
            ),
            Transaction::TYPE_SYSTEM_PAY => app(SystemPayService::class)->createPayment(
                $amount,
                $user->id,
                Transaction::CONTEXT_OPERATION
            ),
            default => throw new ErrorException(
                'Payment Gateway not recognized "' . config('somoney.payment-gateway') . '"'
            ),
        };
    }

    public function paymentBankCard(): View|RedirectResponse
    {
        $initPayment = session()->pull('operations.initPaymentResponse');
        if (! $initPayment) {
            return redirect()->route('operations.credit.index');
        }

        return view('main.templates.operations.credit.payment.bank-card', compact('initPayment'));
    }

    public function sepa(): View
    {
        $genders = [
            'm' => 'M.',
            'f' => 'Mme',
        ];
        $user = CyclosUser::fill(session()->get('auth.user'));
        $maxAmount = session('auth.user.account.status.upperCreditLimit') - session('auth.user.account.status.balance');
        $userGroup = session()->get('auth.user')['group']['internalName'];
        $maxAmount = config('somoney.credit.maximal.' . $userGroup, $maxAmount) < $maxAmount
            ? config('somoney.credit.maximal.' . $userGroup, $maxAmount)
            : $maxAmount;

        return view('main.templates.operations.credit.payment.sepa', compact('genders', 'user', 'maxAmount'));
    }

    public function sepaStore(PaymentSepaRequest $request): RedirectResponse
    {
        $sepa = Sepa::create([
            'user_id' => session()->get('auth.user')['id'],
            'amount' => $request->amount,
            'gender' => $request->gender,
            'last_name' => $request->last_name,
            'first_name' => $request->first_name,
            'email' => $request->email,
            'company' => $request->company,
            'iban' => $request->iban,
            'confirmed' => true,
        ]);
        $this->dispatch(new SendEmailSepaCreated($sepa->id));

        return redirect()->route('operations.historic.index')
            ->with('success', 'Votre demande de prélèvement SEPA a bien été envoyé !');
    }
}
