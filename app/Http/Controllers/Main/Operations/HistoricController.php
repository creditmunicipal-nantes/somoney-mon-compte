<?php

namespace App\Http\Controllers\Main\Operations;

use App\Exports\UserOperationsExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Historic\HistoricExportPdfRequest;
use App\Models\CyclosUser;
use App\Services\Cyclos\AccountService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Date;
use DateInterval;
use DatePeriod;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Excel;
use Barryvdh\DomPDF\Facade\Pdf;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class HistoricController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Cyclos\AccountService $accountService
     *
     * @return \Illuminate\Contracts\View\View
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function index(Request $request, AccountService $accountService): View
    {
        $filters = array_replace_recursive(
            $request->get('filters', []),
            [
                'page' => $request->get('page'),
                'pageSize' => $request->get('pageSize'),
            ]
        );
        $transactions = $accountService->history(
            session('auth.user.id'),
            session('auth.user.account.type.internalName'),
            $filters
        );
        $operations = array_map(static fn($transaction) => $transaction, $transactions->items());
        $months = $accountService->fullHistory(
            session('auth.user.id'),
            session('auth.user.account.type.internalName')
        )->reduce(function (Collection $months, $transaction) {
            $transDate = Carbon::parse(data_get($transaction, 'date'));
            $date = $transDate->copy()->firstOfMonth()->startOfDay();
            $months->put($date->year . '-' . $date->month, [
                'date' => $date->copy(),
                'interval' => $date->format('Y-m-d') . ',' . $date->addMonth()->format('Y-m-d'),
            ]);
            $months->unique();

            return $months;
        }, new Collection())->values();
        $userActivatedAt = session('auth.user.activationDate');
        if ($userActivatedAt) {
            $periodStart = Date::parse($userActivatedAt);
            $periodEnd = Date::now();
            $periodInterval = new DateInterval('P1M');
            $period = new DatePeriod($periodStart, $periodInterval, $periodEnd);
            foreach ($period as $month) {
                $startOfMonth = Date::parse($month)->startOfMonth();
                $item = [
                    'date' => clone $startOfMonth,
                    'interval' => $startOfMonth->format('Y-m-d') . ',' . $startOfMonth->addMonth()->format('Y-m-d'),
                ];
                $count = $months->where('interval', data_get($item, 'interval'))->count();
                if ($count === 0) {
                    $months->push($item);
                }
            }
        }

        $months = $months->sortByDesc('date');

        return view('main.templates.operations.historic')
            ->with(compact('transactions', 'operations', 'months', 'filters'));
    }

    public function generateExportPdf(HistoricExportPdfRequest $request, AccountService $accountService): Response
    {
        $transactions = $accountService->fullHistory(
            session('auth.user.id'),
            session('auth.user.account.type.internalName'),
            array_merge($request->filters, ['orderBy' => 'dateAsc'])
        );
        $monthBalance = 0;
        $operationsArray = $transactions->map(function (array $transaction) use (&$monthBalance) {
            if (data_get($transaction, 'relatedAccount.kind') === 'system') {
                $related = data_get($transaction, 'relatedAccount.type.name');
            } else {
                $related = data_get($transaction, 'relatedAccount.user.display');
            }
            $amount = (float) data_get($transaction, 'amount');
            if ($amount > 0) {
                $description = 'De ' . $related . ' - ' . data_get($transaction, 'description');
            } else {
                $description = 'A ' . $related . ' - ' . data_get($transaction, 'description');
            }
            $monthBalance += $amount;

            return [
                'date' => Carbon::parse(data_get($transaction, 'date'))->format('d/m/Y'),
                'description' => $description,
                'amount' => data_get($transaction, 'amount'),
            ];
        });
        $filters = explode(',', data_get($request->filters, 'datePeriod'));
        $beginMonthDate = array_shift($filters);
        $endMonthDate = array_shift($filters);
        $afterMonthTransactions = $accountService->fullHistory(
            session('auth.user.id'),
            session('auth.user.account.type.internalName'),
            [
                'datePeriod' => $endMonthDate . ',',
            ]
        );
        $currentBalance = (float) session('auth.user.account.status.balance');
        $balanceMonthEnd = $afterMonthTransactions->reduce(
            fn(float $balance, array $transaction) => $balance - (float) data_get($transaction, 'amount'),
            $currentBalance
        );
        $balanceMonthBegin = round($balanceMonthEnd - $monthBalance, 2);
        $balanceMonthBegin = $balanceMonthBegin === 0.0 ? abs($balanceMonthBegin) : $balanceMonthBegin;
        $user = CyclosUser::fill(session()->get('auth.user'));
        setlocale(LC_TIME, 'fr_FR.UTF-8');
        $data = [
            'month' => Carbon::parse($beginMonthDate)->formatLocalized('%B %Y'),
            'startDate' => Carbon::parse($beginMonthDate)->format('d/m/Y'),
            'endDate' => Carbon::parse($endMonthDate)->formatLocalized('%d %B %y'),
            'user' => $user,
            'transactions' => $operationsArray,
            'balanceMonthBegin' => $balanceMonthBegin,
            'balanceMonthEnd' => $balanceMonthEnd,
        ];
        $filename = Str::slug(config('somoney.money')) . '-operations_' . Carbon::parse($beginMonthDate)->format('m-Y');

        return PDF::loadView('pdf.report', $data)->download($filename . '.pdf');
    }

    /**
     * Export all transactions in XLS file
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function generateExportXls(Excel $excel, UserOperationsExport $export): BinaryFileResponse
    {
        return $excel->download(
            $export,
            Str::slug(config('somoney.money')) . '-operations_' . now()->format('Y-m-d_h-i-s') . '.xls'
        );
    }
}
