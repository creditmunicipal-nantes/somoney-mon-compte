<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;

class SeoController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function sitemap()
    {
        return response(
            view('main.seo.sitemap'),
            200,
            ['Content-Type' => 'text/xml']
        );
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function robots()
    {
        return response(
            view('main.seo.robots'),
            200,
            ['Content-Type' => 'text/plain']
        );
    }
}
