<?php

namespace App\Http\Controllers\Main\Contact;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contact\SponsorRequest;
use App\Jobs\Sponsors\SendEmailSponsorStore;
use App\Models\CyclosUser;
use App\Services\Cyclos\UserService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class SponsorController extends Controller
{
    public function __construct(protected UserService $userService)
    {
        //
    }

    public function show(): View
    {
        return view('main.templates.sponsor');
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function store(SponsorRequest $request): RedirectResponse
    {
        $user = CyclosUser::fill($this->userService->get(data_get(session()->get('auth.user'), 'id')));
        $this->dispatch(new SendEmailSponsorStore($request->email, $request->name, $request->message, $user));

        return redirect()->back()->with('success', 'Votre message a bien été envoyé à ' . $request->name . ' !');
    }
}
