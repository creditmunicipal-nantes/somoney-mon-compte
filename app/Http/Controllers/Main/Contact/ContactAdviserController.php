<?php

namespace App\Http\Controllers\Main\Contact;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contact\ContactAdviserRequest;
use App\Jobs\Contact\SendEmailAdviser;
use App\Models\CyclosUser;
use App\Services\Cyclos\UserService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ContactAdviserController extends Controller
{
    public function __construct(protected UserService $userService)
    {
    }

    /**
     * @return \Illuminate\Contracts\View\View
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function show(): View
    {
        $user = $this->userService->get(data_get(session()->get('auth.user'), 'id'));

        return view('main.templates.contact', compact('user'));
    }

    /**
     * @param \App\Http\Requests\Contact\ContactAdviserRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function storeIndividual(ContactAdviserRequest $request): RedirectResponse
    {
        $adviserRecipient = config('somoney.mail.adviser-individual');
        $cyclosUser = CyclosUser::fill($this->userService->get(data_get(session()->get('auth.user'), 'id')));
        $this->dispatch(new SendEmailAdviser($adviserRecipient, $request->message, $cyclosUser));

        return redirect()->route('contact')->with('success', 'Votre message a été envoyé avec succès !');
    }

    /**
     * @param \App\Http\Requests\Contact\ContactAdviserRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function storeProfessional(ContactAdviserRequest $request): RedirectResponse
    {
        $adviserRecipient = config('somoney.mail.adviser-professional');
        $cyclosUser = CyclosUser::fill($this->userService->get(data_get(session()->get('auth.user'), 'id')));
        $this->dispatch(new SendEmailAdviser($adviserRecipient, $request->message, $cyclosUser));

        return redirect()->route('contact')->with('success', 'Votre message a été envoyé avec succès !');
    }
}
