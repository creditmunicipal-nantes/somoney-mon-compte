<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Http\Requests\Newsletter\NewsletterStoreUpdateRequest;
use App\Models\CyclosUser;
use App\Models\Newsletter;
use App\Services\Cyclos\UserService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Contracts\View\View;

class NewsletterController extends Controller
{
    public function index(): View
    {
        $user = CyclosUser::fill(session('auth.user'));
        $newsletters = Newsletter::where('cyclos_user_id', $user->id)->get();

        return view('main.templates.newsletter.index', compact('user', 'newsletters'));
    }

    public function create(): View
    {
        $user = CyclosUser::fill(session('auth.user'));

        return view('main.templates.newsletter.edit', compact('user'));
    }

    /**
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     */
    public function store(NewsletterStoreUpdateRequest $request): RedirectResponse
    {
        /** @var Newsletter $newsletter */
        $newsletter = Newsletter::create(array_merge($request->all(), ['cyclos_user_id' => session('auth.user.id')]));

        if ($request->file('newsletter_image')) {
            $newsletter->addMedia($request->file('newsletter_image'))->toMediaCollection('newsletterImage');
        }

        return redirect()
            ->route('newsletter.index')
            ->with(['success' => 'Votre newsletter a été créée.']);
    }

    public function edit(int $newsletterId = null): View|RedirectResponse
    {
        $user = CyclosUser::fill(session()->get('auth.user'));
        $newsletters = Newsletter::where('cyclos_user_id', $user->id)->get();
        $newsletter = $newsletters->last();
        if ($newsletterId !== $newsletter->id && $newsletterId !== null) {
            return redirect()
                ->route('newsletter.index')
                ->with(['error' => 'L\'édition de cette newsletter n\'est pas autorisée.']);
        }

        return view('main.templates.newsletter.edit', compact('newsletter', 'user'));
    }

    /**
     * @param \App\Models\Newsletter $newsletter
     * @param \App\Http\Requests\Newsletter\NewsletterStoreUpdateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig
     */
    public function update(Newsletter $newsletter, NewsletterStoreUpdateRequest $request): RedirectResponse
    {
        $newsletter->update($request->all());

        if ($request->file('newsletter_image')) {
            $newsletter->addMedia($request->file('newsletter_image'))->toMediaCollection('newsletterImage');
        }

        return redirect()->route('newsletter.index')->with(['success' => 'Votre newsletter a été mise à jour']);
    }

    public function delete(Newsletter $newsletter): RedirectResponse
    {
        $newsletter->delete();

        return back()->with(['success' => 'Votre newsletter a été supprimée']);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function unsubscribe(): RedirectResponse
    {
        $cyclosUserEdit = app(UserService::class)->getDataForEdit(session('auth.user')['id']);
        $cyclosUserEdit['user']['customValues']['commercialinfo'] = false;
        app(UserService::class)->update(session('auth.user')['id'], $cyclosUserEdit['user']);
        $success = "Vous êtes désormais désabonné des newsletters et autres communications commerciales";

        return redirect()->route('home')->with(compact('success'));
    }
}
