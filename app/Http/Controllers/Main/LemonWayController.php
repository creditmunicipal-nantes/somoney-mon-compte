<?php

namespace App\Http\Controllers\Main;

use App\Jobs\LemonWay\ApplyTransactionJob;
use App\Jobs\LemonWay\GetMoneyInDetailsJob;
use App\Jobs\LemonWay\GetPaymentDetailsJob;
use App\Jobs\LemonWay\TransactionStatusUpdate;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Services\Cyclos\TransactionService;
use Illuminate\Support\Facades\Bus;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class LemonWayController extends PaymentGatewayController
{
    protected $paymentGateway = 'lemonway';

    /**
     * @param string $context
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Cyclos\TransactionService $transactionService
     *
     * @return \Illuminate\Contracts\View\View
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function success(string $context, Request $request, TransactionService $transactionService): View
    {
        /** @var \App\Models\Transaction $transaction */
        $transaction = Transaction::where('wk_token', $request->response_wkToken)->firstOrFail();
        if (config('lemonway.mode') === 'vad') {
            if (session()->has('registration')) {
                session()->put('local_user_id_create_membership', session('registration.user_id'));
                session()->forget('registration');
            }
            Bus::dispatch(new TransactionStatusUpdate($transaction));
            if (
                $context === Transaction::CONTEXT_MEMBERSHIP
                && $transaction->childs->where('context', Transaction::CONTEXT_REGISTRATION)->isNotEmpty()
            ) {
                $context = Transaction::CONTEXT_REGISTRATION;
            }

            return $this->displaySuccess($context, $this->groupOfUserTransaction($context, $transaction));
        }
        $transaction = $this->checkMoneyInSuccess($transaction);
        if ($transaction->status === Transaction::STATUS_SUCCESS) {
            $transactionToCM = $this->transferToSCWallet($transaction, $transactionService);
            if ($transactionToCM->status === Transaction::STATUS_SUCCESS) {
                return $this->displaySuccess($context, $this->groupOfUserTransaction($context, $transaction));
            }
        }

        return $this->error($context, $this->groupOfUserTransaction($context, $transaction));
    }

    protected function checkMoneyInSuccess(Transaction $transaction): Transaction
    {
        $moneyInDetails = dispatch_now(new GetMoneyInDetailsJob($transaction));

        return $this->checkLemonwayTransactionSuccess($transaction, $moneyInDetails);
    }

    protected function checkLemonwayTransactionSuccess(Transaction $transaction, array $transactionDetails): Transaction
    {
        switch (data_get($transactionDetails, 'status')) {
            case 3:
                $transaction->fill([
                    'lw_executed_at' => data_get($transactionDetails, 'date', now()),
                    'status' => Transaction::STATUS_SUCCESS,
                ]);
                break;
            default: // In case of status `4` or other:
                $transaction->fill([
                    'lw_executed_at' => data_get($transactionDetails, 'date', now()),
                    'status' => Transaction::STATUS_FAILED,
                    'failure_reason' => data_get($transactionDetails, 'error_message', 'Erreur Inconnue'),
                ]);
        }

        return $transaction;
    }

    /**
     * @param \App\Models\Transaction $transaction
     * @param \App\Services\Cyclos\TransactionService $transactionService
     *
     * @return \App\Models\Transaction
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    protected function transferToSCWallet(Transaction $transaction, TransactionService $transactionService): Transaction
    {
        $wkToken = bin2hex(openssl_random_pseudo_bytes(25));
        $transactionToCM = Transaction::create([
            'type' => Transaction::TYPE_LEMONWAY,
            'amount' => $transaction->amount,
            'source_wallet_id' => $transaction->targetWallet->id,
            'target_wallet_id' => Wallet::where('lw_id', 'SC')->first()->id,
            'status' => Transaction::STATUS_PROCESSING,
            'wk_token' => $wkToken,
        ]);
        $transactionToCM = $this->checkPaymentSuccess(dispatch_now(new ApplyTransactionJob($transactionToCM)));
        if ($transactionToCM->status === Transaction::STATUS_SUCCESS) {
            $transactionService->storeInitialAcompte(
                $transaction->targetWallet->cyclos_user_id,
                $transaction->amount / 100,
                'Crédit initial à la création du compte',
                'stableAccount . toIndividual'
            );
        }

        return $transactionToCM;
    }

    protected function checkPaymentSuccess(Transaction $transaction): Transaction
    {
        $paymentDetails = dispatch_now(new GetPaymentDetailsJob($transaction));

        return $this->checkLemonwayTransactionSuccess($transaction, $paymentDetails);
    }

    public function cancel(string $context, Request $request): View
    {
        /** @var \App\Models\Transaction $transaction */
        $transaction = Transaction::where('wk_token', $request->response_wkToken)->firstOrFail();
        $params = [
            'color' => 'secondary',
            'icon' => 'icon-information',
            'message' => 'Le paiement a bien été annulé',
            'redirection' => $this->getRouteFromContext(
                $context,
                $this->groupOfUserTransaction($context, $transaction)
            ),
        ];

        return view('framework.iframes.lemonway', compact('params'));
    }

    protected function setPaymentGateway(): string
    {
        return Transaction::TYPE_LEMONWAY;
    }
}
