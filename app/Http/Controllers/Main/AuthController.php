<?php

namespace App\Http\Controllers\Main;

use App\Exceptions\Cyclos\PasswordException;
use App\Exceptions\Cyclos\PasswordExpiredException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Jobs\SendEmailNeedMembership;
use App\Jobs\SendEmailResumeRegistration;
use App\Jobs\SendEmailValidationCreation;
use App\Jobs\Users\SendEmailValidation;
use App\Models\CyclosUser;
use App\Models\LocalUser;
use App\Models\Notification;
use App\Models\UserToken;
use App\Models\ValidationUser;
use App\Services\Cyclos\AuthService;
use App\Services\Cyclos\User\PasswordService;
use App\Services\Cyclos\UserService;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function __construct(protected AuthService $authService, protected UserService $userService)
    {
    }

    public function getLogin(): View
    {
        $notification = app(Notification::class)
            ->where('groups', Notification::TOPRELOGIN)
            ->where('display', true)->first();

        return view('main.templates.auth.login', compact('notification'));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function postLogin(LoginRequest $request): RedirectResponse
    {
        $redirect = null;
        try {
            $response = $this->authService->login($request->only('login', 'password'));
        } catch (AuthenticationException) {
            $localUser = LocalUser::where('email', $request->login)->first();
            if ($localUser) {
                $this->checkResumeRegister($localUser);

                return back();
            }

            return back()->withErrors(['login' => 'loginError']);
        }

        $user = $this->userService->get(data_get($response, 'user.id'));
        $user['account'] = $this->userService->getUserAccount($user['id']);
        $user['is_admin'] = Str::startsWith(
            data_get($user, 'group.internalName'),
            config('cyclos.internal-names.admin')
        );
        $cyclosUser = CyclosUser::fill($user);
        $redirect = $this->checkIfPasswordExpired($redirect, $cyclosUser);
        $redirect = $this->checkProfessionalAccepted($redirect, $cyclosUser);

        /*if ($cyclosUser->customValues->get('leavecall', false)) {
            return redirect()->back()
                ->withErrors(['login' => 'isLeavingValidationError']);
        }*/

        $redirect = $this->checkAccountValidated($redirect, $cyclosUser);
        if (! $redirect) {
            session()->put('auth', [
                'sessionToken' => data_get($response, 'sessionToken'),
                'user' => $user,
                'role' => data_get($response, 'user.group.internalName', data_get($response, 'role')),
            ]);
            // redirect to previous url
            $loginRedirects = session()->pull('login_redirect');
            if (is_array($loginRedirects)) {
                $loginRedirect = array_shift($loginRedirects);
                if ($loginRedirect !== route('auth.logout')) {
                    return redirect($loginRedirect);
                }
            }

            return session('auth.user.is_admin') ? redirect()->route('admin.home') : redirect()->route('home');
        }

        return $this->checkIfPasswordReset($redirect, $cyclosUser);
    }

    protected function checkResumeRegister(LocalUser $localUser): void
    {
        if (
            array_key_exists(
                'step',
                config('registration.steps.' . $localUser->group . '.by_id')[$localUser->step]
            )
        ) {
            session()->forget('errors');
            session()->flash('registerNotFinished', true);

            $this->dispatch(new SendEmailResumeRegistration($localUser->id));
        } elseif (
            config('somoney.available.services.membership')
            && config("registration.steps.{$localUser->group}.slug_id.membership") === $localUser->step
        ) {
            session()->forget('errors');
            session()->flash('registerNeedMembership', true);

            $this->dispatch(new SendEmailNeedMembership($localUser));
        } elseif (
            config('registration.steps.' . $localUser->group . '.slug_id.validation')
            === $localUser->step
        ) {
            session()->forget('errors');
            session()->flash('registerNotValidated', true);

            $this->dispatch(new SendEmailValidationCreation($localUser->id, true));
        }
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function checkIfPasswordExpired(
        ?RedirectResponse $redirect,
        CyclosUser $cyclosUser
    ): RedirectResponse|null {
        if (! $redirect) {
            try {
                $this->validatePassword(app(PasswordService::class)->get($cyclosUser->id), $cyclosUser->id);
            } catch (PasswordExpiredException) {
                return redirect()->back()
                    ->withErrors(['password' => 'passwordExpired']);
            }
        }

        return $redirect;
    }

    /**
     * @throws \App\Exceptions\Cyclos\PasswordException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     */
    protected function validatePassword(?array $passwordData, string $userId): bool
    {
        if (
            $passwordData
            && ! in_array(data_get($passwordData, 'status'), [
                PasswordService::STATUS_ACTIVE,
                PasswordService::STATUS_RESET,
            ])
        ) {
            if (data_get($passwordData, 'status') === PasswordService::STATUS_EXPIRED) {
                throw new PasswordExpiredException();
            }

            throw new PasswordException(false, json_encode($passwordData) . ' | Cyclos user => ' . $userId);
        }

        return false;
    }

    protected function checkProfessionalAccepted(?RedirectResponse $redirect, CyclosUser $cyclosUser): ?RedirectResponse
    {
        if (! $redirect) {
            if (
                $cyclosUser->group === config('cyclos.internal-names.groups.professional')
                && $cyclosUser->customValues->get('needsValidation') === true
            ) {
                return redirect()->back()
                    ->withErrors(['login' => 'needsValidationError']);
            }
        }

        return $redirect;
    }

    protected function checkAccountValidated(?RedirectResponse $redirect, CyclosUser $cyclosUser): ?RedirectResponse
    {
        if (! $redirect) {
            if (
                ! $cyclosUser->customValues->get('validatedemail', false)
                && ! Str::startsWith($cyclosUser->group, config('cyclos.internal-names.admin'))
            ) {
                session()->flash('user_id', $cyclosUser->id);

                return redirect()->back()
                    ->withErrors(['login' => 'emailValidationError']);
            }
        }

        return $redirect;
    }

    public function checkIfPasswordReset(?RedirectResponse $redirect, CyclosUser $cyclosUser): RedirectResponse
    {
        if (data_get($cyclosUser->customValues, 'passwordReset', false)) {
            $redirect = redirect(route('account.information'))->with(
                'error',
                'Suite à la regénération de votre mot de passe par un administrateur, pensez à le redéfinir !'
            );
        }

        return $redirect;
    }

    public function logged(): View
    {
        return view('main.templates.auth.logged');
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function logout(): RedirectResponse
    {
        try {
            $this->authService->logout(session()->get('auth.sessionToken', ''));
        } catch (AuthenticationException) {
            // already logged out from cyclos
        } finally {
            session()->forget('auth');
        }

        return redirect()->route('auth.login')
            ->with('success', session('success'));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function validationEmail(string $userId): RedirectResponse
    {
        $cyclosUser = CyclosUser::fill($this->userService->get($userId));
        if ($cyclosUser->customValues->get('validatedemail') === true) {
            return redirect()->route('auth.login');
        }
        UserToken::where('user_id', $userId)->delete();
        $token = UserToken::create([
            'user_id' => $userId,
            'type' => UserToken::TYPE_EMAIL_VALIDATION,
        ]);
        $this->dispatch(new SendEmailValidation($cyclosUser->email, $cyclosUser, $token, true));

        return redirect()->route('auth.login')->with('message', 'E-mail de validation envoyé.');
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function validateEmail(UserToken $token): View
    {
        $user = $this->userService->getDataForEdit($token->user_id)['user'];
        $user['customValues']['validatedemail'] = true;

        $this->userService->update($token->user_id, $user);
        $token->delete();
        ValidationUser::where('cyclos_id', $token->user_id)->delete();

        return view('main.templates.auth.login')->with(
            'success',
            'Votre adresse e-mail <b>' . $user['email'] . '</b> est validée.<br/>Vous pouvez maintenant vous connecter.'
        );
    }
}
