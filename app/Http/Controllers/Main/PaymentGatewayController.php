<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Models\CyclosUser;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use App\Services\Cyclos\UserService;

abstract class PaymentGatewayController extends Controller
{
    protected $paymentGateway = 'lemonway';

    public function __construct()
    {
        $this->paymentGateway = $this->setPaymentGateway();
    }

    abstract protected function setPaymentGateway(): string;

    public function error(string $context, Request $request): View
    {
        /** @var \App\Models\Transaction $transaction */
        $transaction = Transaction::where('wk_token', $request->response_wkToken)->firstOrFail();
        $params = [
            'color' => 'red',
            'icon' => 'icon-cross',
            'message' => 'Erreur lors du paiement',
            'redirection' => $this->getRouteFromContext(
                $context,
                $this->groupOfUserTransaction($context, $transaction)
            ),
        ];

        return view('framework.iframes.' . $this->paymentGateway, compact('params'));
    }

    protected function displaySuccess(string $context, string $userType): View
    {
        $params = [
            'color' => 'green',
            'icon' => 'icon-checkmark',
            'message' => 'Paiement accepté',
            'redirection' => $this->getRouteFromContext($context, $userType),
        ];
        if ($context !== Transaction::CONTEXT_REGISTRATION) {
            if ($context === Transaction::CONTEXT_MEMBERSHIP) {
                session()->flash('success', 'Le renouvellement de votre adhésion va être validé dans quelque minutes');
            } else {
                session()->flash('success', 'Votre compte va être crédité dans quelque minutes');
            }
        }

        return view('framework.iframes.' . $this->paymentGateway, compact('params'));
    }

    protected function getRouteFromContext(string $context, string $userType): string
    {
        return match ($context) {
            Transaction::CONTEXT_REGISTRATION => route('register.' . $userType . '.credit.paid'),
            Transaction::CONTEXT_MEMBERSHIP => route('home'),
            default => route('operations.historic.index'),
        };
    }

    protected function groupOfUserTransaction(string $context, Transaction $transaction)
    {
        if ($context === Transaction::CONTEXT_REGISTRATION) {
            return $transaction->localUser->group;
        }

        return CyclosUser::fill(app(UserService::class)->get($transaction->cyclos_user_id))->group;
    }
}
