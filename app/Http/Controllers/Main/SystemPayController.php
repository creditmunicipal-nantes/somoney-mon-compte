<?php

namespace App\Http\Controllers\Main;

use App\Models\Transaction;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class SystemPayController extends PaymentGatewayController
{
    protected $paymentGateway = 'system-pay';

    public function form($initPayment)
    {
        return view('framework.iframes.system-pay', compact('initPayment'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\View\View
     * @throws \JsonException
     */
    public function feedback(Request $request): View
    {
        $paymentData = json_decode($request->get('kr-answer'), true, 512, JSON_THROW_ON_ERROR);
        /** @var \App\Models\Transaction $transaction */
        $transaction = Transaction::where('id', $paymentData['orderDetails']['orderId'])->firstOrFail();
        $context = $transaction->context;
        if (config('system-pay.status')[$paymentData['orderStatus']] === Transaction::STATUS_SUCCESS) {
            if (session()->has('registration')) {
                session()->put('local_user_id_create_membership', session('registration.user_id'));
                session()->forget('registration');
            }
            if (
                $context === Transaction::CONTEXT_MEMBERSHIP
                && $transaction->childs->where('context', Transaction::CONTEXT_REGISTRATION)->isNotEmpty()
            ) {
                $context = Transaction::CONTEXT_REGISTRATION;
            }

            return $this->displaySuccess($context, $this->groupOfUserTransaction($context, $transaction));
        }

        return $this->error($context, $this->groupOfUserTransaction($context, $transaction));
    }

    protected function setPaymentGateway(): string
    {
        return Transaction::TYPE_SYSTEM_PAY;
    }
}
