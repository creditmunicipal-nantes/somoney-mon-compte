<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Http\Requests\Account\UserUpdateRequest;
use App\Http\Requests\Password\NewPasswordRequest;
use App\Jobs\SendEmailLeaveCall;
use App\Jobs\Users\SendEmailValidation;
use App\Models\CyclosUser;
use App\Models\UserToken;
use App\Services\Cyclos\AuthService;
use App\Services\Cyclos\TransferService;
use App\Services\Cyclos\User\AddressService;
use App\Services\Cyclos\User\ImageService;
use App\Services\Cyclos\User\PasswordService;
use App\Services\Cyclos\User\PhoneService;
use App\Services\Cyclos\UserService;
use App\Services\QrCodeService;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;

class AccountController extends Controller
{
    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function get(UserService $userService): View
    {
        $user = CyclosUser::fill(session()->get('auth.user'));
        $dataForEdit = $userService->getDataForEdit($user->id);

        $genders = Arr::first(
            $dataForEdit['customFields'],
            fn($value) => $value['internalName'] === 'gender'
        )['possibleValues'];

        $professionalCategories = Arr::first(
            $dataForEdit['customFields'],
            fn($value) => $value['internalName'] === 'catpro'
        )['possibleValues'];

        return view(
            'main.templates.account.information',
            compact('user', 'genders', 'professionalCategories')
        );
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \App\Exceptions\QrCodeTokenNotFoundException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function update(
        UserUpdateRequest $updateRequest,
        UserService $userService,
        PhoneService $phoneService,
        ImageService $imageService
    ): RedirectResponse {
        $user = CyclosUser::fill(session()->get('auth.user'));

        $requestUser = $updateRequest->get('user');
        $customValues = $requestUser['customValues'];

        $requestUser['customValues']['birthday'] = $updateRequest->birthday->toDateString();
        $requestUser['customValues']['commercialinfo'] = $customValues['commercialinfo'] ? 'true' : 'false';
        $requestUser['customValues']['optinnewsletter'] = $customValues['optinnewsletter'] ? 'true' : 'false';

        $requestUser['name'] = $user->group === 'professional'
            ? $requestUser['name']
            : $customValues['firstname'] . ' ' . $customValues['lastname'];

        if ($updateRequest->hasFile('avatar')) {
            $imageService->setUserImage('self', $updateRequest->file('avatar'));
        }

        $phoneService->setUserPhone('self', $updateRequest->phone);

        if ($updateRequest->hasFile('idattachment')) {
            $requestUser['customValues']['idattachment'] = $this->storeRequestFile(
                $updateRequest,
                $user->id,
                'idattachment'
            );
        }

        $this->setAddress($user, $updateRequest);
        $userService->setUserData($user->id, $requestUser);
        // Remove qr code images
        File::delete(app(QrCodeService::class)->getCyclosUserQrCodeTokenImage($user, false));
        File::delete(app(QrCodeService::class)->getCyclosUserQrCodeTokenPoster($user, false));

        if (session('newEmail')) {
            $token = UserToken::create([
                'user_id' => $user->id,
                'type' => UserToken::TYPE_EMAIL_VALIDATION,
            ]);
            $this->dispatch(new SendEmailValidation($user->email, $user, $token));

            return redirect()->route('auth.logout')->with(
                'success',
                'Vos modifications ont été enregistrées, veuillez maintenant valider votre nouvelle adresse e-mail.'
            );
        }

        return redirect()->route('account.information')
            ->with('success', 'Vos modifications ont été enregistrées.');
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function delete(UserService $userService, TransferService $transferService): RedirectResponse
    {
        $balance = session('auth.user.account.status.balance');

        $transfers = $transferService->listAll([
            'fromAccountType' => 'stableAccount',
            'user' => session('auth.user.id'),
            'fields' => 'amount',
        ]);

        $credit = $transfers->reduce(function ($carry, $value) {
            $carry += data_get($value, 'amount');

            return $carry;
        }, 0);

        $user = $userService->getDataForEdit(session()->get('auth.user.id'))['user'];
        $user['customValues']['leavecall'] = "true";
        $userService->update(session()->get('auth.user.id'), $user);

        $success = 'Votre compte va être supprimé. Pour toute question, merci de contacter un administrateur '
            . config('somoney.money') . '.';

        if ($balance > $credit) {
            $success = 'Votre solde de ' . config('somoney.money') . " est supérieur au montant que vous avez
                crédité, vous serez remboursé à hauteur de $credit. Vous pourrez utiliser ce montant en "
                . config('somoney.money') . '.';
        }

        $this->dispatch(new SendEmailLeaveCall($user));

        return redirect()->back()->with('success', $success);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function storeNewPassword(
        UserService $userService,
        AuthService $authService,
        NewPasswordRequest $request
    ): RedirectResponse {
        $login = session()->get('auth.user.email');

        try {
            $authService->login([
                'login' => $login,
                'password' => $request->get('old_password'),
            ]);
        } catch (AuthenticationException $exception) {
            return redirect()->back()->withErrors(['login' => 'loginError']);
        }

        (new PasswordService())->update(session()->get('auth.user.id'), $request->get('password'));

        $authService->logout(session()->get('auth.sessionToken', ''));

        session()->forget('auth');

        $response = $authService->login([
            'login' => $login,
            'password' => $request->get('password'),
        ]);

        session()->put('auth', [
            'sessionToken' => data_get($response, 'sessionToken'),
            'user' => $userService->get(data_get($response, 'user.id')),
            'role' => data_get($response, 'user.group.internalName', data_get($response, 'role')),
        ]);

        $dataForEditUser = $userService->getDataForEdit(session('auth.user')['id'])['user'];

        if (
            data_get($dataForEditUser['customValues'], 'passwordReset', false)
            && filter_var($dataForEditUser['customValues']['passwordReset'], FILTER_VALIDATE_BOOLEAN) === true
        ) {
            $dataForEditUser['customValues']['passwordReset'] = false;
            $userService->update(session('auth.user')['id'], $dataForEditUser);
        }

        $success = 'Votre mot de passe a été modifié.';

        return redirect()->route('account.information')->with('success', $success);
    }

    protected function storeRequestFile(
        FormRequest $request,
        string $userId,
        string $fileName
    ): string|null {
        if (! $request->hasFile($fileName)) {
            return null;
        }
        $file = $request->file($fileName);
        $fileExtension = $file->getClientOriginalExtension();
        $path = $file->storePubliclyAs('legal_documents/' . $userId, $fileName . '.' . $fileExtension);

        return (string) url($path);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function setAddress(
        CyclosUser $user,
        UserUpdateRequest $updateRequest
    ): void {
        $manualAddress = [
            'addressLine1' => $updateRequest->addressLine1,
            'zip' => $updateRequest->zip,
            'city' => $updateRequest->city,
        ];
        $gmapAddress = [];

        if ($updateRequest->gmap) {
            $gmapAddress = [
                'addressLine1' => data_get($updateRequest->gmap, 'addressLine1'),
                'zip' => data_get($updateRequest->gmap, 'zip'),
                'city' => data_get($updateRequest->gmap, 'city'),
                'location' => Arr::only($updateRequest->gmap, ['latitude', 'longitude']),
            ];
        }

        if (! data_get($gmapAddress, 'addressLine1')) {
            $newAddress = array_merge($gmapAddress, $manualAddress);
        } else {
            $newAddress = array_merge($manualAddress, $gmapAddress);
        }

        if (
            count(array_diff_assoc(
                Arr::except($newAddress, ['location']),
                Arr::only($user->address, ['addressLine1', 'zip', 'city'])
            ))
            || count(array_diff_assoc(data_get($newAddress, 'location'), data_get($user->address, 'location')))
        ) {
            app(AddressService::class)->setUserAddress($user->id, $newAddress);
        }
    }
}
