<?php

namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use App\Http\Requests\Password\NewPasswordAndCGURequest;
use App\Http\Requests\Password\PasswordChangeStorageRequest;
use App\Http\Requests\Password\PasswordRequestStoreRequest;
use App\Jobs\Password\SendEmailResetPassword;
use App\Models\CyclosUser;
use App\Models\UserToken;
use App\Services\Cyclos\AuthService;
use App\Services\Cyclos\User\PasswordService;
use App\Services\Cyclos\UserService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class PasswordController extends Controller
{
    public function __construct(
        protected UserService $userService,
        protected AuthService $authService,
        protected PasswordService $passwordService
    ) {
    }

    public function request(string $type): View
    {
        return view('main.templates.password.request', compact('type'));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function requestStore(string $type, PasswordRequestStoreRequest $request): RedirectResponse
    {
        $search = $this->userService->search(['keywords' => $request->email]);
        if ($search->total() === 0) {
            return redirect()
                ->route('password.request', ['type' => config('auth.passwords.reset.forgot')])
                ->withErrors('Aucun utilisateur avec cette adresse e-mail n\'a été trouvé');
        }
        $user = CyclosUser::fill($this->userService->get(data_get($search->items(), '0.id')));
        $token = UserToken::create([
            'user_id' => $user->id,
            'type' => UserToken::TYPE_RESET_PASSWORD,
        ]);
        $this->dispatch(new SendEmailResetPassword($request->email, $user, $token, $type));

        return redirect()->route('auth.login')
            ->with('success', 'Un e-mail vient d\'être envoyé pour vous permettre de changer votre mot de passe.');
    }

    public function change(UserToken $resetToken): View
    {
        return view('main.templates.password.change', compact('resetToken'));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Exception
     */
    public function changeStore(UserToken $resetToken, PasswordChangeStorageRequest $request): RedirectResponse
    {
        $userId = $resetToken->user_id;
        $this->passwordService->update($userId, $request->password);
        $resetToken->delete();
        $user = $this->userService->getDataForEdit($userId)['user'];
        if (! data_get($user, 'customValues.validatedemail', false)) {
            $user['customValues']['validatedemail'] = true;

            $this->userService->update($userId, $user);
        }
        if (data_get($user, 'customValues.passwordReset', false)) {
            $user['customValues']['passwordReset'] = false;

            $this->userService->update($userId, $user);
        }

        return redirect()->route('auth.login')
            ->with('success', 'Votre mot de passé a été modifié, vous pouvez maintenant vous connecter.');
    }

    public function newPasswordAndCGU(UserToken $token): View
    {
        return view('main.templates.password.newPasswordCgu', compact('token'));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Exception
     */
    public function storeNewPasswordAndCGU(NewPasswordAndCGURequest $request, UserToken $token): RedirectResponse
    {
        $user = $this->userService->search([
            'profileFields' => [
                'email' => $token->user_id,
            ],
        ]);
        $user = $user->first();
        if ($user) {
            $userId = $user['id'];
            $this->passwordService->update($userId, $request->password);
            $user = $this->userService->getDataForEdit($userId)['user'];
            $user['customValues']['validatedemail'] = true;
            $this->userService->update($userId, $user);
            $token->delete();
            $response = $this->authService->login([
                'login' => data_get($user, 'email'),
                'password' => $request->password,
            ]);
            $user = $this->userService->get($userId);
            session()->put('auth', [
                'sessionToken' => data_get($response, 'sessionToken'),
                'user' => $user,
                'role' => data_get($response, 'user.group.internalName', data_get($response, 'role')),
            ]);

            return redirect()->route('home');
        }

        return redirect()->back()->withErrors([
            'msg' => 'L\'email n\'a pas pu être trouvé dans la base de données',
        ]);
    }
}
