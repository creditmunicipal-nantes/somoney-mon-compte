<?php

namespace App\Http\Controllers\Main\Counter;

use App\Http\Controllers\Controller;
use App\Http\Requests\Counter\TransferBanknotesRequest;
use App\Http\Requests\Counter\UpdateStockBanknotesRequest;
use App\Models\BanknotesStock;
use App\Models\BanknotesTransaction;
use App\Models\Counter;
use App\Services\Cyclos\TransactionService;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class BanknotesController extends Controller
{
    public function show(Counter $counter): View
    {
        return view('main.templates.counter.banknotes', compact('counter'));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function transfer(Counter $counter, TransferBanknotesRequest $request): RedirectResponse
    {
        $transaction = new BanknotesTransaction([
            'operator_cyclos_id' => session('auth.user.id'),
            'user_cyclos_id' => $request->user,
            'counter_id' => $counter->id,
            'type' => $request->type,
            'details' => $request->banknotes,

        ]);
        if ($transaction->amount > $counter->stock_balance) {
            return redirect()->back()->with('error', 'Le stock est insuffisant pour cette transaction');
        }

        $transaction->save();
        if ($request->type === 'in') {
            app(TransactionService::class)->returnBanknotes($transaction->amount, $transaction->description);
        } elseif ($request->type === 'out') {
            app(TransactionService::class)->outputBanknotes($transaction->amount, $transaction->description);
        }

        $money = config('somoney.money');
        $detailsTxt = "<br><br>";
        foreach ($transaction->details as $value => $quantity) {
            if ($quantity) {
                $detailsTxt .= "<b>$quantity</b> x <b>$value <span class=\"c-icon icon-money\"></span></b><br>";
            }
        }
        session()->flash(
            'banknote-transfer-succeed',
            "Vous avez transféré <b>{$transaction->amount}</b> $money<br/>
             à <b>{$transaction->cyclos_user->name}</b>$detailsTxt<hr/>"
        );

        return redirect()->back();
    }

    public function stock(Counter $counter, UpdateStockBanknotesRequest $request): RedirectResponse
    {
        BanknotesStock::create([
            'stock' => $request->stock,
            'counter_id' => $counter->id,
        ]);

        return redirect()->route('counter.show', ['counter' => $counter]);
    }
}
