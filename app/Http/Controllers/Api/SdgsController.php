<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\SearchSdgRequest;
use App\Models\Sdg;
use Illuminate\Http\JsonResponse;

class SdgsController extends Controller
{
    public function search(SearchSdgRequest $request): JsonResponse
    {
        $sdgs = [];
        if ($request->category_id) {
            $sdgs = Sdg::with('professionals')
                ->where('active', true)
                ->where('category_id', $request->category_id)
                ->where('title', 'ilike', '%' . str_replace(' ', '%', $request->get('search', '')) . '%')
                ->get()
                ->filter(fn(Sdg $sdg) => $sdg->professionals()->doesntExist())->values();
        }

        return response()->json($sdgs);
    }
}
