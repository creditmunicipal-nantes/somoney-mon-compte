<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Insee\InseeSearchRequest;
use App\Services\InseeService;
use Illuminate\Http\JsonResponse;

class InseeController extends Controller
{
    public function __construct(protected InseeService $inseeService)
    {
        if (config('registration.available.professional.siren') && ! $inseeService->hasApiKey()) {
            abort(503, 'No api key has been set for using insee api');
        }
    }

    /** @throws \GuzzleHttp\Exception\GuzzleException */
    public function search(InseeSearchRequest $request): JsonResponse
    {
        if ($request->siren) {
            $companies = [$this->inseeService->getCompanySiretWithSiren($request->siren)];
        } else {
            $companies = $this->inseeService->getCompanyFromName($request->q);
        }

        return response()->json($companies);
    }

    /** @throws \GuzzleHttp\Exception\GuzzleException */
    public function details(string $siren): JsonResponse
    {
        return response()->json($this->inseeService->getCompanyFromSiren($siren));
    }
}
