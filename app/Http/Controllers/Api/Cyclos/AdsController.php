<?php

namespace App\Http\Controllers\Api\Cyclos;

use App\Http\Controllers\Controller;
use App\Services\Cyclos\AdService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class AdsController extends Controller
{
    public function __construct(protected AdService $adService)
    {
        //
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index(Request $request): JsonResponse
    {
        $filters = [
            'page' => $request->get('page'),
            'pageSize' => $request->get('pageSize'),
        ];
        $adsPaginate = $this->adService->list($filters);

        if ($adsPaginate->isNotEmpty()) {
            $ads = $adsPaginate->getCollection()->map(fn($ad) => $this->adService->getAsAdmin($ad['id']));

            return response()->json(new LengthAwarePaginator(
                $ads,
                $adsPaginate->total(),
                min([$adsPaginate->perPage(), $adsPaginate->total()]),
                $adsPaginate->currentPage(),
                ['path' => $request->url()]
            ));
        }

        return response()->json();
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function show(string $dealId): JsonResponse
    {
        $deal = $this->adService->getAsAdmin($dealId);

        return response()->json($deal);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function categories(): JsonResponse
    {
        $data = $this->adService->dataForSearch();
        $categories = data_get($data, 'categories');

        return response()->json($categories);
    }
}
