<?php

namespace App\Http\Controllers\Api\Cyclos;

use App\Http\Controllers\Controller;
use App\Services\Cyclos\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;

class CyclosUserController extends Controller
{
    public function __construct(protected UserService $userService)
    {
        //
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index(Request $request): JsonResponse
    {
        $filters = [
            'groups' => $request->get('groups'),
            'page' => $request->get('page'),
            'pageSize' => $request->get('pageSize'),
            'includeGroup' => true,
            'address' => 'all',
            'profileFields' => $request->get('profileFields'),
            'usersIdsToInclude' => $request->included_users_ids,
        ];
        $usersPaginate = $this->userService->search($filters);
        if ($usersPaginate->isNotEmpty()) {
            $users = $usersPaginate->getCollection()->transform(function (array $company): array {
                $company['customValues'] = data_get($this->userService->get($company['id']), 'customValues');

                return $company;
            });

            return response()->json(new LengthAwarePaginator(
                $users,
                $usersPaginate->total(),
                min([$usersPaginate->perPage(), $usersPaginate->total()]),
                $usersPaginate->currentPage(),
                ['path' => $request->url()]
            ));
        }

        return response()->json();
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function show(string $userId): JsonResponse
    {
        $user = $this->userService->get($userId);

        return response()->json($user);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function categories(): JsonResponse
    {
        $data = $this->userService->getDataForSearch('customFields');
        $categories = Arr::where(
            data_get($data, 'customFields'),
            static fn($value) => data_get($value, 'internalName') === 'catpro'
        );

        return response()->json(data_get(array_pop($categories), 'possibleValues'));
    }
}
