<?php

namespace App\Http\Controllers\Api\Statistics;

use App\Services\Cyclos\TransferService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;

class OperationController extends StatisticsController
{
    public function __construct(protected TransferService $transferService)
    {
    }

    /** @throws \Exception */
    public function index(Request $request): JsonResponse
    {
        if ($request->get('begin') && $request->get('end') && $request->get('type')) {
            $dateBegin = Carbon::parse($request->get('begin'));
            $dateEnd = Carbon::parse($request->get('end'));

            $params = [
                'datePeriod' => $dateBegin->toDateString() . ',' . $dateEnd->toDateString(),
            ];

            $intervals = $this->getDatesRange($dateBegin, $dateEnd);

            $channelParamName = $this->getChannelParamName($request->get('type'));

            $professionalOperations = $this->transferService->listAll(array_replace_recursive($params, [
                $channelParamName => 'professional',
            ]))->sortBy('date')
                ->groupBy(
                    fn($operation) => Carbon::createFromFormat('Y-m-d\TH:i:s.uP', $operation['date'])->toDateString()
                )
                ->map(fn($operations) => $operations->sum('amount'));

            $individualOperations = $this->transferService->listAll(array_replace_recursive($params, [
                $channelParamName => 'individual',
            ]))->sortBy('date')
                ->groupBy(
                    fn($operation) => Carbon::createFromFormat('Y-m-d\TH:i:s.uP', $operation['date'])->toDateString()
                )
                ->map(fn($operations) => $operations->sum('amount'));

            $individualOperations = $this->addMissingDateKeys($individualOperations, $intervals);
            $professionalOperations = $this->addMissingDateKeys($professionalOperations, $intervals);

            return response()->json([
                'individuals' => $individualOperations,
                'professionals' => $professionalOperations,
                'intervals' => $intervals,
            ]);
        }

        return response()->json([]);
    }

    protected function getChannelParamName(string $type): string
    {
        if ($type && $type === 'debit') {
            return 'fromAccountType';
        }

        return 'toAccountType';
    }
}
