<?php

namespace App\Http\Controllers\Api\Statistics;

use App\Http\Controllers\Controller;
use Illuminate\Support\Carbon;
use DateInterval;
use DatePeriod;
use Illuminate\Support\Collection;

class StatisticsController extends Controller
{
    /** @throws \Exception */
    protected function getDatesRange(Carbon $startDate, Carbon $endDate): array
    {
        $dates = [];
        $interval = new DateInterval('P1D');
        $endDate->add($interval);
        $range = new DatePeriod($startDate, $interval, $endDate);
        foreach ($range as $date) {
            $dates[] = $date->format('Y-m-d');
        }

        return $dates;
    }

    protected function addMissingDateKeys(Collection $data, array $intervals): array
    {
        foreach ($intervals as $value) {
            if (! $data->has($value)) {
                $data->put($value, 0);
            }
        }
        $data = $data->toArray();
        ksort($data);

        return $data;
    }
}
