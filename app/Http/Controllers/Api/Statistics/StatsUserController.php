<?php

namespace App\Http\Controllers\Api\Statistics;

use App\Services\Cyclos\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class StatsUserController extends StatisticsController
{
    public function __construct(protected UserService $userService)
    {
        //
    }

    public function usersCount(Request $request): JsonResponse
    {
        $params = ['groups' => 'individual,professional'];
        $dateBegin = $request->get('begin');
        $dateEnd = $request->get('end');
        if ($dateBegin && $dateEnd) {
            $params = array_replace_recursive($params, [
                'datePeriod' => $dateBegin . ',' . $dateEnd,
            ]);
        }
        $users = $this->userService->searchAll($params)->reduce(function ($carry, $user) {
            $carry[$user['group']['internalName']] += 1;

            return $carry;
        }, [
            'individual' => 0,
            'professional' => 0,
        ]);

        return response()->json($users);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function registers(Request $request): JsonResponse
    {
        if ($request->get('begin') && $request->get('end')) {
            $dateBegin = Carbon::parse($request->get('begin'));
            $dateEnd = Carbon::parse($request->get('end'));
            $intervals = $this->getDatesRange($dateBegin, $dateEnd);
            $individuals = collect($this->userService->getUsersNewRecent('individual'))
                ->reduce(function (Collection $days, $day) {
                    $days[$day['date']] = intval($day['count']);

                    return $days;
                }, collect());
            $professionals = collect($this->userService->getUsersNewRecent('professional'))
                ->reduce(function (Collection $days, $day) {
                    $days[$day['date']] = intval($day['count']);

                    return $days;
                }, collect());
            $individuals = $this->addMissingDateKeys($individuals, $intervals);
            $professionals = $this->addMissingDateKeys($professionals, $intervals);

            return response()->json([
                'intervals' => $intervals,
                'individuals' => $individuals,
                'professionals' => $professionals,
            ]);
        }

        return response()->json([]);
    }
}
