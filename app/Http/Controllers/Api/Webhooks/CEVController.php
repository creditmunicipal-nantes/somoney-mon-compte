<?php

namespace App\Http\Controllers\Api\Webhooks;

use App\Exceptions\Cyclos\ValidationException;
use App\Http\Controllers\Controller;
use App\Services\Cyclos\TransactionService;
use App\Services\Cyclos\UserService;
use Exception;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use SimpleXMLElement;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CEVController extends Controller
{
    public const NOT_ENOUGH_CREDITS = 'NOT_ENOUGH_CREDITS';

    public const PROCESSED = 'PROCESSED';

    public const RECEIVER_UPPER_CREDIT_LIMIT_REACHED = 'RECEIVER_UPPER_CREDIT_LIMIT_REACHED';

    public const UNKNOWN_ERROR = 'UNKNOWN_ERROR';

    public function get(Request $request): Response
    {
        if ($request->exists('wsdl')) {
            return response(view('webhooks.wsdl.payment'))
                ->header('Content-Type', 'text/xml');
        }

        throw new NotFoundHttpException();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Services\Cyclos\UserService $userService
     * @param \App\Services\Cyclos\TransactionService $transactionService
     *
     * @return mixed
     */
    public function store(Request $request, UserService $userService, TransactionService $transactionService)
    {
        $transfer = $status = null;
        $xmlElement = new SimpleXmlElement($request->getContent());
        $paymentParams = $xmlElement->children('soap', true)
            ->Body
            ->children('ns2', true)
            ->doPayment
            ->children()
            ->params;

        try {
            try {
                $fromUser = $userService->getByCevId($paymentParams->fromMember);
                $toUser = $userService->getByCevId($paymentParams->toMember);
            } catch (Exception $exception) {
                throw new Exception(json_encode(['code' => 'invalidAccessClient']));
            }

            if (! $fromUser || ! $toUser) {
                throw new Exception(json_encode(['code' => 'invalidAccessClient']));
            }

            $type = $this->getPaymentType($fromUser);

            Log::info('Payment type : ' . $type);

            $transfer = $transactionService->performAsUser(
                $fromUser['id'],
                $toUser['id'],
                (float) $paymentParams->amount,
                'Paiement par carte'
            );

            $status = self::PROCESSED;
        } catch (ValidationException $exception) {
            Log::info('Failed processing: ' . json_encode($exception->getMessageBag()->toArray()));
            $status = self::UNKNOWN_ERROR;
        } catch (RequestException $exception) {
            try {
                $jsonMessage = json_decode($exception->getResponse()->getBody(), true);

                if (! key_exists('code', $jsonMessage)) {
                    $status = self::UNKNOWN_ERROR;
                } else {
                    $error = $jsonMessage['code'];
                    Log::info('Error : ' . $error);

                    $status = $this->getStatusFail($error);
                }
            } catch (Exception $exception) {
                Log::info('Unknown_error : ' . $exception::class);
                Log::info($exception->getCode() . ' ' . $exception->getMessage());
                $status = self::UNKNOWN_ERROR;
            }
        } catch (Exception $exception) {
            Log::info('Unknown_error : ' . $exception::class);
            Log::info($exception->getCode() . ' ' . $exception->getMessage());
            $status = self::UNKNOWN_ERROR;
        }

        return response(view(
            'webhooks.payment-response',
            compact('status', 'transfer')
        ))->header('Content-Type', 'text/xml');
    }

    /**
     * @param array $fromUser
     *
     * @return string
     */
    protected function getPaymentType(array $fromUser)
    {
        if ($fromUser['group']['internalName'] === config('cyclos.internal-names.groups.professional')) {
            return 'professional.toProfessional';
        }

        return 'individual.toProfessional';
    }

    /**
     * @param string $error
     *
     * @return string
     */
    protected function getStatusFail(string $error)
    {
        return match ($error) {
            'insufficientBalance' => self::NOT_ENOUGH_CREDITS,
            'destinationUpperLimitReached' => self::RECEIVER_UPPER_CREDIT_LIMIT_REACHED,
            default => self::UNKNOWN_ERROR,
        };
    }
}
