<?php

namespace App\Http\Controllers\Api\Webhooks;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Webhooks\NotificationPaymentMobileRequest;
use App\Jobs\Fidelity\ApplyFidelity;
use App\Jobs\SendEmailsTransferMobile;
use Symfony\Component\HttpFoundation\Response;

class NotificationPaymentController extends Controller
{
    /**
     * @param \App\Http\Requests\Api\Webhooks\NotificationPaymentMobileRequest $request
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function mobile(NotificationPaymentMobileRequest $request)
    {
        $this->dispatch(new SendEmailsTransferMobile($request->transferId));
        if (config('somoney.available.services.fidelity')) {
            $this->dispatch(new ApplyFidelity($request->transferId));
        }

        return response('', Response::HTTP_OK);
    }
}
