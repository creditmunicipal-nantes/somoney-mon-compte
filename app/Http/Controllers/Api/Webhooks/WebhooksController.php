<?php

namespace App\Http\Controllers\Api\Webhooks;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class WebhooksController extends Controller
{
    public function index(): View
    {
        return view('webhooks.services');
    }

    public function payment(Request $request): ?Response
    {
        if ($request->exists('wsdl')) {
            return response()->view(
                'wsdl.payment',
                [],
                HttpResponse::HTTP_OK,
                ['Content-Type' => 'text/xml']
            );
        }
        throw new NotFoundHttpException();
    }
}
