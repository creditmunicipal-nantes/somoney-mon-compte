<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Filters\CityFilter;
use App\Models\City;

class CityController extends Controller
{
    /**
     * @param \App\Http\Filters\CityFilter $filter
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(CityFilter $filter)
    {
        $cities = City::filter($filter)
            ->orderBy('name')
            ->get();

        return response()->json($cities);
    }
}
