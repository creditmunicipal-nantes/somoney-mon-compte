<?php

namespace App\Http\Controllers\Api\DataStudio;

class ExportsController
{
    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function operations()
    {
        return response()->download(
            storage_path('app/exports/data-studio/operations.json'),
            'export-operations-data-studio.json'
        );
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function members()
    {
        return response()->download(
            storage_path('app/exports/data-studio/members.json'),
            'export-members-data-studio.json'
        );
    }
}
