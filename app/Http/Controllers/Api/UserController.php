<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\IndexIndividualRequest;
use App\Http\Requests\Api\UserFindOneRequest;
use App\Models\BanknotesTransaction;
use App\Services\Cyclos\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function __construct(protected UserService $userService)
    {
    }

    public function searchProfessionals(Request $request): JsonResponse
    {
        $filters = [
            'groups' => config('cyclos.internal-names.groups.professional'),
            'usersIdsToExclude' => $request->excluded_users_ids,
            'profileFields' => [
                'leavecall' => 'false',
                'visible' => 'true',
            ],
        ];
        if ($request->has('email')) {
            $filters['profileFields']['email'] = $request->email;
        } else {
            $filters['keywords'] = $request->name;
        }
        $professionals = $this->userService->searchAll($filters)
            ->filter(function ($professional) use ($request) {
                if ($request->has('email')) {
                    return Str::contains(
                        strtolower(data_get($professional, 'email')),
                        strtolower($request->email)
                    );
                }

                return Str::contains(
                    strtolower(data_get($professional, 'name')),
                    strtolower($request->name)
                );
            })
            ->transform(fn($professional) => [
                'id' => data_get($professional, 'email'),
                'name' => data_get($professional, 'name'),
            ])
            ->toArray();

        $professionals = collect(array_values($professionals));
        $nbProfessionals = count($professionals);
        $result = new LengthAwarePaginator(
            $professionals,
            $nbProfessionals,
            $nbProfessionals > 0 ? $nbProfessionals : 1,
            1
        );

        return response()->json($result);
    }

    public function searchIndividuals(IndexIndividualRequest $request): JsonResponse
    {
        $searchData = [
            'keywords' => $request->email,
            'groups' => config('cyclos.internal-names.groups.individual'),
            'usersIdsToExclude' => $request->excluded_users_ids,
            'profileFields' => [
                'leavecall' => 'false',
            ],
        ];

        $individuals = $this->userService->searchAll($searchData)
            ->transform(fn($individual) => [
                'id' => data_get($individual, 'email'),
                'name' => data_get($individual, 'name'),
            ])
            ->toArray();

        $individuals = collect(array_values($individuals));

        $nbIndividuals = count($individuals);
        $result = new LengthAwarePaginator(
            $individuals,
            $nbIndividuals,
            $nbIndividuals > 0 ? $nbIndividuals : 1,
            1
        );

        return response()->json($result);
    }

    public function search(Request $request): JsonResponse
    {
        $groups = [
            config('cyclos.internal-names.groups.professional'),
            config('cyclos.internal-names.groups.individual'),
        ];
        $filters = [
            'keywords' => $request->name,
            'usersIdsToExclude' => $request->excluded_users_ids,
            'profileFields' => [
                'leavecall' => 'false',
            ],
        ];
        if ($request->only_members) {
            $filters['groups'] = implode(',', $groups);
        }

        $users = $this->userService->searchAll($filters)
            ->filter(function ($user) use ($request) {
                if (data_get($user, 'group.internalName') === 'professional') {
                    return Str::contains(
                        strtolower(data_get($user, 'name')),
                        strtolower($request->name)
                    );
                }

                return true;
            })
            ->transform(function ($user) {
                if (data_get($user, 'group.internalName') === 'professional') {
                    return [
                        'id' => data_get($user, 'id'),
                        'name' => data_get($user, 'name'),
                    ];
                }

                return [
                    'id' => data_get($user, 'id'),
                    'name' => data_get($user, 'name'),
                ];
            })
            ->toArray();

        if (count($users) === 0) {
            return response()->json([]);
        }

        $users = collect(array_values($users));

        if ($request->banknote_balance) {
            $users = $users->map(function (array $user) {
                $user['banknotes_balance'] = BanknotesTransaction::getUserBalance($user['id']);

                return $user;
            });
        }

        $nbUsers = count($users);
        $result = new LengthAwarePaginator(
            $users,
            $nbUsers,
            $nbUsers > 0 ? $nbUsers : 1,
            1
        );

        return response()->json($result);
    }

    /**
     * @param \App\Http\Requests\Api\UserFindOneRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function findOne(UserFindOneRequest $request): JsonResponse
    {
        $user = $this->userService->get($request->id);

        return response()->json($user);
    }
}
