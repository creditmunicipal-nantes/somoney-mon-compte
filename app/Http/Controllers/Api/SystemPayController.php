<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\SystemPay\TransactionCallback;
use App\Services\SystemPayService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Sentry\State\Scope;

use function Sentry\configureScope;

class SystemPayController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Lyra\Exceptions\LyraException
     * @throws \JsonException
     */
    public function callback(Request $request): Response
    {
        configureScope(static function (Scope $scope): void {
            $scope->setExtra('kr-answer', json_decode(request()->get('kr-answer'), true, 512, JSON_THROW_ON_ERROR));
        });
        if (! app(SystemPayService::class)->checkHash()) {
            app('sentry')->captureMessage('SystemPay IPN, invalid signature.');
            abort(403, 'Invalid hash.');
        }
        $paymentData = json_decode($request->get('kr-answer'), true, 512, JSON_THROW_ON_ERROR);
        TransactionCallback::dispatch($paymentData);

        return response('succeed');
    }
}
