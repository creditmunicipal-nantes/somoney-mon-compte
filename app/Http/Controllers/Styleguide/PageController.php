<?php

namespace App\Http\Controllers\Styleguide;

use App\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        return view('styleguide.templates.home');
    }
}
