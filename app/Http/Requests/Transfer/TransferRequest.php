<?php

namespace App\Http\Requests\Transfer;

use Illuminate\Support\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class TransferRequest extends FormRequest
{
    protected function prepareForValidation(): void
    {
        $this->merge([
            'recurrent' => filter_var($this->recurrent, FILTER_VALIDATE_BOOLEAN),
            'date' => $this->date ? rescue(
                fn() => Carbon::createFromFormat('d/m/Y', $this->date),
                'XXX',
                false
            ) : null,
            'date_end' => $this->date_end ? rescue(
                fn() => Carbon::createFromFormat('d/m/Y', $this->date_end),
                'XXX',
                false
            ) : null,
        ]);
    }

    public function rules(): array
    {
        return [
            'profil' => ['required', 'string'],
            'amount' => ['required', 'numeric'],
            'professional_email' => ['required_if:profil,"professional"', 'email'],
            'individual_email' => ['required_if:profil,"individual"', 'email'],
            'date' => ['required', 'date', 'after_or_equal:today'],
            'comment' => ['required', 'string'],
            'recurrent' => [
                'required',
                'boolean',
                'cyclos.transfer.min_occurrence',
                'cyclos.transfer.first_occurrence',
            ],
            'frequency' => [$this->recurrent ? 'required' : 'nullable', 'string'],
            'date_end' => [$this->recurrent ? 'required' : 'nullable', 'date'],
        ];
    }

    public function messages(): array
    {
        return [
            'date.after_or_equal' => __('validation.after_or_equal', [
                'attribute' => __('validation.attributes.date'),
                'date' => 'aujourd\'hui.',
            ]),
            'professional_email.required' => __('validation.required_if', [
                'attribute' => __('validation.attributes.professional_email'),
                'other' => __('validation.attributes.profil'),
                'value' => __('validation.attributes.professional'),
            ]),
            'individual_email.required' => __('validation.required_if', [
                'attribute' => __('validation.attributes.individual_email'),
                'other' => __('validation.attributes.profil'),
                'value' => __('validation.attributes.individual'),
            ]),
        ];
    }
}
