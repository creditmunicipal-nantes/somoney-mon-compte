<?php

namespace App\Http\Requests\Contact;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequestRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'profil' => ['required'],
            'professional_email' => ['required_if:profil,"professional"', 'email'],
            'individual_email' => ['required_if:profil,"individual"', 'email'],
            'amount' => ['required'],
            'title' => ['required'],
        ];
    }
}
