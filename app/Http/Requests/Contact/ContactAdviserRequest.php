<?php

namespace App\Http\Requests\Contact;

use Illuminate\Foundation\Http\FormRequest;

class ContactAdviserRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'message' => ['required'],
        ];
    }
}
