<?php

namespace App\Http\Requests\Password;

use Illuminate\Foundation\Http\FormRequest;

class NewPasswordRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'old_password' => ['required'],
        ];
    }
}
