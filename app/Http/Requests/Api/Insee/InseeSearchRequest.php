<?php

namespace App\Http\Requests\Api\Insee;

use Illuminate\Foundation\Http\FormRequest;

class InseeSearchRequest extends FormRequest
{
    protected function prepareForValidation(): void
    {
        $this->merge([
            'siren' => preg_match('#^\d+$#', str_replace(' ', '', $this->q)) ? str_replace(' ', '', $this->q) : null,
        ]);
    }

    public function rules(): array
    {
        return [
            'q' => ['required'],
            'siren' => ['string', 'size:9'],
        ];
    }
}
