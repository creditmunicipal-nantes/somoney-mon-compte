<?php

namespace App\Http\Requests\Api\Webhooks;

use Illuminate\Foundation\Http\FormRequest;

class NotificationPaymentMobileRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'transferId' => [
                'required',
            ],
        ];
    }
}
