<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SearchSdgRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'category_id' => ['nullable', 'int', Rule::in(array_keys(config('sdg.categories.id_raw')))],
            'professional_id' => ['required'],
        ];
    }
}
