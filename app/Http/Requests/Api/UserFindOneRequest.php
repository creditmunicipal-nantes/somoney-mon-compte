<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class UserFindOneRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'id' => ['required'],
        ];
    }
}
