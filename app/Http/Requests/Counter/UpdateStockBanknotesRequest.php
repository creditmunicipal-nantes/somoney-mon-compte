<?php

namespace App\Http\Requests\Counter;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStockBanknotesRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'stock' => [
                'required',
                'integer',
                'min:0',
            ],
        ];
    }
}
