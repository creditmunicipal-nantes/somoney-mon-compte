<?php

namespace App\Http\Requests\Counter;

use App\Models\BanknotesTransaction;
use App\Models\CyclosUser;
use App\Services\Cyclos\UserService;
use Illuminate\Foundation\Http\FormRequest;

class TransferBanknotesRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'amount' => [
                'required',
                'numeric',
                'gt:0',
                function ($attribute, $value, $fail) {
                    if ($this->type === 'in' && BanknotesTransaction::getUserBalance($this->user) < $value) {
                        $fail(__('validation.custom.banknotes.' . $attribute . '.return.max', [
                            'user' => CyclosUser::fill(app(UserService::class)->get($this->user))->name,
                            'value' => $value,
                            'money' => config('somoney.money'),
                        ]));
                    }
                },
            ],
            'banknotes' => ['required', 'array', 'min:1'],
            'banknotes.*' => ['required', 'int'],
            'type' => ['required', 'string', 'in:in,out'],
            'user' => ['required', 'string'],
        ];
    }
}
