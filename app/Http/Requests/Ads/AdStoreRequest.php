<?php

namespace App\Http\Requests\Ads;

use Illuminate\Support\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class AdStoreRequest extends FormRequest
{
    protected function prepareForValidation(): void
    {
        $this->merge([
            'start_date' => $this->start_date ? rescue(
                fn() => Carbon::createFromFormat('d/m/Y', $this->start_date),
                'XXX',
                false
            ) : null,
            'end_date' => $this->end_date ? rescue(
                fn() => Carbon::createFromFormat('d/m/Y', $this->end_date),
                'XXX',
                false
            ) : null,
        ]);
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'description' => ['required', 'string'],
            'start_date' => ['required', 'date'],
            'end_date' => ['required', 'date', 'after_or_equal:start_date'],
            'category' => ['required'],
            'currency' => ['required', 'string'],
            'images.*' => [
                'image', 'mimes:' . implode(',', config('somoney.form_constraints.file.extensions')), 'max:2000',
            ],
            'images' => ['max:' . (int) $this->max_item],
        ];
    }
}
