<?php

namespace App\Http\Requests\Newsletter;

use App\Models\Newsletter;
use Illuminate\Support\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Date;

class NewsletterStoreUpdateRequest extends FormRequest
{
    protected Carbon|null $dateMin = null;

    protected Newsletter|null $lastNewsletter = null;

    protected function prepareForValidation(): void
    {
        $this->merge([
            'send_date' => $this->getSendDateFormatted(),
            'hour' => $this->input('hour') ? (int) $this->input('hour') : null,
        ]);
    }

    public function rules(): array
    {
        $lastNewsletterQuery = Newsletter::where('cyclos_user_id', session('auth.user.id'));
        if ($this->newsletter) {
            $lastNewsletterQuery->where('id', '!=', $this->newsletter->id);
        }

        $this->lastNewsletter = $lastNewsletterQuery->latest()->first();
        if ($this->lastNewsletter) {
            $this->dateMin = Carbon::parse($this->lastNewsletter->send_date)->addMonth();
        }

        $rules = [
            'newsletter_image' => (new Newsletter())->getMediaValidationRules('newsletterImage'),
            'title' => ['required', 'string'],
            'content' => ['required', 'string'],
            'send_date' => ['required', 'date'],
            'hour' => ['required', 'integer', 'between:0,23'],
            'minutes' => ['required', 'integer', 'in:0,15,30,45'],
        ];

        if ($this->lastNewsletter) {
            $rules['send_date'] = 'after_or_equal:' . (
                $this->newsletter
                    ? 'now'
                    : $this->dateMin->format('Y-m-d H:i:s')
                );
        }

        return $rules;
    }

    public function messages(): array
    {
        $dateTxt = ! $this->lastNewsletter ? 'aujourd\'hui' : $this->dateMin->format('d/m/Y');

        return [
            'send_date.after_or_equal' => __(':attribute doit être après le ' . $dateTxt, [
                'attribute' => __('validation.attributes.send_date'),
            ]),
        ];
    }

    private function getSendDateFormatted(): string
    {
        $dateTime = $this->send_date . ' ' . $this->hour . ':'
            . ($this->minutes === '0' ? '00' : $this->minutes) . ':00';

        return rescue(
            fn() => Date::createFromFormat('d/m/Y G:i:s', $dateTime),
            'XXX',
            false
        );
    }
}
