<?php

namespace App\Http\Requests\Admin;

use Illuminate\Support\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DashboardRequest extends FormRequest
{
    protected function prepareForValidation()
    {
        $this->merge([
            'user_type' => $this->user_type !== 'Tous' ? $this->user_type : null,
            'transactions_date_begin' => $this->transactions_date_begin ? rescue(
                fn() => Carbon::createFromFormat('d/m/Y', $this->transactions_date_begin),
                'XXX',
                false
            ) : null,
            'transactions_date_end' => $this->transactions_date_end ? rescue(
                fn() => Carbon::createFromFormat('d/m/Y', $this->transactions_date_end),
                'XXX',
                false
            ) : null,
        ]);
    }

    public function rules(): array
    {
        return [
            'transactions_date_begin' => ['nullable', 'date'],
            'transactions_date_end' => ['nullable', 'date'],
            'user_type' => ['nullable', 'string', Rule::in(['individual', 'professional'])],
        ];
    }
}
