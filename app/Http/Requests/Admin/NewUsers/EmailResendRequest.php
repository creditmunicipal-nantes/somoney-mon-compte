<?php

namespace App\Http\Requests\Admin\NewUsers;

use Illuminate\Foundation\Http\FormRequest;

class EmailResendRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'email' => [
                'required',
                'email:rfc,dns,spoof',
                'max:255',
                'unique_cyclos_email',
                'unique:local_users,email,' . $this->localUser->id,
            ],
        ];
    }
}
