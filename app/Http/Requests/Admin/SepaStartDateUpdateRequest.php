<?php

namespace App\Http\Requests\Admin;

use Illuminate\Support\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class SepaStartDateUpdateRequest extends FormRequest
{
    protected function prepareForValidation(): void
    {
        $this->merge([
            'start_date' => $this->start_date ? rescue(
                fn() => Carbon::createFromFormat('d/m/Y', $this->start_date)->toDateString(),
                'XXX',
                false
            ) : null,
        ]);
    }

    public function rules(): array
    {
        return [
            'start_date' => [
                'required',
                'date',
            ],
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        session()->flash('error', implode('<br/>', array_values($validator->errors()->messages())[0]));
        parent::failedValidation($validator);
    }
}
