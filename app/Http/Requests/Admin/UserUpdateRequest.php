<?php

namespace App\Http\Requests\Admin;

use Illuminate\Support\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    protected function prepareForValidation(): void
    {
        $userData = $this->get('user', []);
        $customValues = [
            'customValues' => [
                'membershipsonantaise' => ! is_null(data_get($userData, 'customValues.membershipsonantaise')),
                'commercialinfo' => ! is_null(data_get($userData, 'customValues.commercialinfo')),
                'optinnewsletter' => ! is_null(data_get($userData, 'customValues.optinnewsletter')),
            ],
        ];

        if ($this->group === 'professional') {
            $customValues['customValues']['visible'] = is_null($this->visible) ? false : true;
        }

        $this->merge([
            'phone' => $this->phone ? preg_replace('#^0#', '+33', preg_replace('#\s#', '', $this->phone)) : null,
            'addressLine2' => is_null($this->addressLine2) ? '' : $this->addressLine2,
            'user' => collect($userData)->replaceRecursive($customValues)->toArray(),
            'birthday' => $this->birthday ? rescue(
                fn() => Carbon::createFromFormat('d/m/Y', $this->birthday),
                'XXX',
                false
            ) : null,
        ]);
    }

    public function rules(): array
    {
        $rules = [
            'address_gmap' => ['required', 'string'],
            'addressLine1' => ['string', 'manual_address_need_city_gmap'],
            'gmap.addressLine1' => ['nullable', 'string'],
            'gmap.city' => ['required', 'string', 'city_gmap_need_manual_address'],
            'gmap.zip' => ['required', 'string'],
            'gmap.latitude' => ['required', 'numeric'],
            'gmap.longitude' => ['required', 'numeric'],
            'addressLine2' => ['nullable', 'string'],
            'user.customValues.firstname' => ['required', 'string'],
            'user.customValues.lastname' => ['required', 'string'],
            'user.email' => [
                'required',
                'email:rfc,dns,spoof',
                'unique_cyclos_email:' . $this->route('id'),
            ],
            'phone' => ['required', 'phone:AUTO'],
            'user.customValues.gender' => ['required'],
            'birthday' => [
                'required',
                'date',
                'after:' . Carbon::createFromDate(today()->year - 100)->startOfYear(),
                'be_major:' . config('somoney.age.min'),
            ],
            'user.customValues.membershipsonantaise' => ['boolean'],
            'user.customValues.optinnewsletter' => ['boolean'],
            'user.customValues.commercialinfo' => ['boolean'],
            'avatar' => [
                'file',
                'mimes:' . implode(',', config('somoney.form_constraints.image.extensions')),
                'max:' . config('somoney.form_constraints.image.maxSize') * 1000,
            ],
            'idattachment' => [
                'file',
                'mimes:' . implode(',', config('somoney.form_constraints.file.extensions')),
                'max:' . config('somoney.form_constraints.file.maxSize') * 1000,
            ],
            'user.customValues.comment' => ['nullable', 'string'],
        ];
        if ($this->group === 'professional') {
            $rules = array_merge($rules, [
                'user.customValues.mailpro' => ['required', 'email'],
                'user.name' => ['required'],
                'user.customValues.website' => ['nullable', 'url'],
                'user.customValues.aboutMe' => ['string', 'nullable'],
                'user.customValues.keyword' => ['string'],
                'user.customValues.facebookurl' => ['nullable', 'url'],
                'user.customValues.twitterurl' => ['nullable', 'url'],
                'birthday' => [
                    'required',
                    'date',
                    'after:' . Carbon::createFromDate(today()->year - 100)->startOfYear(),
                    'be_major:18',
                ],
                'organisationattachment' => [
                    'file',
                    'mimes:' . implode(',', config('somoney.form_constraints.file.extensions')),
                    'max:' . config('somoney.form_constraints.file.maxSize') * 1000,
                ],
                'ribattachment' => [
                    'file',
                    'mimes:' . implode(',', config('somoney.form_constraints.file.extensions')),
                    'max:' . config('somoney.form_constraints.file.maxSize') * 1000,
                ],
                'user.customValues.visible' => ['nullable', 'boolean'],
            ]);
        }

        return $rules;
    }

    public function messages(): array
    {
        return [
            'gmap.city.required' => __('validation.required', [
                'attribute' => __('validation.attributes.address'),
            ]),
            'birthday.be_major' => __('validation.be_major', [
                'age' => $this->group === 'professional' ? '18' : config('somoney.age.min'),
            ]),
        ];
    }
}
