<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ReconversionStoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'maxAmount' => [
                'required',
                'numeric',
            ],
            'amount' => [
                'required',
                'numeric',
                'max:' . $this->maxAmount,
            ],
        ];
    }
}
