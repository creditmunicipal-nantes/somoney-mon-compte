<?php

namespace App\Http\Requests\Admin\Sdgs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SdgsProfessionalsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'status' => [
                'nullable',
                'string',
                Rule::in(['ended', 'ongoing']),
            ],
        ];
    }
}
