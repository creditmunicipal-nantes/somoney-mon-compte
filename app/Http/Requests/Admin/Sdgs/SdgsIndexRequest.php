<?php

namespace App\Http\Requests\Admin\Sdgs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SdgsIndexRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'categorie' => [
                'nullable',
                'string',
                Rule::in(array_keys(config('sdg.categories.slug_raw'))),
            ],
        ];
    }
}
