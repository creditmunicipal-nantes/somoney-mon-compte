<?php

namespace App\Http\Requests\Admin\Sdgs;

use Illuminate\Foundation\Http\FormRequest;

class SdgStoreOrUpdateRequest extends FormRequest
{
    protected function prepareForValidation()
    {
        $this->merge([
            'active' => boolval($this->active),
        ]);
    }

    public function rules(): array
    {
        return [
            'title' => [
                'required',
                'string',
                'max:255',
            ],
            'description' => [
                'required',
                'string',
                'max:65535',
            ],
            'active' => [
                'required',
                'boolean',
            ],
        ];
    }
}
