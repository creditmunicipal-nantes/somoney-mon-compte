<?php

namespace App\Http\Requests\Admin\Counter;

use Illuminate\Foundation\Http\FormRequest;

class StoreCounterRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'operators' => ['required', 'array', 'min:1'],
            'operators.*' => ['required', 'string'],
            'zipcode' => ['required', 'string'],
            'city' => ['required', 'string', 'max:255'],
        ];
    }
}
