<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ManuelCreditStoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'maxAmount' => [
                'required',
                'numeric',
            ],
            'amount' => [
                'required',
                'max:' . $this->maxAmount,
            ],
        ];
    }
}
