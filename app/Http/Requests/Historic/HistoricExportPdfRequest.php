<?php

namespace App\Http\Requests\Historic;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class HistoricExportPdfRequest extends FormRequest
{
    protected function prepareForValidation(): void
    {
        $this->merge([
            'filters' => $this->filters
                ? array_merge($this->filters, [
                    'dateBegin' => $this->has('filters.datePeriod')
                        ? Arr::first(explode(',', $this->filters['datePeriod']))
                        : null,
                    'dateEnd' => $this->has('filters.datePeriod')
                        ? Arr::last(explode(',', $this->filters['datePeriod']))
                        : null,
                    'pageSize' => $this->pageSize ?: 1000,
                ])
                : null,
        ]);
    }

    public function rules(): array
    {
        return [
            'filters' => [
                'required',
                'array',
            ],
            'filters.dateBegin' => [
                'required',
                'date',
            ],
            'filters.dateEnd' => [
                'required',
                'date',
            ],
        ];
    }

    public function getRedirectUrl(): string
    {
        return route('operations.historic.index');
    }
}
