<?php

namespace App\Http\Requests\Notification;

use Illuminate\Foundation\Http\FormRequest;

class StoreNotificationRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'message' => ['required', 'string', 'max:65535'],
            'groups' => ['required', 'string', 'in:' . join(',', config('notifications.groups'))],
        ];
    }
}
