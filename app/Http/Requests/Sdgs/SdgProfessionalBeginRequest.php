<?php

namespace App\Http\Requests\Sdgs;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SdgProfessionalBeginRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'category_id' => [
                'required',
                'int',
                Rule::in(array_keys(config('sdg.categories.id_raw'))),
            ],
            'sdg_id' => [
                'required',
                'exists:sdgs,id',
            ],
        ];
    }
}
