<?php

namespace App\Http\Requests\Membership;

use App\Models\Transaction;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MembershipsOperationsRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'page' => ['nullable', 'int'],
            'month' => ['nullable', 'date_format:m_Y'],
            'type' => [
                'nullable',
                'string',
                Rule::in(['all',Transaction::CONTEXT_MEMBERSHIP, Transaction::REGISTER_TYPE_DONATE]),
            ],
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            'month' => $this->month === '-' ? null : $this->month,
        ]);
    }
}
