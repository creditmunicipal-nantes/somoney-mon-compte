<?php

namespace App\Http\Requests\Membership;

use Illuminate\Foundation\Http\FormRequest;

class MembershipsIndexRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'succeed_at' => ['string', 'nullable', 'in:valid,expired'],
            'page' => ['nullable', 'int']
        ];
    }
}
