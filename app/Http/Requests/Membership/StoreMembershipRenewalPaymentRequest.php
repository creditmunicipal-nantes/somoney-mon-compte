<?php

namespace App\Http\Requests\Membership;

use Illuminate\Foundation\Http\FormRequest;

class StoreMembershipRenewalPaymentRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'amount' => [
                'required',
                'numeric',
                'min:' . config('somoney.credit.membership.min.renewal.' . session('auth.user.group.internalName')),
            ],
        ];
    }
}
