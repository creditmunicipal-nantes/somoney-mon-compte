<?php

namespace App\Http\Requests\Credit\Automatic;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'amount' => [
                'required',
                'numeric',
                'min:' . config('somoney.credit.auto.minimal'),
                'max:' . $this->max_amount,
            ],
            'duration_month' => [
                'required',
                'integer',
                'min:' . config('somoney.credit.auto.minimal-length'),
            ],
            'gender' => ['required', 'string'],
            'last_name' => ['required', 'string', 'max:255'],
            'first_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'max:255'],
            'company' => ['nullable','string', 'max:255'],
            'iban' => [
                'required',
                'string',
                'regex:/^FR\d{12}[0-9A-Z]{11}\d{2}$/',
            ],
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge(['iban' => $this->iban ? str_replace(' ', '', $this->iban) : $this->iban]);
    }
}
