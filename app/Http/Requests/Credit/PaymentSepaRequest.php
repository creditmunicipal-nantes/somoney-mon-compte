<?php

namespace App\Http\Requests\Credit;

use Illuminate\Foundation\Http\FormRequest;

class PaymentSepaRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'max_amount' => ['required', 'numeric', 'min:' . config('somoney.credit.simple.minimal-sepa')],
            'amount' => [
                'required',
                'numeric',
                'min:' . config('somoney.credit.simple.minimal-sepa'),
                'max:' . $this->max_amount,
            ],
            'gender' => ['required', 'string'],
            'last_name' => ['required', 'string', 'max:255'],
            'first_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email:rfc,dns,spoof', 'max:255'],
            'company' => ['nullable', 'string', 'max:255'],
            'iban' => ['required', 'string', 'regex:/^FR\d{12}[0-9A-Z]{11}\d{2}$/'],
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge(['iban' => $this->iban ? str_replace(' ', '', $this->iban) : $this->iban]);
    }
}
