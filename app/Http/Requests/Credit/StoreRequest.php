<?php

namespace App\Http\Requests\Credit;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    public function rules(): array
    {
        $typesAllowed = ['cb'];
        if (
            config('somoney.available.services.sepa-payment')
            && session('auth.user.group.internalName') === 'professional'
        ) {
            $typesAllowed[] = 'sepa';
        }

        return [
            'max_amount' => [
                'required',
                'numeric',
                'min:' . config('somoney.credit.simple.minimal'),
            ],
            'amount' => [
                'required',
                'numeric',
                'min:' . config('somoney.credit.simple.minimal'),
                'max:' . $this->max_amount,
            ],
            'automatic' => ['required', 'boolean'],
            'type' => ['required', 'string', Rule::in($typesAllowed)],
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge(['automatic' => (bool) $this->automatic]);
    }
}
