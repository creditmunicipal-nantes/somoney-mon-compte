<?php

namespace App\Http\Requests\Fidelity;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FidelityUpdateRequest extends FormRequest
{
    protected function prepareForValidation(): void
    {
        $this->merge([
            'counter_as_price' => filter_var($this->counter_as_price, FILTER_VALIDATE_BOOLEAN),
            'discount_as_percent' => filter_var($this->discount_as_percent, FILTER_VALIDATE_BOOLEAN),
            'active' => $this->active ? filter_var($this->active, FILTER_VALIDATE_BOOLEAN) : false,
        ]);
    }

    public function rules(): array
    {
        return [
            'counter' => ['required', 'numeric', 'gt:0'],
            'counter_as_price' => ['required', 'boolean'],
            'discount' => ['required', 'numeric', 'gt:0'],
            'discount_as_percent' => ['required', 'boolean'],
            'groups_type' => [
                'required', 'string', Rule::in([
                    'all',
                    config('cyclos.internal-names.groups.professional'),
                    config('cyclos.internal-names.groups.individual'),
                ]),
            ],
            'active' => ['required', 'boolean'],
        ];
    }
}
