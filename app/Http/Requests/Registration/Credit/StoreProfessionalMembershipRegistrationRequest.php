<?php

namespace App\Http\Requests\Registration\Credit;

class StoreProfessionalMembershipRegistrationRequest extends StoreMembershipRegistrationRequest
{
    protected string $userType = 'professional';
}
