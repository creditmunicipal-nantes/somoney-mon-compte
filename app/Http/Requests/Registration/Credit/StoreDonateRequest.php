<?php

namespace App\Http\Requests\Registration\Credit;

use Illuminate\Foundation\Http\FormRequest;

class StoreDonateRequest extends FormRequest
{
    public function rules(): array
    {
        return ['amount' => ['required', 'min:0']];
    }
}
