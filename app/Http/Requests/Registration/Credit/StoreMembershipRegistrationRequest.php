<?php

namespace App\Http\Requests\Registration\Credit;

use Illuminate\Foundation\Http\FormRequest;

abstract class StoreMembershipRegistrationRequest extends FormRequest
{
    protected string $userType;

    public function rules(): array
    {
        return [
            'amount' => [
                'required',
                'numeric',
                'min:' . config('somoney.credit.membership.min.registration.' . $this->userType),
            ],
        ];
    }
}
