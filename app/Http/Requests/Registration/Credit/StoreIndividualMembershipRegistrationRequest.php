<?php

namespace App\Http\Requests\Registration\Credit;

class StoreIndividualMembershipRegistrationRequest extends StoreMembershipRegistrationRequest
{
    protected string $userType = 'individual';
}
