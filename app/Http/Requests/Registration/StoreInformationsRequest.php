<?php

namespace App\Http\Requests\Registration;

use Illuminate\Support\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Date;

class StoreInformationsRequest extends FormRequest
{
    public function authorize(): bool
    {
        return session()->has('registration.user_id');
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            'phone' => $this->phone ? preg_replace('#^0#', '+33', preg_replace('#\s#', '', $this->phone)) : null,
            'birthday' => $this->birthday ? rescue(
                fn() => Carbon::createFromFormat('d/m/Y', $this->birthday),
                'XXX',
                false
            ) : null,
            'tpeconfig' => ! is_null($this->tpeconfig),
        ]);
    }

    public function rules(): array
    {
        $rules = [
            'firstname' => ['required', 'string'],
            'lastname' => ['required', 'string'],
            'gender' => ['required', 'string'],
            'phone' => ['required', 'phone:AUTO'],
        ];

        if (Str::contains($this->route()->getName(), 'professional')) {
            return array_merge($rules, [
                'birthday' => array_merge(
                    [
                        'required',
                        'before:today',
                        'after:' . Carbon::createFromDate(today()->year - 100)->startOfYear(),
                    ],
                    // special for Js validation call of rules
                    (data_get(debug_backtrace()[1], 'class') === 'Proengsoft\JsValidation\JsValidatorFactory')
                        ? ['date_format:d/m/Y']
                        : ['date', 'be_major:18']
                ),
                'employeenumber' => ['required', 'string'],
                'name' => ['required', 'string', 'max:255'],
                'mailpro' => ['required', 'email'],
                'tpeconfig' => ['required', 'boolean'],
                'siren' => [
                    config('registration.available.professional.siren') ? 'required' : '',
                    'int',
                ],
            ]);
        }

        return array_merge($rules, [
            'birthday' => array_merge(
                [
                    'required',
                    'before:today',
                    'after:' . Date::createFromDate(Date::today()->year - 100)->startOfYear(),
                ],
                // special for Js validation call of rules
                (data_get(debug_backtrace()[1], 'class') === 'Proengsoft\JsValidation\JsValidatorFactory')
                    ? ['date_format:d/m/Y']
                    : ['date', 'be_major:' . config('somoney.age.min')]
            ),
        ]);
    }

    public function messages(): array
    {
        return [
            'birthday.before' => __('validation.before', [
                'attribute' => __('validation.attributes.birthday'),
                'date' => 'aujourd\'hui.',
            ]),
            'birthday.be_major' => __('validation.be_major', [
                'age' => config('somoney.age.min'),
            ]),
        ];
    }
}
