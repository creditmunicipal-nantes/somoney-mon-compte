<?php

namespace App\Http\Requests\Registration\Professional;

use Illuminate\Foundation\Http\FormRequest;

class StoreFilesRequest extends FormRequest
{
    public function authorize(): bool
    {
        return session()->has('registration.user_id');
    }

    public function rules(): array
    {
        return [
            'idattachment' => [
                'required_without:idattachment_path',
                'file',
                'mimes:' . implode(',', config('somoney.form_constraints.file.extensions')),
                'max:' . config('somoney.form_constraints.file.maxSize') * 1000,
            ],
            'ribattachment' => [
                'required_without:ribattachment_path',
                'file',
                'mimes:' . implode(',', config('somoney.form_constraints.file.extensions')),
                'max:' . config('somoney.form_constraints.file.maxSize') * 1000,
            ],
            'organisationattachment' => [
                'required_without:organisationattachment_path',
                'file',
                'mimes:' . implode(',', config('somoney.form_constraints.file.extensions')),
                'max:' . config('somoney.form_constraints.file.maxSize') * 1000,
            ],
            'organisationattachmenttype' => [
                'required',
            ],
        ];
    }

    public function messages(): array
    {
        return [
            'idattachment.required_without' => __(
                'validation.required',
                ['attribute' => __('validation.attributes.idattachment')]
            ),
            'ribattachment.required_without' => __(
                'validation.required',
                ['attribute' => __('validation.attributes.ribattachment')]
            ),
            'organisationattachment.required_without' => __(
                'validation.required',
                ['attribute' => __('validation.attributes.organisationattachment')]
            ),
        ];
    }
}
