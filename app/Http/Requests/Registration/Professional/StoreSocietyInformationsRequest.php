<?php

namespace App\Http\Requests\Registration\Professional;

use App\Http\Requests\Registration\StoreInformationsRequest;
use Illuminate\Support\Carbon;

class StoreSocietyInformationsRequest extends StoreInformationsRequest
{
    public function rules(): array
    {
        return [
            'firstname' => [
                'required',
                'string',
            ],
            'lastname' => [
                'required',
                'string',
            ],
            'gender' => [
                'required',
                'string',
            ],
            'phone' => [
                'required',
                'phone:AUTO',
            ],
            'birthday' => array_merge(
                [
                    'required',
                    'before:today',
                    'after:' . Carbon::createFromDate(today()->year - 100)->startOfYear(),
                ],
                // special for Js validation call of rules
                (data_get(debug_backtrace()[1], 'class') === 'Proengsoft\JsValidation\JsValidatorFactory')
                    ? ['date_format:d/m/Y']
                    : ['date', 'be_major:18']
            ),
            'employeenumber' => [
                'required',
                'string',
            ],
            'name' => [
                'required',
                'string',
                'max:255',
            ],
            'mailpro' => [
                'required',
                'email',
            ],
            'tpeconfig' => [
                'required',
                'boolean',
            ],
            'siren' => [
                config('registration.available.professional.siren') ? 'required' : '',
                'int',
            ],
        ];
    }

    public function messages(): array
    {
        return [
            'birthday.be_major' => __('validation.be_major', [
                'age' => 18,
            ]),
        ];
    }
}
