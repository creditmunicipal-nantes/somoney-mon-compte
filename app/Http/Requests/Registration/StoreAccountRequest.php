<?php

namespace App\Http\Requests\Registration;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class StoreAccountRequest extends FormRequest
{
    // Caractères autorisés : caractères alphanumériques + caractères spéciaux suivants : « @!#%'*+-.:;<=>?
    protected string $availablePasswordCharacters = 'a-zA-Z0-9\`\@\!"\#\$\%\&\'\(\)\*\+,\-\.\/:\;' .
    '\<=\>\?\[\\\\\]\^_\{\}~';

    public function rules(): array
    {
        $userId = session('registration.user_id');

        return array_merge(
            [
                'email' => array_merge(
                    [
                        'required',
                        'email:rfc,dns,spoof',
                    ],
                    $this->has('_jsvalidation') // JS validation does not work with theses rules
                        ? []
                        : [
                        'unique_cyclos_email',
                        'unique:local_users,email' . ($userId ? ',' . $userId : ''),
                    ]
                ),
                'password' => [
                    'required',
                    'string',
                    'min:8',
                    'regex:/^[' . $this->availablePasswordCharacters . ']+$/',
                ],
            ],
            (Str::contains($this->route()->getName(), 'professional')
            && config('registration.available.professional.category')
                ? [
                    'category' => ['required'],
                ]
                : []
            )
        );
    }

    public function messages(): array
    {
        if (! $this->has('email')) {
            return [];
        }
        $userType = Str::contains($this->route()->getName(), 'professional') ? 'professional' : 'individual';
        $url = route('register.' . $userType . '.email.resume', ['email' => $this->email]);
        if (is_string($this->password)) {
            preg_match('/[^' . $this->availablePasswordCharacters . ']/', $this->password);
        }

        return [
            'email.unique' => __('validation.unique', ['attribute' => __('validation.attributes.email')])
                . ' (<a href="' . $url . '" class="c-link">reprendre l\'inscription</a>)',
            'password.regex' => __('validation.custom.password.regex'),
        ];
    }
}
