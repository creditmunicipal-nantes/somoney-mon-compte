<?php

namespace App\Http\Requests\Registration\Individual;

use App\Http\Requests\Registration\StoreInformationsRequest;
use Illuminate\Support\Carbon;

class StorePersonalInformationsRequest extends StoreInformationsRequest
{
    public function rules(): array
    {
        return [
            'firstname' => [
                'required',
                'string',
            ],
            'lastname' => [
                'required',
                'string',
            ],
            'gender' => [
                'required',
                'string',
            ],
            'phone' => [
                'required',
                'phone:AUTO',
            ],
            'birthday' => array_merge(
                [
                    'required',
                    'before:today',
                    'after:' . Carbon::createFromDate(today()->year - 100)->startOfYear(),
                ],
                // special for Js validation call of rules
                (data_get(debug_backtrace()[1], 'class') === 'Proengsoft\JsValidation\JsValidatorFactory')
                    ? ['date_format:d/m/Y']
                    : ['date', 'be_major:' . config('somoney.age.min')]
            ),
        ];
    }

    public function messages(): array
    {
        return [
            'birthday.before' => __('validation.before', [
                'attribute' => __('validation.attributes.birthday'),
                'date' => 'aujourd\'hui.',
            ]),
            'birthday.be_major' => __('validation.be_major', [
                'age' => config('somoney.age.min'),
            ]),
        ];
    }
}
