<?php

namespace App\Http\Requests\Registration;

use Illuminate\Foundation\Http\FormRequest;

class StoreAddressRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'address_gmap' => ['required', 'string'],
            'gmap.addressLine1' => ['nullable', 'string'],
            'gmap.city' => [
                'required',
                'string',
                'city_gmap_need_manual_address',
            ],
            'gmap.zip' => ['string'],
            'gmap.latitude' => ['numeric'],
            'gmap.longitude' => ['numeric'],
            'addressLine1' => [
                'nullable',
                'string',
                'manual_address_need_city_gmap',
            ],
            'addressLine2' => ['nullable', 'string'],
        ];
    }

    public function messages(): array
    {
        return [
            'gmap.city.required' => __('validation.required', [
                'attribute' => __('validation.attributes.address'),
            ]),
        ];
    }
}
