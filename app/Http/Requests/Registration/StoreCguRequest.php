<?php

namespace App\Http\Requests\Registration;

use Illuminate\Foundation\Http\FormRequest;

class StoreCguRequest extends FormRequest
{
    public function authorize(): bool
    {
        return session()->has('registration.user_id');
    }

    public function rules(): array
    {
        $rules = [
            'cgu' => ['required', 'accepted'],
            'newsletter' => ['required', 'boolean'],
            'commercialinfo' => ['required', 'boolean'],
            'optinnewsletter' => ['required', 'boolean'],
            'membershipsonantaise' => [
                'required',
                config('somoney.available.services.membership') ? 'accepted' : 'boolean',
            ],
        ];
        if (config('registration.available.common.comment')) {
            $rules['comment'] = ['nullable', 'string'];
        }

        return $rules;
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            'cgu' => (bool) $this->cgu,
            'newsletter' => (bool) $this->newsletter,
            'commercialinfo' => (bool) $this->commercialinfo,
            'optinnewsletter' => (bool) $this->optinnewsletter,
            'membershipsonantaise' => (bool) $this->membershipsonantaise,
        ]);
        if (config('registration.available.common.comment')) {
            $this->merge([
                'comment' => '- Renseigné par l\'utilisateur : ' . $this->input('user.customValues.comment'),
            ]);
        }
    }
}
