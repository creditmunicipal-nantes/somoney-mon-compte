<?php

namespace App\Http\Requests\Registration;

use Illuminate\Foundation\Http\FormRequest;

class StoreInitialCreditRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'amount' => [
                config('somoney.credit.initial.minimal') === null ? 'nullable' : 'required',
                'numeric',
                'min:' . (float) config('somoney.credit.initial.minimal'),
                'max:' . $this->max_amount,
            ],
        ];
    }
}
