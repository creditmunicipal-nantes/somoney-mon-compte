<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use LemonWay\LemonWayAPI;
use Illuminate\Support\Facades\Request;

class LemonWayServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(LemonWayAPI::class, function () {
            $api = new LemonWayAPI(
                config('lemonway.directkit.url'),
                config('lemonway.webkit.url'),
                config('lemonway.login'),
                config('lemonway.password'),
                config('lemonway.lang'),
                config('lemonway.debug'),
                config('lemonway.ssl_verification'),
                config('lemonway.xmlns')
            );

            $api->config->remote_addr = Request::ip();

            return $api;
        });

        $this->app->alias(LemonWayAPI::class, 'lemonway');
    }

    /**
     * @return array
     */
    public function provides()
    {
        return ['lemonway'];
    }
}
