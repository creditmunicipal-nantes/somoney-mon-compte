<?php

namespace App\Providers;

use App\Models\CyclosUser;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\Validator as ValidatorModel;
use Illuminate\Support\Facades\Validator;

class CustomValidationServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->cyclosValidations();
        $this->dateValidations();
        $this->mapValidations();
        $this->transferValidations();
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function cyclosValidations(): void
    {
        Validator::extend('unique_cyclos_email', function ($attribute, $value, array $parameters) {
            $filters = [
                'profileFields' => ['email' => $value],
                'statuses' => [
                    CyclosUser::STATUS_ACTIVE,
                    CyclosUser::STATUS_BLOCKED,
                    CyclosUser::STATUS_DISABLED,
                    CyclosUser::STATUS_PENDING,
                    CyclosUser::STATUS_PURGED,
                    CyclosUser::STATUS_REMOVED,
                ],
            ];
            if (count($parameters)) {
                $filters['usersIdsToExclude'] = $parameters;
            }

            return ! app(UserService::class)->exists($filters);
        });
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function dateValidations(): void
    {
        Validator::extend('be_major', fn($attribute, $value, array $parameters) => rescue(
            fn() => Carbon::createFromFormat('d/m/Y', $value)->startOfDay()->lte(
                today()->startOfDay()->subYears((int) Arr::get($parameters, '0', 18))
            ),
            'XXX',
            false
        ));
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function mapValidations(): void
    {
        Validator::extend(
            'manual_address_need_city_gmap',
            function (string $attribute, $value, array $parameters, ValidatorModel $validator) {
                $validation = $value && data_get($validator->attributes(), 'gmap.city');
                if (! $validation) {
                    session()->flash('address_not_found_show', true);
                }

                return $validation;
            }
        );
        Validator::extend(
            'city_gmap_need_manual_address',
            function (string $attribute, $value, array $parameters, ValidatorModel $validator) {
                $validation = $value
                    && ((
                            array_key_exists('addressLine1', $validator->attributes()['gmap'])
                            && ! empty($validator->attributes()['gmap']['addressLine1'])
                        )
                        || (
                            array_key_exists('addressLine1', $validator->attributes())
                            && ! empty($validator->attributes()['addressLine1'])
                        ));
                if (! $validation) {
                    session()->flash('address_not_found_show', true);
                }

                return $validation;
            }
        );
    }

    /**
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    protected function transferValidations(): void
    {
        // need more than one occurrence
        Validator::extend(
            'cyclos.transfer.min_occurrence',
            function (string $attribute, $value, array $parameters, ValidatorModel $validator) {
                if (
                    $value && array_key_exists('date', $validator->attributes())
                    && array_key_exists('date_end', $validator->attributes())
                    && array_key_exists('frequency', $validator->attributes())
                ) {
                    return $this->validNbOccurrences(
                        $validator->attributes()['date'],
                        $validator->attributes()['date_end'],
                        $validator
                    );
                }

                return true;
            }
        );
        Validator::extend(
            'cyclos.transfer.first_occurrence',
            function (string $attribute, $value, array $parameters, ValidatorModel $validator) {
                if ($value && array_key_exists('date', $validator->attributes())) {
                    $date = $validator->attributes()['date'];

                    return is_a($date, Carbon::class) && $date->gt(today()->endOfDay());
                }

                return true;
            }
        );
    }

    protected function validNbOccurrences(Carbon $startDate, Carbon $endDate, ValidatorModel $validator): bool
    {
        return match ($validator->attributes()['frequency']) {
            'weekly' => $startDate->diffInDays($endDate) >= (7 * 2),
            'bi-monthly' => $startDate->diffInDays($endDate) >= (15 * 2),
            'monthly' => $startDate->diffInMonths($endDate) >= (1 * 2),
            default => false,
        };
    }
}
