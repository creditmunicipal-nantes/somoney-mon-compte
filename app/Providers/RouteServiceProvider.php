<?php

namespace App\Providers;

use App\Models\UserToken;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\Response;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/';

    /**
     * The controller namespace for the application.
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->configureRateLimiting();
        $this->createBindings();
        $this->routes(function () {
            $this->mapApiRoutes();
            $this->mapStyleguideWebRoutes();
            $this->mapWebRoutes();
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting(): void
    {
        RateLimiter::for(
            'api',
            fn(Request $request) => Limit::perMinute(60)->by($request->user()?->id ?: $request->ip())
        );
    }

    public function createBindings(): void
    {
        app('router')->bind(
            'resetToken',
            fn($tokenId) => UserToken::isResetToken()->findOrFail($tokenId)
        );
        app('router')->bind(
            'emailValidationToken',
            fn($tokenId) => UserToken::isEmailValidationToken()->findOrFail($tokenId)
        );
        app('router')->bind(
            'resumeRegistrationToken',
            fn($tokenId) => UserToken::isResumeRegistrationToken()->findOrFail($tokenId)
        );
        app('router')->bind('needMembershipToken', function ($tokenId) {
            $token = UserToken::isRegistrationNeedMembership()->find($tokenId);

            if (! $token) {
                abort(Response::HTTP_FORBIDDEN, 'Lien expiré');
            }

            return $token;
        });
        app('router')->bind(
            'membershipRenewToken',
            fn($tokenId) => UserToken::isMembershipRenewToken()->findOrFail($tokenId)
        );
    }

    /**
     * Define the "api" routes for the application.
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes(): void
    {
        Route::middleware('api')
            ->as('api.')
            ->prefix('api')
            ->group(base_path('routes/api.php'));
    }

    /**
     * Define the "styleguide" routes for the application.
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapStyleguideWebRoutes(): void
    {
        Route::middleware('web')
            ->domain(config('somoney.domain.styleguide'))
            ->group(base_path('routes/styleguide-web.php'));
    }

    /**
     * Define the "web" routes for the application.
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes(): void
    {
        Route::middleware('web')
            ->domain(config('somoney.domain.main'))
            ->group(base_path('routes/web.php'));

        // Register
        Route::middleware('web')
            ->domain(config('somoney.domain.main'))
            ->group(base_path('routes/register.php'));

        // Admin
        Route::middleware(['web', 'auth', 'restrictToAdmin'])
            ->domain(config('somoney.domain.main'))
            ->as('admin.')
            ->prefix('admin')
            ->group(base_path('routes/admin.php'));

        // Members Only
        Route::middleware([
            'web',
            // Todo : ces deux middlewares bloquent le crédit lors de l'inscription
            // => vérifier si ils peuvent être supprimés, sinon revoir cette implémentation.
            // 'auth',
            // 'onlyMembers'
        ])
            ->domain(config('somoney.domain.main'))
            ->group(base_path('routes/members.php'));
    }
}
