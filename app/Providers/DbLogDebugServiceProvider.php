<?php

namespace App\Providers;

use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class DbLogDebugServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (config('app.debug')) {
            DB::listen(
                function (QueryExecuted $sql) {
                    if (
                        ! Str::contains($sql->sql, 'cyclos_logs')
                        && ! Str::contains($sql->sql, 'select * from "jobs" where "queue"') // avoid log queue listens
                    ) {
                        foreach ($sql->bindings as $i => $binding) {
                            if ($binding instanceof DateTime) {
                                $sql->bindings[$i] = $binding->format('\'Y-m-d H:i:s\'');
                            } else {
                                if (is_string($binding)) {
                                    $sql->bindings[$i] = "'$binding'";
                                }
                            }
                        }

                        // Insert bindings into query
                        $query = str_replace(['%', '?'], ['%%', '%s'], $sql->sql);

                        $query = vsprintf($query, $sql->bindings);

                        $filename = 'query.log';
                        if (config('app.log') === 'daily') {
                            $filename = date('Y-m-d') . '_' . $filename;
                        }

                        // Save the query to file
                        $logFile = fopen(
                            storage_path('logs' . DIRECTORY_SEPARATOR . $filename),
                            'a+'
                        );
                        fwrite($logFile, '[' . date('Y-m-d G:i:s') . '] ' . $query . PHP_EOL);
                        fclose($logFile);
                    }
                }
            );
        }
    }
}
