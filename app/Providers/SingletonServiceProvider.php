<?php

namespace App\Providers;

use App\Services\Cyclos\AccountService;
use App\Services\Cyclos\AdService;
use App\Services\Cyclos\AuthService;
use App\Services\Cyclos\TokenService;
use App\Services\Cyclos\TransactionService;
use App\Services\Cyclos\TransferService;
use App\Services\Cyclos\User\AddressService;
use App\Services\Cyclos\User\ImageService;
use App\Services\Cyclos\User\PasswordService;
use App\Services\Cyclos\User\PhoneService;
use App\Services\Cyclos\UserService;
use App\Services\GeocodingService;
use App\Services\InseeService;
use App\Services\LocalUserService;
use App\Services\MailgunService;
use App\Services\QrCodeService;
use Illuminate\Support\ServiceProvider;

class SingletonServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->singleton(AuthService::class, fn() => new AuthService());

        $this->app->singleton(UserService::class, fn() => new UserService());

        $this->app->singleton(AccountService::class, fn() => new AccountService(session('auth.sessionToken')));

        $this->app->singleton(TransactionService::class, fn() => new TransactionService(session('auth.sessionToken')));

        $this->app->singleton(TransferService::class, fn() => new TransferService());

        $this->app->singleton(ImageService::class, fn() => new ImageService(session('auth.sessionToken')));

        $this->app->singleton(AddressService::class, fn() => new AddressService(session('auth.sessionToken')));

        $this->app->singleton(PhoneService::class, fn() => new PhoneService(session('auth.sessionToken')));

        $this->app->singleton(AdService::class, fn() => new AdService(session('auth.sessionToken')));

        $this->app->singleton(PasswordService::class, fn() => new PasswordService(session('auth.sessionToken')));

        $this->app->singleton(LocalUserService::class, fn() => new LocalUserService());

        if (config('somoney.available.services.qr-code')) {
            $this->app->singleton(TokenService::class, fn() => new TokenService(session('auth.sessionToken')));
            $this->app->singleton(QrCodeService::class, fn() => new QrCodeService());
        }

        if (config('registration.available.professional.siren')) {
            $this->app->singleton(InseeService::class, fn() => new InseeService());

            $this->app->singleton(GeocodingService::class, fn() => new GeocodingService());
        }

        if (config('services.mailgun.secret')) {
            $this->app->singleton(MailgunService::class, fn() => new MailgunService());
        }
    }
}
