<?php

namespace App\Providers;

use Illuminate\Support\Carbon;
use Illuminate\Support\ServiceProvider;

class LocalizeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $appLocale = setlocale(LC_TIME, 'fr_FR.utf8');
        if ($appLocale === false) {
            $message = 'fr_FR.utf8 locale is not installed';
            logger()->error($message);
        }

        Carbon::setLocale(config('app.locale'));
    }
}
