<?php

namespace App\Providers;

use App\Models\Counter;
use App\Models\CyclosUser;
use App\Services\QrCodeService;
use Coderello\SharedData\SharedData;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\View;

class ViewComposerServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        View::composer(
            ['main.partials.header', 'main.partials.aside.user', 'main.partials.aside.admin'],
            function ($view) {
                $navbar = (include base_path() . '/custom-data/current/navbar.php');
                $counters = null;
                if (config('somoney.available.services.counters')) {
                    $counters = Counter::all()->filter(function (Counter $counter) {
                        $isAdmin = Str::startsWith(
                            session('auth.user.group.internalName'),
                            config('cyclos.internal-names.admin')
                        );

                        return $isAdmin || in_array(session('auth.user.id'), $counter->operators);
                    });
                }
                $view->with(compact('counters', 'navbar'));
            }
        );
        View::composer(
            [],
            function ($view) {
                $counters = null;
                if (config('somoney.available.services.counters')) {
                    $counters = Counter::all()->filter(
                        fn(Counter $counter) => in_array(session('auth.user.id'), $counter->operators)
                    );
                }
                $view->with(compact('counters'));
            }
        );
        View::composer(
            ['main.layout', 'main.partials.header'],
            function ($view) {
                $userQrCodeImage = $userQrCodePoster = null;
                if (session('auth.user')) {
                    $user = session('auth.user');
                    $userQrCodeImage = config('somoney.available.services.qr_code') && ! data_get($user, 'is_admin')
                        ? app(QrCodeService::class)->getCyclosUserQrCodeTokenImage(CyclosUser::fill($user))
                        : null;
                    $userQrCodePoster = config('somoney.available.services.qr_code') && ! data_get($user, 'is_admin')
                        ? app(QrCodeService::class)->getCyclosUserQrCodeTokenPoster(CyclosUser::fill($user))
                        : null;
                }

                $laravelSharedData = app(SharedData::class)->render();
                $view->with(compact('userQrCodeImage', 'userQrCodePoster', 'laravelSharedData'));
            }
        );
    }
}
