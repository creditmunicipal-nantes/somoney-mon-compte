<?php

if (! function_exists('getProtocol')) {
    /**
     * get the http protocol
     *
     * @return string
     */
    function getProtocol(): string
    {
        return request()->isSecure() ? 'https://' : 'http://';
    }
}
