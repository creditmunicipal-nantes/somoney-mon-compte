<?php

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

if (! function_exists('paginateCollection')) {
    /**
     * @param \Illuminate\Support\Collection $collection
     * @param int $perPage
     * @param string $pageName
     * @param string|null $fragment
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    function paginateCollection(
        Collection $collection,
        int $perPage,
        string $pageName = 'page',
        ?string $fragment = null
    ) {
        $query = [];
        $currentPage = LengthAwarePaginator::resolveCurrentPage($pageName);
        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
        parse_str(request()->getQueryString(), $query);
        unset($query[$pageName]);

        return new LengthAwarePaginator(
            $currentPageItems,
            $collection->count(),
            $perPage,
            $currentPage,
            [
                'pageName' => $pageName,
                'path' => LengthAwarePaginator::resolveCurrentPath(),
                'query' => $query,
                'fragment' => $fragment,
            ]
        );
    }
}
