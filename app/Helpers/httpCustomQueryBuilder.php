<?php

if (! function_exists('http_custom_build_query')) {
    /*
     * function copied from https://stackoverflow.com/a/43618068
     */
    function http_custom_build_query(array $data): string
    {
        $query = [];
        foreach ($data as $name => $value) {
            $value = (array) $value;
            array_walk_recursive($value, function ($value) use (&$query, $name) {
                $query[] = urlencode($name) . '=' . urlencode($value);
            });
        }

        return implode('&', $query);
    }
}
