<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;

class ApiLog
{
    /**
     * Write in log the request of an API
     *
     * @param string $apiName
     * @param string $method
     * @param string $path
     * @param array $data
     * @param bool $debug
     */
    public static function request(
        string $apiName,
        string $method,
        string $path,
        array $data = [],
        bool $debug = true
    ): void {
        $log = '[' . mb_strtoupper($apiName) . ' API][REQUEST] ' . mb_strtoupper($method) . ' ' . $path;
        $log .= ($data ? ' | ' . json_encode($data) : '');
        if ($debug) {
            Log::debug($log);
        } else {
            Log::info($log);
        }
    }

    /**
     * Write in log the response of an API
     *
     * @param string $apiName
     * @param string $method
     * @param string $path
     * @param int $status
     * @param mixed $response
     * @param bool $isJsonResponse
     * @param bool $debug
     */
    public static function response(
        string $apiName,
        string $method,
        string $path,
        int $status,
        $response,
        bool $isJsonResponse = true,
        bool $debug = true
    ): void {
        $log = '[' . mb_strtoupper($apiName) . ' API][RESPONSE] ' . mb_strtoupper($method) . ' ' . $path;
        $log .= ' ' . $status . ' => ' . ($isJsonResponse ? json_encode($response) : $response);
        if ($debug) {
            Log::debug($log);
        } else {
            Log::info($log);
        }
    }
}
