<?php

namespace App\Services;

use App\Exceptions\QrCodeTokenNotFoundException;
use App\Exceptions\ServiceNotAvailableException;
use App\Models\CyclosUser;
use App\Services\Cyclos\TokenService;
use App\Services\Cyclos\UserService;
use Endroid\QrCode\QrCode;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Intervention\Image\ImageManager;

class QrCodeService
{
    /** @throws \App\Exceptions\ServiceNotAvailableException */
    public function __construct()
    {
        if (! config('somoney.available.services.qr_code')) {
            throw new ServiceNotAvailableException('The Qr Code Service is not available');
        }
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \App\Exceptions\QrCodeTokenNotFoundException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getCyclosUserQrCodeToken(string $cyclosUserId): array
    {
        $tokenService = app(TokenService::class);
        $tokenTypes = $tokenService->getUserTypeTokens($cyclosUserId);
        $qrCodeTokenType = Arr::first(array_filter(
            $tokenTypes,
            fn(array $tokenType) => data_get(
                $tokenType,
                'type.internalName'
            ) === config('cyclos.internal-names.qr_code')
        ));
        if (empty($qrCodeTokenType)) {
            throw new QrCodeTokenNotFoundException('Cyclos User => ' . $cyclosUserId);
        }
        $userQrCodes = $tokenService->getUserTokens($cyclosUserId, data_get($qrCodeTokenType, 'type.id'));
        $activeQrCodes = array_filter(
            data_get($userQrCodes, 'tokens'),
            fn(array $qrCode) => data_get($qrCode, 'status') === 'active'
        );

        return Arr::first($activeQrCodes, null, []);
    }

    /**
     * @param \App\Models\CyclosUser $cyclosUser
     * @param bool $publicUrl
     *
     * @return string|null
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \App\Exceptions\QrCodeTokenNotFoundException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getCyclosUserQrCodeTokenPoster(CyclosUser $cyclosUser, bool $publicUrl = true): ?string
    {
        $qrCodeImg = $this->getCyclosUserQrCodeTokenImage($cyclosUser, false);
        if ($qrCodeImg === null) {
            return null;
        }
        $filename = 'qr-poster-' . $cyclosUser->id . '-' . Str::slug($cyclosUser->name) . '.jpg';
        $qrCodePosterPath = storage_path('app/public/qr-codes/' . $filename);
        if (File::exists($qrCodePosterPath)) {
            return $publicUrl ? asset('storage/qr-codes/' . $filename) : $qrCodePosterPath;
        }
        (new ImageManager(['driver' => 'imagick']))->make(public_path('assets/img/qr-code-overlay.jpg'))
            ->insert($qrCodeImg, 'top-left', 210, 220)
            ->save($qrCodePosterPath);

        return $publicUrl ? asset('storage/qr-codes/' . $filename) : $qrCodePosterPath;
    }

    /**
     * @param \App\Models\CyclosUser $cyclosUser
     * @param bool $publicUrl
     *
     * @return string|null
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \App\Exceptions\QrCodeTokenNotFoundException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getCyclosUserQrCodeTokenImage(CyclosUser $cyclosUser, bool $publicUrl = true): ?string
    {
        $cyclosUser = CyclosUser::fill((app(UserService::class))->get($cyclosUser->id));
        if (! File::exists(storage_path('app/public/qr-codes'))) {
            File::makeDirectory(storage_path('app/public/qr-codes'));
        }
        $filename = 'qr-' . $cyclosUser->id . '-' . Str::slug($cyclosUser->name) . '.png';
        $qrCodeImgPath = storage_path('app/public/qr-codes/' . $filename);
        if (File::exists($qrCodeImgPath)) {
            return $publicUrl ? asset('storage/qr-codes/' . $filename) : $qrCodeImgPath;
        }
        $label = config('somoney.money') . ' - ' . $cyclosUser->name;
        $qrCodeToken = $this->getCyclosUserQrCodeToken($cyclosUser->id);
        if (! array_key_exists('value', $qrCodeToken)) {
            return null;
        }
        File::put($qrCodeImgPath, $this->generate(data_get($qrCodeToken, 'value'), 'png', 300, $label));

        return $publicUrl ? asset('storage/qr-codes/' . $filename) : $qrCodeImgPath;
    }

    public function generate(string $content, string $writer = 'svg', int $size = 150, ?string $label = null): string
    {
        // Create a basic QR code
        $qrCode = new QrCode($content);
        $qrCode->setSize($size);

        // Set advanced options
        $qrCode->setWriterByName($writer);
        $qrCode->setMargin(10);
        $qrCode->setEncoding('UTF-8');
        if ($label) {
            $qrCode->setLabel($label, 16);
        }

        return $qrCode->writeString();
    }
}
