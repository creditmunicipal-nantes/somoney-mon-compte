<?php

namespace App\Services;

use App\Helpers\ApiLog;
use Illuminate\Support\Carbon;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response;

class InseeService
{
    public const API_PATH = '/entreprises/sirene/V3/';

    protected ?GuzzleClient $client;

    protected ?string $accessToken = null;

    protected Carbon $expiryDateToken;

    protected ?string $apiKey;

    protected ?string $secretKey;

    public function __construct()
    {
        $this->client = new GuzzleClient([
            'base_uri' => config('insee.url'),
            'verify' => false,
        ]);

        $this->apiKey = config('insee.api.key');
        $this->secretKey = config('insee.api.secret');
    }

    public function hasApiKey(): bool
    {
        return $this->apiKey && $this->secretKey;
    }

    public function generateAccessToken(): void
    {
        ApiLog::request('insee', 'post', 'token');
        $response = $this->client->request('POST', 'token', [
            'form_params' => [
                'grant_type' => 'client_credentials',
            ],
            'headers' => [
                'Authorization' => 'Basic ' . base64_encode("{$this->apiKey}:{$this->secretKey}"),
            ],
        ]);
        $data = json_decode($response->getBody()->getContents(), true);
        ApiLog::response('insee', 'post', 'token', $response->getStatusCode(), $data);
        $this->accessToken = $data['access_token'];
        $this->expiryDateToken = now()->addRealSeconds($data['expires_in']);
    }

    /** @throws \GuzzleHttp\Exception\GuzzleException */
    public function getCompanyFromSiren(string $siren): ?array
    {
        if (! $this->accessToken || now()->addMinute()->gt($this->expiryDateToken)) {
            $this->generateAccessToken();
        }

        try {
            ApiLog::request('insee', 'get', self::API_PATH . 'siren/' . $siren);
            $response = $this->client->request('GET', self::API_PATH . 'siren/' . $siren, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->accessToken,
                ],
            ]);
            $data = json_decode($response->getBody()->getContents(), true);
            ApiLog::response('insee', 'get', self::API_PATH . 'siren/' . $siren, $response->getStatusCode(), $data);
            if ($response->getStatusCode() === Response::HTTP_OK) {
                // merge global info of company and last periodical infos
                $companyData = data_get($data, 'uniteLegale');
                $companyData = array_merge(
                    Arr::except($companyData, ['periodesUniteLegale']),
                    Arr::first($companyData['periodesUniteLegale'])
                );

                // return info of company without null values
                return array_filter($companyData, fn($value) => ! is_null($value));
            }
        } catch (RequestException $exception) {
            ApiLog::response(
                'insee',
                'get',
                self::API_PATH . 'siren/' . $siren,
                $exception->getCode(),
                $exception->getMessage(),
                false
            );
            abort(
                $exception->getCode(),
                json_encode([
                    'messages' => [
                        $this->getMessageOfError($exception, $siren, 'le siren'),
                    ],
                ])
            );
        }

        return null;
    }

    /** @throws \GuzzleHttp\Exception\GuzzleException */
    public function getCompanyFromName(string $name): ?array
    {
        if (! $this->accessToken || now()->addMinute()->gt($this->expiryDateToken)) {
            $this->generateAccessToken();
        }
        try {
            $departmentFilter = array_reduce(
                explode(',', config('insee.filters.departments')),
                fn(string $departmentFilter, string $department) => $departmentFilter . 'codePostalEtablissement:'
                    . $department . '* OR ',
                ''
            );
            $departmentFilter = Str::replaceLast(' OR ', '', $departmentFilter);
            $departmentFilter = empty($departmentFilter) ?: ' AND (' . $departmentFilter . ')';
            ApiLog::request('insee', 'post', self::API_PATH . 'siret', [
                'q' => 'denominationUniteLegale:' . str_replace(' ', '-', $name) . $departmentFilter,
            ]);
            $response = $this->client->request(
                'POST',
                self::API_PATH . 'siret',
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $this->accessToken,
                    ],
                    'form_params' => [
                        'q' => 'denominationUniteLegale:' . str_replace(' ', '-', $name)
                            . $departmentFilter,
                        'masquerValeursNulles' => true,
                    ],
                ]
            );
            $data = json_decode($response->getBody()->getContents(), true);
            ApiLog::response('insee', 'post', self::API_PATH . 'siret', $response->getStatusCode(), $data);
            if ($response->getStatusCode() === Response::HTTP_OK) {
                return $this->cleanSiretCompanies($data);
            }
        } catch (RequestException $exception) {
            ApiLog::response(
                'insee',
                'post',
                self::API_PATH . 'siret',
                $exception->getCode(),
                $exception->getMessage(),
                false
            );
            abort(
                $exception->getCode(),
                json_encode([
                    'messages' => [
                        $this->getMessageOfError($exception, $name, 'le nom'),
                    ],
                ])
            );
        }

        return null;
    }

    /** @throws \GuzzleHttp\Exception\GuzzleException */
    public function getCompanySiretWithSiren(string $siren): ?array
    {
        if (! $this->accessToken || now()->addMinute()->gt($this->expiryDateToken)) {
            $this->generateAccessToken();
        }

        try {
            ApiLog::request('insee', 'post', self::API_PATH . 'siret', [
                'q' => 'siren:' . $siren,
            ]);
            $response = $this->client->request(
                'POST',
                self::API_PATH . 'siret',
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $this->accessToken,
                    ],
                    'form_params' => [
                        'q' => 'siren:' . $siren,
                        'masquerValeursNulles' => true,
                    ],
                ]
            );
            $data = json_decode($response->getBody()->getContents(), true);
            ApiLog::response('insee', 'post', self::API_PATH . 'siret', $response->getStatusCode(), $data);
            if ($response->getStatusCode() === Response::HTTP_OK) {
                $companies = $this->cleanSiretCompanies($data);

                // return info of company without null values
                return array_shift($companies);
            }
        } catch (RequestException $exception) {
            ApiLog::response(
                'insee',
                'post',
                self::API_PATH . 'siret',
                $exception->getCode(),
                $exception->getMessage(),
                false
            );
            abort(
                $exception->getCode(),
                json_encode([
                    'messages' => [
                        $this->getMessageOfError($exception, $siren, 'le siret'),
                    ],
                ])
            );
        }

        return null;
    }

    protected function getMessageOfError(RequestException $exception, string $value, ?string $filter = null): ?string
    {
        $response = json_decode($exception->getResponse()->getBody()->getContents(), true);
        $status = $response['header']['statut'];
        switch ($status) {
            case 301:
                return 'L\'entreprise est en doublon';
            case 404:
                return 'Aucune entreprise trouvé avec ' . ($filter ? $filter . ' ' : '') . $value;
            case 429:
                throw new RuntimeException('Insee API - ' . $response['header']['message']);
            default:
                throw $exception;
        }
    }

    /**
     * Keep only the last version of information about companies
     */
    protected function cleanSiretCompanies(array $data): array
    {
        $companies = [];
        foreach ($data['etablissements'] as $companyData) {
            if (array_key_exists($companyData['siren'], $companies)) {
                $currentCreationDate = Carbon::parse($companyData['dateCreationEtablissement']);
                $existCreationDate = Carbon::parse($companies[$companyData['siren']]['dateCreationEtablissement']);
                if ($currentCreationDate->lt($existCreationDate)) {
                    // update if newer
                    continue;
                }
            }
            // merge global info of company and last periodical infos
            $companyData = array_merge(
                Arr::except($companyData, ['periodesEtablissement']),
                Arr::first($companyData['periodesEtablissement'])
            );
            // return info of company without null values
            $companies[$companyData['siren']] = array_filter($companyData, fn($value) => ! is_null($value));
        }

        return $companies;
    }
}
