<?php

namespace App\Services\Cyclos;

use App\Services\Cyclos;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class TransactionService extends Cyclos
{
    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function listAsAdmin(array $params = []): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsAdmin();
        $params = array_filter($params);
        $uri = 'transactions?' . http_build_query([
                'page' => max(1, (int) data_get($params, 'page', 1)) - 1,
                'pageSize' => max(1, (int) data_get($params, 'pageSize', 40)),
                'datePeriod' => data_get($params, 'datePeriod'),
                'kinds' => data_get($params, 'kinds'),
                'recurringPaymentStatuses' => data_get($params, 'recurringPaymentStatuses'),
                'scheduledPaymentStatuses' => data_get($params, 'scheduledPaymentStatuses'),
                'fromAccountTypes' => data_get($params, 'fromAccountTypes'),
                'toAccountTypes' => data_get($params, 'toAccountTypes'),
                'transferTypes' => data_get($params, 'transferTypes'),
                'user' => data_get($params, 'user'),
                'groups' => data_get($params, 'groups'),
            ]);

        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function list(array $params = [], bool $clientAdmin = false): LengthAwarePaginator|array|null|int
    {
        if ($clientAdmin) {
            $client = $this->getClientAsAdmin();
        } else {
            $client = $this->getClientAsUser($this->sessionToken);
        }
        $params = array_filter($params);
        $uri = 'self/transactions?' . http_build_query([
                'page' => max(1, (int) data_get($params, 'page', 1)) - 1,
                'pageSize' => max(1, (int) data_get($params, 'pageSize', 40)),
                'datePeriod' => data_get($params, 'datePeriod'),
                'kinds' => data_get($params, 'kinds'),
                'recurringPaymentStatuses' => data_get($params, 'recurringPaymentStatuses'),
                'scheduledPaymentStatuses' => data_get($params, 'scheduledPaymentStatuses'),
                'user' => data_get($params, 'user'),
            ]);

        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function listOfUser(array $params = [], bool $clientAdmin = false): LengthAwarePaginator|array|null|int
    {
        if ($clientAdmin) {
            $client = $this->getClientAsAdmin();
        } else {
            $client = $this->getClientAsUser($this->sessionToken);
        }
        $params = array_filter($params);
        $uri = data_get($params, 'userId') . '/transactions?' . http_build_query([
                'page' => max(1, (int) data_get($params, 'page', 1)) - 1,
                'pageSize' => max(1, (int) data_get($params, 'pageSize', 40)),
                'datePeriod' => data_get($params, 'datePeriod'),
                'kinds' => data_get($params, 'kinds'),
                'recurringPaymentStatuses' => data_get($params, 'recurringPaymentStatuses'),
                'scheduledPaymentStatuses' => data_get($params, 'scheduledPaymentStatuses'),
                'user' => data_get($params, 'user'),
                'fields' => data_get($params, 'fields'),
            ]);
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function listAll(array $params = [], bool $clientAdmin = false): Collection
    {
        return $this->getRequestAllPages(fn($page, $pageSize = 1000) => $this->list(
            array_replace_recursive($params, [
                'pageSize' => $pageSize,
                'page' => $page,
            ]),
            $clientAdmin
        ));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function allAsAdmin(array $params = []): Collection
    {
        return $this->getRequestAllPages(fn($page, $pageSize = 1000) => $this->listAsAdmin(
            array_replace_recursive($params, [
                'pageSize' => $pageSize,
                'page' => $page,
            ])
        ));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(string $key, bool $clientAdmin = false): LengthAwarePaginator|array|null|int
    {
        if ($clientAdmin) {
            $client = $this->getClientAsAdmin();
        } else {
            $client = $this->getClientAsUser($this->sessionToken);
        }
        $uri = 'transactions/' . $key;

        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function store(array $params = []): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $data = [
            'subject' => data_get($params, 'toUser'),
            'amount' => data_get($params, 'amount'),
            'description' => data_get($params, 'description'),
            'type' => data_get($params, 'type'),
        ];
        $scheduleDate = data_get($params, 'scheduleDate');
        $recurrent = data_get($params, 'recurrent');
        if (($scheduleDate->isFuture()) && (! $recurrent)) {
            data_set($data, 'scheduling', 'scheduled');
            data_set($data, 'installmentsCount', 1);
            data_set($data, 'firstInstallmentDate', $scheduleDate->toIso8601String());
        }
        if ($recurrent) {
            data_set($data, 'scheduling', 'recurring');
            data_set($data, 'occurrencesCount', data_get($params, 'occurrencesCount'));
            data_set($data, 'firstOccurrenceDate', $scheduleDate->toIso8601String());
            data_set($data, 'occurrenceInterval', data_get($params, 'occurrenceInterval'));
        }
        $request = $this->createRequest(
            'POST',
            'self/payments',
            [],
            json_encode($data)
        );

        return $this->processRequest($request, $client, true);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function performAsUser(
        string $fromUser,
        string $toUser,
        float $amount,
        string $description,
        string $type = null
    ): LengthAwarePaginator|array|null|int {
        $client = $this->getClientAsAdmin();
        $data = [
            'subject' => $toUser,
            'amount' => $amount,
            'description' => $description,
        ];
        if ($type) {
            $data['type'] = $type;
        }
        $request = $this->createRequest(
            'POST',
            $fromUser . '/payments',
            [],
            json_encode($data)
        );

        return $this->processRequest($request, $client, true);
    }

    /**
     * The function to perform system payment
     *
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function storeInitialAcompte(
        string $toUser,
        float $amount,
        string $description,
        string $type
    ): LengthAwarePaginator|array|null|int {
        $client = $this->getClientAsAdmin();
        $data = [
            'subject' => $toUser,
            'amount' => $amount,
            'description' => $description,
            'type' => $type,
        ];
        $request = $this->createRequest(
            'POST',
            'system/payments',
            [],
            json_encode($data, JSON_THROW_ON_ERROR)
        );

        return $this->processRequest($request, $client, true);
    }

    /**
     * The function to perform reconversion payment
     *
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function storeUserReconversion(
        string $fromUser,
        float $amount,
        string $userType
    ): LengthAwarePaginator|array|null|int {
        return $this->performAsUser(
            $fromUser,
            'system',
            $amount,
            'Reconversion - ' . config('somoney.money'),
            $userType . '.toStableAccount'
        );
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function returnBanknotes(float $amount, string $description): LengthAwarePaginator|array|null|int
    {
        return $this->performAsUser(config('somoney.counters-cyclos-account'), 'system', $amount, $description);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function outputBanknotes(float $amount, string $description): LengthAwarePaginator|array|null|int
    {
        return $this->performAsUser('system', config('somoney.counters-cyclos-account'), $amount, $description);
    }

    public function sumOfTransactions(Collection $transactions): float
    {
        return $transactions->sum(
            fn($transaction) => $transaction['kind'] === 'recurringPayment'
                ? (float) data_get($transaction, 'amount', '0') * (int) $transaction['lastOccurrenceNumber']
                : (float) data_get($transaction, 'amount', '0')
        );
    }

    public function filterFromTo(Collection $transactions, string $fromUser, string $toUser): Collection
    {
        return $transactions->filter(
            fn(array $transaction) => data_get($transaction['type']['from'], 'internalName') === $fromUser
                && data_get($transaction['type']['to'], 'internalName') === $toUser
        );
    }
}
