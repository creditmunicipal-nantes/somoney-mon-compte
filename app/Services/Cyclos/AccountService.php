<?php

namespace App\Services\Cyclos;

use App\Services\Cyclos;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

class AccountService extends Cyclos
{
    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getUserAccounts(string $userId = 'self'): LengthAwarePaginator|array|null|int
    {
        $client = $userId === 'self' ? $this->getClientAsUser($this->sessionToken) : $this->getClientAsAdmin();

        $request = $this->createRequest(
            'GET',
            $userId . '/accounts?fields=id,type.internalName,status'
        );

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function fullHistory(string $userId, string $accountType, array $params = []): Collection
    {
        return $this->getRequestAllPages(fn($page = 1, $pageSize = 1000) => $this->history(
            $userId,
            $accountType,
            array_replace_recursive($params, [
                'page' => $page,
                'pageSize' => $pageSize,
            ])
        ));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function history(
        string $userId,
        string $accountType,
        array $params = []
    ): LengthAwarePaginator|array|null|int {
        $params = array_filter($params);
        $client = $this->getClientAsAdmin();
        $uri = "$userId/accounts/$accountType/history?" . http_build_query([
                'page' => max(1, (int) data_get($params, 'page', 1)) - 1,
                'pageSize' => max(1, (int) data_get($params, 'pageSize', 10)),
                'datePeriod' => data_get($params, 'datePeriod'),
                'direction' => data_get($params, 'direction'),
                'channels' => data_get($params, 'channels'),
                'orderBy' => data_get($params, 'orderBy', 'dateDesc'),
            ]);
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }

    /**
     * @return float|int
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function getEquation()
    {
        $adminsAccounts = $this->getAdminAccounts();
        $stableAccount = Arr::where(
            $adminsAccounts,
            fn($key) => data_get(data_get($key, 'type'), 'internalName') === 'stableAccount'
        );
        $stableBalance = (int) data_get(data_get(array_values($stableAccount), '0'), 'status.balance');
        $stabilityAccount = Arr::where(
            $adminsAccounts,
            fn($key) => data_get(data_get($key, 'type'), 'internalName') === 'stabilityAccount'
        );
        $stabilityBalance = data_get(data_get(array_values($stabilityAccount), '0'), 'status.balance');
        $professionalAccounts = $this->fullUsersAccountsBalance('professional');
        $positiveProBalance = $professionalAccounts->reduce(function ($carry, $value) {
            $balance = (double) data_get($value, 'balance');
            if ($balance > 0) {
                $carry += $balance;
            }

            return $carry;
        }, 0);
        $negativeProBalance = $professionalAccounts->reduce(function ($carry, $value) {
            $balance = (double) data_get($value, 'balance');
            if ($balance < 0) {
                $carry += $balance;
            }

            return $carry;
        }, 0);
        $individualAccounts = $this->fullUsersAccountsBalance('individual');
        $individualBalance =
            $individualAccounts->reduce(fn($carry, $value) => $carry + (double) data_get($value, 'balance'), 0);
        $equation = round($stabilityBalance
            + $individualBalance
            + $positiveProBalance
            + $negativeProBalance
            + $stableBalance, 2);
        if ($equation === -0.0) {
            $equation = 0;
        }

        return $equation;
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getAdminAccounts(): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsAdmin();
        $uri = "system/accounts";
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function fullUsersAccountsBalance(string $accountType, array $params = []): Collection
    {
        return $this->getRequestAllPages(fn($page = 1, $pageSize = 1000) => $this->getUsersAccountsBalance(
            $accountType,
            array_replace_recursive($params, [
                'page' => $page,
                'pageSize' => $pageSize,
            ])
        ));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getUsersAccountsBalance(
        string $accountType,
        array $params = []
    ): LengthAwarePaginator|array|null|int {
        $params = array_filter($params);
        $client = $this->getClientAsAdmin();
        $uri = "accounts/$accountType/user-balances?" . http_build_query([
                'page' => max(1, (int) data_get($params, 'page', 1)) - 1,
                'pageSize' => max(1, (int) data_get($params, 'pageSize', 40)),
                'usersToInclude' => data_get($params, 'usersToInclude'),
                'fields' => data_get($params, 'fields'),
            ]);
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }
}
