<?php

namespace App\Services\Cyclos;

use App\Services\Cyclos;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;

class AdService extends Cyclos
{
    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function list(array $params = []): LengthAwarePaginator|array|null|int
    {
        $params = array_filter($params);
        $client = $this->getClientAsAdmin();
        $uri = 'marketplace?' . http_build_query([
                'page' => max(1, (int) data_get($params, 'page', 1)) - 1,
                'pageSize' => max(1, (int) data_get($params, 'pageSize', 40)),
                'category' => data_get($params, 'category'),
                'owner' => data_get($params, 'owner'),
                'orderBy' => data_get($params, 'orderby', 'date'),
            ]);
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getAsAdmin(string $adId): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsAdmin();
        $uri = 'marketplace/' . $adId;
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(string $adId): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $uri = 'marketplace/' . $adId;
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function store(
        string $name,
        string $description,
        Carbon $startDate,
        Carbon $endDate,
        array $categories,
        string $currency,
        int $userId,
        string $tags = null
    ): LengthAwarePaginator|array|null|int {
        $client = $this->getClientAsUser($this->sessionToken);
        $data = [
            'name' => $name,
            'description' => $description,
            'categories' => $categories,
            'currency' => $currency,
            'price' => 0.0,
            'publicationPeriod' => [
                'begin' => $startDate->toIso8601String(),
                'end' => $endDate->toIso8601String(),
            ],
            'customValues' => [
                'tags' => $tags,
            ],
        ];
        $request = $this->createRequest(
            'POST',
            $userId . '/marketplace',
            [],
            json_encode($data)
        );

        return $this->processRequest($request, $client, true);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function update(
        string $name,
        string $description,
        Carbon $startDate,
        Carbon $endDate,
        array $categories,
        string $adId,
        string $tags = null
    ): LengthAwarePaginator|array|null|int {
        $client = $this->getClientAsUser($this->sessionToken);
        $data = data_get($this->dataForEdit($adId), 'advertisement');
        data_set($data, 'name', $name);
        data_set($data, 'description', $description);
        data_set($data, 'categories', $categories);
        data_set($data, 'publicationPeriod.begin', $startDate->toIso8601String());
        data_set($data, 'publicationPeriod.end', $endDate->toIso8601String());
        data_set($data, 'customValues.tags', $tags);
        $request = $this->createRequest(
            'PUT',
            'marketplace/' . $adId,
            [],
            json_encode($data)
        );

        return $this->processRequest($request, $client, true);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function dataForEdit(string $adId): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $uri = "marketplace/$adId/data-for-edit";
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function dataForNew(string $userId = null): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $uri = ($userId ?: 'self') . '/marketplace/data-for-new';
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function dataForSearch(string $fields = null): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsAdmin();
        $uri = 'marketplace/data-for-search';
        if ($fields) {
            $uri .= '?fields=' . $fields;
        }
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete(string $adId): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $uri = 'marketplace/' . $adId;
        $request = $this->createRequest('DELETE', $uri);

        return $this->processRequest($request, $client, true);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function setAdImage(string $adId, UploadedFile $newImage): LengthAwarePaginator|array|null|int
    {
        $fileExtension = $newImage->getClientOriginalExtension();

        $imageResized = Image::make($newImage->path())
            ->resize(380, 380, function (Constraint $constraint) {
                $constraint->aspectRatio();
            })->stream();

        $imageData = [
            [
                'name' => 'image',
                'contents' => $imageResized,
                'filename' => 'adImage.' . $fileExtension,
            ],
        ];

        return $this->storeImage($adId, $imageData);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function storeImage(string $adId, array $data): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $request = $this->createMultipartRequest('POST', "marketplace/$adId/images", $data);

        return $this->processRequest($request, $client, true);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getAdImages(string $adId): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $request = $this->createRequest('GET', "marketplace/$adId/images");

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function deleteImage(string $imageId): LengthAwarePaginator|array|null
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $request = $this->createRequest('DELETE', 'images/' . $imageId);

        return $this->processRequest($request, $client, true);
    }
}
