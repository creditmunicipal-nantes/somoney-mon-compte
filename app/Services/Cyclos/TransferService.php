<?php

namespace App\Services\Cyclos;

use App\Services\Cyclos;

class TransferService extends Cyclos
{
    public const MOBILE_TRANSFER = 'mobile';

    public const WEB_TRANSFER = 'web';

    /**
     * @param string $transferId
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function get(string $transferId)
    {
        $client = $this->getClientAsAdmin();
        $uri = 'transfers/' . $transferId;
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }

    /**
     * @param array $params
     *
     * @return mixed
     */
    public function listAll(array $params = [])
    {
        return $this->getRequestAllPages(fn($page, $pageSize = 1000) => $this->list(
            array_replace_recursive($params, [
                'pageSize' => $pageSize,
                'page' => $page,
            ])
        ));
    }

    /**
     * @param array $params
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function list(array $params = [])
    {
        $params = array_filter($params);
        $client = $this->getClientAsAdmin();
        $uri = 'transfers?' . http_build_query([
                'datePeriod' => data_get($params, 'datePeriod', []),
                'channels' => implode(',', data_get($params, 'channels', [])),
                'transferFilters' => implode(',', data_get($params, 'transferFilters', [])),
                'page' => max(1, (int) data_get($params, 'page')) - 1,
                'pageSize' => max(1, (int) data_get($params, 'pageSize', 40)),
                'user' => data_get($params, 'user', null),
                'fromAccountType' => data_get($params, 'fromAccountType', null),
                'toAccountType' => data_get($params, 'toAccountType', null),
                'fields' => data_get($params, 'fields', []),
                'by' => data_get($params, 'by'),
            ]);
        $request = $this->createRequest('GET', urldecode($uri));

        return $this->processRequest($request, $client);
    }
}
