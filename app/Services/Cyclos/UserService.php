<?php

namespace App\Services\Cyclos;

use App\Models\LocalUser;
use App\Models\UsersMatching;
use App\Services\Cyclos;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Arr;

class UserService extends Cyclos
{
    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getByCevId(string $cevId, array $params = []): array
    {
        $match = UsersMatching::where('cev_id', $cevId)->firstOrFail();

        return $this->get($match->user_id, $params);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function get(string $id, array $params = []): LengthAwarePaginator|array|null|int
    {
        $params = array_filter($params);
        $uri = 'users/' . $id . '?' . http_build_query([
                'fields' => implode(',', data_get($params, 'fields', [])),
            ]);
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function setUserData(string $userId, array $newUser): LengthAwarePaginator|array|null|int
    {
        $user = $this->getDataForEdit($userId)['user'];
        if (Arr::has($newUser, 'email') && data_get($user, 'email') !== data_get($newUser, 'email')) {
            session()->flash('newEmail', true);
            data_set($user, 'customValues.validatedemail', false);
        }
        $user = array_replace_recursive($user, $newUser);

        return $this->update($userId, $user);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getDataForEdit(string $id): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsAdmin();
        $request = $this->createRequest('GET', 'users/' . $id . '/data-for-edit');

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function update(string $userId, array $data): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsAdmin();
        $request = $this->createRequest('PUT', 'users/' . $userId, [], json_encode($data));

        return $this->processRequest($request, $client, true);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function exists(array $search): bool
    {
        $response = $this->search($search);

        return $response && $response instanceof LengthAwarePaginator && ! $response->isEmpty();
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function search(array $search): ?LengthAwarePaginator
    {
        $searchProfileFields = data_get($search, 'profileFields') ?? [];
        $profileFields = implode(',', array_map(
            function ($key, $value) {
                if (is_array($value)) {
                    $value = implode('|', $value);
                }

                return $key . ':' . $value;
            },
            array_keys($searchProfileFields),
            $searchProfileFields
        ));
        $usersIdsToExclude = data_get($search, 'usersIdsToExclude');
        if (config('somoney.available.services.counters')) {
            $usersIdsToExclude = array_merge(
                Arr::wrap($usersIdsToExclude),
                [config('somoney.counters-cyclos-account')]
            );
        }
        $uri = 'users?' . implode('&', array_filter(array_merge([
                http_build_query(['profileFields' => $profileFields]),
                http_build_query(['keywords' => data_get($search, 'keywords')]),
                http_build_query(['groups' => data_get($search, 'groups')]),
                http_build_query(['page' => max(1, (int) data_get($search, 'page')) - 1]),
                http_build_query(['pageSize' => max(1, (int) data_get($search, 'pageSize', 40))]),
                http_build_query(['addressResult' => data_get($search, 'address', 'none')]),
                http_build_query(['includeGroup' => true]),
                http_custom_build_query(['usersToExclude' => $usersIdsToExclude]),
                http_custom_build_query(['usersToInclude' => data_get($search, 'usersIdsToInclude')]),
                http_custom_build_query(['statuses' => data_get($search, 'statuses')]),
            ])));

        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request);
    }

    public function searchAll(array $search): Collection
    {
        return $this->getRequestAllPages(fn($page, $pageSize = 1000) => $this->search(
            array_replace_recursive($search, [
                'pageSize' => $pageSize,
                'page' => $page,
            ])
        ));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getDataForNew(string $group): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsAdmin();
        $request = $this->createRequest('GET', "users/data-for-new?group=$group");

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getDataForSearch(string $fields = null): LengthAwarePaginator|array|null|int
    {
        $uri = 'users/data-for-search';
        if ($fields) {
            $uri .= '?fields=' . $fields;
        }
        $client = $this->getClientAsAdmin();
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getProfileImage(string $id): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsAdmin();
        $request = $this->createRequest('GET', $id . '/images', ['kind' => 'profile']);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getUserAccount(string $userId = 'self'): mixed
    {
        $accounts = collect(app(AccountService::class)->getUserAccounts($userId));

        return $accounts->first(fn($account) => in_array(
            data_get($account, 'type.internalName'),
            ['individual', 'professional']
        ));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getUsersNewRecent(string $group): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsAdmin([], true);
        $request = $this->createRequest('GET', 'users/new/recent/' . $group);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function createUserFromLocal(string $userId): LengthAwarePaginator|array|null|int
    {
        $localUser = LocalUser::find($userId);
        $data = $localUser->toCyclosArray();
        $data['skipActivationEmail'] = true;
        $client = $this->getClientAsAdmin();
        $data['customValues'] = collect($data['customValues'])->keys()->reduce(
            function ($customValues, $customValueKey) use ($data) {
                $customValue = $data['customValues'][$customValueKey];
                if ($customValue !== null) {
                    $customValues[$customValueKey] =
                        is_bool($customValue) ? json_encode($customValue) : $customValue; // stringify bool
                }

                return $customValues;
            },
            []
        );
        $request = $this->createRequest('POST', 'users', [], json_encode($data));

        return $this->processRequest($request, $client, true);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getStatus(string $userId): LengthAwarePaginator|array|null|int
    {
        $uri = $userId . '/status/';
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function setStatus(
        string $userId,
        string $status,
        string $comment
    ): \Illuminate\Pagination\LengthAwarePaginator|array|null {
        $data = [
            'status' => $status,
            'comment' => $comment,
        ];
        $request = $this->createRequest('POST', $userId . '/status/', [], json_encode($data));

        return $this->processRequest($request);
    }
}
