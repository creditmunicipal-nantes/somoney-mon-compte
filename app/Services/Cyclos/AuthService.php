<?php

namespace App\Services\Cyclos;

use App\Services\Cyclos;

class AuthService extends Cyclos
{
    /**
     * @param array $credentials
     *
     * @return array
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function login(array $credentials): array
    {
        $client = $this->getClientAsUser([
            'user' => data_get($credentials, 'login'),
            'password' => data_get($credentials, 'password'),
        ]);
        $request = $this->createRequest('POST', 'auth/session');

        return $this->processRequest($request, $client);
    }

    /**
     * @param string $token
     *
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function logout(string $token)
    {
        $client = $this->getClientAsUser($token);
        $request = $this->createRequest('DELETE', 'auth/session');
        $this->processRequest($request, $client);
    }

    /**
     * @param string $token
     *
     * @return array
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function currentUser(string $token): array
    {
        $client = $this->getClientAsUser($token);
        $request = $this->createRequest('GET', 'auth');

        return $this->processRequest($request, $client);
    }
}
