<?php

namespace App\Services\Cyclos;

use App\Services\Cyclos;

class TokenService extends Cyclos
{
    /**
     * @param string $cyclosUserId
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getUserTypeTokens(string $cyclosUserId)
    {
        $client = $this->getClientAsAdmin();
        $uri = $cyclosUserId . '/token-types';
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }

    /**
     * @param string $cyclosUserId
     * @param string $tokenType
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getUserTokens(string $cyclosUserId, string $tokenType)
    {
        $client = $this->getClientAsAdmin();
        $uri = $cyclosUserId . '/tokens/' . $tokenType;
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }

    /**
     * @param string $tokenId
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getToken(string $tokenId)
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $uri = 'tokens/' . $tokenId;
        $request = $this->createRequest('GET', $uri);

        return $this->processRequest($request, $client);
    }
}
