<?php

namespace App\Services\Cyclos\User;

use App\Services\Cyclos;
use Illuminate\Pagination\LengthAwarePaginator;

class PhoneService extends Cyclos
{
    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function get(string $id): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $request = $this->createRequest('GET', 'phones/' . $id);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function index(string $userId, bool $asAdmin): LengthAwarePaginator|array|null|int
    {
        $client = $asAdmin ? $this->getClientAsAdmin() : $this->getClientAsUser($this->sessionToken);

        $request = $this->createRequest('GET', $userId . '/phones');

        return $this->processRequest($request, $client);
    }

    /**
     * @param string $userId
     * @param string $newPhone
     * @param bool $asAdmin
     *
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function setUserPhone(string $userId, string $newPhone, bool $asAdmin = false): void
    {
        $phones = $this->index($userId, $asAdmin);
        if (is_array($phones)) {
            foreach ($phones as $phone) {
                $this->delete($phone['id'], $asAdmin);
            }
        }

        $newPhone = preg_replace('/^(33)?0?/', '+33', preg_replace('/\D/', '', $newPhone));
        if (preg_match('/^\+33[67]/', $newPhone)) {
            $phoneType = 'mobile';
        } else {
            $phoneType = 'landLine';
        }

        $phone = data_get($this->getDataForNew($userId, $phoneType, $asAdmin), 'phone');
        $phone['name'] = 'phone';
        $phone['number'] = $newPhone;
        $phone['kind'] = $phoneType;

        $this->store($userId, $phone, $asAdmin);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function store(string $userId, array $data, bool $asAdmin): void
    {
        $client = $asAdmin ? $this->getClientAsAdmin() : $this->getClientAsUser($this->sessionToken);

        $request = $this->createRequest('POST', $userId . '/phones', [], json_encode($data));

        $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function delete(string $id, bool $asAdmin): void
    {
        $client = $asAdmin ? $this->getClientAsAdmin() : $this->getClientAsUser($this->sessionToken);

        $request = $this->createRequest('DELETE', 'phones/' . $id);

        $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getDataForNew(string $userId, string $phoneType, bool $asAdmin): LengthAwarePaginator|array|null|int
    {
        $client = $asAdmin ? $this->getClientAsAdmin() : $this->getClientAsUser($this->sessionToken);

        $request = $this->createRequest('GET', $userId . '/phones/data-for-new?type=' . $phoneType);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getDataForEdit(string $id, bool $asAdmin): LengthAwarePaginator|array|null|int
    {
        $client = $asAdmin ? $this->getClientAsAdmin() : $this->getClientAsUser($this->sessionToken);

        $request = $this->createRequest('GET', 'phones/' . $id . '/data-for-edit');

        return $this->processRequest($request, $client);
    }
}
