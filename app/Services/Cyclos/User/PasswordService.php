<?php

namespace App\Services\Cyclos\User;

use App\Services\Cyclos;
use Illuminate\Pagination\LengthAwarePaginator;

class PasswordService extends Cyclos
{
    public const STATUS_ACTIVE = 'active';

    public const STATUS_EXPIRED = 'expired';

    public const STATUS_RESET = 'reset';

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function get(
        string $userId,
        string $type = 'login'
    ): LengthAwarePaginator|array|null|int {
        $client = $this->getClientAsAdmin();
        $request = $this->createRequest('GET', $userId . '/passwords/' . $type);

        return $this->processRequest($request, $client);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function update(string $userId, string $password): LengthAwarePaginator|array|null|int
    {
        $client = $this->getClientAsAdmin();
        $data = [
            'newPassword' => $password,
        ];
        $request = $this->createRequest('POST', $userId . '/passwords/login/change', [], json_encode($data));

        return $this->processRequest($request, $client, true);
    }
}
