<?php

namespace App\Services\Cyclos\User;

use App\Services\Cyclos;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;

class ImageService extends Cyclos
{
    /**
     * @param string $userId
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function index(string $userId)
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $request = $this->createRequest('GET', $userId . '/images');

        return $this->processRequest($request, $client);
    }

    /**
     * @param string $userId
     * @param \Illuminate\Http\UploadedFile $newImage
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function setUserImage(string $userId, UploadedFile $newImage)
    {
        $images = $this->index($userId);
        foreach ($images as $image) {
            $this->delete($image['id']);
        }
        $fileExtension = $newImage->getClientOriginalExtension();

        $imageResized = Image::make($newImage->path())
            ->resize(380, 380, function (Constraint $constraint) {
                $constraint->aspectRatio();
            })->stream();

        $image = [
            'name' => 'image',
            'contents' => $imageResized,
            'filename' => 'profile.' . $fileExtension,
        ];
        $imageType = [
            'name' => 'kind',
            'contents' => 'profile',
        ];
        $imageData = [$image, $imageType];

        return $this->store($userId, $imageData);
    }

    /**
     * @param string $userId
     * @param array $data
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function store(string $userId, array $data)
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $request = $this->createMultipartRequest('POST', $userId . '/images', $data);

        return $this->processRequest($request, $client);
    }

    /**
     * @param string $id
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function delete(string $id)
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $request = $this->createRequest('DELETE', 'images/' . $id);

        return $this->processRequest($request, $client);
    }
}
