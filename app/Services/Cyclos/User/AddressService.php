<?php

namespace App\Services\Cyclos\User;

use App\Exceptions\Cyclos\NotFoundException;
use App\Services\Cyclos;
use Illuminate\Support\Arr;

class AddressService extends Cyclos
{
    /**
     * @param string $id
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function get(string $id)
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $request = $this->createRequest('GET', 'addresses/' . $id);

        return $this->processRequest($request, $client);
    }

    /**
     * @param string $userId
     * @param array $newAddress
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function setUserAddress(string $userId, array $newAddress)
    {
        $addresses = $this->index($userId);
        $oldAddress = Arr::first($addresses);
        foreach ($addresses as $address) {
            try {
                $this->delete($address['id']);
            } catch (NotFoundException $exception) {
                logger()->error('[CYCLOS] Address DELETE => ' . $exception->getMessage());
            }
        }
        if (is_null($oldAddress)) {
            $address = data_get($this->getDataForNew($userId), 'address', []);
            $address = array_replace_recursive($address, $newAddress);

            return $this->store($userId, $address);
        } else {
            $address = data_get($this->getDataForEdit($oldAddress['id']), 'address', []);
            $address = array_replace_recursive($address, $newAddress);

            return $this->update($oldAddress['id'], $address);
        }
    }

    /**
     * @param string $userId
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function index(string $userId = 'self')
    {
        $client = $userId === 'self' ? $this->getClientAsUser($this->sessionToken) : $this->getClientAsAdmin();
        $request = $this->createRequest('GET', $userId . '/addresses');

        return $this->processRequest($request, $client);
    }

    /**
     * @param string $id
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function delete(string $id)
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $request = $this->createRequest('DELETE', 'addresses/' . $id);

        return $this->processRequest($request, $client, true);
    }

    /**
     * @param string $userId
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getDataForNew(string $userId)
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $request = $this->createRequest('GET', $userId . '/addresses/data-for-new');

        return $this->processRequest($request, $client);
    }

    /**
     * @param string $userId
     * @param array $data
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function store(string $userId, array $data)
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $request = $this->createRequest('POST', $userId . '/addresses', [], json_encode($data));

        return $this->processRequest($request, $client, true);
    }

    /**
     * @param string $id
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getDataForEdit(string $id)
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $request = $this->createRequest('GET', 'addresses/' . $id . '/data-for-edit');

        return $this->processRequest($request, $client);
    }

    /**
     * @param string $id
     * @param array $data
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function update(string $id, array $data)
    {
        $client = $this->getClientAsUser($this->sessionToken);
        $request = $this->createRequest('PUT', 'addresses/' . $id, [], json_encode($data));

        return $this->processRequest($request, $client, true);
    }

    /**
     * @param string $id
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getDataForEditAsAdmin(string $id)
    {
        $request = $this->createRequest('GET', 'addresses/' . $id . '/data-for-edit');

        return $this->processRequest($request);
    }

    /**
     * @param string $userId
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function indexAsAdmin(string $userId)
    {
        $request = $this->createRequest('GET', $userId . '/addresses');

        return $this->processRequest($request);
    }

    /**
     * @param string $id
     * @param array $data
     *
     * @return \Illuminate\Pagination\LengthAwarePaginator|mixed
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function updateAsAdmin(string $id, array $data)
    {
        $request = $this->createRequest('PUT', 'addresses/' . $id, [], json_encode($data));

        return $this->processRequest($request, null, true);
    }
}
