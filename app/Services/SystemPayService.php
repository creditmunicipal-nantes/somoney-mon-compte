<?php

namespace App\Services;

use App\Models\LocalUser;
use App\Models\Transaction;
use App\Services\Cyclos\UserService;
use Lyra\Client as LyraClient;
use Lyra\Exceptions\LyraException;
use Ramsey\Uuid\Uuid;

class SystemPayService
{
    public function __construct()
    {
        LyraClient::setDefaultUsername(config('system-pay.username'));
        LyraClient::setDefaultPassword(config('system-pay.password'));
        LyraClient::setDefaultEndpoint(config('system-pay.endpoint'));
        LyraClient::setDefaultPublicKey(config('system-pay.public_key'));
        LyraClient::setDefaultSHA256Key(config('system-pay.sha_key'));
    }

    /**
     * @param int $amount
     * @param string $userId
     * @param string $context
     * @param \App\Models\Transaction|null $transaction
     *
     * @return string
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Lyra\Exceptions\LyraException
     */
    public function createPayment(
        int $amount,
        string $userId,
        string $context,
        ?Transaction $transaction = null
    ): string {
        if (! $transaction) {
            $transaction = Transaction::create([
                'type' => Transaction::TYPE_SYSTEM_PAY,
                'status' => Transaction::STATUS_PROCESSING,
                'amount' => $amount,
                'context' => $context,
            ]);
        }
        if (Uuid::isValid($userId)) {
            $transaction->update(['local_user_id' => $userId]);
            $email = LocalUser::find($userId)->email;
        } else {
            $transaction->update(['cyclos_user_id' => $userId]);
            $email = app(UserService::class)->get($userId)['email'];
        }
        $response = app(LyraClient::class)->post("V4/Charge/CreatePayment", [
            "amount" => $amount,
            "currency" => "EUR",
            "orderId" => $transaction->id,
            "customer" => [
                "email" => $email,
            ],
            'transactionOptions' => [
                'cardOptions' => [
                    'mid' => $this->getTransactionMid($transaction),
                ],
            ],
        ]);

        return $response['answer']['formToken'];
    }

    /**
     *  check kr-answer object signature
     *
     * @param string|null $key
     *
     * @return bool
     * @throws \Lyra\Exceptions\LyraException
     */
    public function checkHash(?string $key = null): bool
    {
        $supportedHashAlgorithm = ['sha256_hmac'];
        // Check if the hash algorithm is supported.
        if (! in_array(request()->get('kr-hash-algorithm'), $supportedHashAlgorithm, true)) {
            throw new LyraException('Hash algorithm not supported:' . request()->get('kr-hash-algorithm')
                . '. please update your SDK.');
        }
        // On some servers, `/` can be escaped.
        $krAnswer = str_replace('\/', '/', request()->get('kr-answer'));
        // If key is not defined, we use kr-hash-key POST parameter to choose it.
        if (is_null($key)) {
            if (request()->get('kr-hash-key') === 'sha256_hmac') {
                $key = (string) config('system-pay.sha_key');
            } elseif (request()->get('kr-hash-key') === 'password') {
                $key = (string) config('system-pay.password');
            } else {
                throw new LyraException('Invalid kr-hash-key POST parameter.');
            }
        }
        $calculatedHash = hash_hmac('sha256', $krAnswer, $key);
        // Return true if calculated hash and sent hash are the same.
        if ($calculatedHash === request()->get('kr-hash')) {
            return true;
        }
        // Some setup are returning escaped chars.
        $calculatedHash = hash_hmac('sha256', stripslashes($krAnswer), $key);

        return ($calculatedHash === request()->get('kr-hash'));
    }

    public function getTransactionMid(Transaction $transaction): string
    {
        if (
            in_array($transaction->context, [
            Transaction::CONTEXT_MEMBERSHIP,
            Transaction::CONTEXT_REGISTRATION,
            ], true)
        ) {
            return config('system-pay.contracts.memberships');
        }

        return config('system-pay.contracts.credit');
    }
}
