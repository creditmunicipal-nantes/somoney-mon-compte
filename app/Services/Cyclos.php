<?php

namespace App\Services;

use App\Exceptions\Cyclos\NotFoundException;
use App\Exceptions\Cyclos\PasswordExpiredException;
use App\Exceptions\Cyclos\ValidationException;
use App\Exceptions\Cyclos\ValidationSentryException;
use App\Exceptions\Cyclos\WebServiceAuthException;
use App\Models\CyclosLog;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\MessageBag;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response;

abstract class Cyclos
{
    public function __construct(protected ?string $sessionToken = null)
    {
    }

    public function createRequestAsAdmin(string $method, ?string $body = null): Request
    {
        return new Request(
            $method,
            'auth/session',
            [
                'Session-Token' => $this->sessionToken,
            ],
            $body
        );
    }

    public function createRequest(string $method, string $uri, array $headers = [], ?string $body = null): Request
    {
        return new Request($method, $uri, $headers, $body);
    }

    public function createMultipartRequest(string $method, string $uri, array $parts = []): Request
    {
        return new Request($method, $uri, [], new MultipartStream($parts));
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function processRequest(
        Request $request,
        GuzzleClient $client = null,
        bool $raiseNotFoundException = false
    ): LengthAwarePaginator|array|null|int {
        $client = $client ?: $this->getClientAsAdmin();
        $log = null;
        $requestParams = [
            'method' => $request->getMethod(),
            'url' => $request->getUri()->getPath(),
            'query_strings' => $request->getUri()->getQuery(),
            'body' => utf8_encode($request->getBody()->getContents()),
        ];
        if (config('app.debug')) {
            $log = CyclosLog::find(data_get($client->getConfig()['headers'], 'cyclos-log', 1));
            $log->update($requestParams);
        }
        try {
            $response = $client->send($request);
            if (config('app.debug') && isset($log)) {
                $log->status = $response->getStatusCode();
                $log->response = $response->getBody()->getContents();
                $log->save();
            }
            if ($response->hasHeader('X-Page-Count') && $response->hasHeader('X-Page-Size')) {
                return new LengthAwarePaginator(
                    json_decode((string) $response->getBody(), true),
                    (int) $response->getHeader('X-Total-Count')[0],
                    max((int) $response->getHeader('X-Page-Size')[0], 1),
                    (int) $response->getHeader('X-Current-Page')[0] + 1,
                );
            }

            return json_decode((string) $response->getBody(), true);
        } catch (RequestException $exception) {
            $this->manageRequestException(
                $exception,
                $raiseNotFoundException,
                $log,
                $this->isAdminClient($client),
                $requestParams
            );
        }

        return null;
    }

    protected function getClientAsAdmin(array $headers = [], bool $isWebService = false): GuzzleClient
    {
        return $this->getClientAsUser([
            'user' => config('services.cyclos.user'),
            'password' => config('services.cyclos.password'),
        ], $headers, $isWebService);
    }

    protected function getClientAsUser(
        string|array $credentials,
        array $headers = [],
        bool $isWebService = false
    ): GuzzleClient {
        if (is_array($credentials)) {
            $authInfo = 'user -> ' . $credentials['user'];
            $headers['Authorization'] = $this->getAuthorizationHeader(
                data_get($credentials, 'user'),
                data_get($credentials, 'password')
            );
        } else {
            $authInfo = 'session -> ' . $credentials;
            $headers['Session-Token'] = $credentials;
        }
        if (config('app.debug')) { // create cyclos log for debug
            $log = CyclosLog::create([
                'auth' => $authInfo,
            ]);
            $headers['cyclos-log'] = $log->id;
        }
        $options = $this->getClientOptions([
            'headers' => $headers,
        ], $isWebService);

        return $this->getClient($options);
    }

    protected function getAuthorizationHeader(string $user, string $password): string
    {
        return 'Basic ' . base64_encode("$user:$password");
    }

    protected function getClientOptions(array $customOptions = [], bool $isWebService = false): array
    {
        return array_replace_recursive([
            'base_uri' => config('services.cyclos.uri') . '/' . config('services.cyclos.money') . '/' .
                ($isWebService ? 'run' : 'api') . '/',
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],
        ], $customOptions);
    }

    protected function getClient(array $options): GuzzleClient
    {
        return new GuzzleClient($options);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     */
    public function manageRequestException(
        RequestException $exception,
        bool $raiseException = false,
        CyclosLog|null $log = null,
        bool $isAdminClient = true,
        array $requestParams = []
    ): void {
        $this->exceptIfCyclosDown($exception);
        $status = $exception->getResponse()->getStatusCode();
        $response = $exception->getResponse()->getBody()->getContents();
        $this->updateLogFromException($log, $status, $response);

        switch ($status) {
            case Response::HTTP_UNPROCESSABLE_ENTITY:
                $this->requestInvalidData($raiseException, $response, $requestParams, $exception);
                break;
            case Response::HTTP_UNAUTHORIZED:
                $this->requestUnauthorized($isAdminClient, $response);
                break;
            case Response::HTTP_NOT_FOUND:
                $this->requestNotFound($raiseException, $response);
                break;
            case Response::HTTP_FORBIDDEN:
                $this->requestForbidden($isAdminClient, $response);
                break;
            case Response::HTTP_INTERNAL_SERVER_ERROR:
                $this->requestError($response, $exception);
                break;
            default:
                throw $exception;
        }
    }

    /**
     * Iterates through all the pages of a GET request
     *
     * @param callable $callable the method to call
     *
     * @return Collection
     */
    protected function getRequestAllPages(callable $callable): Collection
    {
        $page = 1;
        $result = collect();
        $request = $callable($page);
        if ($request) {
            $request->each(function ($item) use ($result) {
                $result->push($item);
            });
            while ($page++ < $request->lastPage()) {
                $request = $callable($page);
                $request->each(function ($item) use ($result) {
                    $result->push($item);
                });
            }
        }

        return $result;
    }

    /**
     * @param \GuzzleHttp\Exception\RequestException $exception
     */
    protected function exceptIfCyclosDown(RequestException $exception): void
    {
        if (is_null($exception->getResponse())) {
            throw new RuntimeException('Cyclos not available');
        }
    }

    protected function isAdminClient(GuzzleClient $client): bool
    {
        return data_get($client->getConfig('headers'), 'Authorization') === data_get($this->getClientAsAdmin()
                ->getConfig('headers'), 'Authorization');
    }

    protected function updateLogFromException(?CyclosLog $log, int $status, string $response): void
    {
        if ($log) {
            $log->status = $status;
            $log->response = $response;
            $log->save();
        }
    }

    /**
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     */
    protected function requestForbidden(bool $isAdminClient, string $response): void
    {
        if ($isAdminClient) {
            $body = json_decode($response, true);
            if (data_get($body, 'passwordStatus') === 'expired') {
                throw new PasswordExpiredException(true);
            }
        }
    }

    /**
     * @throws \App\Exceptions\Cyclos\ValidationException
     */
    protected function requestError(string $response, RequestException $exception): void
    {
        $body = json_decode($response, true);
        if (array_key_exists('code', $body)) {
            $error = [__('validation.cyclos.' . data_get($body, 'code'))];
            throw new ValidationException(new MessageBag($error));
        }
        throw $exception;
    }

    /**
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function requestUnauthorized(bool $isAdminClient, string $response): void
    {
        if ($isAdminClient) {
            throw new WebServiceAuthException($response);
        }
        throw new AuthenticationException();
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     */
    protected function requestNotFound(bool $raiseException, string $response): void
    {
        if ($raiseException) {
            throw new NotFoundException($response);
        }
    }

    /**
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     */
    protected function requestInvalidData(
        bool $raiseException,
        string $response,
        array $requestParams,
        RequestException $exception
    ): void {
        if ($raiseException) {
            throw new ValidationSentryException(
                $response,
                $requestParams,
                in_array('application/json', $exception->getRequest()->getHeader('Content-Type'))
            );
        }
        $body = json_decode($response, true);
        $errors = data_get($body, 'propertyErrors', []);
        $errors = array_merge($errors, data_get($body, 'customFieldErrors', []));
        throw new ValidationException(new MessageBag($errors));
    }
}
