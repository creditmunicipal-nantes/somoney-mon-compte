<?php

namespace App\Services;

use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\Response;

class GeocodingService
{
    public const API_PATH = '/maps/api/geocode/json';

    protected ?GuzzleClient $client = null;

    protected ?string $googleApiKey = null;

    public function __construct()
    {
        $this->client = new GuzzleClient([
            'base_uri' => config('services.google.geocoding.url'),
        ]);
        $this->googleApiKey = config('services.google.geocoding.api_key');
    }

    public function hasApiKey(): bool
    {
        return (bool) $this->googleApiKey;
    }

    /**
     * @param string $address
     *
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \JsonException
     */
    public function geocodingFromAddress(string $address): array
    {
        logger()->debug('[GEOCODING] QUERY ADDRESS => ' . $address);
        $response = $this->client->request(
            'GET',
            self::API_PATH,
            [
                'query' => [
                    'address' => $address,
                    'key' => $this->googleApiKey,
                ],
            ]
        );
        if ($response->getStatusCode() === Response::HTTP_OK) {
            $data = json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
            logger()->debug(json_encode($data, JSON_THROW_ON_ERROR));
            $result = Arr::first($data['results']);
            logger()->debug('[GEOCODING] RESULT => ' . json_encode($result, JSON_THROW_ON_ERROR));

            return data_get($result, 'geometry.location');
        }
        app('sentry')->captureMessage('Geocoding not found for "' . $address . '"');

        return [];
    }
}
