<?php

namespace App\Services;

use App\Models\ValidationUser;
use Illuminate\Support\Carbon;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class MailgunService
{
    protected PendingRequest $httpRequest;

    protected ?string $apiKey;

    protected string $basePath;

    public function __construct()
    {
        $this->apiKey = config('services.mailgun.secret');
        $this->httpRequest = Http::withBasicAuth('api', $this->apiKey)->acceptJson();
        $this->basePath = 'https://' . config('services.mailgun.endpoint') . '/v3/' . config('services.mailgun.domain');
    }

    /**
     * @param \App\Models\ValidationUser $validationUser
     *
     * @throws \Illuminate\Http\Client\RequestException
     * @throws \JsonException
     */
    public function updateUserEmailStatus(ValidationUser $validationUser): void
    {
        $queryString = ['to' => $validationUser->email];
        $lastEvent = $this->getLastEventOfQuery($this->queryEvents($queryString));

        if ($lastEvent) {
            $this->setStatusEmailFromEvent($lastEvent['event'], $validationUser);
            $registrationSubjects = [
                config('somoney.content.email.subject.register'),
                config('somoney.content.email.subject.remind-register'),
                __('mail.subject.email-validation', ['money' => config('somoney.money')]),
            ];
            if (in_array($lastEvent['message']['headers']['subject'], $registrationSubjects, true)) {
                $queryString = [
                    'message-id' => $lastEvent['message']['headers']['message-id'],
                ];
                $lastEvent = $this->getLastEventOfQuery($this->queryEvents($queryString), [
                    route('auth.validate.email', ['emailValidationToken' => '.+']),
                    route('register.individual.validation', ['emailValidationToken' => '.+']),
                    route('register.professional.validation', ['emailValidationToken' => '.+']),
                ]);
                if ($lastEvent) {
                    $this->setStatusEmailFromEvent($lastEvent['event'], $validationUser);
                }
            }
        }
    }

    /**
     * @param array $data
     * @param array $targetLinks
     *
     * @return array|null
     */
    protected function getLastEventOfQuery(array $data, array $targetLinks = []): ?array
    {
        if ($data && array_key_exists('items', $data)) {
            // keep only useful values
            $lastEvent = Arr::first(
                array_reduce(
                    array_reverse($data['items']), // reverse items order to get the last event happened at the end
                    static function (array $messages, array $emailEvent) use ($targetLinks) {
                        // catch only targeted link clicked
                        if ($targetLinks && $emailEvent['event'] === 'clicked') {
                            foreach ($targetLinks as $targetLink) {
                                $regex =
                                    '#^' . Str::replaceLast('\.', '.', str_replace('.', '\.', $targetLink)) . '#';
                                if (preg_match($regex, $emailEvent['url'])) {
                                    $messages[$emailEvent['recipient']] = $emailEvent;
                                    break;
                                }
                            }
                        } else {
                            $messages[$emailEvent['recipient']] = $emailEvent;
                        }

                        return $messages;
                    },
                    []
                ),
                null,
                []
            );

            return array_filter(
                $lastEvent,
                static fn($key) => in_array($key, ['event', 'timestamp', 'message']),
                ARRAY_FILTER_USE_KEY
            );
        }

        return null;
    }

    /**
     * @param array $queryString
     *
     * @return array
     * @throws \Illuminate\Http\Client\RequestException
     * @throws \JsonException
     */
    protected function queryEvents(array $queryString): array
    {
        $response = $this->httpRequest->get($this->basePath . '/events?' . http_custom_build_query($queryString));
        $response->throw();
        $data = $response->json();
        if (config('app.debug')) {
            Log::debug('[MAILGUN] GET - /events ' . json_encode($queryString, JSON_THROW_ON_ERROR)
                . ' => ' . json_encode($data, JSON_THROW_ON_ERROR));
        }

        return $data;
    }

    protected function isLastEventNewer(?array $lastEvent, ?array $lastEventProposal): bool
    {
        return $lastEventProposal
            && array_key_exists('timestamp', $lastEventProposal)
            && $lastEvent
            && array_key_exists('timestamp', $lastEvent)
            && Carbon::createFromTimestamp($lastEvent['timestamp'])
                ->lt(Carbon::createFromTimestamp($lastEventProposal['timestamp']));
    }

    /**
     * @param string $event
     * @param \App\Models\ValidationUser $validationUser
     *
     * @return void
     */
    protected function setStatusEmailFromEvent(string $event, ValidationUser $validationUser): void
    {
        $validationUser->email_status = match ($event) {
            'accepted', 'delivered' => ValidationUser::NOT_OPENED,
            'opened' => ValidationUser::OPENED,
            'clicked' => ValidationUser::CLICKED,
            'complained' => ValidationUser::SPAM,
            default => ValidationUser::NO_EMAIL,
        };

        $validationUser->save();
    }
}
