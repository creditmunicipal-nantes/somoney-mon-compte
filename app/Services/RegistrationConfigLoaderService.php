<?php

namespace App\Services;

use Illuminate\Support\Arr;

class RegistrationConfigLoaderService
{
    public function load(): void
    {
        $hasAddressStep = ! config('registration.available.professional.siren');
        $hasJustificationDocumentsStep = config('registration.available.professional.justification_documents');
        $this->makeProfessionalSteps([
            [
                'id' => 1,
                'slug' => 'account',
                'step' => 1,
                'route' => 'register.professional.informations.account',
            ],
            [
                'id' => 2,
                'slug' => 'society',
                'step' => 2,
                'route' => 'register.professional.informations.society',
            ],
            [
                'id' => 5,
                'slug' => 'cgu',
                'step' => $this->subStepIndex([$hasAddressStep, $hasJustificationDocumentsStep], 5),
                'route' => 'register.professional.cgu',
            ],
            [
                'id' => 6,
                'slug' => 'success',
                'step' => $this->subStepIndex([$hasAddressStep, $hasJustificationDocumentsStep], 6),
                'route' => 'register.professional.success',
            ],
            [
                'id' => 7,
                'slug' => 'validation',
            ],
            [
                'id' => 8,
                'slug' => 'on-cyclos',
            ],
        ]);
        $hasIdDocument = config('registration.available.individual.id_document');
        $this->makeIndividualSteps([
            [
                'id' => 1,
                'slug' => 'account',
                'step' => 1,
                'route' => 'register.individual.informations.account',
            ],
            [
                'id' => 2,
                'slug' => 'personal',
                'step' => 2,
                'route' => 'register.individual.informations.personal',
            ],
            [
                'id' => 3,
                'slug' => 'address',
                'step' => 3,
                'route' => 'register.individual.informations.address',
            ],
            [
                'id' => 5,
                'slug' => 'cgu',
                'step' => $this->subStepIndex([$hasIdDocument], 5),
                'route' => 'register.individual.cgu',
            ],
            [
                'id' => 6,
                'slug' => 'success',
                'step' => $this->subStepIndex([$hasIdDocument], 6),
                'route' => 'register.individual.success',
            ],
            [
                'id' => 7,
                'slug' => 'validation',
            ],
            [
                'id' => 8,
                'slug' => 'on-cyclos',
            ],
        ]);
    }

    protected function makeProfessionalSteps(array $steps): void
    {
        if (! config('registration.available.professional.siren')) {
            $steps[] = [
                'id' => 3,
                'slug' => 'address',
                'step' => 3,
                'route' => 'register.professional.informations.address',
            ];
        }
        if (config('registration.available.professional.justification_documents')) {
            $steps[] =
                [
                    'id' => 4,
                    'slug' => 'files',
                    'step' => $this->subStepIndex([! config('registration.available.professional.siren')], 4),
                    'route' => 'register.professional.files',
                ];
        }
        if (config('somoney.available.services.membership')) {
            $steps[] = [
                'id' => 9,
                'slug' => 'membership',
                'route' => 'register.professional.credit.membership',
            ];
        }
        if (config('registration.available.professional.donate')) {
            $steps[] = [
                'id' => 11,
                'slug' => 'donate',
                'route' => 'register.professional.credit.donate',
            ];
        }
        $this->setStepsArray('professional', $steps);
    }

    protected function subStepIndex(array $alterStepsBoolean, int $originalStepIndex): int
    {
        $stepIndex = $originalStepIndex;
        foreach ($alterStepsBoolean as $boolean) {
            $stepIndex -= $boolean ? 0 : 1;
        }

        return $stepIndex;
    }

    protected function setStepsArray(string $type, array $steps): void
    {
        $slugToId = [];
        $idToSlug = [];
        $byId = [];
        $stepToRoute = [];
        $byStep = [];
        $bySlug = [];
        foreach ($steps as $step) {
            $slugToId[$step['slug']] = $step['id'];
            $idToSlug[$step['id']] = $step['slug'];
            if (Arr::has($step, 'step')) {
                $stepToRoute[$step['step']] = $step['route'];
                $byStep[$step['step']] = $step;
            }
            $byId[$step['id']] = $step;
            $bySlug[$step['slug']] = $step;
        }
        ksort($stepToRoute);
        ksort($byStep);
        $nav = $stepToRoute;
        config()->set('registration.steps.' . $type, [
            'slug_id' => $slugToId,
            'id_slug' => $idToSlug,
            'by_id' => $byId,
            'by_step' => $byStep,
            'by_slug' => $bySlug,
            'step_route' => $stepToRoute,
            'nav' => $nav,
        ]);
    }

    protected function makeIndividualSteps(array $steps): void
    {
        if (config('registration.available.individual.id_document')) {
            $steps[] = [
                'id' => 4,
                'slug' => 'files',
                'step' => 4,
                'route' => 'register.individual.files',
            ];
        }
        if (config('somoney.available.services.membership')) {
            $steps[] = [
                'id' => 9,
                'slug' => 'membership',
                'route' => 'register.individual.credit.membership',
            ];
        }
        if (config('registration.available.individual.donate')) {
            $steps[] = [
                'id' => 11,
                'slug' => 'donate',
                'route' => 'register.individual.credit.donate',
            ];
        }
        $this->setStepsArray('individual', $steps);
    }
}
