<?php

namespace App\Services;

use App\Exceptions\Cyclos\ValidationSentryException;
use App\Exceptions\FailCreateUserException;
use App\Jobs\LocalUsers\SendEmailConfirmRegistered;
use App\Jobs\SendEmailFailCreateUser;
use App\Models\LocalUser;
use App\Models\Transaction;
use App\Models\UserToken;
use App\Models\Wallet;
use App\Services\Cyclos\User\PhoneService;
use App\Services\Cyclos\UserService;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Carbon;
use ErrorException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class LocalUserService
{
    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function makeNewUser(string $userType): LocalUser
    {
        $data = data_get($this->getDataForNew($userType), 'user');
        $data['group'] = $userType;

        return new LocalUser($data);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getDataForNew(string $userType): LengthAwarePaginator|array|null|int
    {
        return app(UserService::class)->getDataForNew($userType);
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function getDataCustomField(string $userType, string $internalName)
    {
        return data_get(Arr::first(
            $this->getDataForNew($userType)['customFields'],
            fn($value) => $value['internalName'] === $internalName
        ), 'possibleValues');
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \App\Exceptions\FailCreateUserException
     * @throws \ErrorException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \JsonException
     */
    public function createCyclosUser(LocalUser $localUser): LocalUser
    {
        try {
            $cyclosUser = app(UserService::class)->createUserFromLocal($localUser->id)['user'];
            if (! $cyclosUser) {
                throw new ErrorException('Fail to create user on Cyclos');
            }
            $localUser->cyclos_id = $cyclosUser['id'];
            $localUser->step = config("registration.steps.{$localUser->group}.slug_id.on-cyclos");
            $localUser->save();
            $dataForEdit = app(UserService::class)->getDataForEdit($localUser->cyclos_id)['user'];
            $localUserData = array_merge($dataForEdit, $localUser->toCyclosArray());
            $localUserData['customValues'] = collect($localUserData['customValues'])->keys()
                ->reduce(function ($customValues, $customValueKey) use ($localUserData) {
                    $customValue = $localUserData['customValues'][$customValueKey];
                    if ($customValue !== null) {
                        $customValues[$customValueKey] = is_bool($customValue)
                            ? json_encode($customValue)
                            : $customValue; // stringify bool
                    }

                    return $customValues;
                }, []);
            app(PhoneService::class)->setUserPhone($localUser->cyclos_id, $localUser->phone, true);
            app(UserService::class)->update($localUser->cyclos_id, $localUserData);
            if (
                config('somoney.payment-gateway') === Transaction::TYPE_LEMONWAY
                && ! config('somoney.available.services.lemonway-v2')
            ) {
                $this->updateWalletUserId($localUser);
            }
            // Remove validation user of local user.
            $localUser->validation_user->delete();
            // Remove user token linked to registration.
            UserToken::where('user_id', $localUser->id)
                ->whereIn('type', [
                    UserToken::TYPE_EMAIL_VALIDATION,
                    UserToken::TYPE_RESUME_REGISTRATION,
                    UserToken::TYPE_REGISTRATION_NEED_MEMBERSHIP,
                ])
                ->delete();
            if (config('somoney.available.services.membership')) {
                Bus::dispatch(new SendEmailConfirmRegistered($localUser));
            }
        } catch (ValidationSentryException $exception) {
            $body = json_decode($exception->getMessage(), true, 512, JSON_THROW_ON_ERROR);
            if (data_get($body, 'code') === 'validation') {
                Bus::dispatch(new SendEmailFailCreateUser($localUser->id, $body));
                throw new FailCreateUserException(
                    $localUser->group,
                    json_encode($localUser->toCyclosArray(), JSON_THROW_ON_ERROR)
                );
            }

            throw $exception;
        }

        return $localUser;
    }

    protected function updateWalletUserId(LocalUser $localUser): void
    {
        $wallet = Wallet::where('cyclos_user_id', $localUser->id)->first();
        $wallet->cyclos_user_id = $localUser->cyclos_id;
        $wallet->save();
    }

    public function getOldCreated(Carbon $beforeDate): Collection
    {
        return LocalUser::whereNotNull('cyclos_id')->where('updated_at', '<=', $beforeDate)->get();
    }

    /**
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function createUniqueApiKey(): string
    {
        $apiKey = Str::random(64);
        $apiKeysExists = array_filter(
            LocalUser::where('group', 'professional')->get(['customValues'])->reduce(
                fn(array $listApiKeys, LocalUser $localUser) => array_merge(
                    $listApiKeys,
                    [data_get($localUser->customValues, 'apikey')]
                ),
                []
            )
        );
        while (
            app(UserService::class)->exists([
                'profileFields' => ['apikey' => $apiKey],
                'groups' => config('cyclos.internal-names.groups.professional'),
            ])
            || in_array($apiKey, $apiKeysExists, true)
        ) {
            $apiKey = Str::random(64);
        }

        return $apiKey;
    }
}
