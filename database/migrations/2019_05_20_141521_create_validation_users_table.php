<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateValidationUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('validation_users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('cyclos_id')->unique();
            $table->string("group");
            $table->string("email");
            $table->string("email_status");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('validation_users');
    }
}
