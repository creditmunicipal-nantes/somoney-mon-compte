<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFidelityTable extends Migration
{
    public function up(): void
    {
        Schema::table('fidelity', function (Blueprint $table) {
            $table->string('groups_type')->default('all')->before('active');
            $table->renameColumn('condition', 'counter');
        });
        Schema::table('fidelity', function (Blueprint $table) {
            $table->renameColumn('condition_is_price', 'counter_as_price');
        });
        Schema::table('fidelity', function (Blueprint $table) {
            $table->renameColumn('value', 'discount');
        });
        Schema::table('fidelity', function (Blueprint $table) {
            $table->renameColumn('value_is_percent', 'discount_as_percent');
        });
        Schema::rename('fidelity', 'fidelities');
    }

    public function down(): void
    {
        Schema::table('fidelities', function (Blueprint $table) {
            $table->dropColumn('groups_type');
        });
        Schema::table('fidelities', function (Blueprint $table) {
            $table->renameColumn('counter', 'condition');
        });
        Schema::table('fidelities', function (Blueprint $table) {
            $table->renameColumn('counter_as_price', 'condition_is_price');
        });
        Schema::table('fidelities', function (Blueprint $table) {
            $table->renameColumn('discount', 'value');
        });
        Schema::table('fidelities', function (Blueprint $table) {
            $table->renameColumn('discount_as_percent', 'value_is_percent');
        });
        Schema::rename('fidelities', 'fidelity');
    }
}
