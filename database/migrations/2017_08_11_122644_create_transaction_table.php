<?php

use App\Models\Transaction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('amount');
            $table->uuid('source_wallet_id')->nullable();
            $table->uuid('target_wallet_id')->nullable();
            $table->uuid('depends_on_id')->nullable();
            $table->string('lw_id')->nullable();
            $table->string('lw_token')->nullable();
            $table->string('wk_token')->nullable();
            $table->datetime('lw_executed_at')->nullable();
            $table->string('status')->default(Transaction::STATUS_PENDING);
            $table->text('failure_reason')->nullable();

            $table->foreign('source_wallet_id')
                ->references('id')
                ->on('wallet')
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');
            $table->foreign('target_wallet_id')
                ->references('id')
                ->on('wallet')
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');
        });

        Schema::table('transaction', function (Blueprint $table) {
            $table->foreign('depends_on_id')
                ->references('id')
                ->on('transaction')
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction');
    }
}
