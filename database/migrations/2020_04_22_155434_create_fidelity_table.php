<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFidelityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fidelity', function (Blueprint $table) {
            $table->id();
            $table->string('cyclos_user_id');
            $table->double('condition');
            $table->boolean('condition_is_price');
            $table->double('value');
            $table->boolean('value_is_percent');
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fidelity');
    }
}
