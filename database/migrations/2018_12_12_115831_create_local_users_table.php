<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocalUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local_users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('cyclos_id')->nullable();
            $table->smallInteger('step')->default(1);
            $table->string("name")->nullable();
            $table->string("group");
            $table->string("email")->unique();
            $table->text("addresses")->default('[]');
            $table->text("mobilePhones")->default('[]');
            $table->text("landLinePhones")->default('[]');
            $table->text("contactInfos")->default('[]');
            $table->text("passwords")->default('[]');
            $table->text("images")->default('[]');
            $table->boolean("asMember")->default(false);
            $table->boolean("acceptAgreement")->default(false);
            $table->json("customValues");

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('local_users');
    }
}
