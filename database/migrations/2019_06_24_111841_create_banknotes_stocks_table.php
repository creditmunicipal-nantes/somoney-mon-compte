<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBanknotesStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banknotes_stocks', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->double('stock');
            $table->uuid('counter_id');
            $table->foreign('counter_id')
                ->references('id')
                ->on('counters')
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banknotes_stocks');
    }
}
