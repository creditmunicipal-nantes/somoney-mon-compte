<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLastExecutedAtToSepasTable extends Migration
{
    public function up(): void
    {
        Schema::table('sepas', function (Blueprint $table) {
            $table->dateTime('last_executed_at')->nullable()->after('duration_month');
        });
    }

    public function down(): void
    {
        Schema::table('sepas', function (Blueprint $table) {
            $table->dropColumn('last_executed_at');
        });
    }
}
