<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCyclosLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cyclos_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->text('auth');
            $table->string('method')->nullable();
            $table->string('url')->nullable();
            $table->longText('query_strings')->nullable();
            $table->longText('body')->nullable();
            $table->smallInteger('status')->nullable();
            $table->longText('response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cyclos_logs');
    }
}
