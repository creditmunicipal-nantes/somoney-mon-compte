<?php

use App\Models\LocalUser;
use Illuminate\Support\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRemindedToLocalUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('local_users', function (Blueprint $table) {
            $table->dateTime('reminded')->nullable()->after('customValues');
        });

        $localUsers = app(LocalUser::class)
            ->where('updated_at', '<', today()->startOfDay()->subDays(7))
            ->whereNull('reminded')
            ->get();

        $localUsers->each(function (LocalUser $localUser) {
            $localUser->reminded = now();
            $localUser->save();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('local_users', function (Blueprint $table) {
            $table->dropColumn('reminded');
        });
    }
}
