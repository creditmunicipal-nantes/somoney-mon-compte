<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSepasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sepas', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('user_id');
            $table->float('amount');
            $table->char('gender', 1);
            $table->string('last_name');
            $table->string('first_name');
            $table->string('email');
            $table->string('company')->nullable();
            $table->string('iban');
            $table->boolean('done')->default(false);
            $table->boolean('canceled')->default(false);
            $table->boolean('stopped')->default(false);
            $table->boolean('confirmed')->default(false);
            $table->integer('duration_month')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sepas');
    }
}
