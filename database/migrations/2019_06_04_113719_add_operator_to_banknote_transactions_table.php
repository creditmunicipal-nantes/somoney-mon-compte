<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddOperatorToBanknoteTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banknotes_transactions', function (Blueprint $table) {
            $table->string('operator_cyclos_id')->nullable()->before('user_cyclos_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banknotes_transactions', function (Blueprint $table) {
            $table->dropColumn('operator_cyclos_id');
        });
    }
}
