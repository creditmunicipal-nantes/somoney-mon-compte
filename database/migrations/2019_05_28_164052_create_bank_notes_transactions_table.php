<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBanknotesTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banknotes_transactions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('user_cyclos_id');
            $table->longText('details')->default('[]');
            $table->string('type', 3);
            $table->uuid('counter_id');
            $table->foreign('counter_id')
                ->references('id')
                ->on('counters')
                ->onUpdate('CASCADE')
                ->onDelete('RESTRICT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banknotes_transactions');
    }
}
