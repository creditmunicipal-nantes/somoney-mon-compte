<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddLocalUserIdToValidationUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('validation_users', function (Blueprint $table) {
            $table->string('cyclos_id')->nullable()->change();
            $table->uuid('local_user_id')->nullable()->unique()->after('cyclos_id');
            $table->foreign('local_user_id')
                ->references('id')
                ->on('local_users')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('validation_users', function (Blueprint $table) {
            $table->dropColumn('local_user_id');
        });
    }
}
