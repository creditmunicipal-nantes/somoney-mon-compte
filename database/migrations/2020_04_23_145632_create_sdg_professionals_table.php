<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSdgProfessionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdg_professionals', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('sdg_id');
            $table->string('cyclos_user_id');
            $table->dateTime('ended_at')->nullable();
            $table->timestamps();

            $table->foreign('sdg_id')->references('id')->on('sdgs')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdg_professionals');
    }
}
