<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSystemPayLogsTable extends Migration
{
    public function up(): void
    {
        Schema::create('system_pay_logs', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('transaction_id');
            $table->string('status');
            $table->longText('response');
            $table->dateTime('created_at');
            $table->foreign('transaction_id')->references('id')->on('transaction');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('system_pay_logs');
    }
}
