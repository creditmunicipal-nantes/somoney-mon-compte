<?php

namespace Database\Seeders;

use App\Models\UsersMatching;
use Illuminate\Database\Seeder;

class CEVTestUsersMatchingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->getData() as $value) {
            UsersMatching::create([
                'user_id' => $value[0],
                'cev_id' => $value[1],
                'cev_account_id' => $value[2],
            ]);
        }
    }

    /**
     * @return array
     */
    protected function getData()
    {
        return [
            [
                '1111111111111111111',
                '111111',
                '111111',
            ],
            [
                '2222222222222222222',
                '222222',
                '222222',
            ],
            [
                '3333333333333333333',
                '333333',
                '333333',
            ],
            [
                '4444444444444444444',
                '444444',
                '444444',
            ],
            [
                '5555555555555555555',
                '555555',
                '555555',
            ],
            [
                '666666666666666666',
                '666666',
                '666666',
            ],
            [
                '7777777777777777777',
                '777777',
                '777777',
            ],
        ];
    }
}
