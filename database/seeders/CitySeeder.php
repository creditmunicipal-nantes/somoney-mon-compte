<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class CitySeeder extends Seeder
{
    public function run()
    {
        $citiesFile = app()->environment() === 'local'
            ? '/data/cities_28032017-light.csv'
            : '/data/cities_28032017.csv';
        $handle = fopen(__DIR__ . $citiesFile, 'r');
        // Skip first line
        fgetcsv($handle, null, ";");
        while (($data = fgetcsv($handle, null, ";")) !== false) {
            $val = [
                'insee_code' => $data[0],
                'zipcode' => $data[2],
            ];
            if ($data[4] !== '') {
                $val = Arr::add($val, 'name', $data[1] . ' (' . $data[4] . ')');
            } else {
                $val = Arr::add($val, 'name', $data[1]);
            }
            $latLon = explode(', ', $data[5]);
            if (count($latLon) > 1) {
                $val = Arr::add($val, 'lat', $latLon[0]);
                $val = Arr::add($val, 'lon', $latLon[1]);
            }
            City::firstOrCreate($val);
        }
        fclose($handle);
    }
}
