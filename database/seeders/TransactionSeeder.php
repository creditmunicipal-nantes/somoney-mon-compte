<?php

namespace Database\Seeders;

use App\Models\Transaction;
use Illuminate\Database\Seeder;

class TransactionSeeder extends Seeder
{
    public function run(): void
    {
        Transaction::factory(10)->create([
            'status' => Transaction::STATUS_SUCCESS,
            'context' => Transaction::CONTEXT_MEMBERSHIP
        ]);
        Transaction::factory(5)->create([
            'transaction_type' => Transaction::REGISTER_TYPE_DONATE,
            'status' => Transaction::STATUS_SUCCESS,
            'context' => Transaction::CONTEXT_MEMBERSHIP
        ]);
    }
}
