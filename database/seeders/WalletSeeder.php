<?php

namespace Database\Seeders;

use App\Models\Wallet;
use Illuminate\Database\Seeder;

class WalletSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Wallet::create([
            'lw_id' => 'SC',
            'cyclos_user_id' => 'SC',
        ]);

        if (config('somoney.available.services.lemonway-v2')) {
            Wallet::create([
                'lw_id' => config('lemonway.vad_account'),
                'cyclos_user_id' => 'somoney',
            ]);
        }
    }
}
