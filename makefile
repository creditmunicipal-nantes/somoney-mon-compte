.SILENT:

set-as-brest:
	rm -f .env
	cp .env.brest .env
	echo "Replaced ./.env file by ./.env.brest ✔️"
	rm -rf custom-data/current
	cp -r custom-data/brest custom-data/current
	echo "Replaced ./custom-data/current file by ./custom-data/brest ✔️"

set-as-nantes:
	rm -f .env
	cp .env.nantes .env
	echo "Replaced ./.env file by ./.env.nantes ✔️"
	rm -rf custom-data/current
	cp -r custom-data/nantes custom-data/current
	echo "Replaced ./custom-data/current file by ./custom-data/nantes ✔️"

mock-servers:
	node tests/cyclos-mock-server.js &
	node tests/geocoding-mock-server.js &
	node tests/insee-mock-server.js &
	node tests/lemonway-mock-server.js &
