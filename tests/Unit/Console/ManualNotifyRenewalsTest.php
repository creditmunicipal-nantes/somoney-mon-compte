<?php

namespace Tests\Unit\Console;

use App\Console\Commands\Memberships\ManualNotifyRenewals;
use App\Jobs\Memberships\SendEmailManualRenewCreated;
use App\Models\Membership;
use Illuminate\Support\Carbon;
use Faker\Provider\Uuid;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;

class ManualNotifyRenewalsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Bus::fake();
    }

    /** @test */
    public function it_can_send_a_manual_renewal_email_with_invalid_membership_created_in_accepted_range(): void
    {
        // Invalid membership created at accepted range start.
        Carbon::setTestNow(Carbon::parse('2021-01-15 09:00:00'));
        $membership1 = Membership::create(['cyclos_user_id' => Uuid::uuid()]);
        // Invalid membership created at accepted range end.
        Carbon::setTestNow(Carbon::parse('2021-05-03 23:59:59'));
        $membership2 = Membership::create(['cyclos_user_id' => Uuid::uuid()]);
        // Command triggered in accepted range.
        $this->artisan(ManualNotifyRenewals::class);
        // Mails are sent.
        Bus::assertDispatched(fn(SendEmailManualRenewCreated $job) => $job->cyclosUserId
            === $membership1->cyclos_user_id);
        Bus::assertDispatched(fn(SendEmailManualRenewCreated $job) => $job->cyclosUserId
            === $membership2->cyclos_user_id);
    }

    /** @test */
    public function it_can_execute_command_in_dry_run(): void
    {
        // Invalid membership created at accepted range start.
        Carbon::setTestNow(Carbon::parse('2021-01-15 09:00:00'));
        Membership::create(['cyclos_user_id' => Uuid::uuid()]);
        // Invalid membership created at accepted range end.
        Carbon::setTestNow(Carbon::parse('2021-05-03 23:59:59'));
        Membership::create(['cyclos_user_id' => Uuid::uuid()]);
        // Command triggered in accepted range.
        $this->artisan(ManualNotifyRenewals::class, ['--dry' => true]);
        // Mails are not sent even if they would have been in normal mode.
        Bus::assertNotDispatched(SendEmailManualRenewCreated::class);
    }

    /** @test */
    public function it_cant_send_a_manual_renewal_email_with_valid_membership_created_in_accepted_range(): void
    {
        // Valid membership created at accepted range start.
        Carbon::setTestNow(Carbon::parse('2021-01-15 09:00:00'));
        Membership::create(['cyclos_user_id' => Uuid::uuid(), 'succeed_at' => now()]);
        // Valid membership created at accepted range end.
        Carbon::setTestNow(Carbon::parse('2021-05-03 23:59:59'));
        Membership::create(['cyclos_user_id' => Uuid::uuid(), 'succeed_at' => now()]);
        // Command triggered in accepted range.
        $this->artisan(ManualNotifyRenewals::class);
        // No mail is sent.
        Bus::assertNotDispatched(SendEmailManualRenewCreated::class);
    }

    /** @test */
    public function it_cant_send_a_manual_renewal_email_with_invalid_membership_created_out_of_accepted_range(): void
    {
        // Invalid membership created before accepted range start.
        Carbon::setTestNow(Carbon::parse('2021-01-15 08:59:59'));
        Membership::create(['cyclos_user_id' => Uuid::uuid()]);
        // Invalid membership created after accepted range end.
        Carbon::setTestNow(Carbon::parse('2021-05-04 00:00:00'));
        Membership::create(['cyclos_user_id' => Uuid::uuid()]);
        // Command triggered in accepted range.
        Carbon::setTestNow(Carbon::parse('2021-01-15 09:00:00'));
        $this->artisan(ManualNotifyRenewals::class);
        // No mail is sent.
        Bus::assertNotDispatched(SendEmailManualRenewCreated::class);
    }

    /** @test */
    public function it_cant_send_a_manual_renewal_email_with_valid_membership_created_out_of_accepted_range(): void
    {
        // Invalid membership created before accepted range start.
        Carbon::setTestNow(Carbon::parse('2021-01-15 08:59:59'));
        Membership::create(['cyclos_user_id' => Uuid::uuid(), 'succeed_at' => now()]);
        // Invalid membership created after accepted range end.
        Carbon::setTestNow(Carbon::parse('2021-05-04 00:00:00'));
        Membership::create(['cyclos_user_id' => Uuid::uuid(), 'succeed_at' => now()]);
        // Command triggered in accepted range.
        Carbon::setTestNow(Carbon::parse('2021-01-15 09:00:00'));
        $this->artisan(ManualNotifyRenewals::class);
        // No mail is sent.
        Bus::assertNotDispatched(SendEmailManualRenewCreated::class);
    }
}
