<?php

namespace Tests\Unit\Console;

use App\Console\Commands\Registration\EmailValidationReminder;
use App\Jobs\SendEmailValidationCreation;
use App\Models\LocalUser;
use Illuminate\Support\Carbon;
use Faker\Provider\Uuid;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class RegistrationEmailValidationReminderTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Bus::fake();
    }

    /** @test */
    public function it_can_send_validation_reminder_email_when_email_not_validated_after_a_week_and_is_professional(): void
    {
        $localUserId = Uuid::uuid();
        DB::table(app(LocalUser::class)->getTable())->insert([
            'id' => $localUserId,
            'name' => 'Pro',
            'email' => 'test@fake.test',
            'step' => config('registration.steps.professional.by_slug.success')['id'],
            'group' => config('cyclos.internal-names.groups.professional'),
            'updated_at' => Carbon::parse('2021-01-07 23:59:59'),
            'customValues' => '{}',
        ]);
        Carbon::setTestNow(Carbon::parse('2021-01-15'));
        $this->artisan(EmailValidationReminder::class);
        Bus::assertDispatched(fn(SendEmailValidationCreation $job) => $job->localUser->id === $localUserId);
    }

    /** @test */
    public function it_can_send_email_reminder_when_email_not_validated_after_a_week_and_is_individual(): void
    {
        $localUserId = Uuid::uuid();
        DB::table(app(LocalUser::class)->getTable())->insert([
            'id' => $localUserId,
            'name' => 'Part',
            'email' => 'test@fake.test',
            'step' => config('registration.steps.individual.by_slug.success')['id'],
            'group' => config('cyclos.internal-names.groups.individual'),
            'updated_at' => Carbon::parse('2021-01-07 23:59:59'),
            'customValues' => '{}',
        ]);
        Carbon::setTestNow(Carbon::parse('2021-01-15'));
        $this->artisan(EmailValidationReminder::class);
        Bus::assertDispatched(fn(SendEmailValidationCreation $job) => $job->localUser->id === $localUserId);
    }

    /** @test */
    public function it_cant_send_email_reminder_when_email_not_validated_less_than_a_week(): void
    {
        $localUserId = Uuid::uuid();
        DB::table(app(LocalUser::class)->getTable())->insert([
            'id' => $localUserId,
            'name' => 'Pro',
            'email' => 'test@fake.test',
            'step' => config('registration.steps.professional.by_slug.success')['id'],
            'group' => config('cyclos.internal-names.groups.professional'),
            'updated_at' => Carbon::parse('2021-01-08 00:00:00'),
            'customValues' => '{}',
        ]);
        Carbon::setTestNow(Carbon::parse('2021-01-15'));
        $this->artisan(EmailValidationReminder::class);
        Bus::assertNotDispatched(SendEmailValidationCreation::class);
    }

    /** @test */
    public function it_cant_send_email_reminder_when_not_at_validation_step(): void
    {
        $localUserId = Uuid::uuid();
        DB::table(app(LocalUser::class)->getTable())->insert([
            'id' => $localUserId,
            'name' => 'Pro',
            'email' => 'test@fake.test',
            'step' => config('registration.steps.professional.by_slug.account')['id'],
            'group' => config('cyclos.internal-names.groups.professional'),
            'updated_at' => Carbon::parse('2021-01-07 23:59:59'),
            'customValues' => '{}',
        ]);
        Carbon::setTestNow(Carbon::parse('2021-01-15'));
        $this->artisan(EmailValidationReminder::class);
        Bus::assertNotDispatched(SendEmailValidationCreation::class);
    }

    /** @test */
    public function it_cant_send_email_reminder_when_already_reminded(): void
    {
        $localUserId = Uuid::uuid();
        DB::table(app(LocalUser::class)->getTable())->insert([
            'id' => $localUserId,
            'name' => 'Pro',
            'email' => 'test@fake.test',
            'step' => config('registration.steps.professional.by_slug.success')['id'],
            'group' => config('cyclos.internal-names.groups.professional'),
            'updated_at' => Carbon::parse('2021-01-07 23:59:59'),
            'customValues' => '{}',
            'reminded' => Carbon::parse('2021-01-14'),
        ]);
        Carbon::setTestNow(Carbon::parse('2021-01-15'));
        $this->artisan(EmailValidationReminder::class);
        Bus::assertNotDispatched(SendEmailValidationCreation::class);
    }
}
