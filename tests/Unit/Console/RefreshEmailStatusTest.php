<?php

namespace Tests\Unit\Console;

use App\Console\Commands\RefreshEmailStatus;
use App\Jobs\LoadEmailStatus;
use App\Models\LocalUser;
use App\Services\Cyclos\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;

class RefreshEmailStatusTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Bus::fake();
    }

    /** @test */
    public function it_can_execute_refresh_user_email_status_with_local_user(): void
    {
        $stubLocalUser = LocalUser::create([
            'name' => 'Individual User',
            'group' => config('cyclos.internal-names.groups.individual'),
            'email' => 'part@fake.test',
            'customValues' => [],
        ]);
        $this->mock(UserService::class)->shouldReceive('searchAll')
            ->once()
            ->with(['groups' => implode(',', array_values(config('cyclos.internal-names.groups')))])
            ->andReturn(collect([]));
        $this->artisan(RefreshEmailStatus::class);
        Bus::assertDispatched(fn(LoadEmailStatus $job) => $job->userCyclosIds->isEmpty()
            && $job->localUsers->first()->id === $stubLocalUser->id);
    }

    /** @test */
    public function it_can_execute_refresh_user_email_status_with_cyclos_user(): void
    {
        $stubCyclosUserData = [
            "id" => "4036691750709796712",
        ];
        $this->mock(UserService::class)->shouldReceive('searchAll')
            ->once()
            ->with(['groups' => implode(',', array_values(config('cyclos.internal-names.groups')))])
            ->andReturn(collect([
                $stubCyclosUserData,
            ]));
        $this->artisan(RefreshEmailStatus::class);
        Bus::assertDispatched(fn(LoadEmailStatus $job) => $job->userCyclosIds->first() === $stubCyclosUserData['id']
            && $job->localUsers->isEmpty());
    }

    /** @test */
    public function it_cant_execute_refresh_user_email_status_when_no_mailgun_secret_key(): void
    {
        config()->set('services.mailgun.secret', null);
        $this->artisan(RefreshEmailStatus::class)
            ->assertExitCode(1)
            ->expectsOutput('You cannot execute this command if the env var "MAILGUN_SECRET" is not defined');
    }
}
