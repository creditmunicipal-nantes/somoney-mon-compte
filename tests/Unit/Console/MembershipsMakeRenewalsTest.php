<?php

namespace Tests\Unit\Console;

use App\Console\Commands\Memberships\MakeRenewals as MembershipsMakeRenewals;
use App\Jobs\Memberships\SendEmailRenewCreated;
use App\Models\Membership;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;

class MembershipsMakeRenewalsTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Bus::fake();
    }

    /** @test */
    public function it_cant_renew_membership_when_it_is_still_valid(): void
    {
        config()->set('somoney.available.services.membership', true);
        $cyclosUserId = '4036691750709796712';
        // We are on 15/01/2021
        Carbon::setTestNow(Carbon::parse('2021-01-15'));
        // Membership has been paid on 01/10/2020 at 08:59:59.
        Membership::create([
            'cyclos_user_id' => $cyclosUserId,
            'succeed_at' => Carbon::parse('2020-10-01 00:00:00'),
        ]);
        $this->mock(UserService::class)->shouldReceive('searchAll')
            ->once()
            ->with([
                'groups' => implode(',', array_values(config('cyclos.internal-names.groups'))),
                'statuses' => ['active'],
            ])
            ->andReturn(collect([]));
        $this->artisan(MembershipsMakeRenewals::class);
        // Membership is not renewed (no new line in database).
        $this->assertDatabaseCount(app(Membership::class)->getTable(), 1);
        Bus::assertNotDispatched(SendEmailRenewCreated::class);
    }

    /** @test */
    public function it_can_renew_membership_when_it_is_not_valid_anymore(): void
    {
        config()->set('somoney.available.services.membership', true);
        $cyclosUserId = '4036691750709796712';
        // We are on 15/01/2021.
        Carbon::setTestNow(Carbon::parse('2021-01-15'));
        // Membership has been paid on 31/09/2020 at 23:59:59.
        Membership::create([
            'cyclos_user_id' => $cyclosUserId,
            'succeed_at' => Carbon::parse('2020-09-30 23:59:59'),
        ]);
        $this->mock(UserService::class)->shouldReceive('searchAll')
            ->once()
            ->with([
                'groups' => implode(',', array_values(config('cyclos.internal-names.groups'))),
                'statuses' => ['active'],
            ])
            ->andReturn(collect([]));
        $this->artisan(MembershipsMakeRenewals::class);
        // Membership is renewed (1 new line in database)
        $this->assertDatabaseCount(app(Membership::class)->getTable(), 2);
        Bus::assertDispatched(fn(SendEmailRenewCreated $job) => $job->cyclosUserId === $cyclosUserId);
    }

    /** @test */
    public function it_cant_renew_membership_when_not_on_15_january(): void
    {
        config()->set('somoney.available.services.membership', true);
        $cyclosUserId = '4036691750709796712';
        // We are on 15/01/2021.
        Carbon::setTestNow(Carbon::parse('2021-01-14'));
        // Membership has been paid at 31/09/2020 23:59:59.
        Membership::create([
            'cyclos_user_id' => $cyclosUserId,
            'succeed_at' => Carbon::parse('2020-09-30 23:59:59'),
        ]);
        $this->mock(UserService::class)->shouldNotReceive('searchAll');
        $this->artisan(MembershipsMakeRenewals::class);
        // Membership is not renewed (no new line in database).
        $this->assertDatabaseCount(app(Membership::class)->getTable(), 1);
        Bus::assertNotDispatched(SendEmailRenewCreated::class);
    }

    /** @test */
    public function it_can_renew_membership_when_not_on_15_january_but_forced(): void
    {
        config()->set('somoney.available.services.membership', true);
        $cyclosUserId = '4036691750709796712';
        // We are on 15/01/2021.
        Carbon::setTestNow(Carbon::parse('2021-01-14'));
        // Membership has been paid at 31/09/2020 23:59:59.
        Membership::create([
            'cyclos_user_id' => $cyclosUserId,
            'succeed_at' => Carbon::parse('2020-09-30 23:59:59'),
        ]);
        $this->mock(UserService::class)->shouldReceive('searchAll')
            ->once()
            ->with([
                'groups' => implode(',', array_values(config('cyclos.internal-names.groups'))),
                'statuses' => ['active'],
            ])
            ->andReturn(collect([]));
        $this->artisan(MembershipsMakeRenewals::class, ['--force' => true]);
        // Membership is renewed (1 new line in database)
        $this->assertDatabaseCount(app(Membership::class)->getTable(), 2);
        Bus::assertDispatched(fn(SendEmailRenewCreated $job) => $job->cyclosUserId === $cyclosUserId);
    }

    /** @test */
    public function it_can_create_user_membership_from_cyclos_when_not_already_existing(): void
    {
        config()->set('somoney.available.services.membership', true);
        $cyclosUserId = '4036691750709796712';
        // We are on 15/01/2021.
        Carbon::setTestNow(Carbon::parse('2021-01-15'));
        $this->mock(UserService::class)->shouldReceive('searchAll')
            ->once()
            ->with([
                'groups' => implode(',', array_values(config('cyclos.internal-names.groups'))),
                'statuses' => ['active'],
            ])
            ->andReturn(collect([['id' => $cyclosUserId]]));
        $this->artisan(MembershipsMakeRenewals::class);
        // Membership is created from Cyclos (1 new line in database).
        $this->assertDatabaseCount(app(Membership::class)->getTable(), 1);
        Bus::assertDispatched(fn(SendEmailRenewCreated $job) => $job->cyclosUserId === $cyclosUserId);
    }

    /** @test */
    public function it_cant_create_user_membership_from_cyclos_when_already_existing(): void
    {
        config()->set('somoney.available.services.membership', true);
        $cyclosUserId = '4036691750709796712';
        // We are on 15/01/2021.
        Carbon::setTestNow(Carbon::parse('2021-01-15'));
        // Membership is existing for Cyclos user.
        Membership::create(['cyclos_user_id' => $cyclosUserId]);
        $this->mock(UserService::class)->shouldReceive('searchAll')
            ->once()
            ->with([
                'groups' => implode(',', array_values(config('cyclos.internal-names.groups'))),
                'statuses' => ['active'],
            ])
            ->andReturn(collect([['id' => $cyclosUserId]]));
        $this->artisan(MembershipsMakeRenewals::class);
        // No new membership is created (no new line in database).
        $this->assertDatabaseCount(app(Membership::class)->getTable(), 1);
    }
}
