<?php

namespace Tests\Unit\Console\Lemonway;

use App\Console\Commands\Lemonway\WatchTransactionStatus;
use Illuminate\Console\Command;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class WatchTransactionStatusTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_cant_execute_when_payment_gateway_is_not_lemonway(): void
    {
        config()->set('somoney.payment-gateway', 'payment-gateway');
        $this->artisan(WatchTransactionStatus::class)
            ->assertExitCode(Command::INVALID)
            ->expectsOutput('The payment gateway on this instance is not Lemonway.');
    }
}
