<?php

namespace Tests\Unit\Console\Lemonway;

use App\Console\Commands\Lemonway\MoneyOutTransfer;
use App\Models\Transaction;
use Illuminate\Console\Command;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MoneyOutTransferTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_cant_execute_when_payment_gateway_is_not_lemonway(): void
    {
        config()->set('somoney.payment-gateway', 'payment-gateway');
        $this->artisan(MoneyOutTransfer::class)
            ->assertExitCode(Command::INVALID)
            ->expectsOutput('The payment gateway on this instance is not Lemonway.');
    }

    /** @test */
    public function it_cant_execute_when_iban_id_is_not_set(): void
    {
        config()->set('somoney.payment-gateway', Transaction::TYPE_LEMONWAY);
        config()->set('lemonway.iban_id', null);
        $this->artisan(MoneyOutTransfer::class)
            ->assertExitCode(Command::INVALID)
            ->expectsOutput('The Lemonway iban_id for money out is needed.');
    }
}
