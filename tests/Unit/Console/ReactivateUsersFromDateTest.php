<?php

namespace Tests\Unit\Console;

use App\Console\Commands\Users\ReactivateUsersFromDate;
use App\Models\CyclosUser;
use App\Services\Cyclos\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Pagination\LengthAwarePaginator;
use Mockery;
use Symfony\Component\Console\Exception\RuntimeException;
use Tests\TestCase;

class ReactivateUsersFromDateTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutExceptionHandling();
    }

    /** @test */
    public function it_cant_call_command_without_date(): void
    {
        $this->expectException(RuntimeException::class);
        $this->artisan(ReactivateUsersFromDate::class)
            ->assertExitCode(1)
            ->execute();
    }

    /** @test */
    public function it_cant_call_command_when_date_in_wrong_format(): void
    {
        $this->expectException(\ErrorException::class);
        $this->expectExceptionMessage('The date "14/02/2021" is not in the correct format.');
        $this->artisan(ReactivateUsersFromDate::class, ['day' => '14/02/2021'])
            ->assertExitCode(1)
            ->execute();
    }

    /** @test */
    public function it_can_reactivate_user_when_deactivated_on_given_date(): void
    {
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('searchAll')
            ->once()
            ->with(Mockery::on(fn($search) => empty(array_diff(
                data_get($search, 'statuses'),
                [CyclosUser::STATUS_DISABLED]
            ))))
            ->andReturn(collect([['id' => '4036691750709796712']]));
        $userServiceMock->shouldReceive('getStatus')
            ->once()
            ->with('4036691750709796712')
            ->andReturn(new LengthAwarePaginator(
                ['history' => [['period' => ['begin' => '2021-02-14T16:17:20.274+02:00']]]],
                1,
                1
            ));
        $userServiceMock->shouldReceive('setStatus')
            ->once()
            ->with(
                '4036691750709796712',
                CyclosUser::STATUS_ACTIVE,
                'Reactivated by script "users:reactivation" from "SoMoney - App"'
            )
            ->andReturn(null);
        $this->artisan(ReactivateUsersFromDate::class, ['day' => '2021-02-14'])
            ->assertExitCode(0)
            ->expectsOutput('User 4036691750709796712 has been reactivated after being deactivated on 2021-02-14T16:17:20+02:00.')
            ->execute();
    }

    /** @test */
    public function it_cant_reactivate_user_when_deactivated_on_different_date(): void
    {
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('searchAll')
            ->once()
            ->with(Mockery::on(fn($search) => empty(array_diff(
                data_get($search, 'statuses'),
                [CyclosUser::STATUS_DISABLED]
            ))))
            ->andReturn(collect([['id' => '4036691750709796712']]));
        $userServiceMock->shouldReceive('getStatus')
            ->once()
            ->with('4036691750709796712')
            ->andReturn(new LengthAwarePaginator(
                ['history' => [['period' => ['begin' => '2021-02-12T16:17:20.274+02:00']]]],
                1,
                1
            ));
        $this->artisan(ReactivateUsersFromDate::class, ['day' => '2021-02-14'])
            ->assertExitCode(0)
            ->doesntExpectOutput('User 4036691750709796712 has been reactivated after being deactivated on '
                . '2021-02-14T16:17:20+02:00')
            ->execute();
    }
}
