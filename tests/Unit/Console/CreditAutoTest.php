<?php

namespace Tests\Unit\Console;

use App\Console\Commands\MonthlyCreditAuto;
use App\Jobs\SendEmailRemindStopSepa;
use App\Jobs\SepaTransfer;
use App\Models\Sepa;
use Illuminate\Support\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;

class CreditAutoTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Bus::fake();
    }

    /** @test */
    public function it_can_execute_automated_sepa_transfer(): void
    {
        $sepa = Sepa::create([
            'user_id' => '4036691750709796712',
            'amount' => 5,
            'gender' => 'm',
            "last_name" => "NAME",
            "first_name" => "First Name",
            "email" => "test@fake.test",
            "company" => "Bibendum",
            "iban" => "FR5410096000407381914916P38",
            'done' => true,
            'canceled' => false,
            'stopped' => false,
            'confirmed' => true,
            'duration_month' => 6,
            'start_date' => now(),
        ]);
        $this->artisan(MonthlyCreditAuto::class);
        Bus::assertDispatched(fn(SepaTransfer $job) => $job->sepaId === $sepa->id);
    }

    /** @test */
    public function it_cant_execute_automated_sepa_transfer_on_other_day(): void
    {
        Sepa::create([
            'user_id' => '4036691750709796712',
            'amount' => 5,
            'gender' => 'm',
            "last_name" => "NAME",
            "first_name" => "First Name",
            "email" => "test@fake.test",
            "company" => "Bibendum",
            "iban" => "FR5410096000407381914916P38",
            'done' => true,
            'canceled' => false,
            'stopped' => false,
            'confirmed' => true,
            'duration_month' => 6,
            'start_date' => now()->subDay(),
        ]);
        $this->artisan(MonthlyCreditAuto::class);
        Bus::assertNotDispatched(SepaTransfer::class);
    }

    /** @test */
    public function it_cant_execute_automated_sepa_transfer_when_duration_is_exceeded(): void
    {
        Sepa::create([
            'user_id' => '4036691750709796712',
            'amount' => 5,
            'gender' => 'm',
            "last_name" => "NAME",
            "first_name" => "First Name",
            "email" => "test@fake.test",
            "company" => "Bibendum",
            "iban" => "FR5410096000407381914916P38",
            'done' => true,
            'canceled' => false,
            'stopped' => false,
            'confirmed' => true,
            'duration_month' => 6,
            'start_date' => now()->subMonths(6),
        ]);
        $this->artisan(MonthlyCreditAuto::class);
        Bus::assertNotDispatched(SepaTransfer::class);
    }

    /** @test */
    public function it_can_execute_automated_sepa_transfer_on_last_day_of_month_for_shorter_months(): void
    {
        $sepa = Sepa::create([
            'user_id' => '4036691750709796712',
            'amount' => 5,
            'gender' => 'm',
            "last_name" => "NAME",
            "first_name" => "First Name",
            "email" => "test@fake.test",
            "company" => "Bibendum",
            "iban" => "FR5410096000407381914916P38",
            'done' => true,
            'canceled' => false,
            'stopped' => false,
            'confirmed' => true,
            'duration_month' => 6,
            'start_date' => Carbon::create(2021, 1, 31),
        ]);
        Carbon::setTestNow(Carbon::create(2021, 2, 28));
        $this->artisan(MonthlyCreditAuto::class);
        Bus::assertDispatched(fn(SepaTransfer $job) => $job->sepaId === $sepa->id);
    }

    /** @test */
    public function it_can_notify_managers_when_last_sepa_transfer_has_been_executed(): void
    {
        $sepa = Sepa::create([
            'user_id' => '4036691750709796712',
            'amount' => 5,
            'gender' => 'm',
            "last_name" => "NAME",
            "first_name" => "First Name",
            "email" => "test@fake.test",
            "company" => "Bibendum",
            "iban" => "FR5410096000407381914916P38",
            'done' => true,
            'canceled' => false,
            'stopped' => false,
            'confirmed' => true,
            'duration_month' => 6,
            'start_date' => now()->subMonths(5),
        ]);
        $this->artisan(MonthlyCreditAuto::class);
        Bus::assertDispatched(fn(SendEmailRemindStopSepa $job) => $job->sepaId === $sepa->id);
    }

    /** @test */
    public function it_cant_notify_managers_on_execution_of_other_than_last_sepa_transfer(): void
    {
        Sepa::create([
            'user_id' => '4036691750709796712',
            'amount' => 5,
            'gender' => 'm',
            "last_name" => "NAME",
            "first_name" => "First Name",
            "email" => "test@fake.test",
            "company" => "Bibendum",
            "iban" => "FR5410096000407381914916P38",
            'done' => true,
            'canceled' => false,
            'stopped' => false,
            'confirmed' => true,
            'duration_month' => 6,
            'start_date' => now()->subMonths(4),
        ]);
        $this->artisan(MonthlyCreditAuto::class);
        Bus::assertNotDispatched(SendEmailRemindStopSepa::class);
    }
}
