<?php

namespace Tests\Unit\Jobs;

use App\Jobs\LoadEmailStatus;
use App\Jobs\MailgunUpdateUserEmailStatus;
use App\Models\LocalUser;
use App\Models\ValidationUser;
use App\Services\Cyclos\UserService;
use App\Services\MailgunService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;

class LoadEmailStatusTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_dispatch_job_when_update_mailgun_status_of_local_user(): void
    {
        Bus::fake();
        $stubLocalUser = LocalUser::create([
            'name' => 'Individual User',
            'group' => config('cyclos.internal-names.groups.individual'),
            'email' => 'part@fake.test',
            'customValues' => [],
        ]);
        $validationUser = ValidationUser::create([
            'local_user_id' => $stubLocalUser->id,
            'group' => $stubLocalUser->group,
            'email' => $stubLocalUser->email,
        ]);
        $this->mock(MailgunService::class);
        $this->mock(UserService::class)->shouldReceive('searchAll')
            ->once()
            ->with(['groups' => implode(',', array_values(config('cyclos.internal-names.groups')))])
            ->andReturn(collect([]));
        (new LoadEmailStatus(null, collect([$stubLocalUser])))->handle();
        Bus::assertDispatched(
            fn(MailgunUpdateUserEmailStatus $job) => $job->validationUser->id === $validationUser->id
        );
    }

    /** @test */
    public function it_can_dispatch_job_when_update_mailgun_status_of_cyclos_user(): void
    {
        Bus::fake();
        $stubCyclosUserData = [
            "id" => "4036691750709796712",
            'group' => ['internalName' => config('cyclos.internal-names.groups.individual')],
            'email' => 'part@fake.test',
        ];
        $this->mock(UserService::class)->shouldReceive('searchAll')
            ->once()
            ->with(['groups' => implode(',', array_values(config('cyclos.internal-names.groups')))])
            ->andReturn(collect([$stubCyclosUserData]));
        $validationUser = ValidationUser::create([
            'cyclos_id' => $stubCyclosUserData['id'],
            'group' => config('cyclos.internal-names.groups.individual'),
            'email' => 'part@fake.test',
        ]);
        $this->mock(MailgunService::class);
        (new LoadEmailStatus(collect([$stubCyclosUserData['id']])))->handle();
        Bus::assertDispatched(
            fn(MailgunUpdateUserEmailStatus $job) => $job->validationUser->id === $validationUser->id
        );
    }
}
