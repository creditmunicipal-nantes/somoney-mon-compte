<?php

namespace Tests\Unit\Jobs\Memberships;

use App\Jobs\Memberships\SendEmailManualRenewCreated;
use App\Mail\Memberships\RenewCreated;
use App\Models\CyclosUser;
use App\Models\UserToken;
use App\Services\Cyclos\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class SendEmailManualRenewCreatedTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_cant_send_manual_email_renew_membership_when_user_is_not_active(): void
    {
        Mail::fake();
        config()->set('somoney.mail.account', 'manager@fake.test');
        config()->set('somoney.money', 'Moneko');
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'User',
            'display' => 'User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
            'status' => CyclosUser::STATUS_REMOVED,
        ];
        UserToken::create([
            'user_id' => $cyclosUserData['id'],
            'type' => UserToken::TYPE_MEMBERSHIP_RENEW,
        ]);
        $this->mock(UserService::class)
            ->shouldReceive('get')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn($cyclosUserData);
        Bus::dispatch(new SendEmailManualRenewCreated($cyclosUserData['id']));
        Mail::assertNotSent(RenewCreated::class);
    }

    /** @test */
    public function it_can_send_manual_email_renew_membership_when_token_already_exists(): void
    {
        Mail::fake();
        config()->set('somoney.mail.account', 'manager@fake.test');
        config()->set('somoney.money', 'Moneko');
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'User',
            'display' => 'User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
            'status' => CyclosUser::STATUS_ACTIVE,
        ];
        $userToken = UserToken::create([
            'user_id' => $cyclosUserData['id'],
            'type' => UserToken::TYPE_MEMBERSHIP_RENEW,
        ]);
        $this->mock(UserService::class)
            ->shouldReceive('get')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn($cyclosUserData);
        Bus::dispatch(new SendEmailManualRenewCreated($cyclosUserData['id']));
        Mail::assertSent(fn(RenewCreated $mail) => $mail->userToken->is($userToken)
            && $mail->username === $cyclosUserData['display']
            && $mail->hasTo($cyclosUserData['email']));
    }

    /** @test */
    public function it_can_send_manual_email_renew_membership_when_token_doesnt_exists(): void
    {
        Mail::fake();
        config()->set('somoney.mail.account', 'manager@fake.test');
        config()->set('somoney.money', 'Moneko');
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'User',
            'display' => 'User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
            'status' => CyclosUser::STATUS_ACTIVE,
        ];
        $this->mock(UserService::class)
            ->shouldReceive('get')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn($cyclosUserData);
        Bus::dispatch(new SendEmailManualRenewCreated($cyclosUserData['id']));
        Mail::assertSent(fn(RenewCreated $mail) => $mail->userToken->isNot(null)
            && $mail->username === $cyclosUserData['display']
            && $mail->hasTo($cyclosUserData['email']));
        $this->assertDatabaseHas(app(UserToken::class)->getTable(), [
            'user_id' => $cyclosUserData['id'],
            'type' => UserToken::TYPE_MEMBERSHIP_RENEW,
        ]);
    }
}
