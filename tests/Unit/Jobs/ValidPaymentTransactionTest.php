<?php

namespace Tests\Unit\Jobs;

use App\Jobs\ValidPaymentTransaction;
use App\Models\LocalUser;
use App\Models\Membership;
use App\Models\Transaction;
use App\Services\Cyclos\TransactionService;
use App\Services\Cyclos\UserService;
use App\Services\LocalUserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Tests\TestCase;
use Illuminate\Support\Facades\Mail;

class ValidPaymentTransactionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_renew_membership_with_succeeded_transaction(): void
    {
        Mail::fake();
        config()->set('somoney.money', 'Moneko');
        config()->set('somoney.available.services.membership', true);
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'Test',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        Membership::create(['cyclos_user_id' => $cyclosUserData['id']]);
        $transaction = Transaction::create([
            'amount' => 2400,
            'status' => Transaction::STATUS_PROCESSING,
            'type' => Transaction::TYPE_SYSTEM_PAY,
            'cyclos_user_id' => $cyclosUserData['id'],
            'local_user_id' => null,
            'context' => Transaction::CONTEXT_MEMBERSHIP,
        ]);
        (new ValidPaymentTransaction($transaction))->processSuccess();
        self::assertTrue(Membership::whereNotNull('succeed_at')->exists());
    }

    /** @test */
    public function it_cant_credit_on_cyclos_with_registration_context_transaction(): void
    {
        Mail::fake();
        config()->set('somoney.money', 'Moneko');
        config()->set('somoney.available.services.membership', false);
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'Test',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        $stubLocalUser = LocalUser::create([
            'cyclos_id' => $cyclosUserData['id'],
            'name' => $cyclosUserData['name'],
            'email' => $cyclosUserData['email'],
            'group' => config('cyclos.internal-names.groups.professional'),
            'customValues' => '{}',
        ]);
        $transaction = Transaction::create([
            'amount' => 2400,
            'status' => Transaction::STATUS_PROCESSING,
            'type' => Transaction::TYPE_SYSTEM_PAY,
            'cyclos_user_id' => $cyclosUserData['id'],
            'local_user_id' => $stubLocalUser->id,
            'context' => Transaction::CONTEXT_REGISTRATION,
            'parent_id' => null,
            'transaction_type' => Transaction::REGISTER_TYPE_ADHERE,
        ]);
        $this->mock(LocalUserService::class)->shouldReceive('createCyclosUser')
            ->once()
            ->with(Mockery::on(fn(LocalUser $localUser) => $localUser['id'] === $stubLocalUser->id))
            ->andReturn($stubLocalUser);
        $this->mock(UserService::class)->shouldReceive('get')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn($cyclosUserData);
        $this->mock(TransactionService::class)->shouldNotReceive('storeInitialAcompte');
        (new ValidPaymentTransaction($transaction))->processSuccess();
    }

    /** @test */
    public function it_cant_credit_on_cyclos_with_membership_context_transaction(): void
    {
        Mail::fake();
        config()->set('somoney.money', 'Moneko');
        config()->set('somoney.available.services.membership', true);
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'Test',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        Membership::create(['cyclos_user_id' => $cyclosUserData['id']]);
        $transaction = Transaction::create([
            'amount' => 2400,
            'status' => Transaction::STATUS_PROCESSING,
            'type' => Transaction::TYPE_SYSTEM_PAY,
            'cyclos_user_id' => $cyclosUserData['id'],
            'local_user_id' => null,
            'context' => Transaction::CONTEXT_MEMBERSHIP,
        ]);
        $this->mock(TransactionService::class)->shouldNotReceive('storeInitialAcompte');
        (new ValidPaymentTransaction($transaction))->processSuccess();
    }
}
