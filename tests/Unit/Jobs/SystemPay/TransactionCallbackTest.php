<?php

namespace Tests\Unit\Jobs\SystemPay;

use App\Jobs\SystemPay\TransactionCallback;
use App\Models\Transaction;
use App\Services\Cyclos\TransactionService;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;

class TransactionCallbackTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_update_transaction_status_from_processing_to_success(): void
    {
        config()->set('somoney.money', 'Moneko');
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'Test',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        $transactionId = '0f080afa-faad-422f-9014-1791f34014db';
        $paymentData = ['orderStatus' => 'PAID', 'orderDetails' => ['orderId' => $transactionId]];
        $now = now();
        DB::table(app(Transaction::class)->getTable())->insert([
            'id' => $transactionId,
            'amount' => 2400,
            'status' => Transaction::STATUS_PROCESSING,
            'type' => Transaction::TYPE_SYSTEM_PAY,
            'cyclos_user_id' => $cyclosUserData['id'],
            'local_user_id' => null,
            'context' => "operation",
            'parent_id' => null,
            'transaction_type' => "credit",
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        $this->mock(UserService::class)->shouldReceive('get')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn($cyclosUserData);
        $this->mock(TransactionService::class)->shouldReceive('storeInitialAcompte')
            ->once()
            ->with($cyclosUserData['id'], 24, 'Credit Euros - Moneko', 'stableAccount.toProfessional');
        Carbon::setTestNow($now->copy()->addHour());
        Bus::dispatch(new TransactionCallback($paymentData));
        $this->assertDatabaseHas(app(Transaction::class)->getTable(), [
            'id' => $transactionId,
            'amount' => 2400,
            'status' => Transaction::STATUS_SUCCESS,
            'type' => Transaction::TYPE_SYSTEM_PAY,
            'cyclos_user_id' => $cyclosUserData['id'],
            'local_user_id' => null,
            'context' => "operation",
            'parent_id' => null,
            'transaction_type' => "credit",
            'created_at' => $now,
        ]);
        self::assertTrue(Transaction::first()->updated_at->gt($now));
    }

    /** @test */
    public function it_can_update_transaction_status_from_processing_to_failed(): void
    {
        config()->set('somoney.money', 'Moneko');
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'Test',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        $transactionId = '0f080afa-faad-422f-9014-1791f34014db';
        $paymentData = ['orderStatus' => 'UNPAID', 'orderDetails' => ['orderId' => $transactionId]];
        $now = now();
        DB::table(app(Transaction::class)->getTable())->insert([
            "id" => $transactionId,
            "amount" => 2400,
            "status" => Transaction::STATUS_PROCESSING,
            "type" => Transaction::TYPE_SYSTEM_PAY,
            "cyclos_user_id" => $cyclosUserData['id'],
            "local_user_id" => null,
            "context" => "operation",
            "parent_id" => null,
            "transaction_type" => "credit",
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        Carbon::setTestNow($now->copy()->addHour());
        Bus::dispatch(new TransactionCallback($paymentData));
        $this->assertDatabaseHas(app(Transaction::class)->getTable(), [
            "id" => $transactionId,
            "amount" => 2400,
            "status" => Transaction::STATUS_FAILED,
            "type" => Transaction::TYPE_SYSTEM_PAY,
            "cyclos_user_id" => $cyclosUserData['id'],
            "local_user_id" => null,
            "context" => "operation",
            "parent_id" => null,
            "transaction_type" => "credit",
            'created_at' => $now,
        ]);
        self::assertTrue(Transaction::first()->updated_at->gt($now));
    }

    /** @test */
    public function it_cant_update_transaction_status_from_processing_to_processing(): void
    {
        config()->set('somoney.money', 'Moneko');
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'Test',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        $transactionId = '0f080afa-faad-422f-9014-1791f34014db';
        $paymentData = ['orderStatus' => 'RUNNING', 'orderDetails' => ['orderId' => $transactionId]];
        $now = now();
        DB::table(app(Transaction::class)->getTable())->insert([
            "id" => $transactionId,
            "amount" => 2400,
            "status" => Transaction::STATUS_PROCESSING,
            "type" => Transaction::TYPE_SYSTEM_PAY,
            "cyclos_user_id" => $cyclosUserData['id'],
            "local_user_id" => null,
            "context" => "operation",
            "parent_id" => null,
            "transaction_type" => "credit",
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        Carbon::setTestNow($now->copy()->addHour());
        Bus::dispatch(new TransactionCallback($paymentData));
        $this->assertDatabaseHas(app(Transaction::class)->getTable(), [
            "id" => $transactionId,
            "amount" => 2400,
            "status" => Transaction::STATUS_PROCESSING,
            "type" => Transaction::TYPE_SYSTEM_PAY,
            "cyclos_user_id" => $cyclosUserData['id'],
            "local_user_id" => null,
            "context" => "operation",
            "parent_id" => null,
            "transaction_type" => "credit",
            'created_at' => $now,
            'updated_at' => $now,
        ]);
    }

    /** @test */
    public function it_can_update_transaction_status_from_failed_to_success(): void
    {
        config()->set('somoney.money', 'Moneko');
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'Test',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        $transactionId = '0f080afa-faad-422f-9014-1791f34014db';
        $paymentData = ['orderStatus' => 'PAID', 'orderDetails' => ['orderId' => $transactionId]];
        $now = now();
        DB::table(app(Transaction::class)->getTable())->insert([
            "id" => $transactionId,
            "amount" => 2400,
            "status" => Transaction::STATUS_FAILED,
            "type" => Transaction::TYPE_SYSTEM_PAY,
            "cyclos_user_id" => $cyclosUserData['id'],
            "local_user_id" => null,
            "context" => "operation",
            "parent_id" => null,
            "transaction_type" => "credit",
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        Carbon::setTestNow($now->copy()->addHour());
        $this->mock(UserService::class)->shouldReceive('get')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn($cyclosUserData);
        $this->mock(TransactionService::class)->shouldReceive('storeInitialAcompte')
            ->once()
            ->with($cyclosUserData['id'], 24, 'Credit Euros - Moneko', 'stableAccount.toProfessional');
        Bus::dispatch(new TransactionCallback($paymentData));
        $this->assertDatabaseHas(app(Transaction::class)->getTable(), [
            "id" => $transactionId,
            "amount" => 2400,
            "status" => Transaction::STATUS_SUCCESS,
            "type" => Transaction::TYPE_SYSTEM_PAY,
            "cyclos_user_id" => $cyclosUserData['id'],
            "local_user_id" => null,
            "context" => "operation",
            "parent_id" => null,
            "transaction_type" => "credit",
            'created_at' => $now,
        ]);
        self::assertTrue(Transaction::first()->updated_at->gt($now));
    }

    /** @test */
    public function it_cant_update_transaction_status_from_success_to_failed(): void
    {
        config()->set('somoney.money', 'Moneko');
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'Test',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        $transactionId = '0f080afa-faad-422f-9014-1791f34014db';
        $paymentData = ['orderStatus' => 'UNPAID', 'orderDetails' => ['orderId' => $transactionId]];
        $now = now();
        DB::table(app(Transaction::class)->getTable())->insert([
            "id" => $transactionId,
            "amount" => 2400,
            "status" => Transaction::STATUS_SUCCESS,
            "type" => Transaction::TYPE_SYSTEM_PAY,
            "cyclos_user_id" => $cyclosUserData['id'],
            "local_user_id" => null,
            "context" => "operation",
            "parent_id" => null,
            "transaction_type" => "credit",
            'created_at' => $now,
            'updated_at' => $now,
        ]);
        Carbon::setTestNow($now->copy()->addHour());
        Bus::dispatch(new TransactionCallback($paymentData));
        $this->assertDatabaseHas(app(Transaction::class)->getTable(), [
            "id" => $transactionId,
            "amount" => 2400,
            "status" => Transaction::STATUS_SUCCESS,
            "type" => Transaction::TYPE_SYSTEM_PAY,
            "cyclos_user_id" => $cyclosUserData['id'],
            "local_user_id" => null,
            "context" => "operation",
            "parent_id" => null,
            "transaction_type" => "credit",
            'created_at' => $now,
            'updated_at' => $now,
        ]);
    }
}
