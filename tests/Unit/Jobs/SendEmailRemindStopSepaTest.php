<?php

namespace Tests\Unit\Jobs;

use App\Jobs\SendEmailRemindStopSepa;
use App\Mail\Commands\RemindStopSepa;
use App\Models\Sepa;
use App\Services\Cyclos\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class SendEmailRemindStopSepaTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_send_reminder_email_to_managers_for_given_sepa_transfer(): void
    {
        Mail::fake();
        config()->set('somoney.mail.account', 'manager@fake.test');
        config()->set('somoney.money', 'Moneko');
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        $sepaTransfer = Sepa::create([
            'user_id' => $cyclosUserData['id'],
            'amount' => 5,
            'gender' => 'm',
            "last_name" => "NAME",
            "first_name" => "First Name",
            "email" => $cyclosUserData['email'],
            "company" => "Bibendum",
            "iban" => "FR5410096000407381914916P38",
            'done' => true,
            'canceled' => false,
            'stopped' => false,
            'confirmed' => true,
            'duration_month' => 6,
            'start_date' => now(),
            'last_executed_at' => now()->subMonth(),
        ]);
        $this->mock(UserService::class)
            ->shouldReceive('get')
            ->once()
            ->with($sepaTransfer->user_id)
            ->andReturn($cyclosUserData);
        Bus::dispatch(new SendEmailRemindStopSepa($sepaTransfer->id));
        Mail::assertSent(fn(RemindStopSepa $mail) => $mail->sepa->is($sepaTransfer)
            && $mail->hasTo('manager@fake.test'));
    }
}
