<?php

namespace Tests\Unit\Jobs;

use App\Jobs\SepaTransfer;
use App\Models\Sepa;
use App\Services\Cyclos\TransactionService;
use App\Services\Cyclos\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;

class MonthlyCreditAutoTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_execute_sepa_transfer_when_not_already_executed_this_month(): void
    {
        config()->set('somoney.money', 'Moneko');
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        $sepaTransfer = Sepa::create([
            'user_id' => $cyclosUserData['id'],
            'amount' => 5,
            'gender' => 'm',
            "last_name" => "NAME",
            "first_name" => "First Name",
            "email" => $cyclosUserData['email'],
            "company" => "Bibendum",
            "iban" => "FR5410096000407381914916P38",
            'done' => true,
            'canceled' => false,
            'stopped' => false,
            'confirmed' => true,
            'duration_month' => 6,
            'start_date' => now(),
            'last_executed_at' => now()->subMonth()
        ]);
        $this->mock(UserService::class)->shouldReceive('get')
            ->once()
            ->with($sepaTransfer->user_id)
            ->andReturn($cyclosUserData);
        $this->mock(TransactionService::class)->shouldReceive('storeInitialAcompte')
            ->once()
            ->with(
                $sepaTransfer->user_id,
                $sepaTransfer->amount,
                'Credit Euros - Moneko',
                'stableAccount.toProfessional'
            );
        Bus::dispatch(new SepaTransfer($sepaTransfer->id));
        self::assertFalse($sepaTransfer->last_executed_at->eq($sepaTransfer->refresh()->last_executed_at));
    }

    /** @test */
    public function it_cant_execute_sepa_transfer_when_already_executed_this_month(): void
    {
        config()->set('somoney.money', 'Moneko');
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'Test',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        $sepaTransfer = Sepa::create([
            'user_id' => $cyclosUserData['id'],
            'amount' => 5,
            'gender' => 'm',
            "last_name" => "NAME",
            "first_name" => "First Name",
            "email" => $cyclosUserData['email'],
            "company" => "Bibendum",
            "iban" => "FR5410096000407381914916P38",
            'done' => true,
            'canceled' => false,
            'stopped' => false,
            'confirmed' => true,
            'duration_month' => 6,
            'start_date' => now(),
            'last_executed_at' => now()->subMinute()
        ]);
        $this->mock(UserService::class)->shouldNotReceive('get');
        $this->mock(TransactionService::class)->shouldNotReceive('storeInitialAcompte');
        Bus::dispatch(new SepaTransfer($sepaTransfer->id));
        self::assertTrue($sepaTransfer->last_executed_at->eq($sepaTransfer->refresh()->last_executed_at));
    }
}
