<?php

namespace Tests\Unit\Services;

use App\Models\LocalUser;
use App\Models\Membership;
use App\Models\Transaction;
use App\Services\Cyclos\UserService;
use App\Services\SystemPayService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Lyra\Client as LyraClient;
use Mockery;
use Tests\TestCase;

class SystemPayServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @throws \App\Exceptions\Cyclos\NotFoundException
     * @throws \App\Exceptions\Cyclos\PasswordExpiredException
     * @throws \App\Exceptions\Cyclos\ValidationException
     * @throws \App\Exceptions\Cyclos\ValidationSentryException
     * @throws \App\Exceptions\Cyclos\WebServiceAuthException
     * @throws \Illuminate\Auth\AuthenticationException
     * @throws \Lyra\Exceptions\LyraException
     */
    public function it_can_create_payment_transaction_for_credit(): void
    {
        config()->set('somoney.payment-gateway', 'system-pay');
        $transactionAmount = 2500;
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'Test',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        $createPaymentResponse = [
            "webService" => "Charge/CreatePayment",
            "version" => "V4",
            "applicationVersion" => "5.17.0",
            "status" => "SUCCESS",
            "answer" => ["formToken" => "random-token", "_type" => "V4/Charge/PaymentForm"],
            "ticket" => null,
            "serverDate" => "2020-12-22T14:00:14+00:00",
            "applicationProvider" => "NPS",
            "metadata" => null,
            "_type" => "V4/WebService/Response",
        ];
        $this->mock(UserService::class)->shouldReceive('get')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn($cyclosUserData);
        $this->mock(LyraClient::class)->shouldReceive('post')
            ->once()
            ->with('V4/Charge/CreatePayment', Mockery::on(fn(array $data) => $data['amount'] === $transactionAmount
                && $data['currency'] === 'EUR'
                && is_string($data['orderId'])
                && is_array($data['customer'])
                && $data['customer']['email'] === 'test@fake.test'))
            ->andReturn($createPaymentResponse);
        $formToken = app(SystemPayService::class)->createPayment(
            $transactionAmount,
            $cyclosUserData['id'],
            Transaction::CONTEXT_OPERATION
        );
        self::assertEquals($formToken, $createPaymentResponse['answer']['formToken']);
        $this->assertDatabaseHas('transaction', [
            'cyclos_user_id' => $cyclosUserData['id'],
            'amount' => $transactionAmount,
            'context' => Transaction::CONTEXT_OPERATION,
        ]);
    }

    /** @test */
    public function it_can_valid_hash_of_system_pay_request(): void
    {
        request()->merge([
            'kr-hash-algorithm' => 'sha256_hmac',
            'kr-hash-key' => 'password',
            'kr-hash' => 'a0fdc7197b8e558d26024a74ee692c3edf9fb6a630f8b13908f3e7b4f586610c',
            'kr-answer' => json_encode(['Foo' => 'BAR'], JSON_THROW_ON_ERROR),
        ]);
        self::assertTrue(app(SystemPayService::class)->checkHash());
    }


    /** @test */
    public function it_can_make_transaction_to_membership_contract_account_when_transaction_from_registration(): void
    {
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'Test',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        $stubLocalUser = LocalUser::create([
            'cyclos_id' => $cyclosUserData['id'],
            'name' => $cyclosUserData['name'],
            'email' => $cyclosUserData['email'],
            'group' => config('cyclos.internal-names.groups.professional'),
            'customValues' => '{}',
        ]);

        $transaction = Transaction::create([
            'amount' => 2400,
            'status' => Transaction::STATUS_PROCESSING,
            'type' => Transaction::TYPE_SYSTEM_PAY,
            'cyclos_user_id' => $cyclosUserData['id'],
            'local_user_id' => $stubLocalUser->id,
            'context' => Transaction::CONTEXT_REGISTRATION,
            'parent_id' => null,
            'transaction_type' => Transaction::REGISTER_TYPE_ADHERE,
        ]);

        self::assertEquals(
            config('system-pay.contracts.memberships'),
            app(SystemPayService::class)->getTransactionMid($transaction)
        );
    }

    /** @test */
    public function it_can_make_transaction_to_membership_contract_when_transaction_from_membership_renew(): void
    {
        $cyclosUserData = [
            'id' => '4036691750709796712',
        ];
        Membership::create(['cyclos_user_id' => $cyclosUserData['id']]);

        $transaction = Transaction::create([
            'amount' => 2400,
            'status' => Transaction::STATUS_PROCESSING,
            'type' => Transaction::TYPE_SYSTEM_PAY,
            'cyclos_user_id' => $cyclosUserData['id'],
            'context' => Transaction::CONTEXT_MEMBERSHIP,
        ]);

        self::assertEquals(
            config('system-pay.contracts.memberships'),
            app(SystemPayService::class)->getTransactionMid($transaction)
        );
    }

    /** @test */
    public function it_can_make_transaction_to_credit_contract_when_transaction_from_credit(): void
    {
        $cyclosUserData = [
            'id' => '4036691750709796712',
        ];

        $transaction = Transaction::create([
            'amount' => 2400,
            'status' => Transaction::STATUS_PROCESSING,
            'type' => Transaction::TYPE_SYSTEM_PAY,
            'cyclos_user_id' => $cyclosUserData['id'],
            'context' => Transaction::CONTEXT_OPERATION,
        ]);

        self::assertEquals(
            config('system-pay.contracts.credit'),
            app(SystemPayService::class)->getTransactionMid($transaction)
        );
    }
}
