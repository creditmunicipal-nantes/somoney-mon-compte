<?php

namespace Tests\Unit\Services;

use App\Models\LocalUser;
use App\Models\ValidationUser;
use App\Services\MailgunService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class MailgunServiceTest extends TestCase
{
    use RefreshDatabase;

    protected string $basePath;

    protected function setUp(): void
    {
        parent::setUp();
        $this->basePath = 'https://' . config('services.mailgun.endpoint') . '/v3/' . config('services.mailgun.domain');
    }

    /**
     * @test
     * @throws \Illuminate\Http\Client\RequestException
     * @throws \JsonException
     */
    public function it_can_update_email_status_of_local_user_when_email_event_delivered_found(): void
    {
        $localUser = LocalUser::create([
            'group' => config('cyclos.internal-names.groups.professional'),
            'email' => 'fake@somoney.test',
            'customValues' => '{}',
            'step' => config('registration.steps.professional.slug_id.membership'),
        ]);
        $validationUser = ValidationUser::create([
            'local_user_id' => $localUser->id,
            'group' => $localUser->group,
            'email' => $localUser->email,
        ]);
        Http::fake([
            $this->basePath . '/*' => Http::response([
                'items' => [
                    [
                        'event' => 'delivered',
                        'recipient' => $localUser['email'],
                        'message' => [
                            'headers' => [
                                'message-id' => 'fkopzekfopkzeionhfoznehuifhzeih',
                                'subject' => 'Random subject',
                            ],
                        ],
                        'timestamp' => now()->timestamp,
                    ],
                ],
                'paging' => [],
            ]),
        ]);
        app(MailgunService::class)->updateUserEmailStatus($validationUser);
        $this->assertDatabaseHas(
            (new ValidationUser())->getTable(),
            [
                'local_user_id' => $validationUser->local_user_id,
                'email_status' => ValidationUser::NOT_OPENED,
                'created_at' => $validationUser->created_at,
            ]
        );
    }

    /**
     * @test
     * @throws \Illuminate\Http\Client\RequestException
     * @throws \JsonException
     */
    public function it_can_update_email_status_of_cyclos_user_when_email_event_delivered_found(): void
    {
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        $validationUser = ValidationUser::create([
            'cyclos_id' => $cyclosUserData['id'],
            'group' => $cyclosUserData['group']['internalName'],
            'email' => $cyclosUserData['email'],
        ]);
        Http::fake([
            $this->basePath . '/*' => Http::response([
                'items' => [
                    [
                        'event' => 'delivered',
                        'recipient' => $cyclosUserData['email'],
                        'message' => [
                            'headers' => [
                                'message-id' => 'fkopzekfopkzeionhfoznehuifhzeih',
                                'subject' => 'Random subject',
                            ],
                        ],
                        'timestamp' => now()->timestamp,
                    ],
                ],
                'paging' => [],
            ]),
        ]);
        app(MailgunService::class)->updateUserEmailStatus($validationUser);
        $this->assertDatabaseHas(
            (new ValidationUser())->getTable(),
            [
                'cyclos_id' => $validationUser->cyclos_id,
                'email_status' => ValidationUser::NOT_OPENED,
                'created_at' => $validationUser->created_at,
            ]
        );
    }

    /**
     * @test
     * @throws \Illuminate\Http\Client\RequestException
     * @throws \JsonException
     */
    public function it_cant_update_email_status_of_local_user_when_no_email_event_found(): void
    {
        $localUser = LocalUser::create([
            'group' => config('cyclos.internal-names.groups.professional'),
            'email' => 'fake@somoney.test',
            'customValues' => '{}',
            'step' => config('registration.steps.professional.slug_id.membership'),
        ]);
        $validationUser = ValidationUser::create([
            'local_user_id' => $localUser->id,
            'group' => $localUser->group,
            'email' => $localUser->email,
        ]);
        Http::fake([
            $this->basePath . '/*' => Http::response([
                'items' => [],
                'paging' => [],
            ]),
        ]);
        app(MailgunService::class)->updateUserEmailStatus($validationUser);
        $this->assertDatabaseHas(
            (new ValidationUser())->getTable(),
            [
                'local_user_id' => $validationUser->local_user_id,
                'email_status' => ValidationUser::NO_EMAIL,
                'created_at' => $validationUser->created_at,
            ]
        );
    }

    /**
     * @test
     * @throws \Illuminate\Http\Client\RequestException
     * @throws \JsonException
     */
    public function it_cant_update_email_status_of_cyclos_user_when_no_email_event_found(): void
    {
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        $validationUser = ValidationUser::create([
            'cyclos_id' => $cyclosUserData['id'],
            'group' => $cyclosUserData['group']['internalName'],
            'email' => $cyclosUserData['email'],
        ]);
        Http::fake([
            $this->basePath . '/*' => Http::response([
                'items' => [],
                'paging' => [],
            ]),
        ]);
        app(MailgunService::class)->updateUserEmailStatus($validationUser);
        $this->assertDatabaseHas(
            (new ValidationUser())->getTable(),
            [
                'cyclos_id' => $validationUser->cyclos_id,
                'email_status' => ValidationUser::NO_EMAIL,
                'created_at' => $validationUser->created_at,
            ]
        );
    }

    /**
     * @test
     * @throws \Illuminate\Http\Client\RequestException
     * @throws \JsonException
     */
    public function it_can_update_email_status_of_local_user_when_email_event_click_link_found(): void
    {
        $localUser = LocalUser::create([
            'group' => config('cyclos.internal-names.groups.professional'),
            'email' => 'fake@somoney.test',
            'customValues' => '{}',
            'step' => config('registration.steps.professional.slug_id.membership'),
        ]);
        $validationUser = ValidationUser::create([
            'local_user_id' => $localUser->id,
            'group' => $localUser->group,
            'email' => $localUser->email,
        ]);
        Http::fake([
            $this->basePath . '/*' => Http::response([
                'items' => [
                    [
                        'event' => 'clicked',
                        'recipient' => $localUser['email'],
                        'message' => [
                            'headers' => [
                                'message-id' => 'fkopzekfopkzeionhfoznehuifhzeih',
                                'subject' => config('somoney.content.email.subject.register'),
                            ],
                        ],
                        'timestamp' => now()->timestamp,
                        'url' => 'https://somoney.pro',
                    ],
                ],
                'paging' => [],
            ]),
        ]);
        app(MailgunService::class)->updateUserEmailStatus($validationUser);
        $this->assertDatabaseHas(
            (new ValidationUser())->getTable(),
            [
                'local_user_id' => $validationUser->local_user_id,
                'email_status' => ValidationUser::CLICKED,
                'created_at' => $validationUser->created_at,
            ]
        );
    }

    /**
     * @test
     * @throws \Illuminate\Http\Client\RequestException
     * @throws \JsonException
     */
    public function it_can_update_email_status_of_cyclos_user_when_event_click_link_found(): void
    {
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        $validationUser = ValidationUser::create([
            'cyclos_id' => $cyclosUserData['id'],
            'group' => $cyclosUserData['group']['internalName'],
            'email' => $cyclosUserData['email'],
        ]);
        Http::fake([
            $this->basePath . '/*' => Http::response([
                'items' => [
                    [
                        'event' => 'clicked',
                        'recipient' => $cyclosUserData['email'],
                        'message' => [
                            'headers' => [
                                'message-id' => 'fkopzekfopkzeionhfoznehuifhzeih',
                                'subject' => config('somoney.content.email.subject.register'),
                            ],
                        ],
                        'timestamp' => now()->timestamp,
                        'url' => 'https://somoney.pro',
                    ],
                ],
                'paging' => [],
            ]),
        ]);
        app(MailgunService::class)->updateUserEmailStatus($validationUser);
        $this->assertDatabaseHas(
            (new ValidationUser())->getTable(),
            [
                'cyclos_id' => $validationUser->cyclos_id,
                'email_status' => ValidationUser::CLICKED,
                'created_at' => $validationUser->created_at,
            ]
        );
    }
}
