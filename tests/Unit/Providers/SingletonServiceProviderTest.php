<?php

namespace Tests\Unit\Providers;

use App\Services\Cyclos\TransactionService;
use Tests\TestCase;

class SingletonServiceProviderTest extends TestCase
{
    /** @test */
    public function it_cant_create_new_instance_of_service_transaction(): void
    {
        self::assertSame(app(TransactionService::class), app(TransactionService::class));
        self::assertNotSame(app(TransactionService::class), new TransactionService());
    }
}
