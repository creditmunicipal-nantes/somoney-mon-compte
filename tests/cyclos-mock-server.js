const express = require('express');
const app = express();
const fs = require('fs');
const bodyParser = require('body-parser');
let lastPayment = 0;
const getJsonFileResponse = filePath => {
    return JSON.parse(fs.readFileSync(__dirname + '/fixtures/mock-responses/cyclos/' + filePath));
};
// middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.route(/^\/somoneyapp\/api\/auth\/session$/).post((req, res) => {
    let user = Buffer.from(req.headers.authorization.replace(/Basic\s/, ''), 'base64').toString();
    if (user.match(/^pro/)) {
        res.json(getJsonFileResponse('auth/post-session-professional.json'));
    } else {
        if (user.match(/ter/)) {
            res.json(getJsonFileResponse('auth/post-session-individual-ter.json'));
        } else {
            res.json(getJsonFileResponse('auth/post-session-individual.json'));
        }
    }
});
//individual user
app.route(/^\/somoneyapp\/api\/users\/4036691751515103080$/).get((req, res) => {
    res.json(getJsonFileResponse('users/get-individual.json'));
});
app.route(/^\/somoneyapp\/api\/users\/4036691751515103080\/data-for-edit$/).get((req, res) => {
    res.json(getJsonFileResponse('users/get-data-for-edit-individual.json'));
});

//individual ter user
app.route(/^\/somoneyapp\/api\/users\/4036693601572265899$/).get((req, res) => {
    res.json(getJsonFileResponse('users/get-individual-ter.json'));
});
app.route(/^\/somoneyapp\/api\/users\/4036693601572265899\/data-for-edit$/).get((req, res) => {
    res.json(getJsonFileResponse('users/get-data-for-edit-individual-ter.json'));
});

//professional user
app.route(/^\/somoneyapp\/api\/users\/4036691750709796712$/).get((req, res) => {
    res.json(getJsonFileResponse('users/get-professional.json'));
});
app.route(/^\/somoneyapp\/api\/users\/4036691750709796712\/data-for-edit$/).get((req, res) => {
    res.json(getJsonFileResponse('users/get-data-for-edit-professional.json'));
});

// both users
app.route(/^\/somoneyapp\/api\/\d+\/accounts$/).get((req, res) => {
    let balance = 66.98 - lastPayment;
    res.json([
        {
            'id': '4036691743998910312',
            'type': {'internalName': 'individual'},
            'status': {'balance': balance, 'creditLimit': '0.00', 'upperCreditLimit': '2000.00'}
        }
    ]);
    lastPayment = 0;
});
app.route(/^\/somoneyapp\/api\/\d+\/phones$/).get((req, res) => {
    res.json(
        [
            {
                'id': '4036691715813187432',
                'name': 'phone',
                'number': '03 99 99 99 99',
                'type': 'landLine',
                'normalizedNumber': '+33399999999',
                'verified': false,
                'enabledForSms': false,
                'hidden': false
            }
        ]
    );
});
app.route(/^\/somoneyapp\/api\/self\/transactions$/).get((req, res) => {
    res.set({
        'X-Page-Size': '1000',
        'X-Current-Page': '0',
        'X-Page-Count': '1',
        'X-Has-Next-Page': 'false'
    });
if ('kinds' in req.query) {
    if (req.query.kinds === 'scheduledPayment') {
        res.set({
            'x-total-count': '1'
            });
        res.json([
            {
                'id': '4036698068875124584',
                'kind': 'scheduledPayment',
                'date': '2018-12-28T16:48:50.976+01:00',
                'amount': '-1.64',
                'relatedKind': 'user',
                'relatedUser': {
                    'id': '4036691779969261416',
                    'display': 'Particulier TEST',
                    'shortDisplay': '896174'
                },
                'type': {
                    'id': '4036693600766959464',
                    'name': 'Paiement à un particulier',
                    'internalName': 'individual.toIndividual',
                    'from': {
                        'id': '4036693601035394920',
                        'name': 'Compte de particulier',
                        'internalName': 'individual'
                    },
                    'to': {
                        'id': '4036693601035394920',
                        'name': 'Compte de particulier',
                        'internalName': 'individual'
                    }
                },
                'currency': 'mlc',
                'description': 'Auto Testing transfer',
                'installmentCount': 1,
                'processedInstallments': 0,
                'scheduledPaymentStatus': 'open'
                }
        ]);
    } else if (req.query.kinds === 'recurringPayment') {
        res.set({
            'x-total-count': '1'
            });
        res.json([
            {
                'id': '4036698149405761384',
                'kind': 'recurringPayment',
                'date': '2018-06-14T17:10:13.033+02:00',
                'amount': '-1.00',
                'relatedKind': 'user',
                'relatedUser': {
                    'id': '4036691762789392232',
                    'display': 'Green Firm',
                    'shortDisplay': '187986',
                    'image': {
                        'id': '4036693468965150568',
                        'name': 'Green Firm',
                        'contentType': 'image/png',
                        'length': 5995,
                        'url': 'https://cyclos.nantes.somoney.pro/api/images/content/ErrtUqcvOgUwVyxICtohgqyVnMkpXM9GPeyq38do8d8A4yBhDJreDKHLIG1MrWSR_260x260.png',
                        'width': 260,
                        'height': 260
                    }
                },
                'type': {
                    'id': '4036693601840701288',
                    'name': 'Paiement à un professionnel',
                    'internalName': 'individual.toProfessional',
                    'from': {
                        'id': '4036693601035394920',
                        'name': 'Compte de particulier',
                        'internalName': 'individual'
                    },
                    'to': {
                        'id': '4036693600230088552',
                        'name': 'Compte de professionnel',
                        'internalName': 'professional'
                    }
                },
                'currency': 'mlc',
                'description': 'Paiement en Heol récurrent',
                'nextOccurrenceDate': '2019-05-10T00:00:00.000+02:00',
                'recurringPaymentStatus': 'open'
                }
        ]);
    } else {
        res.json(getJsonFileResponse('transactions.json'));
    }
}
});
app.route(/^\/somoneyapp\/api\/transactions\/\d+$/).get((req, res) => {
    res.json(
        {
            'id': '4036696735019343720',
            'display': '4036696735019343720',
            'kind': 'recurringPayment',
            'date': '2019-03-08T12:08:37.056+01:00',
            'amount': '1.00',
            'fromKind': 'user',
            'fromUser': {
                'id': '4036691751515103080',
                'display': 'Bruce Wayne',
                'shortDisplay': '964501',
                'image': {
                    'id': '4036693469233586024',
                    'name': 'Capture d’écran 2018-04-25 à 08.33.53',
                    'contentType': 'image/png',
                    'length': 260908,
                    'url': 'https://cyclos.nantes.somoney.pro/somoneyapp/api/images/content/FYgqFhHdq8xLsLPTSjBKzYdsXXW1xlvGEZykyzAaM04ZvxuesIL8KaI4XfymaTFg_345x397.png',
                    'width': 345,
                    'height': 397
                }
            },
            'toKind': 'user',
            'toUser': {'id': '4036691762789392232', 'display': 'Green Firm', 'shortDisplay': '187986'},
            'type': {
                'id': '4036693601840701288',
                'name': 'Paiement à un professionnel',
                'internalName': 'individual.toProfessional',
                'from': {'id': '4036693601035394920', 'name': 'Compte de particulier', 'internalName': 'individual'},
                'to': {'id': '4036693600230088552', 'name': 'Compte de professionnel', 'internalName': 'professional'}
            },
            'currency': {
                'id': '4036693601572265832',
                'name': 'MLC',
                'internalName': 'mlc',
                'symbol': 'MLC',
                'suffix': 'MLC',
                'decimalDigits': 2
            },
            'description': 'Paiement en Eusko',
            'channel': {'id': '4036693600766959464', 'name': 'Services internet', 'internalName': 'webServices'},
            'usersWhichCanAddToContacts': 'none',
            'usersWhichCanViewProfile': 'both',
            'by': {'display': 'Administration', 'shortDisplay': 'Administration'},
            'received': false,
            'authorizations': [],
            'recurringPaymentStatus': 'open',
            'recurringPaymentPermissions': {'cancel': true},
            'nextOccurrenceDate': '2019-04-06T01:00:00.000+02:00',
            'occurrenceInterval': {'amount': 7, 'field': 'days'},
            'occurrencesCount': 5,
            'occurrences': [
                {
                    'id': '4036696633550741352',
                    'number': 1,
                    'date': '2019-03-09T00:38:23.858+01:00',
                    'amount': '1.00',
                    'status': 'processed',
                    'transferId': '4036696633550741352'
            }, {
                'id': '4036696629255774056',
                'number': 2,
                'date': '2019-03-16T00:38:24.474+01:00',
                'amount': '1.00',
                'status': 'processed',
                'transferId': '4036696629255774056'
            }, {
                'id': '4036696647509385064',
                'number': 3,
                'date': '2019-03-23T00:38:23.933+01:00',
                'amount': '1.00',
                'status': 'processed',
                'transferId': '4036696647509385064'
            }, {
                'id': '4036696663078641512',
                'number': 4,
                'date': '2019-03-30T00:38:24.628+01:00',
                'amount': '1.00',
                'status': 'processed',
                'transferId': '4036696663078641512'
            }
            ]
        }
    );
});
app.route(/^\/somoneyapp\/api\/users\/data-for-new$/).get((req, res) => {
    res.json(getJsonFileResponse('users/get-data-for-new.json'));
});
app.route(/^\/somoneyapp\/api\/users$/).get((req, res) => {
    res.set({
        'x-current-page': '0',
        'content-type': 'application/json;charset=UTF-8',
        'x-page-size': '40',
        'x-has-next-page': 'false',
        'x-total-count': '0',
        'x-page-count': '0'
    });
if ('profileFields' in req.query) {
    if (req.query.profileFields.match(/email:register-test@mail.com/)
        || req.query.profileFields.match(/phone:\+33399999999/)
        || req.query.profileFields.match(/apikey:/)) {
        res.json([]);
    } else if (req.query.profileFields.match(/email:part-bis@mail.com/)) {
        res.set({
            'X-Total-Count': '1',
            'X-Page-Count': '1',
            'X-Has-Next-Page': 'false'
            });
        res.json(getJsonFileResponse('users/get-email-part-bis-at-mail-com.json'));
    } else if (req.query.profileFields.match(/email:pro@mail.com/)) {
        res.set({
            'X-Total-Count': '1',
            'X-Page-Count': '1',
            'X-Has-Next-Page': 'false'
            });
        res.json(getJsonFileResponse('users/get-email-pro-at-mail-com.json'));
    } else if (req.query.profileFields.match(/email:part@mail.com/)) {
        res.set({
            'X-Total-Count': '1',
            'X-Page-Count': '1',
            'X-Has-Next-Page': 'false'
            });
        res.json(getJsonFileResponse('users/get-email-part-at-mail-com.json'));
    } else if (req.query.profileFields.match(/email:pro-bis@mail.com/)) {
        res.set({
            'X-Total-Count': '1',
            'X-Page-Count': '1',
            'X-Has-Next-Page': 'false'
            });
        res.json(getJsonFileResponse('users/get-email-pro-bis-at-mail-com.json'));
    }
} else {
    res.sendStatus(404);
}
});
app.route(/^\/somoneyapp\/api\/users$/).post((req, res) => {
    res.status(201);
    if (req.body.group === 'individual') {
        res.json(getJsonFileResponse('users/post-individual.json'));
    } else {
        res.json(getJsonFileResponse('users/post-professional.json'));
    }
});
app.route(/^\/somoneyapp\/api\/users\/\d+$/).put((req, res) => {
    res.sendStatus(204);
});

app.route(/^\/somoneyapp\/api\/users\/\d+$/).get((req, res) => {
    res.sendStatus(204);
});
app.route(/^\/somoneyapp\/api\/phones\/\d+$/).delete((req, res) => {
    res.sendStatus(204);
});
app.route(/^\/somoneyapp\/api\/\d+\/phones\/data-for-new$/).get((req, res) => {
    res.json(getJsonFileResponse('phones/get-data-for-new.json'));
});
app.route(/^\/somoneyapp\/api\/\d+\/phones$/).post((req, res) => {
    res.sendStatus(201);
});
app.route(/^\/somoneyapp\/api\/system\/payments$/).post((req, res) => {
    res.status(201);
    let json = getJsonFileResponse('payments/post-system.json');
    json.amount = req.body.amount + '.00';
    res.json(json);
});
app.route(/^\/somoneyapp\/api\/self\/payments$/).post((req, res) => {
    res.status(201);
    let json = getJsonFileResponse('payments/post-professional-to-professional.json');
    json.amount = req.body.amount + '.00';
    lastPayment = req.body.amount;
    res.json(json);
});
app.route(/^\/somoneyapp\/api\/\d+\/token-types$/).get((req, res) => {
    res.status(200);
    let json = getJsonFileResponse('users/get-user-token-types.json');
    res.json(json);
});
app.route(/^\/somoneyapp\/api\/\d+\/tokens\/\d+$/).get((req, res) => {
    res.status(200);
    let json = getJsonFileResponse('users/get-user-token-qr-code.json');
    res.json(json);
});


app.route(/^\/somoneyapp\/api\/4036693601572265899\/passwords\/login$/).get((req, res) => {
    res.status(200);
    let json = {
        'date': '2020-06-23T17:19:11.401+02:00',
        'status': 'reset',
        'type': {
            'id': '4036693601572265832',
            'name': 'Mot de passe de connexion',
            'internalName': 'login',
            'global': true,
            'mode': 'manual',
            'description': 'Veuillez entrer votre mot de passe ci-dessous. Ce mot de passe est utilisé pour vous connecter au système. Le mot de passe doit contenir entre 4 et 12 caractères.'
        },
        'requireOldPasswordForChange': false,
        'permissions': {
            'change': true,
            'changeGenerated': false,
            'generate': false,
            'allowGeneration': false,
            'disable': false,
            'enable': false,
            'resetGenerated': false,
            'resetAndSend': true,
            'unblock': false
        }
    };
    res.json(json);
});


app.route(/^\/somoneyapp\/api\/\d+\/passwords\/login$/).get((req, res) => {
    res.status(200);
    let json = {
        'date': '2020-06-23T17:19:11.401+02:00',
        'status': 'active',
        'type': {
            'id': '4036693601572265832',
            'name': 'Mot de passe de connexion',
            'internalName': 'login',
            'global': true,
            'mode': 'manual',
            'description': 'Veuillez entrer votre mot de passe ci-dessous. Ce mot de passe est utilisé pour vous connecter au système. Le mot de passe doit contenir entre 4 et 12 caractères.'
        },
        'requireOldPasswordForChange': false,
        'permissions': {
            'change': true,
            'changeGenerated': false,
            'generate': false,
            'allowGeneration': false,
            'disable': false,
            'enable': false,
            'resetGenerated': false,
            'resetAndSend': true,
            'unblock': false
        }
    };
    res.json(json);
});

app.route('/').get((req, res) => {
    res.send('Welcome to node mock server of Cyclos');
});

app.listen(3000, () => {});
