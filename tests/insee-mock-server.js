const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fs = require('fs');
const getJsonFileResponse = filePath => {
    return JSON.parse(fs.readFileSync(__dirname + '/fixtures/mock-responses/insee/' + filePath));
};
// middlewares
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.route(/^\/token$/).post((req, res) => {
    res.json({
        'access_token': 'ea3a4b69-b100-3a01-8597-4fadca110881',
        'scope': 'am_application_scope default',
        'token_type': 'Bearer',
        'expires_in': 108430
    });
});

app.route(/^\/entreprises\/sirene\/V3\/siret$/).post((req, res) => {
    res.json(getJsonFileResponse('post-siret.json'));
});

app.route('/').get((req, res) => {
    res.send('Welcome to node mock server of Insee API');
});
app.listen(3002, () => {});
