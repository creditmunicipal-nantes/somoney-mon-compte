<?php

use Tests\CreatesApplication;

require __DIR__ . '/../bootstrap/autoload.php';
(new class () {
    use CreatesApplication;
})->createApplication();
