const express = require('express');
const app = express();
const bodyParser = require('body-parser');
// middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.route(/^\/maps\/api\/geocode\/json$/).get((req, res) => {
    res.json({
        'results': [
            {
                'address_components': [
                    {'long_name': '12', 'short_name': '12', 'types': ['street_number']},
                    {'long_name': 'Mail Louise Bourgeois', 'short_name': 'Mail Louise Bourgeois', 'types': ['route']},
                    {'long_name': 'Rennes', 'short_name': 'Rennes', 'types': ['locality', 'political']}, {
                        'long_name': 'Ille-et-Vilaine',
                        'short_name': 'Ille-et-Vilaine',
                        'types': ['administrative_area_level_2', 'political']
                }, {
                    'long_name': 'Bretagne',
                    'short_name': 'Bretagne',
                    'types': ['administrative_area_level_1', 'political']
                }, {'long_name': 'France', 'short_name': 'FR', 'types': ['country', 'political']},
                    {'long_name': '35000', 'short_name': '35000', 'types': ['postal_code']}
                ],
                'formatted_address': '12 Mail Louise Bourgeois, 35000 Rennes, France',
                'geometry': {
                    'bounds': {
                        'northeast': {'lat': 48.1026655, 'lng': - 1.6622196},
                        'southwest': {'lat': 48.1022684, 'lng': - 1.6628393}
                    },
                    'location': {'lat': 48.1024058, 'lng': - 1.6624865},
                    'location_type': 'ROOFTOP',
                    'viewport': {
                        'northeast': {'lat': 48.1038159302915, 'lng': - 1.661180469708498},
                        'southwest': {'lat': 48.1011179697085, 'lng': - 1.663878430291502}
                    }
                },
                'place_id': 'ChIJTe_ZEa7fDkgR1wXJ0W2fXoQ',
                'types': ['premise']
        }
        ], 'status': 'OK'
    });
});

app.route('/').get((req, res) => {
    res.send('Welcome to node mock server of Google Geocoding');
});
app.listen(3001, () => {});
