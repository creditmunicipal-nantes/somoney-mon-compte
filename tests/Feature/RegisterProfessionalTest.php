<?php

namespace Tests\Feature;

use App\Jobs\SendEmailValidationCreation;
use App\Models\LocalUser;
use App\Models\Transaction;
use App\Services\SystemPayService;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Bus;
use Mockery;
use Symfony\Component\HttpFoundation\Response;

class RegisterProfessionalTest extends RegisterTest
{
    /** @test */
    public function it_can_register_professional_basic(): void
    {
        Bus::fake();
        self::setRegisterOptions('professional');
        $this->get(route('register'))->assertStatus(Response::HTTP_OK);
        $this->registerProfessionalAccountInformation();
        $this->registerProfessionalSocietyInformation('register.professional.informations.address');
        $this->registerProfessionalAddressInformation('register.professional.cgu');
        $this->registerProfessionalCgu();
        $this->get(route('register.professional.success'))->assertStatus(Response::HTTP_OK);
        $localUser = LocalUser::first();
        Bus::assertDispatched(
            SendEmailValidationCreation::class,
            fn(SendEmailValidationCreation $job) => $job->localUser->id === $localUser->id
        );
    }

    /** @test */
    public function it_can_register_professional_with_justification_documents(): void
    {
        Bus::fake();
        self::setRegisterOptions('professional', ['justification_documents']);
        $this->get(route('register'))->assertStatus(Response::HTTP_OK);
        $this->registerProfessionalAccountInformation();
        $this->registerProfessionalSocietyInformation('register.professional.informations.address');
        $this->registerProfessionalAddressInformation('register.professional.files');
        $this->registerProfessionalFiles();
        $this->registerProfessionalCgu();
        $this->get(route('register.professional.success'))->assertStatus(Response::HTTP_OK);
        $localUser = LocalUser::first();
        Bus::assertDispatched(
            SendEmailValidationCreation::class,
            fn(SendEmailValidationCreation $job) => $job->localUser->id === $localUser->id
        );
    }

    /** @test */
    public function it_can_register_professional_with_justification_documents_and_category(): void
    {
        Bus::fake();
        self::setRegisterOptions('professional', ['justification_documents', 'category']);
        $this->get(route('register'))->assertStatus(Response::HTTP_OK);
        $this->registerProfessionalAccountInformation(['category' => "4036693596471992168"]);
        $this->registerProfessionalSocietyInformation('register.professional.informations.address');
        $this->registerProfessionalAddressInformation('register.professional.files');
        $this->registerProfessionalFiles();
        $this->registerProfessionalCgu();
        $this->get(route('register.professional.success'))->assertStatus(Response::HTTP_OK);
        $localUser = LocalUser::first();
        Bus::assertDispatched(
            SendEmailValidationCreation::class,
            fn(SendEmailValidationCreation $job) => $job->localUser->id === $localUser->id
        );
    }

    /** @test */
    public function it_can_register_professional_with_category(): void
    {
        Bus::fake();
        self::setRegisterOptions('professional', ['category']);
        $this->get(route('register'))->assertStatus(Response::HTTP_OK);
        $this->registerProfessionalAccountInformation(['category' => "4036693596471992168"]);
        $this->registerProfessionalSocietyInformation('register.professional.informations.address');
        $this->registerProfessionalAddressInformation('register.professional.cgu');
        $this->registerProfessionalCgu();
        $this->get(route('register.professional.success'))->assertStatus(Response::HTTP_OK);
        $localUser = LocalUser::first();
        Bus::assertDispatched(
            SendEmailValidationCreation::class,
            fn(SendEmailValidationCreation $job) => $job->localUser->id === $localUser->id
        );
    }

    /** @test */
    public function it_can_register_professional_with_siren(): void
    {
        Bus::fake();
        self::setRegisterOptions('professional', ['siren']);
        $this->get(route('register'))->assertStatus(Response::HTTP_OK);
        $this->registerProfessionalAccountInformation();
        $this->registerProfessionalSocietyInformation('register.professional.cgu', ['siren' => "264400557"]);
        $this->registerProfessionalCgu();
        $this->get(route('register.professional.success'))->assertStatus(Response::HTTP_OK);
        $localUser = LocalUser::first();
        Bus::assertDispatched(
            SendEmailValidationCreation::class,
            fn(SendEmailValidationCreation $job) => $job->localUser->id === $localUser->id
        );
    }

    /** @test */
    public function it_can_register_professional_with_justification_documents_and_siren(): void
    {
        Bus::fake();
        self::setRegisterOptions('professional', ['justification_documents', 'siren']);
        $this->get(route('register'))->assertStatus(Response::HTTP_OK);
        $this->registerProfessionalAccountInformation(['category' => "4036693596471992168"]);
        $this->registerProfessionalSocietyInformation('register.professional.files', ['siren' => "264400557"]);
        $this->registerProfessionalFiles();
        $this->registerProfessionalCgu();
        $this->get(route('register.professional.success'))->assertStatus(Response::HTTP_OK);
        $localUser = LocalUser::first();
        Bus::assertDispatched(
            SendEmailValidationCreation::class,
            fn(SendEmailValidationCreation $job) => $job->localUser->id === $localUser->id
        );
    }

    /** @test */
    public function it_can_register_professional_with_category_and_siren(): void
    {
        Bus::fake();
        self::setRegisterOptions('professional', ['category', 'siren']);
        $this->get(route('register'))->assertStatus(Response::HTTP_OK);
        $this->registerProfessionalAccountInformation(['category' => "4036693596471992168"]);
        $this->registerProfessionalSocietyInformation('register.professional.cgu', ['siren' => "264400557"]);
        $this->registerProfessionalCgu();
        $this->get(route('register.professional.success'))->assertStatus(Response::HTTP_OK);
        $localUser = LocalUser::first();
        Bus::assertDispatched(
            SendEmailValidationCreation::class,
            fn(SendEmailValidationCreation $job) => $job->localUser->id === $localUser->id
        );
    }

    /** @test */
    public function it_can_register_professional_with_justification_documents_and_category_and_siren(): void
    {
        Bus::fake();
        self::setRegisterOptions('professional', ['justification_documents', 'category', 'siren']);
        $this->get(route('register'))->assertStatus(Response::HTTP_OK);
        $this->registerProfessionalAccountInformation(['category' => "4036693596471992168"]);
        $this->registerProfessionalSocietyInformation('register.professional.files', ['siren' => "264400557"]);
        $this->registerProfessionalFiles();
        $this->registerProfessionalCgu();
        $this->get(route('register.professional.success'))->assertStatus(Response::HTTP_OK);
        $localUser = LocalUser::first();
        Bus::assertDispatched(
            SendEmailValidationCreation::class,
            fn(SendEmailValidationCreation $job) => $job->localUser->id === $localUser->id
        );
    }

    /** Make test on account information step of professional user */
    protected function registerProfessionalAccountInformation(array $extraData = []): void
    {
        $availablePasswordCharacters = 'a-zA-Z0-9\`\@\!"\#\$\%\&\'\(\)\*\+,\-\.\/:\;\<=\>\?\[\\\\\]\^_\{\}~';
        do {
            $password = self::$faker->password(12);
        } while (! preg_match('/^[' . $availablePasswordCharacters . ']+$/', $password));
        $this->get(route('register.professional.informations.account'))->assertStatus(Response::HTTP_OK);
        $this->post(route("register.professional.informations.account.post"), array_merge(
            [
                'email' => 'register-test@mail.com',
                'password' => $password,
            ],
            $extraData
        ))->assertRedirect(route('register.professional.informations.society'));
    }

    /** Make test on society information step of professional user */
    protected function registerProfessionalSocietyInformation(string $redirectRoute, array $extraData = []): void
    {
        $this->get(route('register.professional.informations.society'))->assertStatus(Response::HTTP_OK);
        $this->post(route('register.professional.informations.society.post'), array_merge(
            [
                "gender" => "4036693600498524008",
                "firstname" => self::$faker->firstName(),
                "lastname" => self::$faker->lastName(),
                "birthday" => "01/01/2000",
                "phone" => "02 02 02 02 02",
                "employeenumber" => "4036693603182878568",
                "name" => self::$faker->company(),
                "mailpro" => self::$faker->companyEmail(),
                "tpeconfig" => false,
            ],
            $extraData
        ))->assertRedirect(route($redirectRoute));
    }

    /** Make test on address information step of professional user */
    protected function registerProfessionalAddressInformation(string $redirectRoute): void
    {
        $this->get(route('register.professional.informations.address'))->assertStatus(Response::HTTP_OK);
        $this->post(route('register.professional.informations.address.post'), [
            "gmap" => [
                "addressLine1" => self::$faker->streetAddress(),
                "zip" => self::$faker->postcode(),
                "city" => self::$faker->city(),
                "latitude" => 47.2118126,
                "longitude" => -1.5508543999999,
            ],
            "address_gmap" => "Random address, Random City, France",
        ])->assertRedirect(route($redirectRoute));
    }

    /** Make test on files step of professional user */
    protected function registerProfessionalFiles(): void
    {
        $this->get(route('register.professional.files'))->assertStatus(Response::HTTP_OK);
        $this->post(route('register.professional.files.post'), [
            "idattachment_path" => "id-file.pdf",
            "idattachment" => UploadedFile::fake()->create('fake-id.pdf', 512),
            "ribattachment_path" => "rib-file.pdf",
            "ribattachment" => UploadedFile::fake()->create('fake-rib.pdf', 512),
            "organisationattachment_path" => "company-document-file.pdf",
            "organisationattachment" => UploadedFile::fake()->create('fake-company-document.pdf', 512),
            "organisationattachmenttype" => "4036693609625329512",
        ])->assertRedirect(route('register.professional.cgu'));
    }

    /** Make test on cgu step of professional user */
    protected function registerProfessionalCgu(): void
    {
        $this->get(route('register.professional.cgu'))->assertStatus(Response::HTTP_OK);
        $this->post(route('register.professional.cgu.post'), [
            "cgu" => true,
            "newsletter" => self::$faker->boolean(),
            "commercialinfo" => self::$faker->boolean(),
            "optinnewsletter" => self::$faker->boolean(),
            "membershipsonantaise" => true,
        ])->assertRedirect(route('register.professional.success'));
    }

    /** Make test on credit step of professional user */
    protected function registerProfessionalSystemPayCredit(): void
    {
        config()->set(['somoney.credit.membership.professional' => 20]);
        $donateAmount = random_int(0, 50);
        $this->mock(SystemPayService::class)
            ->shouldReceive('createPayment')
            ->once()
            ->with(
                ($donateAmount + config('somoney.credit.membership.professional')) * 100,
                session('registration.user_id'),
                Transaction::CONTEXT_REGISTRATION,
                Mockery::type(Transaction::class)
            )
            ->andReturn("random-string");

        $this->get(route('register.professional.credit.membership'))->assertStatus(Response::HTTP_OK);
        $this->post(route('register.professional.credit.membership.post'))
            ->assertRedirect(route('register.professional.credit.donate'));
        $this->post(route('register.professional.credit.donate.post'), [
            'amount' => $donateAmount,
        ])->assertRedirect(route('register.professional.credit.payment'));
    }
}
