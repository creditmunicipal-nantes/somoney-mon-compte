<?php

namespace Tests\Feature;

use App\Jobs\SendEmailNeedMembership;
use App\Models\LocalUser;
use App\Services\Cyclos\AuthService;
use App\Services\Cyclos\User\PasswordService;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Facades\Bus;
use Illuminate\Auth\AuthenticationException;

class LoginTest extends BaseFeatureTest
{
    /** @test */
    public function it_can_login_when_user_is_individual()
    {
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'User Part',
            'email' => 'part@mail.com',
            'password' => 'this-is-a-fake-password',
            'group' => 'individual',
        ];
        $this->mock(AuthService::class)
            ->shouldReceive('login')
            ->once()
            ->with([
                'login' => $cyclosUserData['email'],
                'password' => $cyclosUserData['password'],
            ])
            ->andReturn([
                'user' => [
                    'id' => $cyclosUserData['id'],
                ],
            ]);
        $mockUserService = $this->mock(UserService::class);
        $mockUserService->shouldReceive('get')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn([
                "id" => $cyclosUserData['id'],
                "display" => $cyclosUserData['name'],
                "name" => $cyclosUserData['name'],
                "email" => $cyclosUserData['email'],
                "group" => [
                    "internalName" => $cyclosUserData['group'],
                ],
                'customValues' => [
                    [
                        "booleanValue" => true,
                        "field" => [
                            "internalName" => "validatedemail",
                            "type" => "boolean",
                        ],
                    ],
                ],
            ]);
        $mockUserService->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn([
                "status" => ["balance" => "4426.29"],
            ]);
        $this->mock(PasswordService::class)
            ->shouldReceive('get')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn([
                'status' => PasswordService::STATUS_ACTIVE,
            ]);
        $this->post(route('auth.postLogin'), [
            'login' => $cyclosUserData['email'],
            'password' => $cyclosUserData['password'],
        ])
            ->assertRedirect(route('home'))
            ->assertSessionHas('auth');
    }

    /** @test */
    public function it_can_login_when_user_is_professional()
    {
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'User Pro',
            'email' => 'pro@mail.com',
            'password' => 'this-is-a-fake-password',
            'group' => 'professional',
        ];
        $this->mock(AuthService::class)
            ->shouldReceive('login')
            ->once()
            ->with([
                'login' => $cyclosUserData['email'],
                'password' => $cyclosUserData['password'],
            ])
            ->andReturn([
                'user' => [
                    'id' => $cyclosUserData['id'],
                ],
            ]);
        $mockUserService = $this->mock(UserService::class);
        $mockUserService->shouldReceive('get')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn([
                "id" => $cyclosUserData['id'],
                "display" => $cyclosUserData['name'],
                "name" => $cyclosUserData['name'],
                "email" => $cyclosUserData['email'],
                "group" => [
                    "internalName" => $cyclosUserData['group'],
                ],
                'customValues' => [
                    [
                        "booleanValue" => true,
                        "field" => [
                            "internalName" => "validatedemail",
                            "type" => "boolean",
                        ],
                    ],
                ],
            ]);
        $mockUserService->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn([
                "status" => ["balance" => "4426.29"],
            ]);
        $this->mock(PasswordService::class)
            ->shouldReceive('get')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn([
                'status' => PasswordService::STATUS_ACTIVE,
            ]);
        $this->post(route('auth.postLogin'), [
            'login' => $cyclosUserData['email'],
            'password' => $cyclosUserData['password'],
        ])
            ->assertRedirect(route('home'))
            ->assertSessionHas('auth');
    }

    /** @test */
    public function it_can_login_when_user_is_admin()
    {
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'Admin',
            'email' => 'admin@mail.com',
            'password' => 'this-is-a-fake-password',
            'group' => 'admin_network',
        ];
        $this->mock(AuthService::class)
            ->shouldReceive('login')
            ->once()
            ->with([
                'login' => $cyclosUserData['email'],
                'password' => $cyclosUserData['password'],
            ])
            ->andReturn([
                'user' => [
                    'id' => $cyclosUserData['id'],
                ],
            ]);
        $mockUserService = $this->mock(UserService::class);
        $mockUserService->shouldReceive('get')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn([
                "id" => $cyclosUserData['id'],
                "display" => $cyclosUserData['name'],
                "name" => $cyclosUserData['name'],
                "email" => $cyclosUserData['email'],
                "group" => [
                    "internalName" => $cyclosUserData['group'],
                ],
            ]);
        $mockUserService->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn([
                "status" => ["balance" => "4426.29"],
            ]);
        $this->mock(PasswordService::class)
            ->shouldReceive('get')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn([
                'status' => PasswordService::STATUS_ACTIVE,
            ]);
        $this->post(route('auth.postLogin'), [
            'login' => $cyclosUserData['email'],
            'password' => $cyclosUserData['password'],
        ])
            ->assertRedirect(route('admin.home'))
            ->assertSessionHas('auth');
    }

    /** @test */
    public function it_cant_login_when_registration_membership_needed()
    {
        Bus::fake();
        config()->set('somoney.available.services.membership', true);
        $cyclosUserData = [
            'email' => 'new.user@mail.com',
            'password' => 'this-is-a-fake-password',
            'group' => config('cyclos.internal-names.groups.individual'),
        ];
        $stubLocalUser = LocalUser::create([
            'name' => 'Individual User',
            'group' => $cyclosUserData['group'],
            'email' => $cyclosUserData['email'],
            'customValues' => [],
            'step' => config("registration.steps.{$cyclosUserData['group']}.slug_id.membership"),
        ]);

        $this->mock(AuthService::class)
            ->shouldReceive('login')
            ->once()
            ->with([
                'login' => $cyclosUserData['email'],
                'password' => $cyclosUserData['password'],
            ])
            ->andThrowExceptions([new AuthenticationException()]);
        $this->from(route('auth.login'))
            ->post(route('auth.postLogin'), [
                'login' => $cyclosUserData['email'],
                'password' => $cyclosUserData['password'],
            ])
            ->assertRedirect(route('auth.login'))
            ->assertSessionHas('registerNeedMembership');
        Bus::assertDispatched(
            SendEmailNeedMembership::class,
            fn(SendEmailNeedMembership $job) => $job->localUser->id == $stubLocalUser->id
        );
    }
}
