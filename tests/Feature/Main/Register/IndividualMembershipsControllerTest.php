<?php

namespace Tests\Feature\Main\Register;

use App\Models\LocalUser;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Services\RegistrationConfigLoaderService;
use App\Services\SystemPayService;
use Illuminate\Support\Facades\Bus;
use Illuminate\Foundation\Testing\RefreshDatabase;
use LemonWay\ApiResponse;
use LemonWay\LemonWayAPI;
use Mockery;
use SimpleXMLElement;
use Tests\TestCase;

class IndividualMembershipsControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware();
        $this->withoutMix();
    }

    /** @test */
    public function it_can_go_to_donate_step_with_fixed_membership_amount(): void
    {
        Bus::fake();
        config()->set('somoney.available.services.membership', true);
        config()->set('registration.available.individual.donate', true);
        config()->set('somoney.credit.membership.individual', 6);
        app(RegistrationConfigLoaderService::class)->load();
        $localUser = LocalUser::create([
            'group' => config('cyclos.internal-names.groups.individual'),
            'email' => 'fake@somoney.test',
            'customValues' => '{}',
        ]);
        $this->withSession(['registration' => ['user_id' => $localUser->id]])
            ->post(route('register.individual.credit.membership.post'), ['amount' => 6])
            ->assertRedirect(route('register.individual.credit.donate'));
        $this->assertDatabaseHas(app(Transaction::class)->getTable(), [
            'context' => Transaction::CONTEXT_REGISTRATION,
            'transaction_type' => Transaction::REGISTER_TYPE_ADHERE,
            'local_user_id' => $localUser->id,
            'amount' => 600,
        ]);
    }

    /** @test */
    public function it_can_go_to_payment_step_without_donate_step_with_system_pay(): void
    {
        Bus::fake();
        config()->set('somoney.available.services.membership', true);
        config()->set('registration.available.individual.donate', false);
        config()->set('somoney.credit.membership.individual', 6);
        config()->set('somoney.payment-gateway', Transaction::TYPE_SYSTEM_PAY);
        app(RegistrationConfigLoaderService::class)->load();
        $localUser = LocalUser::create([
            'group' => config('cyclos.internal-names.groups.individual'),
            'email' => 'fake@somoney.test',
            'customValues' => '{}',
        ]);
        $this->mock(SystemPayService::class)
            ->shouldReceive('createPayment')
            ->once()
            ->with(
                600,
                $localUser->id,
                Transaction::CONTEXT_REGISTRATION,
                Mockery::type(Transaction::class)
            )
            ->andReturn("random-string");
        $this->withSession(['registration' => ['user_id' => $localUser->id]])
            ->post(route('register.individual.credit.membership.post'), ['amount' => 6])
            ->assertRedirect(route('register.individual.credit.payment'));
    }

    /** @test */
    public function it_can_go_to_payment_step_without_donate_step_with_lemonway(): void
    {
        config()->set('somoney.available.services.membership', true);
        config()->set('registration.available.individual.donate', false);
        config()->set('somoney.credit.membership.individual', 6);
        config()->set('somoney.payment-gateway', Transaction::TYPE_LEMONWAY);
        config()->set('lemonway.webkit.url', 'https://dummy-webkit.lemonway.fr');
        config()->set('lemonway.vad_account', 'ECM');
        config()->set('somoney.available.services.register', true);
        config()->set('somoney.available.services.lemonway-v2', false);
        app(RegistrationConfigLoaderService::class)->load();
        $lemonwayWallet = Wallet::create(['lw_id' => '1517319177dyag3sfb', 'cyclos_user_id' => 'somoney']);
        $localUser = LocalUser::create([
            'group' => config('cyclos.internal-names.groups.individual'),
            'email' => 'fake@somoney.test',
            'customValues' => [
                'lemonwayId' => $lemonwayWallet->lw_id,
            ],
        ]);
        $this->mock(LemonWayAPI::class)
            ->shouldReceive('MoneyInWebInit')
            ->once()
            ->with(Mockery::on(fn(array $data) => is_string($data['wallet'])
                && $data['amountTot'] === '6.00'
                && $data['amountCom'] === '0.00'
                && is_string($data['wkToken'])
                && $data['returnUrl'] === route('lemonway.success', ['context' => Transaction::CONTEXT_REGISTRATION])
                && $data['errorUrl'] === route('lemonway.error', ['context' => Transaction::CONTEXT_REGISTRATION])
                && $data['cancelUrl'] === route('lemonway.cancel', ['context' => Transaction::CONTEXT_REGISTRATION])
                && $data['autoCommission'] === 0))
            ->andReturn(new ApiResponse(new SimpleXMLElement(<<<XML
<MoneyInWebInitResult>
    <MONEYINWEB>
        <TOKEN>2517404164b8dL9f910OVxb5ypt4PFAKiI10</TOKEN>
        <ID>30276</ID>
    </MONEYINWEB>
</MoneyInWebInitResult>
XML
            )));
        $this->withSession(['registration' => ['user_id' => $localUser->id]])
            ->post(route('register.individual.credit.membership.post'), ['amount' => 6])
            ->assertRedirect(route('register.individual.credit.payment'));
        $this->assertDatabaseHas(app(Transaction::class)->getTable(), [
            'deleted_at' => null,
            'amount' => '600',
            'source_wallet_id' => null,
            'target_wallet_id' => $lemonwayWallet->id,
            'depends_on_id' => null,
            'lw_id' => '30276',
            'lw_token' => '2517404164b8dL9f910OVxb5ypt4PFAKiI10',
            'lw_executed_at' => null,
            'status' => Transaction::STATUS_PROCESSING,
            'failure_reason' => null,
            'type' => Transaction::TYPE_LEMONWAY,
            'cyclos_user_id' => null,
            'local_user_id' => $localUser->id,
            'context' => Transaction::CONTEXT_REGISTRATION,
            'parent_id' => null,
            'transaction_type' => Transaction::REGISTER_TYPE_ADHERE,
        ]);
    }

    /** @test */
    public function it_can_set_amount_on_membership_registration_step(): void
    {
        Bus::fake();
        config()->set('somoney.available.services.membership', true);
        config()->set('registration.available.individual.donate', true);
        config()->set('somoney.credit.membership.min.individual', 1);
        app(RegistrationConfigLoaderService::class)->load();
        $localUser = LocalUser::create([
            'group' => config('cyclos.internal-names.groups.individual'),
            'email' => 'fake@somoney.test',
            'customValues' => '{}',
            'amount' => 6,
        ]);
        $this->withSession(['registration' => ['user_id' => $localUser->id]])
            ->post(route('register.individual.credit.membership.post'), ['amount' => 6])
            ->assertRedirect(route('register.individual.credit.donate'));
        $this->assertDatabaseHas(app(Transaction::class)->getTable(), [
            'context' => Transaction::CONTEXT_REGISTRATION,
            'transaction_type' => Transaction::REGISTER_TYPE_ADHERE,
            'local_user_id' => $localUser->id,
            'amount' => 600,
        ]);
    }

    /** @test */
    public function it_cant_set_amount_under_min_of_membership_amount(): void
    {
        Bus::fake();
        config()->set('somoney.available.services.membership', true);
        config()->set('registration.available.individual.donate', true);
        config()->set('somoney.credit.membership.min.individual', 6);
        app(RegistrationConfigLoaderService::class)->load();
        $localUser = LocalUser::create([
            'group' => config('cyclos.internal-names.groups.individual'),
            'email' => 'fake@somoney.test',
            'customValues' => '{}',
        ]);
        $this->withSession(['registration' => ['user_id' => $localUser->id]])
            ->from(route('register.individual.credit.membership'))
            ->post(route('register.individual.credit.membership.post'), ['amount' => 4])
            ->assertSessionHasErrors()
            ->assertRedirect(route('register.individual.credit.membership'));
        $this->assertDatabaseMissing(app(Transaction::class)->getTable(), [
            'context' => Transaction::CONTEXT_REGISTRATION,
            'transaction_type' => Transaction::REGISTER_TYPE_ADHERE,
            'local_user_id' => $localUser->id,
        ]);
    }

    /** @test */
    public function it_can_go_to_payment_step_from_donate_step_with_system_pay(): void
    {
        Bus::fake();
        config()->set('somoney.available.services.membership', true);
        config()->set('registration.available.individual.donate', true);
        config()->set('somoney.payment-gateway', Transaction::TYPE_SYSTEM_PAY);
        app(RegistrationConfigLoaderService::class)->load();
        $localUser = LocalUser::create([
            'group' => config('cyclos.internal-names.groups.individual'),
            'email' => 'fake@somoney.test',
            'customValues' => '{}',
        ]);
        $transaction = Transaction::create([
            'amount' => '600',
            'status' => Transaction::STATUS_PROCESSING,
            'type' => Transaction::TYPE_SYSTEM_PAY,
            'local_user_id' => $localUser->id,
            'context' => Transaction::CONTEXT_REGISTRATION,
            'transaction_type' => Transaction::REGISTER_TYPE_ADHERE,
        ]);
        $this->mock(SystemPayService::class)
            ->shouldReceive('createPayment')
            ->once()
            ->with(
                1600,
                $localUser->id,
                Transaction::CONTEXT_REGISTRATION,
                Mockery::type(Transaction::class)
            )
            ->andReturn("random-string");
        $this->withSession([
            'registration' => [
                'user_id' => $localUser->id,
                'parent_transaction_id' => $transaction->id,
            ],
        ])
            ->post(route('register.individual.credit.donate.post'), ['amount' => 10])
            ->assertRedirect(route('register.individual.credit.payment'));
    }

    /** @test */
    public function it_can_go_to_payment_step_from_donate_step_with_lemonway(): void
    {
        config()->set('somoney.available.services.membership', true);
        config()->set('registration.available.individual.donate', true);
        config()->set('somoney.payment-gateway', Transaction::TYPE_LEMONWAY);
        config()->set('lemonway.webkit.url', 'https://dummy-webkit.lemonway.fr');
        config()->set('lemonway.vad_account', 'ECM');
        config()->set('somoney.available.services.register', true);
        config()->set('somoney.available.services.lemonway-v2', false);
        app(RegistrationConfigLoaderService::class)->load();
        $lemonwayWallet = Wallet::create(['lw_id' => '1517319177dyag3sfb', 'cyclos_user_id' => 'somoney']);
        $localUser = LocalUser::create([
            'group' => config('cyclos.internal-names.groups.individual'),
            'email' => 'fake@somoney.test',
            'customValues' => [
                'lemonwayId' => $lemonwayWallet->lw_id,
            ],
        ]);
        $transaction = Transaction::create([
            'amount' => '600',
            'target_wallet_id' => $lemonwayWallet->id,
            'lw_id' => '30276',
            'lw_token' => '2517404164b8dL9f910OVxb5ypt4PFAKiI10',
            'status' => Transaction::STATUS_PROCESSING,
            'type' => Transaction::TYPE_LEMONWAY,
            'local_user_id' => $localUser->id,
            'context' => Transaction::CONTEXT_REGISTRATION,
            'transaction_type' => Transaction::REGISTER_TYPE_ADHERE,
        ]);
        $this->mock(LemonWayAPI::class)
            ->shouldReceive('MoneyInWebInit')
            ->once()
            ->with(Mockery::on(fn(array $data) => is_string($data['wallet'])
                && $data['amountTot'] === '16.00'
                && $data['amountCom'] === '0.00'
                && is_string($data['wkToken'])
                && $data['returnUrl'] === route('lemonway.success', ['context' => Transaction::CONTEXT_REGISTRATION])
                && $data['errorUrl'] === route('lemonway.error', ['context' => Transaction::CONTEXT_REGISTRATION])
                && $data['cancelUrl'] === route('lemonway.cancel', ['context' => Transaction::CONTEXT_REGISTRATION])
                && $data['autoCommission'] === 0))
            ->andReturn(new ApiResponse(new SimpleXMLElement(<<<XML
<MoneyInWebInitResult>
    <MONEYINWEB>
        <TOKEN>2517404164b8dL9f910OVxb5ypt4PFAKiI10</TOKEN>
        <ID>30276</ID>
    </MONEYINWEB>
</MoneyInWebInitResult>
XML
            )));
        $this->withSession([
            'registration' => [
                'user_id' => $localUser->id,
                'parent_transaction_id' => $transaction->id,
            ],
        ])
            ->post(route('register.individual.credit.donate.post'), ['amount' => 10])
            ->assertRedirect(route('register.individual.credit.payment'));
    }

    /** @test */
    public function it_been_redirect_when_payment_token_not_found_with_system_pay(): void
    {
        Bus::fake();
        config()->set('somoney.available.services.membership', true);
        config()->set('somoney.payment-gateway', Transaction::TYPE_SYSTEM_PAY);
        app(RegistrationConfigLoaderService::class)->load();
        $localUser = LocalUser::create([
            'group' => config('cyclos.internal-names.groups.individual'),
            'email' => 'fake@somoney.test',
            'customValues' => '{}',
        ]);
        $this->from(route('register.individual.credit.membership'))
            ->withSession([
                'registration' => [
                    'user_id' => $localUser->id,
                ],
            ])
            ->get(route('register.individual.credit.payment'))
            ->assertRedirect(route('register.individual.credit.membership'))
            ->assertSessionHas('error');
    }
}
