<?php

namespace Tests\Feature\Main\Operations;

use Tests\Feature\BaseFeatureTest;

class TransferMobileTest extends BaseFeatureTest
{
    /** @test */
    public function it_can_notify_when_mobile_payment_on_cyclos()
    {
        $this->json(
            'POST',
            route('api.webhooks.notification.payment.mobile'),
            [
                'transferId' => '4036696735019343720',
            ],
            [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'Authorize-Key' => config('cyclos.api.authorize-key'),
            ]
        )->assertStatus(200);
    }
}
