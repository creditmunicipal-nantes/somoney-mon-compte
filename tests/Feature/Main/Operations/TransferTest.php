<?php

namespace Tests\Feature\Main\Operations;

use App\Jobs\SendEmailsTransfer;
use App\Services\Cyclos\TransactionService;
use App\Services\Cyclos\TransferService;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Bus;
use Mockery;
use Tests\Feature\BaseFeatureTest;

class TransferTest extends BaseFeatureTest
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        Bus::fake();
    }

    /** @test */
    public function it_can_transfer_to_an_individual_user_when_is_individual(): void
    {
        $cyclosApplicantUserData = [
            'id' => '4036691750709796714',
            'name' => 'Individual Applicant User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.individual')],
            'email' => 'part@fake.test',
        ];
        $cyclosRecipientUserData = [
            'id' => '4036691750709796716',
            'name' => 'Individual Recipient User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.individual')],
            'email' => 'part.bis@fake.test',
        ];
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('get')
            ->once()
            ->with($cyclosApplicantUserData['id'])
            ->andReturn($cyclosApplicantUserData);
        $userServiceMock->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosApplicantUserData['id'])
            ->andReturn([]);
        $userServiceMock->shouldReceive('search')
            ->once()
            ->with(Mockery::on(fn(array $data) => $data['profileFields']['email']
                === $cyclosRecipientUserData['email']))
            ->andReturn(new LengthAwarePaginator([$cyclosRecipientUserData], 1, 1, 1));
        $this->mock(TransactionService::class)->shouldReceive('store')
            ->once()
            ->with(Mockery::on(fn(array $data) => $data['toUser'] === $cyclosRecipientUserData['id']
                && $data['amount'] === '55.00'
                && $data['description'] === 'Testing transfer individual to individual'
                && $data['scheduleDate']->format('d/m/Y') === today()->format('d/m/Y')
                && $data['type'] === 'individual.toIndividual'))
            ->andReturn();
        $this->withSession(['auth' => ['user' => $cyclosApplicantUserData]])
            ->from(route('operations.transfer.index'))
            ->post(route('operations.transfer.store'), [
                'profil' => 'individual',
                'amount' => '55.00',
                'individual_email' => $cyclosRecipientUserData['email'],
                'date' => today()->format('d/m/Y'),
                'comment' => "Testing transfer individual to individual",
            ])->assertRedirect(route('operations.transfer.index'))
            ->assertSessionHasNoErrors();
        Bus::assertDispatched(
            fn(SendEmailsTransfer $job) => $job->transferCanal === TransferService::WEB_TRANSFER
                && $job->applicantEmail === $cyclosApplicantUserData['email']
                && $job->recipientEmail === $cyclosRecipientUserData['email']
                && is_array($job->transferDetails)
                && $job->isScheduled === false
        );
    }

    /** @test */
    public function it_can_transfer_to_an_professional_user_when_is_individual(): void
    {
        $cyclosApplicantUserData = [
            'id' => '4036691750709796714',
            'name' => 'Individual Applicant User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.individual')],
            'email' => 'part@fake.test',
        ];
        $cyclosRecipientUserData = [
            'id' => '4036691750709796716',
            'name' => 'Professional Recipient User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'pro@fake.test',
        ];
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('get')
            ->once()
            ->with($cyclosApplicantUserData['id'])
            ->andReturn($cyclosApplicantUserData);
        $userServiceMock->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosApplicantUserData['id'])
            ->andReturn([]);
        $userServiceMock->shouldReceive('search')
            ->once()
            ->with(Mockery::on(fn(array $data) => $data['profileFields']['email']
                === $cyclosRecipientUserData['email']))
            ->andReturn(new LengthAwarePaginator([$cyclosRecipientUserData], 1, 1, 1));
        $this->mock(TransactionService::class)->shouldReceive('store')
            ->once()
            ->with(Mockery::on(fn(array $data) => $data['toUser'] === $cyclosRecipientUserData['id']
                && $data['amount'] === '55.00'
                && $data['description'] === 'Testing transfer individual to professional'
                && $data['scheduleDate']->format('d/m/Y') === today()->format('d/m/Y')
                && $data['type'] === 'individual.toProfessional'))
            ->andReturn();
        $this->withSession(['auth' => ['user' => $cyclosApplicantUserData]])
            ->from(route('operations.transfer.index'))
            ->post(route('operations.transfer.store'), [
                'profil' => 'professional',
                'amount' => '55.00',
                'professional_email' => $cyclosRecipientUserData['email'],
                'date' => today()->format('d/m/Y'),
                'comment' => "Testing transfer individual to professional",
            ])->assertRedirect(route('operations.transfer.index'))
            ->assertSessionHasNoErrors();
        Bus::assertDispatched(
            fn(SendEmailsTransfer $job) => $job->transferCanal === TransferService::WEB_TRANSFER
                && $job->applicantEmail === $cyclosApplicantUserData['email']
                && $job->recipientEmail === $cyclosRecipientUserData['email']
                && is_array($job->transferDetails)
                && $job->isScheduled === false
        );
    }

    /** @test */
    public function it_can_transfer_to_an_professional_user_when_is_professional(): void
    {
        $cyclosApplicantUserData = [
            'id' => '4036691750709796714',
            'name' => 'Professional Applicant User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'pro@fake.test',
        ];
        $cyclosRecipientUserData = [
            'id' => '4036691750709796716',
            'name' => 'Professional Recipient User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'pro@fake.test',
        ];
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('get')
            ->once()
            ->with($cyclosApplicantUserData['id'])
            ->andReturn($cyclosApplicantUserData);
        $userServiceMock->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosApplicantUserData['id'])
            ->andReturn([]);
        $userServiceMock->shouldReceive('search')
            ->once()
            ->with(Mockery::on(fn(array $data) => $data['profileFields']['email']
                === $cyclosRecipientUserData['email']))
            ->andReturn(new LengthAwarePaginator([$cyclosRecipientUserData], 1, 1, 1));
        $this->mock(TransactionService::class)->shouldReceive('store')
            ->once()
            ->with(Mockery::on(fn(array $data) => $data['toUser'] === $cyclosRecipientUserData['id']
                && $data['amount'] === '55.00'
                && $data['description'] === 'Testing transfer professional to professional'
                && $data['scheduleDate']->format('d/m/Y') === today()->format('d/m/Y')
                && $data['type'] === 'professional.toProfessional'))
            ->andReturn();
        $this->withSession(['auth' => ['user' => $cyclosApplicantUserData]])
            ->from(route('operations.transfer.index'))
            ->post(route('operations.transfer.store'), [
                'profil' => 'professional',
                'amount' => '55.00',
                'professional_email' => $cyclosRecipientUserData['email'],
                'date' => today()->format('d/m/Y'),
                'comment' => "Testing transfer professional to professional",
            ])->assertRedirect(route('operations.transfer.index'))
            ->assertSessionHasNoErrors();
        Bus::assertDispatched(
            fn(SendEmailsTransfer $job) => $job->transferCanal === TransferService::WEB_TRANSFER
                && $job->applicantEmail === $cyclosApplicantUserData['email']
                && $job->recipientEmail === $cyclosRecipientUserData['email']
                && is_array($job->transferDetails)
                && $job->isScheduled === false
        );
    }

    /** @test */
    public function it_can_transfer_to_an_individual_user_when_is_professional(): void
    {
        $cyclosApplicantUserData = [
            'id' => '4036691750709796714',
            'name' => 'Professional Applicant User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'pro@fake.test',
        ];
        $cyclosRecipientUserData = [
            'id' => '4036691750709796716',
            'name' => 'Professional Recipient User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.individual')],
            'email' => 'part@fake.test',
        ];
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('get')
            ->once()
            ->with($cyclosApplicantUserData['id'])
            ->andReturn($cyclosApplicantUserData);
        $userServiceMock->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosApplicantUserData['id'])
            ->andReturn([]);
        $userServiceMock->shouldReceive('search')
            ->once()
            ->with(Mockery::on(fn(array $data) => $data['profileFields']['email']
                === $cyclosRecipientUserData['email']))
            ->andReturn(new LengthAwarePaginator([$cyclosRecipientUserData], 1, 1, 1));
        $this->mock(TransactionService::class)->shouldReceive('store')
            ->once()
            ->with(Mockery::on(fn(array $data) => $data['toUser'] === $cyclosRecipientUserData['id']
                && $data['amount'] === '55.00'
                && $data['description'] === 'Testing transfer professional to individual'
                && $data['scheduleDate']->format('d/m/Y') === today()->format('d/m/Y')
                && $data['type'] === 'professional.toIndividual'))
            ->andReturn();
        $this->withSession(['auth' => ['user' => $cyclosApplicantUserData]])
            ->from(route('operations.transfer.index'))
            ->post(route('operations.transfer.store'), [
                'profil' => 'individual',
                'amount' => '55.00',
                'individual_email' => $cyclosRecipientUserData['email'],
                'date' => today()->format('d/m/Y'),
                'comment' => "Testing transfer professional to individual",
            ])->assertRedirect(route('operations.transfer.index'))
            ->assertSessionHasNoErrors();
        Bus::assertDispatched(
            fn(SendEmailsTransfer $job) => $job->transferCanal === TransferService::WEB_TRANSFER
                && $job->applicantEmail === $cyclosApplicantUserData['email']
                && $job->recipientEmail === $cyclosRecipientUserData['email']
                && is_array($job->transferDetails)
                && $job->isScheduled === false
        );
    }

    /** @test */
    public function it_can_transfer_with_recurrence(): void
    {
        $cyclosApplicantUserData = [
            'id' => '4036691750709796714',
            'name' => 'Professional Applicant User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'pro@fake.test',
        ];
        $cyclosRecipientUserData = [
            'id' => '4036691750709796716',
            'name' => 'Professional Recipient User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.individual')],
            'email' => 'part@fake.test',
        ];
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('get')
            ->once()
            ->with($cyclosApplicantUserData['id'])
            ->andReturn($cyclosApplicantUserData);
        $userServiceMock->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosApplicantUserData['id'])
            ->andReturn([]);
        $userServiceMock->shouldReceive('search')
            ->once()
            ->with(Mockery::on(fn(array $data) => $data['profileFields']['email']
                === $cyclosRecipientUserData['email']))
            ->andReturn(new LengthAwarePaginator([$cyclosRecipientUserData], 1, 1, 1));
        $this->mock(TransactionService::class)->shouldReceive('store')
            ->once()
            ->with(Mockery::on(fn(array $data) => $data['toUser'] === $cyclosRecipientUserData['id']
                && $data['amount'] === '5.00'
                && $data['description'] === 'Testing transfer recurrent'
                && $data['scheduleDate']->format('d/m/Y') === Carbon::tomorrow()->format('d/m/Y')
                && $data['type'] === 'professional.toIndividual'))
            ->andReturn();
        $this->withSession(['auth' => ['user' => $cyclosApplicantUserData]])
            ->from(route('operations.transfer.index'))
            ->post(route('operations.transfer.store'), [
                'profil' => 'individual',
                'amount' => '5.00',
                'individual_email' => $cyclosRecipientUserData['email'],
                'date' => Carbon::tomorrow()->format('d/m/Y'),
                'comment' => "Testing transfer recurrent",
                'recurrent' => true,
                'frequency' => 'monthly',
                'date_end' => Carbon::tomorrow()->addMonths(6)->format('d/m/Y'),
            ])->assertRedirect(route('operations.transfer.index'))
            ->assertSessionHasNoErrors();
        Bus::assertDispatched(
            fn(SendEmailsTransfer $job) => is_array($job->transferDetails)
                && $job->transferDetails['recurrent'] === true
                && data_get($job->transferDetails, 'frequency') === __('monthly')
                && data_get($job->transferDetails, 'date_end') === Carbon::tomorrow()->addMonths(6)->format('d/m/Y')
        );
    }
}
