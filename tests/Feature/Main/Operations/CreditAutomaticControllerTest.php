<?php

namespace Tests\Feature\Main\Operations;

use App\Jobs\SendEmailConfirmCreditAutomatic;
use App\Models\Sepa;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Tests\TestCase;

class CreditAutomaticControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware();
        Bus::fake();
    }

    /** @test */
    public function it_can_store_automated_sepa_transfer(): void
    {
        config()->set('somoney.available.services.sepa-payment', true);
        config()->set('somoney.credit.auto.minimal', 30);
        config()->set('somoney.credit.auto.minimal-length', 6);
        $this->withSession(['auth' => ['user' => ['id' => 1]]])
            ->post(route('operations.credit.automatic.store'), [
                'gender' => 'm',
                'max_amount' => 50,
                'amount' => 30,
                'last_name' => 'NAME',
                'first_name' => 'First Name',
                'email' => 'test@fake.test',
                'company' => 'Bibendum',
                'iban' => 'FR5410096000407381914916P38',
                'duration_month' => 12,
            ])
            ->assertRedirect(route('home'));
        $this->assertDatabaseHas(app(Sepa::class)->getTable(), [
            "user_id" => 1,
            "amount" => 30,
            "gender" => "m",
            "last_name" => "NAME",
            "first_name" => "First Name",
            "email" => "test@fake.test",
            "company" => "Bibendum",
            "iban" => "FR5410096000407381914916P38",
            "duration_month" => 12,
        ]);
        Bus::assertDispatched(fn(SendEmailConfirmCreditAutomatic $job) => $job->sepaId === Sepa::first()->id);
    }

    /** @test */
    public function it_can_store_automated_sepa_transfers_when_no_company(): void
    {
        config()->set('somoney.available.services.sepa-payment', true);
        config()->set('somoney.credit.auto.minimal', 30);
        config()->set('somoney.credit.auto.minimal-length', 6);
        $this->withSession(['auth' => ['user' => ['id' => 1]]])
            ->post(route('operations.credit.automatic.store'), [
                'amount' => 50,
                'max_amount' => 100,
                'duration_month' => 12,
                'gender' => 'm',
                'last_name' => 'Dupont',
                'first_name' => 'Bob',
                'email' => 'test@fake.com',
                'iban' => 'FR1830003000701455199121F63',
            ])->assertRedirect(route('home'));
        $this->assertDatabaseHas(app(Sepa::class)->getTable(), [
            "user_id" => 1,
            "amount" => 50,
            "gender" => "m",
            "last_name" => "Dupont",
            "first_name" => "Bob",
            "email" => 'test@fake.com',
            "iban" => "FR1830003000701455199121F63",
            'company' => null,
            "duration_month" => 12,
        ]);
        Bus::assertDispatched(fn(SendEmailConfirmCreditAutomatic $job) => $job->sepaId === Sepa::first()->id);
    }
}
