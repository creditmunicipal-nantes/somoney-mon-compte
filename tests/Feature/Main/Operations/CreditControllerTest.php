<?php

namespace Tests\Feature\Main\Operations;

use App\Jobs\SendEmailSepaCreated;
use App\Models\Sepa;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Services\Cyclos\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use LemonWay\ApiResponse;
use LemonWay\LemonWayAPI;
use Lyra\Client as LyraClient;
use Mockery;
use SimpleXMLElement;
use Tests\TestCase;

class CreditControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutMiddleware();
        $this->withoutMix();
    }

    /** @test */
    public function it_can_credit_user_with_bank_card_on_lemonway_v1(): void
    {
        config()->set('somoney.available.services.sepa-payment', false);
        config()->set('somoney.available.services.lemonway-v2', false);
        config()->set('somoney.available.services.show-credit', true);
        config()->set('somoney.payment-gateway', Transaction::TYPE_LEMONWAY);
        config()->set('somoney.credit.simple.minimal', 10);
        config()->set('lemonway.webkit.url', 'https://dummy-webkit.lemonway.fr');
        config()->set('lemonway.vad_account', 'ECM');
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'Test',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        $lemonwayWallet = Wallet::create(['lw_id' => '1517319177dyag3sfb', 'cyclos_user_id' => 'somoney']);
        $this->mock(LemonWayAPI::class)
            ->shouldReceive('MoneyInWebInit')
            ->once()
            ->with(Mockery::on(fn(array $data) => is_string($data['wallet'])
                && $data['amountTot'] === '15.00'
                && $data['amountCom'] === '0.00'
                && is_string($data['wkToken'])
                && $data['returnUrl'] === route('lemonway.success', ['context' => Transaction::CONTEXT_OPERATION])
                && $data['errorUrl'] === route('lemonway.error', ['context' => Transaction::CONTEXT_OPERATION])
                && $data['cancelUrl'] === route('lemonway.cancel', ['context' => Transaction::CONTEXT_OPERATION])
                && $data['autoCommission'] === 0))
            ->andReturn(new ApiResponse(new SimpleXMLElement(<<<XML
<MoneyInWebInitResult>
    <MONEYINWEB>
        <TOKEN>2517404164b8dL9f910OVxb5ypt4PFAKiI10</TOKEN>
        <ID>30276</ID>
    </MONEYINWEB>
</MoneyInWebInitResult>
XML
            )));
        $this->withSession(['auth' => ['user' => $cyclosUserData],])
            ->post(route('operations.credit.store'), ['max_amount' => 30, 'amount' => 15, 'type' => 'cb'])
            ->assertSessionHas([
                'operations.initPaymentResponse' => 'https://dummy-webkit.lemonway.fr'
                    . '?moneyintoken=2517404164b8dL9f910OVxb5ypt4PFAKiI10&lang=fr',
            ])
            ->assertRedirect(route('operations.credit.payment.bank-card'));
        $this->assertDatabaseHas(app(Transaction::class)->getTable(), [
            'deleted_at' => null,
            'amount' => '1500',
            'source_wallet_id' => null,
            'target_wallet_id' => $lemonwayWallet->id,
            'depends_on_id' => null,
            'lw_id' => '30276',
            'lw_token' => '2517404164b8dL9f910OVxb5ypt4PFAKiI10',
            'lw_executed_at' => null,
            'status' => Transaction::STATUS_PROCESSING,
            'failure_reason' => null,
            'type' => Transaction::TYPE_LEMONWAY,
            'cyclos_user_id' => '4036691750709796712',
            'local_user_id' => null,
            'context' => Transaction::CONTEXT_OPERATION,
            'parent_id' => null,
            'transaction_type' => 'credit',
        ]);
    }

    /** @test */
    public function it_can_credit_user_with_bank_card_on_lemonway_v2(): void
    {
        config()->set('somoney.available.services.sepa-payment', false);
        config()->set('somoney.available.services.lemonway-v2', true);
        config()->set('somoney.available.services.show-credit', true);
        config()->set('somoney.payment-gateway', Transaction::TYPE_LEMONWAY);
        config()->set('somoney.credit.simple.minimal', 10);
        config()->set('lemonway.webkit.url', 'https://dummy-webkit.lemonway.fr');
        config()->set('lemonway.vad_account', 'ECM');
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'Test',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        Wallet::create(['lw_id' => '1517319177dyag3sfb', 'cyclos_user_id' => 'somoney']);
        $this->mock(LemonWayAPI::class)
            ->shouldReceive('MoneyInWebInit')
            ->once()
            ->with(Mockery::on(fn(array $data) => is_string($data['wallet'])
                && $data['amountTot'] === '15.00'
                && $data['amountCom'] === '0.00'
                && is_string($data['wkToken'])
                && $data['returnUrl'] === route('lemonway.success', ['context' => Transaction::CONTEXT_OPERATION])
                && $data['errorUrl'] === route('lemonway.error', ['context' => Transaction::CONTEXT_OPERATION])
                && $data['cancelUrl'] === route('lemonway.cancel', ['context' => Transaction::CONTEXT_OPERATION])
                && $data['autoCommission'] === 0))
            ->andReturn(new ApiResponse(new SimpleXMLElement(<<<XML
<MoneyInWebInitResult>
    <MONEYINWEB>
        <TOKEN>2517404164b8dL9f910OVxb5ypt4PFAKiI10</TOKEN>
        <ID>30276</ID>
    </MONEYINWEB>
</MoneyInWebInitResult>
XML
            )));
        $this->withSession(['auth' => ['user' => $cyclosUserData],])
            ->post(route('operations.credit.store'), ['max_amount' => 30, 'amount' => 15, 'type' => 'cb'])
            ->assertSessionHas([
                'operations.initPaymentResponse' => 'https://dummy-webkit.lemonway.fr'
                    . '?moneyintoken=2517404164b8dL9f910OVxb5ypt4PFAKiI10&lang=fr',
            ])
            ->assertRedirect(route('operations.credit.payment.bank-card'));
        $this->assertDatabaseHas(app(Transaction::class)->getTable(), [
            'deleted_at' => null,
            'amount' => '1500',
            'source_wallet_id' => null,
            'target_wallet_id' => null,
            'depends_on_id' => null,
            'lw_id' => '30276',
            'lw_token' => '2517404164b8dL9f910OVxb5ypt4PFAKiI10',
            'lw_executed_at' => null,
            'status' => Transaction::STATUS_PROCESSING,
            'failure_reason' => null,
            'type' => Transaction::TYPE_LEMONWAY,
            'cyclos_user_id' => '4036691750709796712',
            'local_user_id' => null,
            'context' => Transaction::CONTEXT_OPERATION,
            'parent_id' => null,
            'transaction_type' => 'credit',
        ]);
    }

    /** @test */
    public function it_can_credit_user_with_bank_card_on_system_pay(): void
    {
        config()->set('somoney.available.services.sepa-payment', false);
        config()->set('somoney.available.services.show-credit', true);
        config()->set('somoney.payment-gateway', Transaction::TYPE_SYSTEM_PAY);
        config()->set('somoney.credit.simple.minimal', 10);
        config()->set('system-pay.endpoint', 'https://dummy-api.systempay.fr');
        $cyclosUserData = [
            'id' => '4036691750709796712',
            'name' => 'Test',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'test@fake.test',
        ];
        $this->mock(LyraClient::class)
            ->shouldReceive('post')
            ->once()
            ->with('V4/Charge/CreatePayment', Mockery::on(fn(array $data) => $data['amount'] === 1500
                && $data['currency'] === 'EUR'
                && is_string($data['orderId'])
                && $data['customer'] === ["email" => 'test@fake.test']))
            ->andReturn(['answer' => ['formToken' => 'random-string']]);
        $this->mock(UserService::class)->shouldReceive('get')
            ->once()
            ->with($cyclosUserData['id'])
            ->andReturn($cyclosUserData);
        $this->withSession(['auth' => ['user' => $cyclosUserData]])
            ->post(route('operations.credit.store'), ['max_amount' => 30, 'amount' => 15, 'type' => 'cb'])
            ->assertSessionHas(['operations.initPaymentResponse' => 'random-string'])
            ->assertRedirect(route('operations.credit.payment.bank-card'));
        $this->assertDatabaseHas(app(Transaction::class)->getTable(), [
            'deleted_at' => null,
            'amount' => '1500',
            'source_wallet_id' => null,
            'target_wallet_id' => null,
            'depends_on_id' => null,
            'lw_id' => null,
            'lw_token' => null,
            'lw_executed_at' => null,
            'status' => Transaction::STATUS_PROCESSING,
            'failure_reason' => null,
            'type' => Transaction::TYPE_SYSTEM_PAY,
            'cyclos_user_id' => '4036691750709796712',
            'local_user_id' => null,
            'context' => Transaction::CONTEXT_OPERATION,
            'parent_id' => null,
            'transaction_type' => 'credit',
        ]);
    }

    /** @test */
    public function it_can_store_sepa_transfers(): void
    {
        Bus::fake();
        config()->set('somoney.available.services.sepa-payment', true);
        config()->set('somoney.credit.simple.minimal-sepa', 30);
        $this->withSession(['auth' => ['user' => ['id' => 1]]])
            ->post(route('operations.credit.payment.sepa.store'), [
                'max_amount' => 50,
                'amount' => 40,
                'gender' => 'm',
                'last_name' => 'Dupont',
                'first_name' => 'Bob',
                'email' => 'test@fake.com',
                'iban' => 'FR1830003000701455199121F63',
                'company' => 'Entreprise'
            ])
            ->assertRedirect(route('operations.historic.index'));
        $this->assertDatabaseHas(app(Sepa::class)->getTable(), [
            "user_id" => 1,
            "amount" => 40,
            "gender" => "m",
            "last_name" => "Dupont",
            "first_name" => "Bob",
            "email" => 'test@fake.com',
            "iban" => "FR1830003000701455199121F63",
            'company' => 'Entreprise',
        ]);
        Bus::assertDispatched(fn(SendEmailSepaCreated $job) => $job->sepaId === Sepa::first()->id);
    }

    /** @test */
    public function it_can_store_sepa_transfers_when_no_company(): void
    {
        Bus::fake();
        config()->set('somoney.available.services.sepa-payment', true);
        config()->set('somoney.credit.simple.minimal-sepa', 30);
        $this->withSession(['auth' => ['user' => ['id' => 1]]])
            ->post(route('operations.credit.payment.sepa.store'), [
                'max_amount' => 50,
                'amount' => 40,
                'gender' => 'm',
                'last_name' => 'Dupont',
                'first_name' => 'Bob',
                'email' => 'test@fake.com',
                'iban' => 'FR1830003000701455199121F63',
            ])
            ->assertRedirect(route('operations.historic.index'));
        $this->assertDatabaseHas(app(Sepa::class)->getTable(), [
            "user_id" => 1,
            "amount" => 40,
            "gender" => "m",
            "last_name" => "Dupont",
            "first_name" => "Bob",
            "email" => 'test@fake.com',
            "iban" => "FR1830003000701455199121F63",
            'company' => null,
        ]);
        Bus::assertDispatched(fn(SendEmailSepaCreated $job) => $job->sepaId === Sepa::first()->id);
    }
}
