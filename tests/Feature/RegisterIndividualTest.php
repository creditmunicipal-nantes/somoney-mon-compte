<?php

namespace Tests\Feature;

use App\Jobs\SendEmailValidationCreation;
use App\Models\LocalUser;
use App\Models\Transaction;
use App\Models\UserToken;
use App\Services\SystemPayService;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Bus;
use LemonWay\ApiResponse;
use LemonWay\LemonWayAPI;
use Mockery;
use SimpleXMLElement;
use Symfony\Component\HttpFoundation\Response;

class RegisterIndividualTest extends RegisterTest
{
    /** @test */
    public function it_can_register_individual_basic(): void
    {
        Bus::fake();
        self::setRegisterOptions('individual');
        config()->set('somoney.available.services.register', true);
        $this->get(route('register'))->assertStatus(Response::HTTP_OK);
        $this->registerIndividualAccountInformation();
        $this->registerIndividualPersonalInformation();
        $this->registerIndividualAddressInformation('register.individual.cgu');
        $this->registerIndividualCgu();
        $this->get(route('register.individual.success'))->assertStatus(Response::HTTP_OK);
        $localUser = LocalUser::first();
        Bus::assertDispatched(
            SendEmailValidationCreation::class,
            fn(SendEmailValidationCreation $job) => $job->localUser->id === $localUser->id
        );
    }

    /** @test */
    public function it_can_register_individual_wrong_character_password(): void
    {
        Bus::fake();
        self::setRegisterOptions('individual');
        config()->set('somoney.available.services.register', true);
        $this->get(route('register'))->assertStatus(Response::HTTP_OK);
        $this->registerIndividualAccountInformation(true, true);
    }

    /** @test */
    public function it_can_register_individual_account_exists(): void
    {
        Bus::fake();
        self::setRegisterOptions('individual');
        config()->set('somoney.available.services.register', true);
        $this->get(route('register'))->assertStatus(Response::HTTP_OK);
        $this->registerIndividualAccountInformation(true);
    }

    /** @test */
    public function it_can_register_individual_with_id_document(): void
    {
        Bus::fake();
        self::setRegisterOptions('individual', ['id_document']);
        config()->set('somoney.available.services.register', true);
        $this->get(route('register'))->assertStatus(Response::HTTP_OK);
        $this->registerIndividualAccountInformation();
        $this->registerIndividualPersonalInformation();
        $this->registerIndividualAddressInformation('register.individual.files');
        $this->registerIndividualFiles();
        $this->registerIndividualCgu();
        $this->get(route('register.individual.success'))->assertStatus(Response::HTTP_OK);
        $localUser = LocalUser::first();
        Bus::assertDispatched(
            SendEmailValidationCreation::class,
            fn(SendEmailValidationCreation $job) => $job->localUser->id === $localUser->id
        );
    }

    /** @test */
    public function it_cant_resume_registration_on_membership_step_when_link_with_token_already_call(): void
    {
        Bus::fake();
        config()->set('somoney.available.services.membership', true);
        self::setRegisterOptions('individual', []);

        $localUser = LocalUser::create([
            'name' => 'Jean-claude',
            'email' => 'jean.claude@test.fr',
            'step' => config('registration.steps.individual.slug_id.membership'),
            'group' => 'individual',
            'customValues' => [],
        ]);
        $token = UserToken::create([
            'user_id' => $localUser->id,
            'type' => UserToken::TYPE_REGISTRATION_NEED_MEMBERSHIP,
        ]);
        // Simulate first request
        $this->get(route('register.individual.need_membership', ['needMembershipToken' => $token->id]))
            ->assertStatus(Response::HTTP_FOUND);
        // Tested request
        $this->get(route('register.individual.need_membership', ['needMembershipToken' => $token->id]))
            ->assertStatus(Response::HTTP_FORBIDDEN)
            ->assertSee('Lien expiré');
    }

    /** Make test on account information step of individual user */
    protected function registerIndividualAccountInformation(
        bool $duplicateAccount = false,
        bool $wrongPassword = false
    ): void {
        $this->get(route('register.individual.informations.account'))->assertStatus(Response::HTTP_OK);
        $email = $duplicateAccount ? 'part@mail.com' : 'register-test@mail.com';
        $password = self::$faker->password(12);
        if ($wrongPassword) {
            $password .= '€';
        } else {
            $availablePasswordCharacters = 'a-zA-Z0-9\`\@\!"\#\$\%\&\'\(\)\*\+,\-\.\/:\;\<=\>\?\[\\\\\]\^_\{\}~';
            while (! preg_match('/^[' . $availablePasswordCharacters . ']+$/', $password)) {
                $password = self::$faker->password(12);
            }
        }
        $response = $this->post(route("register.individual.informations.account.post"), [
            'email' => $email,
            'password' => $password,
        ]);
        if ($duplicateAccount) {
            $response->assertRedirect(route('register.individual.informations.account'));
            self::assertNotNull(session('errors'));
            self::assertEquals(__('validation.unique_cyclos_email', [
                'attribute' => __('validation.attributes.email'),
            ]), session('errors')->get('email')[0]);
        } elseif ($wrongPassword) {
            $response->assertRedirect(route('register.individual.informations.account'));
            self::assertNotNull(session('errors'));
            self::assertEquals(__('validation.custom.password.regex'), session('errors')->get('password')[0]);
        } else {
            $response->assertRedirect(route('register.individual.informations.personal'));
        }
    }

    /** Make test on personal information step of individual user */
    protected function registerIndividualPersonalInformation(): void
    {
        $this->get(route('register.individual.informations.personal'))->assertStatus(Response::HTTP_OK);
        $this->post(route('register.individual.informations.personal.post'), [
            "gender" => "4036693600498524008",
            "firstname" => self::$faker->firstName(),
            "lastname" => self::$faker->lastName(),
            "birthday" => "01/01/2000",
            "phone" => "02 02 02 02 02",
        ])->assertRedirect(route('register.individual.informations.address'));
    }

    /**
     * Make test on address information step of individual user
     *
     * @test
     */
    protected function registerIndividualAddressInformation(string $redirectRoute): void
    {
        $this->get(route('register.individual.informations.address'))->assertStatus(Response::HTTP_OK);
        $this->post(route('register.individual.informations.address.post'), [
            "gmap" => [
                "addressLine1" => self::$faker->streetAddress(),
                "zip" => self::$faker->postcode(),
                "city" => self::$faker->city(),
                "latitude" => 47.2118126,
                "longitude" => -1.5508543999999,
            ],
            "address_gmap" => "Random address, Random City, France",
        ])->assertRedirect(route($redirectRoute));
    }

    /** Make test on cgu step of individual user */
    protected function registerIndividualCgu(): void
    {
        $this->get(route('register.individual.cgu'))->assertStatus(Response::HTTP_OK);
        $this->post(route('register.individual.cgu.post'), [
            "cgu" => true,
            "newsletter" => self::$faker->boolean(),
            "commercialinfo" => self::$faker->boolean(),
            "optinnewsletter" => self::$faker->boolean(),
            "membershipsonantaise" => true,
        ])->assertRedirect(route('register.individual.success'));
    }

    /** Make test on files step of individual user */
    protected function registerIndividualFiles(): void
    {
        $this->get(route('register.individual.files'))->assertStatus(Response::HTTP_OK);
        $this->post(route('register.individual.files.post'), [
            "idattachment_path" => "file-test.pdf",
            "idattachment" => UploadedFile::fake()->create('fake.pdf', 512),
        ])->assertRedirect(route('register.individual.cgu'));
    }

    /**
     * Make test on credit step of individual user
     *
     * @throws \Exception
     */
    protected function registerIndividualLemonwayCredit(): void
    {
        config()->set('somoney.credit.membership.individual', 5);
        $this->mock(LemonWayAPI::class)
            ->shouldReceive('MoneyInWebInit')
            ->once()
            ->with(Mockery::on(fn(array $data) => is_string($data['wallet'])
                && $data['amountTot'] === '15.00'
                && $data['amountCom'] === '0.00'
                && is_string($data['wkToken'])
                && $data['returnUrl'] === route('lemonway.success', ['context' => Transaction::CONTEXT_REGISTRATION])
                && $data['errorUrl'] === route('lemonway.error', ['context' => Transaction::CONTEXT_REGISTRATION])
                && $data['cancelUrl'] === route('lemonway.cancel', ['context' => Transaction::CONTEXT_REGISTRATION])
                && $data['autoCommission'] === 0))
            ->andReturn(new ApiResponse(new SimpleXMLElement(<<<XML
<MoneyInWebInitResult>
    <MONEYINWEB>
        <TOKEN>2517404164b8dL9f910OVxb5ypt4PFAKiI10</TOKEN>
        <ID>30276</ID>
    </MONEYINWEB>
</MoneyInWebInitResult>
XML
            )));
        $localUserId = LocalUser::latest()->first()->id;
        $this->withSession(['registration' => ['user_id' => $localUserId]])
            ->get(route('register.individual.credit.membership'))
            ->assertStatus(Response::HTTP_OK);
        $this->withSession(['registration' => ['user_id' => $localUserId]])
            ->post(route('register.individual.credit.membership.post'))
            ->assertRedirect(route('register.individual.credit.donate'));
        $this->withSession([
            'registration' => [
                'user_id' => $localUserId,
                'parent_transaction_id' => Transaction::where('context', Transaction::CONTEXT_REGISTRATION)
                    ->where('transaction_type', Transaction::REGISTER_TYPE_ADHERE)
                    ->latest()
                    ->first()->id,
            ],
        ])
            ->post(route('register.individual.credit.donate.post'), ['amount' => 10])
            ->assertRedirect(route('register.individual.credit.payment'))
            ->assertSessionHas([
                'registration.initPaymentResponse' => 'https://dummy-webkit.lemonway.fr'
                    . '?moneyintoken=2517404164b8dL9f910OVxb5ypt4PFAKiI10&lang=fr',
            ]);
        $this->assertDatabaseHas(app(Transaction::class)->getTable(), [
            'deleted_at' => null,
            'amount' => 500,
            'source_wallet_id' => null,
            'depends_on_id' => null,
            'lw_id' => '30276',
            'lw_token' => '2517404164b8dL9f910OVxb5ypt4PFAKiI10',
            'lw_executed_at' => null,
            'status' => Transaction::STATUS_PROCESSING,
            'failure_reason' => null,
            'type' => Transaction::TYPE_LEMONWAY,
            'cyclos_user_id' => null,
            'local_user_id' => $localUserId,
            'context' => Transaction::CONTEXT_REGISTRATION,
            'parent_id' => null,
            'transaction_type' => Transaction::REGISTER_TYPE_ADHERE,
        ]);
        $this->assertDatabaseHas(app(Transaction::class)->getTable(), [
            'deleted_at' => null,
            'amount' => 1000,
            'source_wallet_id' => null,
            'depends_on_id' => null,
            'lw_id' => null,
            'lw_token' => null,
            'lw_executed_at' => null,
            'status' => Transaction::STATUS_PROCESSING,
            'failure_reason' => null,
            'type' => Transaction::TYPE_LEMONWAY,
            'cyclos_user_id' => null,
            'local_user_id' => $localUserId,
            'context' => Transaction::CONTEXT_REGISTRATION,
            'transaction_type' => Transaction::REGISTER_TYPE_DONATE,
        ]);
    }

    /** Make test on credit step of individual user */
    protected function registerIndividualSystemPayCredit(): void
    {
        config()->set('somoney.credit.membership.individual', 5);
        $this->get(route('register.individual.credit.membership'))->assertStatus(Response::HTTP_OK);
        $this->post(route('register.individual.credit.membership.post'))
            ->assertRedirect(route('register.individual.credit.donate'));
        $this->mock(SystemPayService::class)
            ->shouldReceive('createPayment')
            ->once()
            ->with(
                2000,
                session('registration.user_id'),
                Transaction::CONTEXT_REGISTRATION,
                Mockery::type(Transaction::class)
            )
            ->andReturn("random-string");
        $this->post(route('register.individual.credit.donate.post'), ['amount' => 15])
            ->assertRedirect(route('register.individual.credit.payment'));
    }
}
