<?php

namespace Tests\Feature;

use App\Services\RegistrationConfigLoaderService;

abstract class RegisterTest extends BaseFeatureTest
{
    /** Set true options listed in array */
    protected static function setRegisterOptions(string $userType, array $options = []): void
    {
        array_map(function (string $name) use ($userType, $options) {
            if (array_key_exists($name, array_flip($options))) {
                config()->set("registration.available.$userType.$name", true);
            } else {
                config()->set("registration.available.$userType.$name", false);
            }
        }, array_keys(config("registration.available.$userType")));

        app(RegistrationConfigLoaderService::class)->load();
    }
}
