<?php

namespace Tests\Feature\Admin;

use App\Http\Controllers\Admin\NewUsersController;
use App\Jobs\Memberships\SendEmailNewCreated;
use App\Models\LocalUser;
use App\Models\Membership;
use App\Services\Cyclos\UserService;
use App\Services\LocalUserService;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;
use Tests\TestCase;
use Illuminate\Support\Facades\Date;

class NewUsersControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutMix();
        Bus::fake();
    }

    /** @test */
    public function it_can_create_valid_membership_when_manually_forced_by_admin_in_membership_mode(): void
    {
        config()->set('somoney.available.services.qr_code', false);
        config()->set('somoney.available.services.admin-valid-new', true);
        $newUserCyclosId = '4036691750709796715';
        $cyclosAdminUserData = [
            'id' => '4036691750709796712',
            'name' => 'Admin',
            'group' => ['internalName' => config('cyclos.internal-names.admin')],
            'email' => 'admin@fake.test',
        ];
        $stubLocalUser = LocalUser::create([
            'name' => 'Individual User',
            'group' => config('cyclos.internal-names.groups.individual'),
            'email' => 'part@fake.test',
            'customValues' => [],
        ]);
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('get')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn($cyclosAdminUserData);
        $userServiceMock->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn([]);
        $this->mock(LocalUserService::class)->shouldReceive('createCyclosUser')
            ->once()
            ->with(Mockery::on(fn(LocalUser $localUser) => $localUser->id === $stubLocalUser->id))
            ->andReturn($stubLocalUser->fill(['cyclos_id' => $newUserCyclosId]));
        $newUserIndexUrl = route('admin.users.new.index', [
            mb_strtolower(__('validation.attributes.' . $stubLocalUser->group)),
        ]);
        Carbon::setTestNow();
        $this->withSession(['auth' => ['user' => $cyclosAdminUserData]])
            ->from($newUserIndexUrl)
            ->post(route('admin.users.new.forceValidation', [
                mb_strtolower(__('validation.attributes.' . $stubLocalUser->group)),
                $stubLocalUser->id,
                NewUsersController::FORCE_MEMBERSHIPS,
            ]))
            ->assertRedirect($newUserIndexUrl);
        $this->assertDatabaseHas(app(Membership::class)->getTable(), [
            'cyclos_user_id' => $newUserCyclosId,
            'succeed_at' => now()->toDateTimeString(),
            'created_at' => now()->toDateTimeString(),
        ]);
    }

    /** @test */
    public function it_can_create_expired_membership_when_manually_forced_by_admin_in_simple_mode(): void
    {
        config()->set('somoney.available.services.qr_code', false);
        config()->set('somoney.available.services.admin-valid-new', true);

        Date::setTestNow('2022-05-16 12:22:22');

        $newUserCyclosId = '4036691750709796715';
        $cyclosAdminUserData = [
            'id' => '4036691750709796712',
            'name' => 'Admin',
            'group' => ['internalName' => config('cyclos.internal-names.admin')],
            'email' => 'admin@fake.test',
        ];
        $stubLocalUser = LocalUser::create([
            'name' => 'Individual User',
            'group' => config('cyclos.internal-names.groups.individual'),
            'email' => 'part@fake.test',
            'customValues' => [],
        ]);
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('get')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn($cyclosAdminUserData);
        $userServiceMock->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn([]);
        $this->mock(LocalUserService::class)->shouldReceive('createCyclosUser')
            ->once()
            ->with(Mockery::on(fn(LocalUser $localUser) => $localUser->id === $stubLocalUser->id))
            ->andReturn($stubLocalUser->fill(['cyclos_id' => $newUserCyclosId]));
        $newUserIndexUrl = route('admin.users.new.index', [
            mb_strtolower(__('validation.attributes.' . $stubLocalUser->group)),
        ]);
        $this->withSession(['auth' => ['user' => $cyclosAdminUserData]])
            ->from($newUserIndexUrl)
            ->post(route('admin.users.new.forceValidation', [
                mb_strtolower(__('validation.attributes.' . $stubLocalUser->group)),
                $stubLocalUser->id,
                NewUsersController::FORCE_SIMPLE,
            ]))
            ->assertRedirect($newUserIndexUrl);
        $this->assertDatabaseHas(app(Membership::class)->getTable(), [
            'cyclos_user_id' => $newUserCyclosId,
            'succeed_at' => null,
            'created_at' => Date::now(),
        ]);
    }

    /** @test */
    public function it_can_send_renew_membership_email_when_manually_forced_by_admin_in_simple_mode(): void
    {
        config()->set('somoney.available.services.qr_code', false);
        config()->set('somoney.available.services.admin-valid-new', true);
        $newUserCyclosId = '4036691750709796715';
        $cyclosAdminUserData = [
            'id' => '4036691750709796712',
            'name' => 'Admin',
            'group' => ['internalName' => config('cyclos.internal-names.admin')],
            'email' => 'admin@fake.test',
        ];
        $stubLocalUser = LocalUser::create([
            'name' => 'Individual User',
            'group' => config('cyclos.internal-names.groups.individual'),
            'email' => 'part@fake.test',
            'customValues' => [],
        ]);
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('get')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn($cyclosAdminUserData);
        $userServiceMock->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn([]);
        $this->mock(LocalUserService::class)->shouldReceive('createCyclosUser')
            ->once()
            ->with(Mockery::on(fn(LocalUser $localUser) => $localUser->id === $stubLocalUser->id))
            ->andReturn($stubLocalUser->fill(['cyclos_id' => $newUserCyclosId]));
        $newUserIndexUrl = route('admin.users.new.index', [
            mb_strtolower(__('validation.attributes.' . $stubLocalUser->group)),
        ]);
        Carbon::setTestNow();
        $this->withSession(['auth' => ['user' => $cyclosAdminUserData]])
            ->from($newUserIndexUrl)
            ->post(route('admin.users.new.forceValidation', [
                mb_strtolower(__('validation.attributes.' . $stubLocalUser->group)),
                $stubLocalUser->id,
                NewUsersController::FORCE_SIMPLE,
            ]))
            ->assertRedirect($newUserIndexUrl);
        Bus::assertDispatched(fn(SendEmailNewCreated $job) => $job->cyclosUserId === $newUserCyclosId);
    }

    /** @test */
    public function it_cant_refresh_email_status_when_mailgun_is_not_configured(): void
    {
        config()->set('somoney.available.services.email-status', true);
        config()->set('services.mailgun.secret', null);
        $cyclosAdminUserData = [
            'id' => '4036691750709796712',
            'name' => 'Admin',
            'group' => ['internalName' => config('cyclos.internal-names.admin')],
            'email' => 'admin@fake.test',
        ];
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('get')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn($cyclosAdminUserData);
        $userServiceMock->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn([]);
        $newUserIndexUrl = route('admin.users.new.index', [mb_strtolower(__('validation.attributes.individual'))]);
        $response = $this->withSession(['auth' => ['user' => $cyclosAdminUserData]])
            ->from($newUserIndexUrl)
            ->get(route('admin.users.new.email.refresh', [mb_strtolower(__('validation.attributes.individual'))]));
        $response->assertRedirect($newUserIndexUrl)
            ->assertSessionHas(
                'error',
                'Mailgun n\'est pas configuré pour permettre de rafraichir le statut des emails.'
            );
    }
}
