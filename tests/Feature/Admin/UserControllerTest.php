<?php

namespace Tests\Feature\Admin;

use App\Models\CyclosUser;
use App\Services\Cyclos\AccountService;
use App\Services\Cyclos\TokenService;
use App\Services\Cyclos\User\PhoneService;
use App\Services\Cyclos\UserService;
use App\Services\QrCodeService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Pagination\LengthAwarePaginator;
use Mockery;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutMix();
    }

    /** @test */
    public function it_can_show_user_without_account_balance(): void
    {
        config()->set('somoney.available.services.qr_code', false);
        $cyclosAdminUserData = [
            'id' => '4036691750709796712',
            'name' => 'Admin',
            'group' => ['internalName' => config('cyclos.internal-names.admin')],
            'email' => 'admin@fake.test',
        ];
        $cyclosPartUserData = [
            'id' => '4036691750709796714',
            'name' => 'Individual User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.individual')],
            'email' => 'part@fake.test',
        ];
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('get')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn($cyclosAdminUserData);
        $userServiceMock->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn([]);
        $userServiceMock->shouldReceive('get')
            ->once()
            ->with($cyclosPartUserData['id'])
            ->andReturn($cyclosPartUserData);
        $userServiceMock->shouldReceive('getDataForEdit')
            ->once()
            ->with($cyclosPartUserData['id'])
            ->andReturn(['customFields' => []]);
        $this->mock(AccountService::class)->shouldReceive('getUsersAccountsBalance')
            ->once()
            ->with(
                $cyclosPartUserData['group']['internalName'],
                [
                    'usersToInclude' => $cyclosPartUserData['id'],
                    'fields' => 'balance',
                ]
            )
            ->andReturn(new LengthAwarePaginator([], 0, 40, 1));
        $this->withSession(['auth' => ['user' => $cyclosAdminUserData]])
            ->get(route('admin.users.show', $cyclosPartUserData['id']))
            ->assertOk()
            ->assertSeeTextInOrder([$cyclosPartUserData['name'], 'Solde : 0.00']);
    }

    /**
     * @test
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength))
     */
    public function it_regenerate_qr_code_images_when_professional_user_updated(): void
    {
        config()->set('somoney.available.services.qr_code', true);
        $adminUserData = [
            'id' => '4036691750709796712',
            'name' => 'Admin',
            'group' => ['internalName' => config('cyclos.internal-names.admin')],
            'email' => 'admin@fake.com',
        ];
        $proUserData = [
            'id' => '4036691750709796714',
            'name' => 'Professional User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'pro@fake.com',
            'phone' => '+33202020202',
            'addresses' => [
                [
                    'addressLine1' => '8 rue des roses', 'zip' => "44000", 'city' => 'Nantes', 'location' => [
                        'latitude' => 47.21650055156631,
                        'longitude' => -1.576520530421806,
                    ],
                ],
            ],
        ];
        $professionalNewName = 'Professional new name';
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('get')->once()->with($adminUserData['id'])->andReturn($adminUserData);
        $userServiceMock->shouldReceive('getUserAccount')->once()->with($adminUserData['id'])->andReturn([]);
        $userServiceMock->shouldReceive('get')->times(5)->with($proUserData['id'])->andReturn($proUserData);
        $userServiceMock->shouldReceive('exists')
            ->once()
            ->with(Mockery::on(fn(array $search) => $search['profileFields']['email'] === $proUserData['email']))
            ->andReturn(false);
        $userServiceMock->shouldReceive('setUserData')
            ->once()
            ->with($proUserData['id'], [
                'name' => $professionalNewName,
                'email' => $proUserData['email'],
                'customValues' => [
                    "gender" => "4036691750709796724",
                    "firstname" => "Professional",
                    "lastname" => "User",
                    "mailpro" => $proUserData['email'],
                    "visible" => "true",
                    "membershipsonantaise" => false,
                    "commercialinfo" => "false",
                    "optinnewsletter" => "false",
                    "birthday" => "1997-04-16",
                ],
            ]);
        $this->mock(PhoneService::class)
            ->shouldReceive('setUserPhone')
            ->once()
            ->with($proUserData['id'], $proUserData['phone']);
        $tokenServiceMock = $this->mock(TokenService::class);
        $qrCodeTokenTypeId = '4036693603451314024';
        $tokenServiceMock->shouldReceive('getUserTypeTokens')
            ->twice()
            ->with($proUserData['id'])
            ->andReturn([
                [
                    'type' => [
                        'id' => $qrCodeTokenTypeId,
                        'internalName' => 'codeqr',
                    ],
                ],
            ]);
        $tokenServiceMock->shouldReceive('getUserTokens')
            ->twice()
            ->with($proUserData['id'], $qrCodeTokenTypeId)
            ->andReturn([
                'tokens' => [
                    [
                        'id' => '4036693649353777000',
                        'value' => 'atensizalsosketandivhagrkzaixneh',
                        'status' => 'active',
                    ],
                ],
            ]);
        $qrCodeImageCreatedAt =
            filemtime(app(QrCodeService::class)->getCyclosUserQrCodeTokenImage(CyclosUser::fill($proUserData), false));
        sleep(1);// wait to have different timestamp between first creation and re-creation after
        $this->withSession(['auth' => ['user' => $adminUserData]])
            ->post(route('admin.users.update', $proUserData['id']), [
                'address_gmap' => '8 rue des roses 44000 Nantes',
                'gmap' => array_merge(
                    $proUserData['addresses'][0],
                    $proUserData['addresses'][0]['location']
                ),
                'user' => [
                    'name' => $professionalNewName,
                    'email' => $proUserData['email'],
                    'customValues' => [
                        'gender' => '4036691750709796724',
                        'firstname' => 'Professional',
                        'lastname' => 'User',
                        'mailpro' => $proUserData['email'],
                        'visible' => 'true',
                    ],
                ],
                'phone' => $proUserData['phone'],
                'birthday' => '16/04/1997',
            ])
            ->assertSessionHasNoErrors()
            ->assertRedirect(route('admin.users.edit', ['id' => $proUserData['id']]));
        $this->assertNotEquals(
            $qrCodeImageCreatedAt,
            filemtime(app(QrCodeService::class)->getCyclosUserQrCodeTokenImage(
                CyclosUser::fill(array_merge($proUserData, ['name' => $professionalNewName])),
                false
            ))
        );
    }
}
