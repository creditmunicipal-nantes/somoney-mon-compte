<?php

namespace Tests\Feature\Admin;

use App\Services\Cyclos\AccountService;
use App\Services\Cyclos\TransactionService;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Date;
use Tests\TestCase;

class PageControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutMix();
    }

    /** @test */
    public function it_can_filter_stats_by_period(): void
    {
        $cyclosAdminUserData = [
            'id' => '4036691750709796712',
            'name' => 'Admin',
            'group' => ['internalName' => config('cyclos.internal-names.admin')],
            'email' => 'admin@fake.test',
        ];
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('get')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn($cyclosAdminUserData);
        $userServiceMock->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn([]);
        $transactionServiceMock = $this->partialMock(TransactionService::class);
        $transactionServiceMock->shouldReceive('listAsAdmin')
            ->once()
            ->with(['pageSize' => 25])
            ->andReturn(new LengthAwarePaginator([
                ['from' => ['kind' => 'system'], 'to' => ['kind' => 'system']],
            ], 1, 1));
        $transactionServiceMock->shouldReceive('allAsAdmin')
            ->once()
            ->with([
                'datePeriod' => Date::create(2021, 10, 17)->toDateString() . ',' .
                    Date::create(2021, 10, 23)->toDateString(),
            ])
            ->andReturn(collect([
                [
                    'date' => Date::create(2021, 10, 23)->toISOString(),
                    'amount' => '5.00',
                    'type' => [
                        'from' => ['internalName' => 'individual'],
                        'to' => ['internalName' => 'professional'],
                    ],
                    'kind' => 'payment',
                ],
                [
                    'date' => Date::create(2021, 10, 20)->toISOString(),
                    'amount' => '10.00',
                    'type' => [
                        'from' => ['internalName' => 'professional'],
                        'to' => ['internalName' => 'individual'],
                    ],
                    'kind' => 'payment',
                ],
                [
                    'date' => Date::create(2021, 10, 17)->toISOString(),
                    'amount' => '15.00',
                    'type' => [
                        'from' => ['internalName' => 'individual'],
                        'to' => ['internalName' => 'professional'],
                    ],
                    'kind' => 'payment',
                ],
            ]));
        $this->withSession(['auth' => ['user' => $cyclosAdminUserData]])
            ->get(route('admin.home', [
                'transactions_date_begin' => '17/10/2021',
                'transactions_date_end' => '23/10/2021',
            ]))
            ->assertOk()
            ->assertSeeInOrder([
                'Nombre de transactions: <b>3</b>',
                'Somme des transactions:',
                '30,00',
            ], false);
    }

    /** @test */
    public function it_can_have_stats_of_reconversion(): void
    {
        $cyclosAdminUserData = [
            'id' => '4036691750709796712',
            'name' => 'Admin',
            'group' => ['internalName' => config('cyclos.internal-names.admin')],
            'email' => 'admin@fake.test',
        ];
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('get')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn($cyclosAdminUserData);
        $userServiceMock->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn([]);
        $transactionServiceMock = $this->partialMock(TransactionService::class);
        $transactionServiceMock->shouldReceive('listAsAdmin')
            ->once()
            ->with(['pageSize' => 25])
            ->andReturn(new LengthAwarePaginator([
                ['from' => ['kind' => 'system'], 'to' => ['kind' => 'system']],
            ], 1, 1));
        $transactionServiceMock->shouldReceive('allAsAdmin')
            ->once()
            ->with(['datePeriod' => Date::today()->toDateString() . ',' . Date::today()->toDateString()])
            ->andReturn(collect([
                [
                    'date' => Date::today()->toISOString(),
                    'amount' => '5.00',
                    'type' => [
                        'from' => ['internalName' => 'individual'],
                        'to' => ['internalName' => 'stableAccount'],
                    ],
                    'kind' => 'payment',
                ],
                [
                    'date' => Date::today()->toISOString(),
                    'amount' => '10.00',
                    'type' => [
                        'from' => ['internalName' => 'professional'],
                        'to' => ['internalName' => 'stableAccount'],
                    ],
                    'kind' => 'payment',
                ],
            ]));
        $this->withSession(['auth' => ['user' => $cyclosAdminUserData]])
            ->get(route('admin.home'))
            ->assertOk()
            ->assertSeeInOrder([
                'Reconversion de compte des particuliers',
                '5,00',
                'Reconversion de compte des professionnels',
                '10,00',
            ], false);
    }

    /** @test */
    public function it_can_have_stats_of_credit(): void
    {
        $cyclosAdminUserData = [
            'id' => '4036691750709796712',
            'name' => 'Admin',
            'group' => ['internalName' => config('cyclos.internal-names.admin')],
            'email' => 'admin@fake.test',
        ];
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('get')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn($cyclosAdminUserData);
        $userServiceMock->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn([]);
        $transactionServiceMock = $this->partialMock(TransactionService::class);
        $transactionServiceMock->shouldReceive('listAsAdmin')
            ->once()
            ->with(['pageSize' => 25])
            ->andReturn(new LengthAwarePaginator([
                ['from' => ['kind' => 'system'], 'to' => ['kind' => 'system']],
            ], 1, 1));
        $transactionServiceMock->shouldReceive('allAsAdmin')
            ->once()
            ->with(['datePeriod' => Date::today()->toDateString() . ',' . Date::today()->toDateString()])
            ->andReturn(collect([
                [
                    'date' => Date::today()->toISOString(),
                    'amount' => '5.00',
                    'type' => [
                        'from' => ['internalName' => 'stableAccount'],
                        'to' => ['internalName' => 'individual'],
                    ],
                    'kind' => 'payment',
                ],
                [
                    'date' => Date::today()->toISOString(),
                    'amount' => '10.00',
                    'type' => [
                        'from' => ['internalName' => 'stableAccount'],
                        'to' => ['internalName' => 'professional'],
                    ],
                    'kind' => 'payment',
                ],
            ]));
        $this->withSession(['auth' => ['user' => $cyclosAdminUserData]])
            ->get(route('admin.home'))
            ->assertOk()
            ->assertSeeInOrder([
                'Crédit de compte des particuliers',
                '5,00',
                'Crédit de compte des professionnels',
                '10,00',
            ], false);
    }

    /** @test */
    public function it_does_not_generate_qr_code_for_admin_users(): void
    {
        $cyclosAdminUserData = [
            'id' => '4036691750709796712',
            'name' => 'Admin',
            'group' => ['internalName' => config('cyclos.internal-names.admin')],
            'email' => 'admin@fake.test',
        ];
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('get')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn($cyclosAdminUserData);
        $userServiceMock->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn([]);
        $transactionServiceMock = $this->partialMock(TransactionService::class);
        $transactionServiceMock->shouldReceive('listAsAdmin')
            ->once()
            ->with(['pageSize' => 25])
            ->andReturn(new LengthAwarePaginator([
                ['from' => ['kind' => 'system'], 'to' => ['kind' => 'system']],
            ], 1, 1));
        $transactionServiceMock->shouldReceive('allAsAdmin')
            ->once()
            ->with(['datePeriod' => Date::today()->toDateString() . ',' . Date::today()->toDateString()])
            ->andReturn(collect([
                [
                    'date' => Date::today()->toISOString(),
                    'amount' => '5.00',
                    'type' => [
                        'from' => ['internalName' => 'stableAccount'],
                        'to' => ['internalName' => 'individual'],
                    ],
                    'kind' => 'payment',
                ],
                [
                    'date' => Date::today()->toISOString(),
                    'amount' => '10.00',
                    'type' => [
                        'from' => ['internalName' => 'stableAccount'],
                        'to' => ['internalName' => 'professional'],
                    ],
                    'kind' => 'payment',
                ],
            ]));
        $this->withSession(['auth' => ['user' => $cyclosAdminUserData]])
            ->get(route('admin.home'))
            ->assertOk()
            ->assertDontSee('#qr-code-modal');
    }
}
