<?php

namespace Tests\Feature\Admin;

use App\Models\Membership;
use App\Models\Transaction;
use App\Services\Cyclos\UserService;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MembershipControllerTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->withoutMix();
    }

    /** @test */
    public function it_can_display_succeeded_membership_transactions(): void
    {
        config()->set('somoney.available.services.membership', true);
        $cyclosAdminUserData = [
            'id' => '4036691750709796712',
            'name' => 'Admin',
            'group' => ['internalName' => config('cyclos.internal-names.admin')],
            'email' => 'admin@fake.test',
        ];
        $cyclosProUserData = [
            'id' => '4036691750709796715',
            'name' => 'Professional USer',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'pro@fake.test',
        ];
        $cyclosPartUserData = [
            'id' => '4036691750709796714',
            'name' => 'Individual User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.individual')],
            'email' => 'part@fake.test',
        ];
        Membership::create(['cyclos_user_id' => $cyclosPartUserData['id']]);
        Membership::create(['cyclos_user_id' => $cyclosProUserData['id']]);
        Transaction::create([
            'amount' => 2400,
            'status' => Transaction::STATUS_CANCELED,
            'type' => Transaction::TYPE_SYSTEM_PAY,
            'cyclos_user_id' => $cyclosPartUserData['id'],
            'local_user_id' => null,
            'context' => Transaction::CONTEXT_MEMBERSHIP,
        ]);
        $succeedTransaction = Transaction::create([
            'amount' => 2300,
            'status' => Transaction::STATUS_SUCCESS,
            'type' => Transaction::TYPE_SYSTEM_PAY,
            'cyclos_user_id' => $cyclosProUserData['id'],
            'local_user_id' => null,
            'context' => Transaction::CONTEXT_MEMBERSHIP,
        ]);
        $userServiceMock = $this->mock(UserService::class);
        $userServiceMock->shouldReceive('get')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn($cyclosAdminUserData);
        $userServiceMock->shouldReceive('getUserAccount')
            ->once()
            ->with($cyclosAdminUserData['id'])
            ->andReturn([]);
        $userServiceMock->shouldReceive('searchAll')
            ->once()
            ->with([
                'groups' => implode(',', array_values(config('cyclos.internal-names.groups'))),
            ])
            ->andReturn(collect([$cyclosProUserData]));
        $this->withSession(['auth' => ['user' => $cyclosAdminUserData]])
            ->get(route('admin.memberships.operations'))
            ->assertSeeTextInOrder([
                $cyclosProUserData['name'],
                'Adhésion',
                number_format($succeedTransaction->amount / 100, 2, ',', ' ')
            ])
            ->assertDontSeeText($cyclosPartUserData['name']);
    }
}
