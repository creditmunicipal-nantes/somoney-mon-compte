<?php

namespace Tests\Feature\Api;

use App\Jobs\SystemPay\TransactionCallback;
use App\Models\Transaction;
use App\Services\SystemPayService;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Bus;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class SystemPayControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_dispatch_transaction_callback_job_from_api_request_when_hash_is_ok(): void
    {
        Bus::fake();
        $transaction = Transaction::create(['amount' => 2000]);
        $paymentData = [
            'orderStatus' => 'Dummy',
            'orderDetails' => ['orderId' => $transaction->id],
        ];
        $this->mock(SystemPayService::class)->shouldReceive('checkHash')
            ->once()
            ->withNoArgs()
            ->andReturn(true);
        $this->postJson(
            route('api.system_pay.callback'),
            ['kr-answer' => json_encode($paymentData, JSON_THROW_ON_ERROR)]
        )->assertStatus(Response::HTTP_OK);
        Bus::assertDispatched(fn(TransactionCallback $job) => $job->paymentData === $paymentData);
    }

    /** @test */
    public function it_cant_dispatch_transaction_callback_job_from_api_request_when_hash_is_nok(): void
    {
        Bus::fake();
        $transaction = Transaction::create(['amount' => 2000]);
        $paymentData = [
            'orderStatus' => 'Dummy',
            'orderDetails' => ['orderId' => $transaction->id],
        ];
        $this->mock(SystemPayService::class)->shouldReceive('checkHash')
            ->once()
            ->withNoArgs()
            ->andReturn(false);
        $this->postJson(
            route('api.system_pay.callback'),
            ['kr-answer' => json_encode($paymentData, JSON_THROW_ON_ERROR)]
        )->assertStatus(Response::HTTP_FORBIDDEN);
        Bus::assertNotDispatched(TransactionCallback::class);
    }
}
