<?php

namespace Tests\Feature\Api\Cyclos;

use App\Jobs\SystemPay\TransactionCallback;
use App\Models\Transaction;
use App\Services\Cyclos\UserService;
use App\Services\SystemPayService;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Bus;
use Mockery;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CyclosUserControllerTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_search_professional_user_when_user_not_exist(): void
    {
        $cyclosUserData = [
            'id' => '4036691750709796716',
            'name' => 'Professional Recipient User',
            'group' => ['internalName' => config('cyclos.internal-names.groups.professional')],
            'email' => 'pro@fake.test',
        ];
        $this->mock(UserService::class)->shouldReceive('search')
            ->once()
            ->with(Mockery::on(fn(array $data) => $data['groups'] === $cyclosUserData['group']['internalName']
                && $data['usersIdsToInclude'] === $cyclosUserData['id']
                && ! $data['profileFields']['needsValidation']))
            ->andReturn(new LengthAwarePaginator([], 0, 1000, 1));
        $this->get(route('api.cyclos.index', [
            'groups' => $cyclosUserData['group']['internalName'],
            'included_users_ids' => $cyclosUserData['id'],
            'page' => 0,
            'pageSize' => 1000,
            'profileFields' => ['needsValidation' => false],
        ]))
            ->assertExactJson([])
            ->assertStatus(Response::HTTP_OK);
    }
}
