const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fs = require('fs');
const getJsonFileResponse = filePath => {
    return JSON.parse(fs.readFileSync(__dirname + '/fixtures/mock-responses/lemonway/' + filePath));
};
// middlewares
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.route(/^\/GetMoneyInTransDetails$/).post((req, res) => {
    res.json(getJsonFileResponse('success.json'));
});

app.route('/').get((req, res) => {
    res.send('Welcome to node mock server of Lemonway API');
});
app.listen(3004, () => {});
