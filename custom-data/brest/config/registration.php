<?php

return [
    'available' => [
        'common' => [
            'comment' => false,
            'next_steps_infos' => true,
            'directory_infos' => true,
        ],
        'individual' => [
            'id_document' => false,
            'donate' => true,
        ],
        'professional' => [
            'siren' => false,
            'address' => true,
            'justification_documents' => true,
            'category' => false,
            'newsletter' => true,
            'association-membership' => true,
            'donate' => true,
        ],
    ],
];
