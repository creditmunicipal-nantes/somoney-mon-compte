<?php

use App\Models\Transaction;

return [
    'available' => [
        'services' => [
            'register' => true,
            'transfer' => [
                'confirm-email' => true,
            ],
            'sepa-payment' => true,
            'admin-credit' => true,
            'reconversion-user' => true,
            'email-status' => true,
            'bank-notes' => true,
            'counters' => true,
            'lemonway-v2' => true,
            'show-credit' => true,
            'admin-valid-new' => true,
            'qr_code' => true,
            'fidelity' => true,
            'sdgs' => true,
            'newsletters' => true,
            'membership' => true,
            'membership_notify_for_renew' => true,
            'membership_block_expired_accounts' => true,
        ],
    ],
    'payment-gateway' => Transaction::TYPE_LEMONWAY,
    'money' => 'Heol',
    'banknotes' => [1, 2, 5, 10, 20],
    'counters-cyclos-account' => env('COUNTERS_CYCLOS_ACCOUNT'),
    'region' => 'Pays de Brest',
    'occupant' => 'utilisateurs d\'Heol',
    'asso' => 'Association Heol',
    'person' => 'utilisateurs d\'Heol',
    'url' => [
        'facebook' => 'https://www.facebook.com/monnaieheol',
        'twitter' => null,
        'youtube' => null,
        'cgu' => 'https://heol-moneiz.bzh/charte/',
        'contact' => 'https://heol-moneiz.bzh/contact/',
    ],
    'contact' => [
        'title' => 'Association Heol',
        'address' => '208 rue Jean Jaurès',
        'zipcode' => '29200',
        'city' => 'Brest',
        'phone' => null,
        'email' => 'bonjour@heol-moneiz.bzh',
        'siret' => null,
        'naf' => null,
    ],
    'cgu' => [
        'professional' => 'https://e.issuu.com/embed.html?u=somoney&d=cgu_heol_structures',
        'individual' => 'https://e.issuu.com/embed.html?u=somoney&d=cgu_heol_particuliers',
    ],
    'age' => [
        'min' => 18,
    ],
    'credit' => [
        'maximal' => [
            'individual' => 500,
            'professional' => 2500,
        ],
        'simple' => [
            'minimal' => 20,
            'minimal-sepa' => 5,
        ],
        'auto' => [
            'minimal' => 20,
            'minimal-length' => 36,
        ],
        'sepa' => [
            'sampling_month_day' => 5,
        ],
        'membership' => [
            'min' => [
                'registration' => [
                    'individual' => 5,
                    'professional' => 15,
                ],
                'renewal' => [
                    'individual' => 5,
                    'professional' => 15,
                ],
            ],
        ],
    ],
    'map' => [
        'zoom' => 13,
        'center' => [
            'lat' => '48.3877999',
            'lng' => '-4.4883928',
        ],
    ],
    'content' => [
        'signup' => [
            'individual' => [
                'info' => 'La dernière étape de l\'inscription consiste à régler votre adhésion. La cotisation annuelle de 5€ est obligatoire. <b>Si votre adhésion pour l\'année en cours est à jour, veuillez-vous arrêter à la dernière étape</b>. L\'équipe HEOL validera votre compte dans un délai 48h (jours ouvrés), le temps de vérifier que votre adhésion est à jour. Vous recevrez un mail vous informant de l\'ouverture de votre compte',
            ],
            'professional' => [
                'info' => 'Veuillez noter que l\'inscription des professionnels est payante. <a class=\'c-link\' target=\'blank\' href=\'https://heol-moneiz.bzh/contact/\'> Contactez-nous</a> avant inscription pour plus de détail.',
                'success' => '<p class=\'c-p u-large\'>Félicitations !<br>Votre adresse e-mail a bien été validée.</p><p class=\'c-p u-large u-mg-bottom\'>Votre inscription est réussie, vous pourrez vous connecter à votre espace quand votre compte aura été approuvé par Heol.</p>',
                'success_membership' => <<<EOL
<div class="u-large u-mg-top">
    Vous pourrez accéder à votre compte une fois votre inscription validée par un membre de l'équipe. Merci de votre compréhension !',
</div>
EOL,
            ],
        ],
        'email' => [
            'subject' => [
                'register' => 'Bienvenue dans la communauté Heol !',
                'confirm-registered' => 'Confirmation de votre adhésion à Heol !',
                'remind-register' => 'Rappel de validation d\'email pour l\'inscription à Heol',
                'renew-created' => 'Renouvellement de votre adhésion à l\'association Heol',
                'renew-warning' => 'Risque de blocage de votre compte Heol',
                'new-created' => 'Mettre à jour votre adhésion à l\'association Heol',
            ],
            'membership' => [
                'intro' => <<<EOL
<p class="c-p">
    La monnaie locale circule au sein de son réseau d'adhérents, particuliers et professionnels.
    Pour continuer à utiliser Heol et soutenir son déploiement, nous vous invitons à renouveler votre adhésion pour cette nouvelle année civile :
</p>
EOL,
            ],
        ],
    ],
    'conversion-fee-rate' => 0.02,
    'emails' => [
        'contact_type' => App\Mail\Mail::CONTACT_LINK_URL,
    ],
];
