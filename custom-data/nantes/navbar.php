<?php

return [
    [
        'name' => "L'annuaire",
        'url' => "https://annuaire.moneko.org/",
    ],
    [
        'name' => "FAQ",
        'url' => "https://moneko.org/faq/",
    ],
    [
        'name' => "Contact",
        'url' => "https://moneko.org/contact/",
    ],
    [
        'name' => "Pour les pros",
        'url' => "https://moneko.org/professionnels/",
    ],
];
