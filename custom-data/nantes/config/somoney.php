<?php

use App\Models\Transaction;

return [
    'available' => [
        'services' => [
            'register' => true,
            'transfer' => [
                'confirm-email' => true,
            ],
            'sepa-payment' => true,
            'admin-credit' => true,
            'reconversion-user' => true,
            'email-status' => true,
            'counters' => true,
            'lemonway-v2' => false,
            'show-credit' => true,
            'admin-valid-new' => true,
            'qr_code' => true,
            'fidelity' => true,
            'sdgs' => true,
            'newsletters' => true,
            'membership' => true,
            'membership_notify_for_renew' => false,
            'membership_block_expired_accounts' => false,
        ],
    ],
    'payment-gateway' => Transaction::TYPE_SYSTEM_PAY,
    'money' => 'Moneko',
    'banknotes' => [0.5, 1, 2, 5, 10, 20, 44],
    'counters-cyclos-account' => env('COUNTERS_CYCLOS_ACCOUNT'),
    'counters-cash-out' => 'Retrait',
    'counters-cash-in' => 'Dépôt',
    'region' => 'Loire-Atlantique',
    'occupant' => 'ligériens et ligériennes',
    'asso' => 'MLC44',
    'person' => 'membres Moneko',
    'url' => [
        'facebook' => 'https://www.facebook.com/MonekoMLC44',
        'twitter' => 'https://twitter.com/moneko_mlc44',
        'instagram' => 'https://www.instagram.com/moneko_mlc44',
        'youtube' => null,
        'linkedin' => 'https://www.linkedin.com/company/moneko-monnaie-locale-de-loire-atlantique',
        'cgu' => 'https://moneko.org/cgu',
        'contact' => 'https://moneko.org/contact',
        'counters' => 'https://moneko.org/nos-comptoirs-de-change/',
        'application' => [
            'android' => 'https://play.google.com/store/apps/details?id=fr.sonantes.mobile&hl=ln',
            'apple' => 'https://apps.apple.com/fr/app/moneko/id1414460652',
        ],
    ],
    'contact' => [
        'title' => 'MLC44',
        'address' => '8 rue Saint Domingue',
        'zipcode' => '44200',
        'city' => 'Nantes',
        'phone' => '07-66-87-76-60',
        'email' => 'contact@moneko.org',
        'siret' => '840 746 176 00030',
        'naf' => '9499Z',
    ],
    'cgu' => [
        'professional' => 'https://e.issuu.com/embed.html#28805209/55034603',
        'individual' => 'https://e.issuu.com/embed.html#28805209/54913813',
    ],
    'age' => [
        'min' => 18,
    ],
    'credit' => [
        'maximal' => [
            'individual' => 250,
            'professional' => 2500,
        ],
        'simple' => [
            'minimal' => 20,
            'minimal-sepa' => 30,
        ],
        'auto' => [
            'minimal' => 30,
            'minimal-length' => 6,
        ],
        'sepa' => [
            'sampling_month_day' => 5,
        ],
        'membership' => [
            'min' => [
                'registration' => [
                    'individual' => 6,
                    'professional' => 32,
                ],
                'renewal' => [
                    'individual' => 6,
                    'professional' => 36,
                ],
            ],
        ],
    ],
    'map' => [
        'zoom' => 13,
        'center' => [
            'lat' => '47.214914',
            'lng' => '-1.550317',
        ],
    ],
    'content' => [
        'signup' => [
            'individual' => [
                'info' => <<<EOL
Pour permettre le fonctionnement de l’association citoyenne qui porte Moneko et parce que c’est un préalable obligatoire à l’utilisation de la monnaie locale (comme le mentionne la loi du 31 juillet 2014 relative à l’économie sociale et solidaire), chaque utilisateur et utilisatrice est adhérent.e à l’association.<br>
Pour les particuliers, le montant de la cotisation annuelle classique est de 12€.
La cotisation de soutien à 24€ et la cotisation de soutien au projet jeunesse de 36€ (12€ seront attribués au projet jeunesse).
Moneko tient à être accessible à toutes et tous : les personnes à faibles revenus bénéficient d’un tarif réduit à 6€ basé sur la confiance (pas de justificatif demandé).
Libre à vous de contribuer et soutenir davantage la monnaie locale selon vos souhaits et possibilités.<br>
Merci pour votre engagement !
<a class="c-link" target="blank" href="https://moneko.org/contact" title="Contactez-nous">Contactez-nous</a> pour plus d’information.
EOL,
                'success_membership' => <<<EOL
<div class="u-large u-mg-top u-mg-bottom">
    Si vous avez déjà réglé votre adhésion, merci d'envoyer un email à <a class="c-link" href="mailto:contact@moneko.org" title="Contactez-nous">contact@moneko.org</a>.<br/>
    Vous pourrez accéder à votre compte une fois votre inscription validée par un membre de l'équipe. Merci de votre compréhension !
</div>
<div class="u-large u-mg-bottom">
    Retourner sur <a class="c-link" href="https://moneko.org" target="blank" title="Retourner sur moneko.org">moneko.org</a>
</div>',
EOL,
                'membership' => [
                    'registration_price_grid' => <<<EOL
Grille des cotisations :<br/>
<small>(Adoptée lors de l’Assemblée Générale du 28 novembre 2022)</small><br>
<br>
<ul class="u-inline-left u-pd-left u-pd-top-sm">
    <li><span class="u-color--primary">Adhésion annuelle classique</span> à 12€</li>
    <li><span class="u-color--primary">Adhésion de soutien</span> à 24€/an ou 2€/mois, payable par virement automatique en euros ou Moneko</li>
    <li><span class="u-color--primary">Adhésion de soutien au Projet solidarité jeunesse</span> à 36€/an ou 3€/mois, 12€ seront reversés directement au développement du projet</li>
    <li><span class="u-color--primary">Possibilité de payer un tarif social</span> à 6€ sans justificatif pour les personnes à faibles revenus</li>
</ul>
<br>
EOL,
                    'registration_price_grid_bottom_information' => <<<EOL
<div class="u-large u-mg-top">
    Retrouvez les grilles tarifaires détaillées et toutes les informations sur la <a class="c-link" target="blank" href="https://moneko.org/tarifs" title="Consulter la page d'adhésion">page d’adhésion du site moneko.org</a>.
</div>
EOL,
                    'membership_renewal_price_grid' => <<<EOL
Grille des cotisations :<br/>
<small>(Adoptée lors de l’Assemblée Générale du 28 novembre 2022)</small><br>
<br>
<ul class="u-inline-left u-pd-left u-pd-top-sm">
    <li><span class="u-color--primary">Adhésion annuelle classique</span> à 12€</li>
    <li><span class="u-color--primary">Adhésion de soutien</span> à 24€/an ou 2€/mois, payable par virement automatique en euros ou Moneko</li>
    <li><span class="u-color--primary">Adhésion de soutien au Projet solidarité jeunesse</span> à 36€/an ou 3€/mois, 12€ seront reversés directement au développement du projet</li>
    <li><span class="u-color--primary">Possibilité de payer un tarif social</span> à 6€ sans justificatif pour les personnes à faibles revenus</li>
</ul>
<br>
EOL,
                ],
            ],
            'professional' => [
                'info' => <<<EOL
Pour permettre le fonctionnement de l’association citoyenne qui porte Moneko et parce que c’est un préalable obligatoire à l’utilisation de la monnaie locale (comme le mentionne la loi du 31 juillet 2014 relative à l’économie sociale et solidaire), chaque utilisateur et utilisatrice est adhérent.e à l’association.<br>
Professionnels, la cotisation annuelle est adaptée à la taille de votre structure. Nous vous invitons à consulter les <a class="c-link" target="blank" href="https://moneko.org/tarifs" title="Consulter la grille tarifaire">grilles tarifaires entreprises et associations</a> sur notre site moneko.org.<br>
Merci pour votre engagement !
<a class="c-link" href="mailto:contact@moneko.org" title="Contactez-nous">Contactez-nous</a> pour plus d’information.
EOL,
                'success' => '<p class="c-p u-large">Félicitations !<br>Votre adresse e-mail a bien été validée.</p><p class="c-p u-large u-mg-bottom">Pour finaliser votre inscription, vous devez régler votre adhésion.</p>',
                'success_membership' => '<p class="c-p u-large u-mg-top u-mg-bottom">Si vous avez déjà réglé votre adhésion, merci d\'envoyer un email à <a class="c-link" href="mailto:contact@moneko.org" title="Contactez-nous">contact@moneko.org</a></p><p class="c-p u-large u-mg-bottom">Vous pourrez accéder à votre espace Moneko une fois votre adhésion validée par l\'association. Nous vous invitons à y compléter votre profil pour donner toutes les informations pratiques sur votre activité aux utilisateurs Moneko.</p><p class="c-p u-large u-mg-bottom">Retourner sur  <a href="https://moneko.org" class="c-link" target="blank">moneko.org</a></p>',
                'membership' => [
                    'registration_price_grid' => <<<EOL
Grille des cotisations :<br/>
<small>(Adoptée lors de l’Assemblée Générale du 28 novembre 2022)</small><br>
<br>
<b>Entreprise :</b><br>
<ul class="u-inline-left u-pd-left u-pd-top-sm">
    <li><span class="u-color--primary">0 salarié.e :</span> CSD* de 32€ + cotisation à partir de 4€/mois ou 48€/an</li>
    <li><span class="u-color--primary">1 à 2 salarié.e.s :</span> CSD de 56€ + cotisation à partir de 7€/mois ou 84€/an</li>
    <li><span class="u-color--primary">3 à 5 salarié.e.s :</span> CSD de 80€ + cotisation à partir de 10€/mois ou 120€/an</li>
    <li><span class="u-color--primary">6 à 9 salarié.e.s :</span> CSD de 120€ + cotisation à partir de 15€/mois ou 180€/an</li>
    <li><span class="u-color--primary">10 à 19 salarié.e.s :</span> CSD de 200€ + cotisation à partir de 25€/mois ou 300€/an</li>
    <li><span class="u-color--primary">20 à 49 salarié.e.s :</span> CSD de 240€ + cotisation à partir de 40€/mois ou 480€/an</li>
    <li><span class="u-color--primary">50 à 99 salarié.e.s :</span> CSD de 240€ + cotisation à partir de 65€/mois ou 780€/an</li>
    <li><span class="u-color--primary">100 à 199 salarié.e.s :</span> CSD de 240€ + cotisation à partir de 100€/mois ou 1200€/an</li>
    <li><span class="u-color--primary">Plus de 200 salarié.e.s :</span> CSD de 240€ + cotisation à partir de 165€/mois ou 1980€/an</li>
</ul>
<br>
<b>Association :</b><br>
<ul class="u-inline-left u-pd-left u-pd-top-sm">
    <li><span class="u-color--primary">0 salarié :</span> cotisation à partir de 3€/mois ou 36€/an</li>
    <li><span class="u-color--primary">1 à 2 salarié.e.s :</span> cotisation à partir de 6€/mois ou 72€/an</li>
    <li><span class="u-color--primary">3 à 5 salarié.e.s :</span> cotisation à partir de 9€/mois ou 108€/an</li>
    <li><span class="u-color--primary">6 à 9 salarié.e.s :</span> cotisation à partir de 15€/mois ou 180€/an</li>
    <li><span class="u-color--primary">10 et plus salarié.e.s :</span> même grille que pour une entreprise</li>
</ul>
<br>
EOL,
                    'registration_price_grid_bottom_information' => <<<EOL
<div class="u-large u-mg-top">
    L'adhésion court sur l’année civile, du 31 janvier au 31 décembre. Vous adhérez en cours d’année ?
    Réglez l’adhésion au prorata (le mois entamé vous est offert) !
    Besoin d’aide ? Contactez-nous à <a class="c-link" href="mailto:pro@moneko.org" title="Contactez-nous">pro@moneko.org</a>.
</div>
EOL,
                    'membership_renewal_price_grid' => <<<EOL
Grille des cotisations :<br/>
<small>(Adoptée lors de l’Assemblée Générale du 28 novembre 2022)</small><br>
<br>
<b>Entreprise :</b><br>
<ul class="u-inline-left u-pd-left u-pd-top-sm">
    <li><span class="u-color--primary">0 salarié.e :</span> 48€/an (4€/mois) en cotisation classique ou 72€/an (6€/mois) en soutien</li>
    <li><span class="u-color--primary">1 à 2 salarié.e.s :</span> 84€/an (7€/mois) en cotisation classique ou 126€/an (10,50€/mois) en soutien</li>
    <li><span class="u-color--primary">3 à 5 salarié.e.s :</span> 120€/an (10€/mois) en cotisation classique ou 180€/an (15€/mois) en soutien</li>
    <li><span class="u-color--primary">6 à 9 salarié.e.s :</span> 180€/an (15€/mois) en cotisation classique ou 270€/an (22,50€/mois) en soutien</li>
    <li><span class="u-color--primary">10 à 19 salarié.e.s :</span> 300€/an (25€/mois) en cotisation classique ou 450€/an (37,50€/mois) en soutien </li>
    <li><span class="u-color--primary">20 à 49 salarié.e.s :</span> 480€/an (40€/mois) en cotisation classique ou 720€/an (60€/mois) en soutien</li>
    <li><span class="u-color--primary">50 à 99 salarié.e.s :</span> 780€/an (65€/mois) en cotisation classique ou 1170€/an (97,50€/mois) en soutien</li>
    <li><span class="u-color--primary">100 à 199 :</span> 1200€/an (100€/mois) en cotisation classique ou 1800€/an (150€/mois) en soutien</li>
    <li><span class="u-color--primary">+ de 200 :</span> 1980€/an (165€/mois) en cotisation classique ou 2970€/an (247,50€/mois) en soutien</li>
</ul>
<br>
<b>Association :</b><br>
<ul class="u-inline-left u-pd-left u-pd-top-sm">
    <li><span class="u-color--primary">0 salarié :</span> 36€/an (3€/mois) en cotisation classique ou 54€/an (4,50€/mois) en soutien</li>
    <li><span class="u-color--primary">1 à 2 salarié.e.s :</span> 72€/an (6€/mois) en cotisation classique ou 108€/an (9€/mois) en soutien</li>
    <li><span class="u-color--primary">3 à 5 salarié.e.s :</span> 108€/an (9€/mois) en cotisation classique ou 162€/an (13,50€/mois) en soutien</li>
    <li><span class="u-color--primary">6 à 9 salarié.e.s :</span> 180€/an (15€/mois) en cotisation classique ou 270€/an (22,50€/mois) en soutien</li>
    <li><span class="u-color--primary">10 et plus salarié.e.s :</span> même grille que les entreprises</li>
</ul>
<br>
EOL,
                    'membership_renewal_price_grid_bottom_information' => <<<EOL
<div class="u-large u-mg-top">
    <b>
        Parrainez un ou des professionnels : un nouveau professionnel adhère grâce à vous ?
        Moneko vous reverse 1/3 de son adhésion classique.
        Remboursez une partie de votre adhésion et contribuez à faire grandir le réseau !
    </b>
</div>
EOL,
                    'payment_bottom_information' => <<<EOL
<div class="u-large u-mg-top">
    <b>Bon à savoir !</b><br>
    <ul class="u-inline-left u-pd-left u-pd-top-sm">
        <li><b>Parrainage entre Professionnels :</b> pour chaque nouveau professionnel qui adhère à Moneko grâce à vous, Moneko vous reverse 1/3 de son adhésion classique. Remboursez une partie de votre adhésion et contribuez à faire grandir le réseau !</li>
        <li>Pour les entreprises, <b>la Contribution Solidaire de Développement* (CSD)</b> n’est réglée qu’une seule fois la 1ere année, ensuite vous réglez seulement la cotisation annuelle</li>
        <li>Vous préférez un <b>règlement mensualisé</b> ? Contactez-nous à <b><a class="c-link" href="mailto:comptablite@moneko.org" title="Contactez-nous">comptablite@moneko.org</a></b> pour le mettre en place</li>
        <li><b>Cette grille indique les montants minimums, libre à vous de soutenir davantage la monnaie locale selon vos souhaits et possibilités. Retrouvez les grilles tarifaires détaillées et toutes les informations (CSD, parrainage, etc.) sur <a class="c-link" href="https://moneko.org/tarifs" title="Consulter la page d'adhésion">la page d’adhésion du site moneko.org</a></b></li>
    </ul>
</div>
EOL,
                ],
            ],
        ],
        'email' => [
            'subject' => [
                'register' => 'Bienvenue dans la communauté Moneko !',
                'confirm-registered' => 'Confirmation de votre adhésion à Moneko !',
                'remind-register' => 'Rappel de validation d\'email pour l\'adhésion à Moneko',
                'renew-created' => 'Appel à réadhésion à votre monnaie locale Moneko',
                'renew-warning' => 'Dernier rappel pour réadhérer à Moneko',
                'new-created' => 'Appel à l\'adhésion à votre monnaie locale Moneko',
            ],
            'membership' => [
                'intro' => <<<EOL
<p class="c-p">
    Moneko circule au sein d’un réseau local qui rassemble des adhérent.e.s particuliers, professionnels et des collectivités engagé.e.s dans la transition écologique et solidaire en Loire-Atlantique.
    Grâce à vous Moneko s’est développée et le changement d'échelle initié depuis 2020 se concrétise.
    Aujourd’hui Moneko est l’une des monnaies locales les plus importantes et actives de France ! Ne nous arrêtons pas en si bon chemin...<br>
    <br>
    Après des semaines d'échanges et de consultation, l'Assemblée Générale de l'association du 28 novembre 2022 a validé une nouvelle grille tarifaire.
    Les objectifs sont simples : développer l’autonomie de l'association, la pérenniser ainsi que ses emplois, pour continuer à démultiplier l’impact de la monnaie locale au service des acteurs et actrices engagé.e. s du territoire.<br>
    <br>
    Tout cela grâce à vos échanges, achats et dépenses du quotidien et… à votre adhésion !
    Pour continuer à utiliser Moneko, soutenir son déploiement et renforcer son impact, nous vous invitons à renouveler votre adhésion pour cette nouvelle année civile.
</p>
EOL,
                'renew' => <<<EOL
<p class="c-p">
    Le renouvellement de votre adhésion est bien sûr possible en Moneko :
</p>
<ul class="c-p">
    <li>Moneko numériques : faire un paiement à un professionnel "Moneko Asso MLC44"</li>
    <li>Moneko coupons : à déposer ou envoyer à l’Association MLC44 - 8 rue Saint Domingue 44200 Nantes ou dans vos comptoirs de change</li>
</ul>
EOL,
                'price' => <<<EOL
<p class="c-p">
    <b>Pour les Particuliers :</b>
</p>
<ul class="c-p">
    <li>l'adhésion annuelle classique est à 12€</li>
    <li>l'adhésion de soutien à 24€ par an ou 2€ par mois payable par virement automatique en euros ou monekos</li>
    <li>l'adhésion de soutien au Projet solidarité jeunesse à 36€ par an ou 3€ par mois, 12€ seront reversés directement au développement du projet</li>
    <li>les personnes à faibles revenus ont la possibilité de payer un tarif social à 6€ sans justificatif</li>
</ul>
<p class="c-p">
    <b>Pour les Professionnels :</b> l'adhésion est adaptée à la taille de votre structure. Nous vous invitons à consulter les grilles tarifaires Entreprises et Associations.
</p>
EOL,
                'sponsorship' => <<<EOL
<p class="c-p">
    <b>Parrainage entre professionnels :</b> un nouveau professionnel adhère à Moneko (ou 2 ou plus!) grâce à vous ?
    Moneko vous reverse 1/3 de son adhésion classique. Remboursez une partie de votre adhésion et contribuez à faire grandir le réseau !<br>
    <br>
    <b>Option futée : 2 mois d'adhésion offerts sur l'année pour une réadhésion réglée ou engagée avant le 31 janvier.
    Vous pouvez retirer directement cette remise du montant de votre adhésion lors de votre règlement.</b><br>
    Calcul de la remise = montant d'adhésion selon votre nombre de salarié.e.s (voir la grille tarifaire) x 2/12.<br>
    Ex: une entreprise de 2 salariés qui règle une cotisation classique de 84€/Moneko bénéficiera d'une remise de 14€/mk.
    Son adhésion avant le 31 janvier sera réduite à 70€/Mk.<br>
    Vous préférez mensualiser le règlement de votre cotisation ? Indiquezle-nous à l'adresse <a href="mailto:comptabilite@moneko.org" title="Contactez-nous">comptabilite@moneko.org</a>.<br>
    <br>
    Retrouvez les grilles tarifaires particuliers, entreprises et associations et toutes les informations de réadhésion sur le site internet moneko.org, à la page des <a href="https://moneko.org/tarifs" title="Consulter notre grille tarifaire">tarifs d'adhésion Moneko</a>.
</p>
EOL,
            ],
            'footer' => [
                'contact' => 'ou appelez-nous au 07 66 87 76 60 ou venez nous rencontrer au Solilab 8 rue St Domingue 44200 NANTES.',
            ],
        ],
    ],
    'conversion-fee-rate' => 0,
    'emails' => [
        'contact_type' => App\Mail\Mail::CONTACT_LINK_EMAIL,
    ],
];
