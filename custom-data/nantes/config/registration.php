<?php

return [
    'available' => [
        'common' => [
            'comment' => true,
            'next_steps_infos' => false,
            'directory_infos' => false,
        ],
        'individual' => [
            'id_document' => true,
            'donate' => false,
        ],
        'professional' => [
            'siren' => false,
            'address' => true,
            'justification_documents' => true,
            'category' => false,
            'newsletter' => true,
            'association-membership' => true,
            'donate' => false,
        ],
    ],
];
