# SoMoney - App

## Table of Contents

* [Installation](#installation)
  * [Requirements](#requirements)
  * [Import and git setup](#import-and-git-setup)
  * [DNS setup](#dns-setup)
  * [### White label customization](#white-label-customization)
  * [Project configuration and dependencies installation](#project-configuration-and-dependencies-installation)
  * [White label customization](#white-label-customization)
* [Docker](#docker)
* [Database](#database)
* [Building resources](#building-resources)
* [Previewing emails](#previewing-emails)
* [IDE helper](#ide-helper)
* [Testing](#testing)
* [Debugging](#debugging)
* [Changelog](#changelog)

## Installation

### Requirements

* Latest Git stable version
* Latest Composer stable version
* Latest Docker Community Edition stable version: https://docs.docker.com/install
* Latest Node stable version
* Latest Yarn stable version

### Import and git setup

Clone the project from `git@gitlab.com:creditmunicipal-nantes/somoney-mon-compte.git`.

### DNS setup

Set your project domain resolution in your virtualhost : `sudo vim /etc/hosts`

```sh
# add these lines in your /etc/hosts file
127.0.0.1       moncompte.somoney.test
127.0.0.1       styleguide.somoney.test
```

### White label customization

This project is white-labelled and has specific configurations related to the existing instances:
* `brest`
* `nantes`

To configure it for one of the available instances declared in the `custom-data` folder, you'll have to:
* Copy each `.env.<instance>.example` file into `.env.<instance>` and configure it for your local environment needs
* Execute the following command: `make set-as-<instance>` that will override the `.env` file with the `.env.<instance` one and set the related configuration folder in the `custiom-data` directory

This command will also allow you to switch from one instance to another one when needed for local dev.

### Project configuration and dependencies installation 

First, execute the following commands on your host machine :

* `make set-as-nantes` or `make set-as-brest`
* `composer install --no-scripts --ignore-platform-reqs`
* `sail up -d`
* `sail composer update`
* `sail artisan key:generate`
* `sail artisan storage:link`
* `sail artisan migrate:refresh --seed`
* `yarn install`
* `yarn upgrade`
* `yarn dev` or `yarn watch`

## Docker

This project uses a Docker local development environment provided by [Laravel Sail](https://laravel.com/docs/sail).

See how to use it on the [official documentation](https://laravel.com/docs/sail#executing-sail-commands).

## Database

Execute a database reset with the following command: `sail artisan migrate:refresh --seed`. This will execute all
migrations and seeds.

## Building resources

Compile all your project resources (mainly sass and javascript) by executing the following commands. You can run these
commands from your docker workspace, Node and Yarn are being installed. However, running them from your host machine
will be quicker.

* `yarn dev` (does merge but does not minify resources).
* `yarn prod` (does merge **and** minify).
* `yarn watch` (recompile automatically when a change is being detected in a resource file).

## Previewing emails

Laravel Sail provides Mailhog to intercept and render emails in a web interface.

You can access the Mailhog interface at http://localhost:8025 when Laravel Sail is running.

## IDE helper

To manually generate IDE helper files, run the following command: `sail composer ide-helper`.

## Testing

* To launch the project test, run the following command: `sail composer test`.

## Debugging

Laravel Telescope and Laravel Horizon are pre-installed:

* To open the `telescope` dashboard, add `/telescope` to the base URL

## Changelog

See the [CHANGELOG](CHANGELOG.md) for more information on what has been pushed in production.
