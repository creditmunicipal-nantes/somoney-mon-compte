<row>
    <columns>
        <spacer size="10"></spacer>
        @if($showSocialLinks)
            <center>
                <p class="c-p">
                    Pour suivre les actus {{ config('somoney.money') }}, retrouvez-nous sur
                    <a href="{{ getProtocol().config('somoney.domain.landing') }}">{{ config('somoney.domain.landing') }}</a>
                    et sur les réseaux sociaux de l’association.
                </p>
                <menu align="center">
                    @if(config('somoney.url.facebook'))
                        <item href="{{ config('somoney.url.facebook') }}">
                            <img src="{{ asset('assets/img/email/facebook.png') }}" alt="Facebook">
                        </item>
                    @endif
                    @if(config('somoney.url.twitter'))
                        <item href="{{ config('somoney.url.twitter') }}">
                            <img src="{{ asset('assets/img/email/twitter.png') }}" alt="Twitter">
                        </item>
                    @endif
                    @if(config('somoney.url.instagram'))
                        <item href="{{ config('somoney.url.instagram') }}">
                            <img src="{{ asset('assets/img/email/instagram.png') }}" alt="Instagram">
                        </item>
                    @endif
                    @if(config('somoney.url.youtube'))
                        <item href="{{ config('somoney.url.youtube') }}">
                            <img src="{{ asset('assets/img/email/youtube.png') }}" alt="Youtube">
                        </item>
                    @endif
                    @if(config('somoney.url.linkedin'))
                        <item href="{{ config('somoney.url.linkedin') }}">
                            <img src="{{ asset('assets/img/email/linkedin.png') }}" alt="Linkedin">
                        </item>
                    @endif
                </menu>
            </center>

            <spacer size="20"></spacer>
        @endif
        <p class="c-p">
            @if($contactType === \App\Mail\Mail::LABEL_CONTACT_QUESTION)
                Pour toute question, écrivez-nous à
            @else
                Pour toute information :
            @endif
            @if(config('somoney.emails.contact_type') === \App\Mail\Mail::CONTACT_LINK_URL)
                <a href="{{ config('somoney.url.contact') }}"
                   target="_blank">{{ config('somoney.url.contact') }}
                </a>
            @else
                <a href="mailto:{{ config('somoney.contact.email') }}">{{ config('somoney.contact.email') }}</a>
                {{ config('somoney.content.email.footer.contact') }}
            @endif.
        </p>
        <p class="c-p">
            @switch($closingType)
                @case(\App\Mail\Mail::CLOSING_SIMPLE)
                Merci et à bientôt,
                @break
                @case(\App\Mail\Mail::CLOSING_ENGAGEMENT)
                Merci pour votre engagement et à bientôt,
                @break
                @case(\App\Mail\Mail::CLOSING_SUPPORT)
                Merci d’avance pour votre engagement et votre soutien !
                @break
            @endswitch
        </p>
        <spacer size="20"></spacer>
        <p class="c-p">Le Collectif d’Orientation citoyen de {{ config('somoney.money') }}</p>
        <spacer size="40"></spacer>
    </columns>
</row>
