@if($showDirectoryLink)
    <p class="c-p">
        Pour découvrir les professionnels membres du réseau, <a href="{{ getProtocol().config('somoney.domain.directory') }}">rendez-vous sur l’annuaire {{ config('somoney.money') }}</a>
        et votre application {{ config('somoney.money') }}. Vos commerçants et artisans préférés n’y figurent pas encore ?
        Dites-le-nous par mail ou suivez-nous sur les réseaux sociaux.
    </p>

    <spacer size="20"></spacer>
@endif

@if($showFacebookLikePage)
    <p class="c-p">
        <a href="{{ config('somoney.url.facebook') }}" target="_blank">Aimez notre page Facebook</a>
    </p>
@endif
