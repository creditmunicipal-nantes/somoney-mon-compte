@if(isset($isAlreadyRegistered))
    <p class="c-p">
        Pour vos paiements, utilisez l’application {{ config('somoney.money') }} téléchargeable ici
    </p>
@else
    <p class="c-p">
        N'oubliez pas de télécharger l'application {{ config('somoney.money') }} pour effectuer vos paiements
    </p>
@endif

<menu align="center">
    @if(config('somoney.url.application.android'))
        <item href="{{ config('somoney.url.application.android') }}">
            <img src="{{ asset('assets/img/email/google-play-badge.png') }}"
                 alt="Télécharger l'application Android">
        </item>
    @endif
    @if(config('somoney.url.application.apple'))
        <item href="{{ config('somoney.url.application.apple') }}">
            <img src="{{ asset('assets/img/email/app-store-badge.png') }}"
                 alt="Télécharger l'application IOS">
        </item>
    @endif
</menu>

<spacer size="20"></spacer>