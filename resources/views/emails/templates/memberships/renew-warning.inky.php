@extends('emails.layout')

@section('content')

    <row>
        <columns>
            <p class="c-p">Bonjour {{ $username }},</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Votre adhésion à l’association {{ config('somoney.asso') }}, porteuse de la monnaie
                locale {{ config('somoney.money') }} n’est plus à jour.
                <br/>
                Le monnaie locale circule au sein de son réseau d'adhérents, particulier et professionnels.Si votre
                adhésion n’est pas renouvelée d’ici là, nous vous informons que votre compte
                {{ config('somoney.money') }} sera bloqué le 01/07 prochain.
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Pour renouveler votre adhésion pour l’année civile en cours, c'est très simple :
            </p>

            <center>
                <spacer size="20"></spacer>
                <button href="{{ route('memberships.renew.index', ['membershipRenewToken' => $userToken->id]) }}"
                        class="c-btn c-btn--primary text-center">
                    Renouveler mon adhésion
                </button>
                <spacer size="20"></spacer>
            </center>

            <spacer size="20"></spacer>

            <p class="c-p">
                Le renouvellement de votre adhésion peut aussi se faire en {{ config('somoney.money') }} numériques ou coupons.
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                <a href="{{ route('account.information', ['leaveService' => true]) }}">Demander à quitter {{ config('somoney.money') }}</a>
            </p>

            @if($showDirectoryLink || $showFacebookLikePage)
                @include('emails.partials.infos')
            @endif
        </columns>
    </row>

@stop
