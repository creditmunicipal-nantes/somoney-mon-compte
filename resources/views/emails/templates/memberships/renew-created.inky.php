@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">Bonjour {{ $username }},</p>
            <spacer size="20"></spacer>
            <p class="c-p">
                Votre adhésion annuelle à l'association {{ config('somoney.asso') }},
                qui porte votre monnaie locale {{ config('somoney.money') }}, est arrivée à son terme.
            </p>
            @if(config('somoney.content.email.membership.intro'))
                <spacer size="20"></spacer>
                <p class="c-p">
                    {!! config('somoney.content.email.membership.intro') !!}
                </p>
            @endif
            <center>
                <spacer size="20"></spacer>
                <button href="{{ route('memberships.renew.index', ['membershipRenewToken' => $userToken->id]) }}"
                        class="c-btn c-btn--primary text-center">
                    Renouveler mon adhésion
                </button>
                <spacer size="20"></spacer>
            </center>
            @if(config('somoney.content.email.membership.renew'))
                <spacer size="20"></spacer>
                {!! config('somoney.content.email.membership.renew') !!}
            @endif
            @if(config('somoney.content.email.membership.price'))
                <spacer size="20"></spacer>
                {!! config('somoney.content.email.membership.price') !!}
            @endif
            @if(config('somoney.content.email.membership.sponsorship'))
                <spacer size="20"></spacer>
                {!! config('somoney.content.email.membership.sponsorship') !!}
            @endif
            <spacer size="20"></spacer>
            <p class="c-p">
                <a href="{{ route('account.information', ['leaveService' => true]) }}">Demander à quitter {{ config('somoney.money') }}</a>
            </p>
            <spacer size="20"></spacer>
            @if($showDirectoryLink || $showFacebookLikePage)
                @include('emails.partials.infos')
            @endif
        </columns>
    </row>

@stop
