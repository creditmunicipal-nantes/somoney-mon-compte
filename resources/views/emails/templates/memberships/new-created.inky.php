@extends('emails.layout')

@section('content')

    <row>
        <columns>
            <p class="c-p">Bonjour {{ $username }},</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Votre adhésion annuelle à l'association {{ config('somoney.asso') }}, qui porte votre
                monnaie locale {{ config('somoney.money') }}, n'est pas à jour !
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                La monnaie locale circule au sein de son réseau d'adhérents, particuliers et professionnels. Pour
                continuer à utiliser {{ config('somoney.money') }} et soutenir son déploiement, nous vous invitons à
                renouveler votre adhésion pour cette nouvelle année civile :
            </p>

            <center>
                <spacer size="20"></spacer>
                <button href="{{ route('memberships.renew.index', ['membershipRenewToken' => $userToken->id]) }}"
                        class="c-btn c-btn--primary text-center">
                    Mettre à jour mon adhésion
                </button>
                <spacer size="20"></spacer>
            </center>

            @if(config('somoney.content.email.membership.renew'))
                <spacer size="20"></spacer>
                <p class="c-p">
                    {!! config('somoney.content.email.membership.renew') !!}
                </p>
            @endif

            @if(config('somoney.content.email.membership.price'))
                <spacer size="20"></spacer>
                <p class="c-p">
                    {!! config('somoney.content.email.membership.price') !!}
                </p>
            @endif

            <spacer size="20"></spacer>

            <p class="c-p">
                <a href="{{ route('account.information', ['leaveService' => true]) }}">Demander à quitter {{ config('somoney.money') }}</a>
            </p>

            <spacer size="20"></spacer>

            @if($showDirectoryLink || $showFacebookLikePage)
                @include('emails.partials.infos')
            @endif
        </columns>
    </row>

@stop
