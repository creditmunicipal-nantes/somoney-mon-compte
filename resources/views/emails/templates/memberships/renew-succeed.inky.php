@extends('emails.layout')

@section('content')

    <row>
        <columns>
            <p class="c-p">Bonjour {{ $username }},</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Nous vous confirmons votre réadhésion à {{ config('somoney.money') }}. Félicitations !
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Toute l’équipe vous remercie chaleureusement pour votre fidélité et la poursuite de
                votre action citoyenne vos achats en monnaie locale ! Grâce à vous, {{ config('somoney.money') }} peut
                continuer à se déployer et favoriser une économie locale plus durable et solidaire sur notre territoire.
            </p>

            <center>
                <spacer size="20"></spacer>
                <button href="{{ route('auth.login') }}"
                        class="c-btn c-btn--primary text-center">
                    Me connecter
                </button>
                <spacer size="20"></spacer>
            </center>

            <br>

            <p class="c-p">
                Vous souhaitez créditer votre compte {{ config('somoney.money') }} numérique ?
            </p>

            <center>
                <spacer size="20"></spacer>
                <button href="{{ route('operations.credit.index') }}"
                        class="c-btn c-btn--primary text-center">
                    Créditer mon compte
                </button>
                <spacer size="20"></spacer>
            </center>

            @if(config('somoney.url.application.android') || config('somoney.url.application.apple'))
                @include('emails.partials.applications', ['isAlreadyRegistered' => true])
            @endif

            @if(config('somoney.url.counters'))
                <spacer size="20"></spacer>

                <p class="c-p">
                    Vous souhaitez utiliser les {{ config('somoney.money') }} papier ? rendez-vous dans les comptoirs de
                    change du réseau. (
                    <a href="{{ config('somoney.url.counters') }}">{{ config('somoney.url.counters') }}</a>
                    )
                </p>
            @endif

            <br>

            @if($showDirectoryLink || $showFacebookLikePage)
                @include('emails.partials.infos')
            @endif
        </columns>
    </row>

@stop
