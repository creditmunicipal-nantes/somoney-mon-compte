@extends('emails.layout')

@section('content')

    <row>
        <columns>
            <p class="c-p">Bonjour {{ $username }},</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Pour rappel votre adhésion à l'association {{ config('somoney.asso') }}, porteuse de la monnaie
                locale {{ config('somoney.money') }} n’est plus à jour.
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Pour renouveler votre adhésion pour l’année civile en cours et continuer à utiliser
                {{ config('somoney.money') }}, c’est très simple ! Veuillez cliquer sur le bouton ci-dessous :
            </p>

            <center>
                <spacer size="20"></spacer>
                <button href="{{ route('memberships.renew.index', ['membershipRenewToken' => $userToken->id]) }}"
                        class="c-btn c-btn--primary text-center">
                    Renouveler mon adhésion
                </button>
                <spacer size="20"></spacer>
            </center>

            @if(config('somoney.content.email.membership.renew'))
                <spacer size="20"></spacer>
                <p class="c-p">
                    {!! config('somoney.content.email.membership.renew') !!}
                </p>
            @endif

            @if(config('somoney.content.email.membership.price'))
                <spacer size="20"></spacer>
                <p class="c-p">
                    {!! config('somoney.content.email.membership.price') !!}
                </p>
            @endif

            @if($showDirectoryLink || $showFacebookLikePage)
                @include('emails.partials.infos')
            @endif
        </columns>
    </row>

@stop
