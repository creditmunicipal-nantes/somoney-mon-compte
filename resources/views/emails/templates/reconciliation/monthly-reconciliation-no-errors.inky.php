@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">Bonjour</p>

            <spacer size="20"></spacer>

            <p class="c-p">Le traitement du fichier {{ $fileName }} s’est déroulé sans erreur.</p>

            <spacer size="20"></spacer>

            <p class="c-p">Cordialement,</p>

        </columns>
    </row>
@stop
