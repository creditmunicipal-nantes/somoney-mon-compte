@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">Bonjour</p>

            <spacer size="20"></spacer>

            <p class="c-p">Le traitement du fichier {{ $fileName }} s’est déroulé avec des erreurs.</p>

            <spacer size="20"></spacer>

            <p class="c-p">Erreurs :</p>

            <ul class="c-p">
                @foreach ($errors as $error)
                    <li class="c-p">{{ $error['name'] }} (Ligne {{ $error['line'] }})</li>
                @endforeach
            </ul>

            <spacer size="20"></spacer>

            <p class="c-p">Cordialement,</p>

        </columns>
    </row>
@stop
