@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">Bonjour</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Vous avez fait une demande de crédit automatique d'un montant
                de {{ $sepa->amount }} {{ config('somoney.money') }} par mois, pour une durée
                de {{ $sepa->duration_month }} mois
            </p>

            <center>
                <button href="{{ route('operations.credit.automatic.confirm', ['sepa' => $sepa->id]) }}"
                        class="c-btn c-btn--primary text-center">Confirmer la demande
                </button>
                <spacer size="20"></spacer>
                <button href="{{ route('operations.credit.automatic.cancel', ['sepa' => $sepa->id]) }}"
                        class="c-btn c-btn--primary text-center">Annuler la demande
                </button>
            </center>

            <spacer size="20"></spacer>

            <p class="c-p">Cordialement,</p>
        </columns>
    </row>
@stop
