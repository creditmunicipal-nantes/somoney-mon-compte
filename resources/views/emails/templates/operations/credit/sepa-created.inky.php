@extends('emails.layout')

@section('content')

    <row>
        <columns>
            <p class="c-p">Bonjour</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                <a href="{{ route('admin.users.show',['id'=> $sepa->user_id]) }}">L'utilisateur {{ $username }}</a>
                a fait une demande de prélèvement SEPA pour {{ $sepa->amount }} {{ config('somoney.money') }}.
            </p>

            @if($sepa->is_recurrent)
                <p class="c-p">
                    Ce prélèvement est un crédit automatique sur {{ $sepa->duration_month }} mois
                </p>
            @endif

            <p class="c-p">
                Retrouvez sa demande sur
                <a href="{{ route('admin.operations.sepas') }}">la page liste des demandes SEPA</a>
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">Cordialement,</p>
        </columns>
    </row>
@stop
