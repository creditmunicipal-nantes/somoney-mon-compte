@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">
                {{ $amount }} {{ config('somoney.money') }}
                vont être virés sur votre compte de la part
                de {{ $fromUserName }} pour un premier versement
                le {{ $transferDetails["date"] }} avec une
                récurrence {{ $transferDetails["frequency"] }},
                jusqu'au {{ $transferDetails['date_end'] }}.
            </p>

            <p class="c-p">
                Commentaire : {{ $transferDetails["label"] }}
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Vous pourrez consulter cette opération dans votre espace et consulter
                <a href="{{ getProtocol().config('somoney.domain.directory') }}" target="_blank">l’annuaire</a>
                pour savoir où les réutiliser.
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                <a href="{{ route('home') }}" target="_blank">Consulter mon compte</a>
            </p>
        </columns>
    </row>
@stop
