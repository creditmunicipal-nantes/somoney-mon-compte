@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">
                Votre virement de {{ $amount }} {{ config('somoney.money') }}
                @if($isScheduled)
                sera envoyé
                @else
                a bien été envoyé
                @endif
                le {{ $transferDetails["date"] }} à {{ $transferDetails["hour"] }} à {{ $toUserName }}.
            </p>

            <p class="c-p">
                Commentaire : {{ $transferDetails["label"] }}
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">Nous avons informé {{ $toUserName }} par e-mail.</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Vous pouvez consulter cette opération dans votre espace
                @if($isScheduled)
                    <a href="{{ route('operations.transfer.index') }}" target="_blank">mes virements</a>.
                @else
                    <a href="{{ route('home') }}" target="_blank">mon compte</a>.
                @endif
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Si vous souhaitez inviter votre entourage pour pouvoir échanger des {{ config('somoney.money') }} avec
                eux à l’avenir rien de plus simple !
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                <a href="{{ route('sponsor') }}" target="_blank">
                    Envoyer des invitations à rejoindre {{ config('somoney.money') }}
                </a>
            </p>
        </columns>
    </row>

@stop
