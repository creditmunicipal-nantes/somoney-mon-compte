@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">
                Votre virement de {{ $amount }} {{ config('somoney.money') }} a bien été planifié pour
                le {{ $transferDetails["date"] }} à {{ $toUserName }} avec une
                récurrence {{ $transferDetails["frequency"] }}, jusqu'au {{ $transferDetails['date_end'] }}.
            </p>

            <p class="c-p">
                Commentaire : {{ $transferDetails["label"] }}
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">Nous avons informé {{ $toUserName }} par mail.</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Vous pouvez consulter cette opération dans votre espace
                <a href="{{ route('home') }}" target="_blank">mon compte</a>
                .
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Si vous souhaitez inviter votre entourage pour pouvoir échanger des {{ config('somoney.money') }} avec
                eux à l’avenir rien de plus simple !
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                <a href="{{ route('sponsor') }}" target="_blank">
                    Envoyer des invitations à rejoindre {{ config('somoney.money') }}
                </a>
            </p>
        </columns>
    </row>

@stop
