@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">
                {{ $amount }} {{ config('somoney.money') }}
                @if($isScheduled)
                    sera virés, le {{ $transferDetails["date"] }} à {{ $transferDetails["hour"] }},
                @else
                    ont été virés
                @endif
                    sur votre compte de la part de {{ $fromUserName }}.
            </p>

            <p class="c-p">
                Commentaire : {{ $transferDetails["label"] }}
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Vous pourrez consulter cette opération dans votre espace
                <a href="{{ route('home') }}" target="_blank">mon compte</a>
                .
                @if($isScheduled)
                à partir du {{ $transferDetails['date'] }} à {{ $transferDetails["hour"] }}
                @endif
                ou consulter
                <a href="{{ getProtocol().config('somoney.domain.directory') }}" target="_blank">l’annuaire</a>
                pour savoir où les réutiliser.
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                <a href="{{ route('home') }}" target="_blank">Consulter mon compte</a>
            </p>
        </columns>
    </row>
@stop
