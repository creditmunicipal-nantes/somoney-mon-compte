@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">Bonjour</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Je vous invite à me régler en {{ config('somoney.money') }} concernant {{ $title }}
                pour {{ $amount }} {{ config('somoney.money') }} afin de favoriser ensemble l’économie locale.
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Il vous suffit de vous connecter à votre compte {{ config('somoney.money') }} ou de vous inscrire.
            </p>

            <spacer size="20"></spacer>

            <button href="{{ route('register') }}" class="c-btn c-btn--primary text-center">
                Rejoindre {{ config('somoney.money') }}
            </button>

            <button href="{{ route('account.information') }}" class="c-btn c-btn--primary text-center">
                Aller sur mon compte
            </button>

            <spacer size="20"></spacer>

            <p class="c-p">Cordialement,</p>

            <spacer size="20"></spacer>

            <p class="c-p">{{ $username }}</p>
        </columns>
    </row>

@stop
