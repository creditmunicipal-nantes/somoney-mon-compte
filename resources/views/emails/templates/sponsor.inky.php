@extends('emails.layout')

@section('content')

    <row>
        <columns>

            <p class="c-p">Bonjour {{ $senderName }} !</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                J’ai adhéré à la monnaie locale {{ config('somoney.money') }} pour favoriser l’économie locale et parce
                que je partage les valeurs de développement durable qu’elle porte.
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">{{ $content }}</p>

            @if ($status === 'individual')
                <center>
                    <spacer size="20"></spacer>
                    <button href="{{ route('register') }}" class="c-btn c-btn--primary text-center">
                        Rejoindre {{ config('somoney.money') }}</button>
                    <spacer size="20"></spacer>
                </center>
            @else
                <spacer size="20"></spacer>

                <p class="c-p">
                    Je vous invite à rejoindre la communauté {{ config('somoney.money') }} en contactant un conseiller.
                </p>

                <center>
                    <spacer size="20"></spacer>
                    <button href="{{ route('register') }}" class="c-btn c-btn--primary text-center">
                        Rejoindre {{ config('somoney.money') }}
                    </button>
                    <spacer size="20"></spacer>
                </center>
            @endif

            <p class="c-p">
                Pour plus d’information rendez-vous sur
                <a href="{{ route('home') }}" target="_blank">{{ config('somoney.money') }}</a>
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">Belle journée,</p>

            <spacer size="20"></spacer>

            <p class="c-p">{{ $username }}</p>

        </columns>
    </row>

@stop
