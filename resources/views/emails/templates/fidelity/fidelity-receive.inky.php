@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">

                Vous avez profité de {{ $amount }} {{ config('somoney.money') }} de réduction de la part de {{ $fromUserName
            }}.
                Merci pour votre fidélité !
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Vous pouvez consulter cette opération dans votre espace
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Si vous souhaitez inviter votre entourage pour pouvoir échanger des {{ config('somoney.money') }} avec
                eux à l’avenir rien de plus simple !
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                <a href="{{ route('sponsor') }}" target="_blank">
                    Envoyer des invitations à rejoindre {{ config('somoney.money') }}
                </a>
            </p>
        </columns>
    </row>
@stop
