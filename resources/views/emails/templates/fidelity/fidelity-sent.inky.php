@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">

                {{ $toUserName }} à profité de {{ $amount }} {{ config('somoney.money') }} de réduction grâce à votre
                                  programme de
                                  fidélité
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Vous pouvez consulter cette opération dans votre espace
            </p>

            <spacer size="20"></spacer>

        </columns>
    </row>
@stop
