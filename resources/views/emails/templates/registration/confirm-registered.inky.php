@extends('emails.layout')

@section('content')

    <row>
        <columns>
            <p class="c-p">Bonjour,</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Nous vous confirmons votre adhésion à {{ config('somoney.money') }}.
            </p>
            <p class="c-p">
                Félicitations ! Grâce à vous, nous faisons un pas de plus vers une économie locale plus durable et
                solidaire
            </p>
            <p class="c-p">
                Vous pouvez maintenant vous connecter à votre compte
            </p>

            <center>
                <spacer size="20"></spacer>
                <button href="{{ route('auth.login') }}"
                        class="c-btn c-btn--primary text-center">
                    Me connecter
                </button>
                <spacer size="20"></spacer>
            </center>

            <spacer size="20"></spacer>

            <p class="c-p">
                Vous souhaitez commencer à régler vos achats en {{ config('somoney.money') }} numérique ?
                Vous pouvez créditer votre compte dès à présent
            </p>

            <center>
                <spacer size="20"></spacer>
                <button href="{{ route('operations.credit.index') }}"
                        class="c-btn c-btn--primary text-center">
                    Créditer mon compte
                </button>
                <spacer size="20"></spacer>
            </center>

            @if(config('somoney.url.application.android') || config('somoney.url.application.apple'))
                @include('emails.partials.applications', ['isAlreadyRegistered' => false])
            @endif

            @if(config('somoney.url.counters'))
                <spacer size="20"></spacer>

                <p class="c-p">
                    Vous souhaitez obtenir des {{ config('somoney.money') }} papier ?
                    <a href="{{ config('somoney.url.counters') }}">
                        Rendez-vous dans les comptoirs de change du réseau.
                    </a>
                </p>
            @endif

            <br>

            @if($showDirectoryLink || $showFacebookLikePage)
                @include('emails.partials.infos')
            @endif
        </columns>
    </row>

@stop
