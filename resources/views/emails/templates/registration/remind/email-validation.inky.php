@extends('emails.layout')

@section('content')

    <row>
        <columns>
            <p class="c-p">Bonjour {{ $username }},</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Ceci est un e-mail de rappel pour
                @if($userType === 'individual' && config('somoney.available.services.membership'))
                continuer
                @else
                valider
                @endif
                votre inscription et
                confirmer votre adresse e-mail, il suffit de cliquer sur le lien suivant :
            </p>

            <center>
                <spacer size="20"></spacer>
                <button href="{{ route('register.'.$userType.'.validation', ['emailValidationToken' => $token->id]) }}"
                        class="c-btn c-btn--primary text-center">
                    Confirmer mon e-mail
                </button>
                <spacer size="20"></spacer>
            </center>

            <br>

            @if($showDirectoryLink || $showFacebookLikePage)
                @include('emails.partials.infos')
            @endif
        </columns>
    </row>

@stop
