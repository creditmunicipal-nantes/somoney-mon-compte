@extends('emails.layout')

@section('content')

    <row>
        <columns>
            <p class="c-p">Bonjour {{ $name }},</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Vous recevez cet email car votre inscription n'est pas terminée.
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Pour la terminer, vous devez adhérer à l'association portant {{ config('somoney.money') }} en cliquant sur
                le bouton ci-dessous
            </p>

            <center>
                <spacer size="20"></spacer>
                <button href="{{ route('register.individual.need_membership', ['needMembershipToken' => $token->id]) }}"
                        class="c-btn c-btn--primary text-center">
                    Adhérer à l'association
                </button>
                <spacer size="20"></spacer>
            </center>

            <br>

            @if($showDirectoryLink || $showFacebookLikePage)
                @include('emails.partials.infos')
            @endif
        </columns>
    </row>

@stop
