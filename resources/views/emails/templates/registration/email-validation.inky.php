@extends('emails.layout')

@section('content')

<row>
    <columns>
        <p class="c-p">Bonjour {{ $username }} et bienvenue dans la communauté {{ config('somoney.money') }} !</p>

        <spacer size="20"></spacer>

        <p class="c-p">
            Toute l’équipe vous remercie chaleureusement pour votre démarche d’inscription à la monnaie locale de
            {{ config('somoney.region') }}. Grâce à vous, nous faisons un pas de plus vers une
            économie locale plus durable et plus solidaire.
        </p>

        <p class="c-p">
            Pour valider votre inscription, confirmez simplement votre adresse e-mail en cliquant sur le lien suivant
        </p>

        <center>
            <spacer size="20"></spacer>
            <button href="{{ route('register.'.$userType.'.validation', ['emailValidationToken' => $token->id]) }}"
                    class="c-btn c-btn--primary text-center">
                Confirmer mon e-mail
            </button>
            <spacer size="20"></spacer>
        </center>

        <br>

        @if($showDirectoryLink || $showFacebookLikePage)
            @include('emails.partials.infos')
        @endif
    </columns>
</row>

@stop
