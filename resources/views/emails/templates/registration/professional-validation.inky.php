@extends('emails.layout')

@section('content')

    <row>
        <columns>
            <p class="c-p">Bonjour {{ $username }} et bienvenue dans la communauté {{ config('somoney.money') }} !</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Toute l'équipe vous félicite et vous remercie chaleureusement pour votre démarche d'inscription.
                Grâce à vous, nous faisons un pas de plus vers une économie plus durable, plus locale et plus
                humaine.
            </p>

            <spacer size="20"></spacer>

            @if($showDirectoryLink || $showFacebookLikePage)
                @include('emails.partials.infos')
            @endif
        </columns>
    </row>

@stop
