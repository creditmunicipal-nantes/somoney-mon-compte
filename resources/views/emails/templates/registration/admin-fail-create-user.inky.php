@extends('emails.layout')

@section('content')

    <row>
        <columns>
            <p class="c-p">Bonjour,</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Vous recevez cet email car l'inscription de {{ $localUser->name ?? $localUser->email }} à échoué.
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                @foreach($errors as $error)
                    - {{ $error }}<br/>
                @endforeach
            </p>

            <br>
        </columns>
    </row>

@stop
