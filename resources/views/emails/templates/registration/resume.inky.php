@extends('emails.layout')

@section('content')

    <row>
        <columns>
            <p class="c-p">Bonjour,</p>

            <spacer size="20"></spacer>

            <p class="c-p">Vous recevez cette e-mail car vous avez essayer de vous inscrire sur
                <a href="{{ getProtocol().config('somoney.domain.landing') }}">{{ config('somoney.money') }}</a>
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Cependant,vous avez une inscription en cours, vous pouvez la continuer en cliquant sur
                le bouton ci-dessous
            </p>

            <center>
                <spacer size="20"></spacer>
                <button href="{{ route('register.'.$userType.'.resume', ['resumeRegistrationToken' => $token->id]) }}"
                        class="c-btn c-btn--primary text-center">
                    Continuer l'inscription
                </button>
                <spacer size="20"></spacer>
            </center>

            <br>

            @if($showDirectoryLink || $showFacebookLikePage)
                @include('emails.partials.infos')
            @endif
        </columns>
    </row>

@stop
