@extends('emails.layout')

@section('content')

    <row>
        <columns>
            <p class="c-p">Bonjour {{ $localUser->name }},</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Nous vous informons que la finalisation de votre inscription à échoué.<br/>
                Vous serez contacté prochainement par les administrateur.
            </p>

            <spacer size="20"></spacer>

            <br>

            @if($showDirectoryLink || $showFacebookLikePage)
                @include('emails.partials.infos')
            @endif
        </columns>
    </row>

@stop
