@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">Bonjour</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                L'utilisateur {{ $user['name'] }} a demandé à quitter {{ config('somoney.money') }}.
            </p>

            <center>
                <spacer size="20"></spacer>
                <button href="{{ route('admin.users.leaving.index') }}"
                        class="c-btn c-btn--primary text-center">
                    Réaliser la procédure
                </button>
                <spacer size="20"></spacer>
            </center>

            <spacer size="20"></spacer>

            <p class="c-p">Cordialement,</p>
        </columns>
    </row>
@stop
