@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">Bonjour</p>

            <spacer size="20"></spacer>

            <p class="c-p">L'utilisateur {{ $user['name'] }} a bloqué son compte.
                <a href="{{ config('services.cyclos.uri') }}/#users.users.search|users.users.profile!id={{ $id }}">
                    Vous pouvez débloquer le compte de l'utilisateur sur Cyclos
                </a>
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">Cordialement,</p>
        </columns>
    </row>
@stop
