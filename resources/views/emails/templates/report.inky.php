@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">
                Bonjour,
            </p>
            <spacer size="20"></spacer>
            <p class="c-p">
                Voici la liste des inscriptions et opérations réalisées par {{ config('somoney.money') }} en date
                du {{ date('d/m/Y') }}.
            </p>
            <center>
                <spacer size="20"></spacer>
                <button href="{{ route('admin.operations.historic.export') }}" class="c-btn c-btn--primary text-center">
                    Liste opérations
                </button>
                <spacer size="20"></spacer>
            </center>
            <center>
                <spacer size="20"></spacer>
                <button href="{{ route('admin.users.xls') }}" class="c-btn c-btn--primary text-center">
                    Liste utilisateurs
                </button>
                <spacer size="20"></spacer>
            </center>
            <p class="c-p">
                Belle journée,
            </p>
            <spacer size="20"></spacer>
            <p class="c-p">
                L’équipe {{ config('somoney.money') }}
            </p>
        </columns>
    </row>
@stop
