@extends('emails.layout')

@section('content')

    <row>
        <columns>

            <p class="c-p">Bonjour {{ $user->name }} !</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                @if($type === config('auth.passwords.reset.forgot'))
                    Bon, on ne vous jette pas la pierre : on n’oublie nous-même régulièrement nos mots de passe…
                @else
                    Vous recevez cet e-mail car votre mot de passe est expiré et que vous souhaitez renseigner le
                    nouveau
                @endif
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">Pour changer votre nouveau mot de passe, cliquez sur le lien suivant :</p>

            <center>
                <spacer size="20"></spacer>
                <button href="{{ route('password.change', [$token->id]) }}"
                        class="c-btn c-btn--primary text-center">
                    Changer mon mot de passe
                </button>
                <spacer size="20"></spacer>
            </center>

            @if($type === config('auth.passwords.reset.forgot'))
                <p class="c-p">On se revoit dans quelques jours quand vous l’aurez à nouveau oublié ;)</p>

                <spacer size="20"></spacer>
            @endif

            <p class="c-p">L’équipe {{ config('somoney.money') }}</p>

        </columns>
    </row>
@stop
