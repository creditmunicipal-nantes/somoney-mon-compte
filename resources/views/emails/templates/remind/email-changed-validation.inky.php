@extends('emails.layout')

@section('content')

    <row>
        <columns>
            <p class="c-p">Bonjour {{ $username }},</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Ceci est un e-mail de rappel vous invitant à confirmer votre adresse e-mail. Pour cela, il suffit de
                cliquer sur le lien suivant :
            </p>

            <center>
                <spacer size="20"></spacer>
                <button href="{{ route('auth.validate.email', ['emailValidationToken' => $token->id]) }}"
                        class="c-btn c-btn--primary text-center">
                    Confirmer mon adresse e-mail
                </button>
                <spacer size="20"></spacer>
            </center>

            <br>

            @if($showDirectoryLink || $showFacebookLikePage)
                @include('emails.partials.infos')
            @endif
        </columns>
    </row>

@stop
