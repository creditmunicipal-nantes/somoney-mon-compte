@extends('emails.layout')

@section('content')

    <row>
        <columns>
            <h1>{{ $newsletter->title }}</h1>

            <spacer size="20"></spacer>

            <div class="newsletter-image">
                @if($illustration = $newsletter->getFirstMedia('newsletterImage'))
                    {!! $illustration->img() !!}
                @endif
            </div>
            <spacer size="20"></spacer>

            <p>{{ $newsletter->content }}</p>

            <spacer size="20"></spacer>

            <a href="{{ route('newsletter.unsubscribe') }}">Me désabonner</a>

            <br>
        </columns>
    </row>
@stop
