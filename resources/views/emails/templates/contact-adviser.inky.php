@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">Message de {{ $username }} ({{ $email }})</p>
            <p class="c-p">{{ $content }}</p>
        </columns>
    </row>
@stop
