@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">Bonjour</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Le crédit automatique de
                <a href="{{ route('admin.users.show',['id'=> $sepa->user_id]) }}">{{ $username }}</a>
                arrive à terme le {{ $sepa->end_date->format('d/m/Y') }}.
            </p>

            <p class="c-p">
                Pour arrêter ce crédit automatique vous devez vous rendre sur
                <a href="{{ route('admin.operations.sepas') }}">la page liste des demandes SEPA</a>
            </p>

            <spacer size="20"></spacer>

            <p class="c-p">Cordialement,</p>
        </columns>
    </row>
@stop
