@extends('emails.layout')

@section('content')
    <row>
        <columns>
            <p class="c-p">Bonjour {{ $user['name'] }},</p>

            <spacer size="20"></spacer>

            <p class="c-p">
                Nous vous informons que votre compte {{ config('somoney.money') }} a été supprimé.
            </p>

            <br>
            <p class="c-p">
                Nous regrettons votre départ mais n'hésitez pas à nous recontacter si vous souhaitez de nouveau échanger
                dans le réseau
            </p>
        </columns>
    </row>
@stop
