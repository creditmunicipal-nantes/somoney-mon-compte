if ($('#payment-lock').length || $('#payment-register-lock').length) {
    var isRegistering = $('#payment-register-lock').length === 1;
    // +12h (60000 = 1 min)
    var end = new Date((new Date().getTime()) + (60000 * 60 * 12));

    setInterval(function () {
        if (Math.round((new Date()).getTime() / 1000) === Math.round(end.getTime() / 1000)) {
            if (isRegistering) {
                $('#payment-register-lock').modal('show');
                // delete session
                document.cookie.split(';')
                    .forEach(function (c) {
                        document.cookie =
                            c.replace(/^ +/, '').replace(/=.*/, '=;expires=' + new Date().toUTCString() + ';path=/');
                    });
                window.location.href = '/';
            } else {
                $('#payment-lock').modal('show');
                window.location.reload();
            }
        }
    }, 1000);
}