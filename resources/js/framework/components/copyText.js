// copytext.js
function copyText() {
    var Clipboard = require('clipboard');
    var clipboardInstance = new Clipboard('.js-copytextbtn');

    clipboardInstance.on('success', function (event) {
        var clipboardTrigger = $(event.trigger);
        clipboardTrigger.text('Texte copié');
        setTimeout(function () {
            clipboardTrigger.text('Copier');
        }, 3000);
    });
}

export default copyText;
