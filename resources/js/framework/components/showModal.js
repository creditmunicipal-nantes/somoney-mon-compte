// showModal.js
function showModal() {
	$('*[data-show]').modal('show');
}

export default showModal;
