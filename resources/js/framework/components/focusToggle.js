// focusToggle.js
function focusToggle() {

	var $formControl = $('.js-form-control');

	$formControl.focusin(function(event) {
		$(event.target).parents('div.c-form-group').addClass('is-focused');
	});

	$formControl.focusout(function(event) {
		$(event.target).parents('div.c-form-group').removeClass('is-focused');
	});

}

export default focusToggle;
