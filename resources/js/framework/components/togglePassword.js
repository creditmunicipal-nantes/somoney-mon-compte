// togglePassword.js
function togglePassword() {

	var $togglePassword = $('.js-toggle-password');

	$togglePassword.click(function() {
		if ($togglePassword.siblings('.c-form-control').attr('type') === 'password') {
			$togglePassword.siblings('.c-form-control').attr('type', 'text');
		}
		else {
			$togglePassword.siblings('.c-form-control').attr('type', 'password');
		}
	});


}

export default togglePassword;
