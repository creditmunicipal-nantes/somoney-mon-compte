// computeInput.js
function computeInput() {

    const $input = $('.js-input-computed');
    const $inputTarget = $('#' + $input.data('targetInput'));
    const ratio = parseFloat($input.data('computeRatio'));

    $inputTarget.keyup(event => {
        $input.val(parseFloat($(event.currentTarget).val()) * ratio);
    });

    $inputTarget.keyup();
}

export default computeInput;
