// tooltip.js
function tooltip() {
	$('.js-tooltip-trigger').tooltip({
		template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner c-tooltip-inner"></div></div>',
	});
}

export default tooltip;
