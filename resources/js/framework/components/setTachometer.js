// Tachometer
function setTachometer() {

	$('.js-tachometer').each(function() {
		//init
		var tachometerProgress = $(this).attr('data-progress');
		var tachometerNegatif = false;
		var tachometerDouble = false;
		// A part of the svg can be not visible, so we need to shift the progress bar
		var tachometerProgressPercent = 0;

		// value for progress bar gauge
		var tachometerProgressValue = 0;
		var StrokeValue = 0;

		// value for hand spinner
		var handAngle = 0;

		// take svg length
		var path = $(this).find('.c-tachometer__main').get(0);
		var lengthSvg = path.getTotalLength();

		if (tachometerProgress < 0) {
			tachometerProgress = -tachometerProgress;
			tachometerNegatif = true;
		}

		if ($(this).hasClass('c-tachometer--double')) {
			tachometerDouble = true;
		}

		// calcul value
		if (tachometerDouble) {
			tachometerProgressPercent = (tachometerProgress * (96 / 100) + 4);
			tachometerProgressValue = (lengthSvg * (100 - tachometerProgressPercent) / 100);
			StrokeValue = lengthSvg * 2 - tachometerProgressValue;
			handAngle = 90 * tachometerProgressPercent / 100;
		}

		else {
			tachometerProgressValue = (lengthSvg * (tachometerProgress) / 100);
			StrokeValue = lengthSvg - tachometerProgressValue;
			handAngle = (180 * tachometerProgress / 100) - 90;
		}

		// base css
		$(this).find('.c-tachometer__progress-value').css('stroke-dasharray', lengthSvg);

		// apply css
		if (tachometerDouble) {
			if (tachometerNegatif) {
				$(this).find('.c-tachometer__start .c-tachometer__progress-value').css('opacity', '1').css('stroke-dashoffset', StrokeValue);
				$(this).find('.c-tachometer__hand').css('transform', 'rotate(' + -handAngle + 'deg)');
			}
			else if (tachometerProgress !== null) {
				$(this).find('.c-tachometer__end .c-tachometer__progress-value').css('opacity', '1').css('stroke-dashoffset', StrokeValue);
				$(this).find('.c-tachometer__hand').css('transform', 'rotate(' + handAngle + 'deg)');
			}
		}
		else {
			$(this).find('.c-tachometer__progress-value').css('opacity', '1').css('stroke-dashoffset', StrokeValue);
			$(this).find('.c-tachometer__hand').css('transform', 'rotate(' + handAngle + 'deg)');
		}
	});
}

export default setTachometer;
