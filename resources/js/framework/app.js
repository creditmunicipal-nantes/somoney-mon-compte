global.$ = global.jQuery = require('jquery');

//vendors
import 'bootstrap-sass';
import 'bootstrap-3-typeahead';
import 'select2/dist/js/select2.full.js';
import 'select2/dist/js/i18n/fr.js';
import 'clipboard';
import '../main/cookiechoices.js';
// Import components
import tooltip from './components/tooltip';
import togglePassword from './components/togglePassword';
import focusToggle from './components/focusToggle';
import setTachometer from './components/setTachometer';
import copyText from './components/copyText';
import showModal from './components/showModal';
import enableConfirmationModals from '../main/confirmationModals';
import prepareReconversionMessage from '../main/reconversionConfirmationMessage';
import computeInput from './components/computeInput';
import submitOnChange from './utils/submitOnChange';
import clickChildSpin from './utils/clickChildSpin';
import imgLazyLoad from './utils/imgLazyLoad';

//const _ = require('lodash');

require('./fontawesome.js');

// Call components
tooltip();
togglePassword();
focusToggle();
setTachometer();
copyText();
showModal();
enableConfirmationModals();
prepareReconversionMessage();
computeInput();
submitOnChange();
clickChildSpin();
imgLazyLoad();
