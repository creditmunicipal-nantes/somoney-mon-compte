// imgLazyLoad.js

function imgLazyLoad() {
    const observer = lozad('.js-lazyload', {
        threshold: 0
    });
    observer.observe();
}

export default imgLazyLoad;
