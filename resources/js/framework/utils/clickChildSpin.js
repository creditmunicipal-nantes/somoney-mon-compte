// clickChildSpin.js

function clickChildSpin() {
    $('[data-child-spin]').click(event => {
        const $field = $(event.currentTarget);
        $field.find('svg').addClass('fa-spin fa-spinner').removeClass('fa-sync');
    });
}

export default clickChildSpin;