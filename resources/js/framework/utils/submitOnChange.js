// submitOnChange.js

function submitOnChange() {
    $('[data-submit]').change(event => {
        const $field = $(event.currentTarget);
        if (!$field.data('except') || $field.data('except') !== $field.val()) {
            $field.closest('form').submit();
        }
    });
}

export default submitOnChange;