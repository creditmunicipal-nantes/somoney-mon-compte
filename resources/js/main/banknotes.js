// banknotes.js

const updateAmount = () => {
    const $banknotesFields = $('.c-form-control--banknote');
    const $amountField = $('input[name=amount]');
    const $btnSubmit = $amountField.closest('form').find('button[type=submit]');
    let amount = 0;
    $banknotesFields.map((index, $banknoteField) => {
        amount += $banknoteField.value * $banknoteField.dataset.amount;
    });
    $amountField.val(amount);
    $btnSubmit.prop('disabled', amount === 0);
};

/**
 * Compute the amount with all bank notes numbers
 *
 */
function banknotesCompute() {
    const $banknotesFields = $('.c-form-control--banknote');
    if ($banknotesFields.length) {
        $banknotesFields.keyup(() => {
            updateAmount();
        });
        updateAmount();
    }
}

/**
 * Update the banknote number when click on + or -
 *
 */
function updateBanknoteNumber() {
    const $banknotesFieldSubButtons = $('.c-form-input--banknote .btn-sub');
    const $banknotesFieldAddButtons = $('.c-form-input--banknote .btn-add');
    if ($banknotesFieldSubButtons.length) {
        $banknotesFieldSubButtons.click(event => {
            let $field = $(event.currentTarget).next();
            let value = _.toInteger($field.val()) || 0;
            if (value > 0) {
                $field.val(value - 1);
                $field.trigger('keyup');
            }
        });
    }
    if ($banknotesFieldAddButtons.length) {
        $banknotesFieldAddButtons.click(event => {
            let $field = $(event.currentTarget).prev();
            let value = _.toInteger($field.val()) || 0;
            $field.val(value + 1);
            $field.trigger('keyup');
        });
    }
}

export {
    banknotesCompute,
    updateBanknoteNumber
};
