// charts.js

import Chart from 'chart.js';

export function creditOperationsChart() {
    var chartDiv = $('#creditOperationsChart');

    var today = new Date();
    var begin = new Date();

    begin.setDate(today.getDate() - 30);

    var params = {
        type: 'credit',
        begin: getDateString(begin),
        end: getDateString(today)
    };

    if (chartDiv.length) {

        $.get('/api/statistics/operations', params, function(response) {
            var chart = new Chart(chartDiv, {
                title: 'Somme des opérations de crédit',
                type: 'line',
                data: {
                    labels: response.intervals,
                    datasets: [
                        {
                            label: 'Professionels',
                            data: objectToArray(response.professionals),
                            backgroundColor: 'rgba(0, 163, 179, 0.2)'
                        },
                        {
                            label: 'Particuliers',
                            data: objectToArray(response.individuals),
                            backgroundColor: 'rgba(94, 67, 131, 0.2)'
                        }
                    ]
                }
            });
        });
    }
}

export function debitOperationsChart() {
    var chartDiv = $('#debitOperationsChart');

    var today = new Date();
    var begin = new Date();

    begin.setDate(today.getDate() - 30);

    var params = {
        type: 'debit',
        begin: getDateString(begin),
        end: getDateString(today)
    };

    if (chartDiv.length) {

        $.get('/api/statistics/operations', params, function(response) {
            var chart = new Chart(chartDiv, {
                title: 'Somme des opérations de débit',
                type: 'line',
                data: {
                    labels: response.intervals,
                    datasets: [
                        {
                            label: 'Professionels',
                            data: objectToArray(response.professionals),
                            backgroundColor: 'rgba(0, 163, 179, 0.2)'
                        },
                        {
                            label: 'Particuliers',
                            data: objectToArray(response.individuals),
                            backgroundColor: 'rgba(94, 67, 131, 0.2)'
                        }
                    ]
                }
            });
        });
    }
}

export function usersRepartitionChart() {
    var chartDiv = $('#usersRepartitionChart');

    if (chartDiv.length) {

        $.get('/api/statistics/users-count', [], function(response) {
            var chart = new Chart(chartDiv, {
                title: 'Répartition des professionels / particuliers',
                type: 'doughnut',
                data: {
                    labels: ['Particuliers', 'Professionels'],
                    datasets: [
                        {
                            label: 'Répartition des professionels / particuliers',
                            data: [response.individual, response.professional],
                            backgroundColor: [
                                'rgba(94, 67, 131, 0.2)',
                                'rgba(0, 163, 179, 0.2)'
                            ]
                        }
                    ]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false
                }
            });
        });
    }
}

export function registersChart() {
    var chartDiv = $('#registersChart');

    var today = new Date();
    var begin = new Date();

    begin.setDate(today.getDate() - 30);

    var params = {
        begin: getDateString(begin),
        end: getDateString(today)
    };

    if (chartDiv.length) {

        $.get('/api/statistics/registers', params, function (response) {
            var chart = new Chart(chartDiv, {
                title: 'Inscriptions par jour (30 derniers jours)',
                type: 'bar',
                data: {
                    labels: response.intervals,
                    datasets: [
                        {
                            label: 'Professionnels',
                            data: objectToArray(response.professionals),
                            backgroundColor: 'rgba(0, 163, 179, 0.2)'
                        },
                        {
                            label: 'Particuliers',
                            data: objectToArray(response.individuals),
                            backgroundColor: 'rgba(94, 67, 131, 0.2)'
                        }
                    ]
                }
            });
        });
    }
}

function objectToArray(object) {
    return $.map(object, function(value, key) {
        return {
            x: key,
            y: value
        };
    });
}

function getDateString(date) {
    return 'Y-m-d'.replace('Y', date.getFullYear())
        .replace('m', date.getMonth() + 1)
        .replace('d', date.getDate());
}
