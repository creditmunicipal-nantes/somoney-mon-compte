// phoneFormat.js
function phoneFormat() {

    $('.js-phone').keyup((event) => {
        $(event.currentTarget).val(
            (i, phone) => phone.replace(/\s/g, '').substr(0, 10).replace(/(\d{2})/g, '$1 ').replace(/\s$/, '')
        );
    });

    // format on load
    $('.js-phone').keyup();

}

export default phoneFormat;
