const Tagify = require('@yaireo/tagify');

export function tagsInput() {
    $('input[data-tagify]').each(function (index, element) {
        new Tagify(element, {
            originalInputValueFormat: valuesArr => valuesArr.map(item => item.value).join(',')
        });
    });
}
