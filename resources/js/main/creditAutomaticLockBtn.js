// creditAutomaticLockBtn.js

export function creditAutomaticLockBtn() {

    $('#checkbox-automatic').change(event => {
        $('#btn-bank-card-payment').prop('disabled', $(event.currentTarget).is(':checked'));
    });
}
