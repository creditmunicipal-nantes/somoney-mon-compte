if (document.getElementById('map')) {

    let autocomplete = null;
    let oldValueCity = null;
    let oldValueAddress = null;

    const setAddressSearchOldValues = () => {
        oldValueCity = $('input[name="gmap[city]"]').val();
        oldValueAddress = $('input[name="gmap[addressLine1]"]').val() + ' ' + $('input[name="gmap[city]"]').val();
    };

    const typeSanitize = type => type === '(cities)' ? 'city' : type;

    const generateAutocomplete = (type, input, map, placeChanged, clearData) => {
        const typeSanitized = typeSanitize(type);
        $(input).attr(
            'placeholder',
            $(input).data('placeholder' + typeSanitized.charAt(0).toUpperCase() + typeSanitized.slice(1))
        );
        const options = {
            componentRestrictions: {country: 'fr'}
        };
        autocomplete = new google.maps.places.Autocomplete(input, options);
        autocomplete.setTypes([type]);
        autocomplete.bindTo('bounds', map);
        autocomplete.setFields(['address_components', 'geometry']);

        autocomplete.addListener('place_changed', placeChanged);

        $('.label-gmap').toggleClass('u-hide');

        if (clearData) {
            $(input).val(type === '(cities)' ? oldValueCity : oldValueAddress);
            $('input[name="gmap[addressLine1]"]').val('');
            $('input[name="gmap[city]"]').val('');
            $('input[name="gmap[zip]"]').val('');
            $('input[name="gmap[latitude]"]').val('');
            $('input[name="gmap[longitude]"]').val('');
        }
    };

    const addressNotFoundToggler = (init, input, map, placeChanged) => {
        if (! init) {
            $('#address-not-found-block').toggleClass('u-hide');
        }

        if ($('#address-not-found-block').hasClass('u-hide')) {
            generateAutocomplete('address', input, map, placeChanged, ! init);
        } else {
            generateAutocomplete('(cities)', input, map, placeChanged, ! init);
        }
        if (init) {
            placeChanged();
        }

        return false;
    };

    $.when(
        $.getScript('https://maps.googleapis.com/maps/api/js?key=' + GOOGLE_MAP.api_key + '&libraries=places'),
        $.Deferred(function (deferred) {
            $(deferred.resolve);
        })
    ).done(() => {
        const geocoder = new google.maps.Geocoder();
        const map = new google.maps.Map(document.getElementById('map'), {
            zoom: parseInt(GOOGLE_MAP.zoom),
            center: _.mapValues(GOOGLE_MAP.center, (attr) => _.toNumber(attr)),
            mapTypeControl: false
        });

        const input = document.getElementById('address-search');
        const marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, - 29)
        });

        const placeChanged = () => {
            marker.setVisible(false);
            const place = autocomplete.getPlace();

            if (place) {
                let address = '';
                let city = '';
                let zipcode = '';
                _.map(place.address_components, component => {
                    if (_.intersection(component.types, ['street_number', 'route']).length > 0) {
                        address += component.short_name + ' ';
                    }
                    if (component.types.includes('locality')) {
                        city = component.short_name;
                    }
                    if (component.types.includes('postal_code')) {
                        zipcode = component.short_name;
                    }
                });

                $('input[name="gmap[addressLine1]"]').val(address.replace(/\s$/, ''));
                $('input[name="gmap[city]"]').val(city);
                $('input[name="gmap[zip]"]').val(zipcode);
                $('input[name="gmap[latitude]"]').val(place.geometry.location.lat());
                $('input[name="gmap[longitude]"]').val(place.geometry.location.lng());
                setAddressSearchOldValues();

                if (! zipcode) {
                    let latLng = {
                        lat: parseFloat(place.geometry.location.lat()),
                        lng: parseFloat(place.geometry.location.lng())
                    };
                    geocoder.geocode({'location': latLng}, function (results, status) {
                        if (status === 'OK') {
                            if (results[0]) {
                                _.map(results[0].address_components, component => {
                                    if (component.types.includes('postal_code')) {
                                        zipcode = component.short_name;
                                    }
                                });

                                $('input[name="gmap[zip]"]').val(zipcode);
                            }
                        }
                    });
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
            }
        };

        setAddressSearchOldValues();
        generateAutocomplete('address', input, map, placeChanged, false);

        let location = {
            'lat': _.toNumber($('input[name="gmap[latitude]"]').val()),
            'lng': _.toNumber($('input[name="gmap[longitude]"]').val())
        };
        const initPlaceMarker = () => {
            marker.setVisible(false);
            // If the place has a geometry, then present it on a map.
            map.setCenter(location);
            map.setZoom(17);  // Why 17? Because it looks good.
            marker.setPosition(location);
            marker.setVisible(true);
        };

        $('#address-not-found-link').click(() => {
            addressNotFoundToggler(false, input, map, placeChanged);
        });

        $('#address-search').keypress(e => e.which !== 13);

        addressNotFoundToggler(true, input, map, placeChanged);
        placeChanged();

        if (location.lat && location.lng) {
            initPlaceMarker();
        }
    });

}
