// ibanFormat.js
/**
 * Format iban field on key pressed
 *
 */
function ibanFormatField() {

    const ibanField = $('.iban-field');

    if (ibanField.length) {
        ibanField.keyup((event) => {
            let ibanFieldHtml = $(event.currentTarget);

            ibanFieldHtml.val(ibanFormat(ibanFieldHtml.val()));
        });
    }
}

/**
 * Format iban label on load
 *
 */
function ibanFormatLabel() {

    const ibanLabel = $('.iban-label');
    if (ibanLabel.length) {
        ibanLabel.text(ibanFormat(ibanLabel.html()));
    }
}

/**
 *  Format string of IBAN as pretty
 *
 * @param ibanStr
 * @return {string}
 */
function ibanFormat(ibanStr) {

    return ibanStr.replace(/\s/g, '')
        .toUpperCase()
        .replace(/(\w{4})/g, '$1 ')
        .replace(/\s$/, '');
}

export {
    ibanFormatField,
    ibanFormatLabel
};
