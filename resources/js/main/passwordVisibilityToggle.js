let passwordVisible = false;

$('#password-toggler').click((event) => {
    passwordVisible = !passwordVisible;

    if (passwordVisible) {
        $(event.currentTarget).find('svg').attr('data-icon','eye-slash');
        $(event.currentTarget).parent().find('input').attr('type', 'text');
    } else {
        $(event.currentTarget).find('svg').attr('data-icon','eye');
        $(event.currentTarget).parent().find('input').attr('type', 'password');
    }
});