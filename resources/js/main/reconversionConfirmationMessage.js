function prepareReconversionMessage() {

    $(document).ready(function () {
        $('#to-confirm-modal').click(function () {
            var amount = $('#amount').val();
            var rate = window.app.conversionFeeRate;

            $('#reconversion-amount').text(parseFloat(amount).toFixed(2));
            $('#reconversion-substract').text(parseFloat(rate*100));
            $('#final-amount').text(parseFloat(amount - (rate * amount)).toFixed(2));
        });
    });
}

export default prepareReconversionMessage;