function enableConfirmationModals() {
    $( document ).ready(function() {
        // prevent form submit
        $('#to-confirm-modal').click(function (e) {
            e.preventDefault();
        });

        // submit form after confirmation modals button clicked
        $('#modal-submit-form').click(function() {
            $('#submit-from-modal').submit();
        });
    });
}

export default enableConfirmationModals;