// transferShowPermanentParams.js

export function transferShowPermanentParams() {

    $('#checkbox-recurrent-transfer').change(event => {
        $('#transfer-permanent-params').toggleClass('u-hide');
        $('#transfer-permanent-date-suffix').toggleClass('u-hide');
    });
}
