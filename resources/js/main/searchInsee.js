// searchInsee.js

if (document.getElementById('btn-search-insee')) {
    let companies = [];
    let companySelected = {};

    /**
     *  Search company with the text from the search field
     *
     */
    const searchCompanies = () => {
        setLoading(true);
        clearResults();
        clearMessage();
        $.ajax({
            method: 'POST',
            url: '/api/insee/search',
            datatype: 'json',
            delay: 250,
            data: {
                q: $('#search-insee-field').val()
            }
        }).done(data => {
            setLoading(false);
            companies = data;

            if (companies.length === 1) {
                selectCompany(0);
            } else {
                buildViewListCompanies();
            }

        }).fail((xhr) => {
            setLoading(false);
            let messages = ['Une erreur imprévu est survenue'];
            if (_.has(xhr.responseJSON, 'errors')) {
                messages = _.reduce(xhr.responseJSON.errors, (agg, error) => {
                    return _.merge(agg, _.values(error));
                });
            } else {
                messages = xhr.responseJSON.message.match(/messages/)
                    ? JSON.parse(xhr.responseJSON.message).messages
                    : messages;
            }
            setMessage('error', messages.join('<br/>'));
        });
    };

    /**
     *  Get more information on the company
     *
     * @param company
     */
    const companyDetails = company => {
        clearMessage();
        setLoading(true);
        $.ajax({
            method: 'GET',
            url: '/api/insee/details/' + company.siren,
            delay: 250
        }).done(data => {
            setLoading(false);
            companySelected.uniteLegale = data;
            clearResults();

            setSirenField(company);
            setEmployeeNumber(company);
            setCompanyName(company);
            setMessage(
                'success',
                `<div class="u-full-width u-inline-center u-mg-top u-mg-bottom u-bold">${companySelected.uniteLegale.denominationUniteLegale} - SIREN ${companySelected.siren} -
                ${companySelected.adresseEtablissement.libelleCommuneEtablissement}</div>`
            );
        });
    };

    /**
     *  Display a message under the field for searching
     *
     * @param type
     * @param message
     */
    const setMessage = (type, message) => {
        $('#message-search-insee').addClass(type).html(message);
    };

    /**
     *  Remove results find by searching
     */
    const clearResults = () => {
        companies = [];
        $('#results-search-insee').html('');
    };

    /**
     *  Remove the message below the search field
     */
    const clearMessage = () => {
        $('#message-search-insee').removeClass('error').removeClass('success').html('');
    };

    /**
     *  Show spin animation when it's searching
     *
     * @param boolean
     */
    const setLoading = boolean => {
        if (boolean) {
            $('#loading-search-insee').removeClass('u-hide');
        } else {
            $('#loading-search-insee').addClass('u-hide');
        }
    };

    /**
     * Update or add hidden field with siren value
     *
     * @param company
     */
    const setSirenField = company => {
        let $sirenField = $('input[name="siren"]');
        if ($sirenField.length) {
            $sirenField.first().val(company.siren);
        } else {
            $('#search-insee-field').closest('form').append($(`
            <input type="hidden" name="siren" value="${company.siren}"/>            
`));
        }
    };

    /**
     * Set automatically the value of the select of the employe number
     *
     * @param company
     */
    const setEmployeeNumber = company => {
        if (_.isObject(window.insee.values.trancheEffectifsEtablissement)) {
            let converted = insee.values.trancheEffectifsEtablissement[company.trancheEffectifsEtablissement];
            $('#field-employeenumber').find('option[value="' + converted + '"]').attr('selected', 'selected');
        }
    };
    /**
     * Set automatically the value of the field for the company name
     *
     * @param company
     */
    const setCompanyName = company => {
        if (_.isObject(window.insee.values.trancheEffectifsEtablissement)) {
            $('#field-name').val(_.capitalize(company.uniteLegale.denominationUniteLegale));
        }
    };

    /**
     * Select a company from the list
     *
     * @param indexCompany
     */
    const selectCompany = (indexCompany) => {
        companySelected = companies[indexCompany];
        $('#search-insee-field').val(companySelected.siren);
        companyDetails(companySelected);
    };

    /**
     *  Make list of anchor for selecting a company in the list
     */
    const buildViewListCompanies = () => {
        _.map(companies, (company, index) => {
            $('#results-search-insee').append(`
            <a id="company-suggested-${index}" 
                href="javascript:void(0);" 
                data-company-index="${index}" 
                class="company-suggested c-link u-full-width u-block u-mg-bottom">
                ${company.uniteLegale.denominationUniteLegale} - SIREN ${company.siren} - 
                ${company.adresseEtablissement.libelleCommuneEtablissement}
            </a>
            `);
        });
    };

    $(document).ready(() => {
        $('#results-search-insee').on('click', '.company-suggested', event => {
            event.preventDefault();
            selectCompany($(event.currentTarget).data('company-index'));

            return false;
        });

        $('#btn-search-insee').click(event => {
            event.preventDefault();
            searchCompanies();

            return false;
        });

        $('#search-insee-field').keypress(event => {
            if (event.keyCode === 13) {
                event.preventDefault();
                searchCompanies();

                return false;
            }
        });

        if ($('#search-insee-field').val().match(/^\d{9}$/)) {
            $('#btn-search-insee').click();
        }
    });
}