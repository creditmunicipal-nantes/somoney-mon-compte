const opositeUserType = userType => userType === 'individual' ? 'professional' : 'individual';

$('.disclaimer-toggler').click((event) => {
    let userType = $(event.currentTarget).data('userType');

    $('#disclaimer-btn-' + userType).addClass('is-active');
    $('#disclaimer-btn-' + opositeUserType(userType)).removeClass('is-active');

    $('#disclaimer-' + userType).removeClass('u-hide');
    $('#disclaimer-' + opositeUserType(userType)).addClass('u-hide');
});