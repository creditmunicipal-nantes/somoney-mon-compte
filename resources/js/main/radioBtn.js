$('.c-radio--btn+label').click(event => {
    let inputHtml = $(event.currentTarget).prev();
    inputHtml.attr('checked', 'checked');
    $('input[type="radio"][name="' + inputHtml.attr('name') + '"]+label.is-active').removeClass('is-active');
    $(event.currentTarget).addClass('is-active');
});

$('.c-checkbox--btn+label').click(event => {
    let inputHtml = $(event.currentTarget).prev();
    inputHtml.attr('checked', 'checked');
    $('input[type="checkbox"][name="' + inputHtml.attr('name') + '"]+label.is-active').removeClass('is-active');
    $(event.currentTarget).addClass('is-active');

    if ($(event.currentTarget).is('[data-submit]')) {
        $(event.currentTarget).closest('form').submit();
    }
});