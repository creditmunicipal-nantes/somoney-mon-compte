// dashboardTransfer.js
export function dashboardTransfer(selector) {
    selector.find($('.is-individual')).hide();
    selector.find($('.is-professional')).hide();
    selector.find($('input[name$="profil"]')).on('click', function () {
        selector.find($('div.desc > :input')).val(null);
        selector.find($('div.desc')).hide();
        selector.find($('.is-' + $(this).val())).show();
    });
    const oldSelected = selector.find('input[checked]');
    if (oldSelected.length) {
        oldSelected.click();
    }
}
