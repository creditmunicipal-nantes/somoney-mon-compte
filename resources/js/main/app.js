require('../framework/app');
import {fileInput} from './fileInput';
import {cityInput} from './api/cityInput';
import {professionalUsersCompletion} from './api/professionalUsersCompletion';
import {individualUsersCompletion} from './api/individualUsersCompletion';
import {sdgCompletion} from './api/sdgCompletion';
import {usersCompletion} from './api/usersCompletion';
import {dashboardTransfer} from './dashboardTransfer';
import {recurrentTransfer} from './recurrentTransfer';
import {singleCollapse} from './singleCollapse';
import {creditOperationsChart, debitOperationsChart, registersChart, usersRepartitionChart} from './charts';
import {disableComponent} from './disableComponent';
import {creditMethodChange} from './creditMethodChange';
import {creditAutomaticLockBtn} from './creditAutomaticLockBtn';
import {transferShowPermanentParams} from './transferShowPermanentParams';
import {ibanFormatField, ibanFormatLabel} from './ibanFormat';
import {tagsInput} from './tagsInput';
import phoneFormat from './phoneFormat';
import {banknotesCompute, updateBanknoteNumber} from './banknotes';

require('./passwordVisibilityToggle');
require('./disclaimerToggle');
require('./radioBtn');
require('./registerAddressMap');
require('../../../vendor/proengsoft/laravel-jsvalidation/public/js/jsvalidation');
require('./searchInsee');
require('./paymentLock');
require('./openConfirmDeleteModalByUrl');
require('./air-datepicker');

// Imports
// -----------------------

fileInput('.js-file-input');
cityInput();
professionalUsersCompletion();
individualUsersCompletion();
sdgCompletion();
usersCompletion();
dashboardTransfer($('.recipientChooser'));
recurrentTransfer($('.occurrencesCountChooser'));
singleCollapse();
creditOperationsChart();
debitOperationsChart();
usersRepartitionChart();
registersChart();
disableComponent('.js-disable-on-click');
creditMethodChange();
creditAutomaticLockBtn();
transferShowPermanentParams();
ibanFormatField();
ibanFormatLabel();
phoneFormat();
banknotesCompute();
updateBanknoteNumber();
tagsInput();
