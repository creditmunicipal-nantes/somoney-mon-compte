// cFileInput.js

export function fileInput(selector) {
    $(selector).on('change', function(element) {
        var value = element.target.value;
        if (value) {
            var fileName = value.split('\\').pop();
            $(this).parent()
                .find('.js-file-input-display')
                .val(fileName);
        }
    });

    $('.js-file-multiple-input').on('change', function(element) {
        var value = element.target.files;
        if (value) {
            var filesName = [];
            $.each(value, function(key, val) {
                filesName[key] = val.name;
            });

            $(this).parent().find('.js-file-input-display').val(filesName.toString());
        }
    });
}
