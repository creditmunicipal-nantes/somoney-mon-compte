// recurrentTransfer.js

export function recurrentTransfer(selector) {
	selector.find($('#transfer-occurence-number')).hide();

	$(document).ready(function() {
		selector.find($('form input[name="displayOccurencesCount"]')).change(function(event) {
			var show = event.currentTarget.value === 'true';
			selector.find($('#transfer-occurence-number')).toggle(show);
		});
	});
}
