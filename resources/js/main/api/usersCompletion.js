// users js

export function usersCompletion() {
    const $selector = $('select.autocomplete-users');
    let exclude = [$selector.attr('data-exclude')].filter(Boolean);
    let eventOnBalanceNull = !_.isUndefined($selector.attr('data-event-on-balance-null'));
    let $onEventDisabled = eventOnBalanceNull ? $($selector.attr('data-event-disable')) : null;
    let onlyMembers = !_.isUndefined($selector.attr('data-only-members'));
    const autocomplete = {
        options: {
            ajax: {
                url: '/api/users',
                datatype: 'json',
                delay: 250,
                data: params => (
                    {
                        name: params.term,
                        excluded_users_ids: exclude,
                        banknote_balance: eventOnBalanceNull,
                        only_members: onlyMembers
                    }
                ),
                processResults: (data, page) => (
                    {
                        results: data.data
                    }
                ),
                cache: true
            },
            minimumInputLength: 3,
            language: {
                inputTooShort: args => 'Votre recherche doit comporter au moins 3 caractères.',
                errorLoading: () => 'Les résultats ne peuvent pas être chargés.',
                noResults: () => 'Aucun résultat trouvé.',
                searching: () => 'Recherche en cours…'
            },
            templateResult: data => data.loading ? data.text : data.name,
            templateSelection: data => {
                return data.name ? data.name : data.text;
            },
            val: data => {
                return data.id ? data.id : data.text;
            }
        },
        init: function (element) {
            const $element = $(element);
            element.select2(this.options);
            $(element).on('select2:selecting', event => {
                if ($element.attr('multiple')) {
                    exclude.push(event.params.args.data.id);
                }
                if ($onEventDisabled) {
                    let disableCondition = eventOnBalanceNull && event.params.args.data.banknotes_balance === 0;
                    $onEventDisabled.toggleClass('disabled', disableCondition);
                }
            });
        }
    };

    autocomplete.init($selector);
    let userIdSelected = $selector.closest('form').data('userSelected');
    if (userIdSelected) {
        $.ajax({
            url: '/api/user',
            datatype: 'json',
            delay: 250,
            data: {
                id: userIdSelected
            }
        }).done((data) => {
            $selector.append(new Option(data.name, data.id, true, true));
            if (typeof $selector.data('submit') === 'undefined') {
                $selector.val([data.id]).trigger('change');
            }
            exclude.push(data.id);
        });
    }
    let usersIdSelected = $selector.closest('form').data('usersSelected');
    if (usersIdSelected) {
        _.map(usersIdSelected, userIdSelected => {
            $.ajax({
                url: '/api/user',
                datatype: 'json',
                delay: 250,
                data: {
                    id: userIdSelected
                }
            }).done((data) => {
                $selector.append(new Option(data.name, data.id, true, true));
                if (typeof $selector.data('submit') === 'undefined') {
                    $selector.val(usersIdSelected).trigger('change');
                }
                exclude.push(data.id);
            });
        });
    }
}
