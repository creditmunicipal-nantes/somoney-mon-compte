// professional users js
export function professionalUsersCompletion() {
    const $selector = $('select.autocomplete-professional-users');
    const exclude = $selector.attr('data-exclude');
    const autocomplete = {
        options: {
            placeholder: 'Rechercher ...',
            ajax: {
                url: '/api/professional-users',
                datatype: 'json',
                delay: 250,
                data: params => (
                    {
                        name: params.term,
                        excluded_users_ids: exclude
                    }
                ),
                processResults: (data, page) => (
                    {
                        results: data.data
                    }
                ),
                cache: true
            },
            minimumInputLength: 3,
            language: {
                inputTooShort: args => 'Votre recherche doit comporter au moins 3 caractères.',
                errorLoading: () => 'Les résultats ne peuvent pas être chargés.',
                noResults: () => 'Aucun résultat trouvé.',
                searching: () => 'Recherche en cours…'
            },
            templateResult: data => data.loading ? data.text : data.name,
            templateSelection: data => data.name ? data.name : data.text,
            val: data => data.id ? data.id : data.text,
            containerCssClass: 'c-form-control'
        },
        init: function (element) {
            element.select2(this.options);
        }
    };
    autocomplete.init($selector);
    let emailOldSelected = $('input[type="hidden"][name="professional_email_old"]').val();
    if (emailOldSelected) {
        $.ajax({
            url: '/api/professional-users',
            datatype: 'json',
            delay: 250,
            data: {
                email: emailOldSelected,
                excluded_users_ids: exclude
            }
        }).done((data) => {
            const user = data.data[0];
            $selector.append(new Option(user.name, user.id, true, true));
            if (typeof $selector.data('submit') === 'undefined') {
                $selector.val([user.id]).trigger('change');
            }
        });
    }
}
