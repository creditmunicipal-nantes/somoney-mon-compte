// individual users js

export function individualUsersCompletion() {
    const $selector = $('select.autocomplete-individual-users');
    const exclude = $selector.attr('data-exclude');
    const autocomplete = {
        options: {
            placeholder: 'Rechercher ...',
            ajax: {
                url: '/api/individual-users',
                datatype: 'json',
                delay: 500,
                data: params => (
                    {
                        email: params.term,
                        excluded_users_ids: exclude
                    }
                ),
                processResults: (data, page) => (
                    {
                        results: data.data
                    }
                ),
                cache: true
            },
            templateResult: data => data.loading ? data.text : data.name,
            templateSelection: data => data.name ? data.name : data.text,
            val: data => data.id ? data.id : data.text
        },
        init: function (element) {
            element.select2(this.options);
        }
    };
    autocomplete.init($selector);
    let emailOldSelected = $('input[type="hidden"][name="individual_email_old"]').val();
    if (emailOldSelected) {
        $.ajax({
            url: '/api/individual-users',
            datatype: 'json',
            delay: 500,
            data: {
                email: emailOldSelected,
                excluded_users_ids: exclude
            }
        }).done((data) => {
            const user = data.data[0];
            $selector.append(new Option(user.name, user.id, true, true));
            if (typeof $selector.data('submit') === 'undefined') {
                $selector.val([user.id]).trigger('change');
            }
        });
    }
}
