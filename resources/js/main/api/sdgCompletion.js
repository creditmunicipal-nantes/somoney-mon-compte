// sdg completion js

export function sdgCompletion() {
    const $selector = $('select.autocomplete-sdg');
    const $categorySelect = $('select[name=category_id]');
    const autocomplete = {
        options: {
            placeholder: 'Choisissez une catégorie',
            ajax: {
                url: '/api/sdgs',
                datatype: 'json',
                delay: 500,
                data: params => (
                    {
                        search: params.term,
                        category_id: $categorySelect.val(),
                        professional_id: $selector.data('professionalId')
                    }
                ),
                processResults: (data, page) => (
                    {
                        results: data
                    }
                ),
                cache: true
            },
            templateResult: data => data.loading ? data.text : data.title,
            templateSelection: data => {
                if (data.title) {
                    $('#sdg-description').removeClass('u-hide');
                    $('#sdg-description-content').text(data.description);

                    return data.title;
                }
                $('#sdg-description').addClass('u-hide');

                return data.text;
            },
            val: data => data.id ? data.id : data.text
        },
        init: function (element) {
            element.select2(this.options);
        }
    };

    $categorySelect.change(() => {
        autocomplete.options.placeholder = 'Rechercher ...';
        $selector.select2('destroy').select2(autocomplete.options);
    });

    if ($categorySelect.val()) {
        autocomplete.options.placeholder = 'Rechercher ...';
    }
    autocomplete.init($selector);
}
