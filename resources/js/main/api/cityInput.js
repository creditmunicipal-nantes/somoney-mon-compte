// cityInput.js

export function cityInput() {
    //Input file$
    $('#ZipcodeInput').keyup(function() {
        var zipcode = $('#ZipcodeInput').val();
        if (zipcode.length === 5) {
            var params = {
                zipcode: zipcode
            };

            $.get('/api/cities', params, function(response) {
                $('#CitiesBinds').html(null);
                if (response.length > 0) {
                    $('#CitiesBinds')
                        .attr('disabled', null);
                    $.each(response, function(index, value) {
                        var newOpt = $('<option>', {value: value.name}).text(value.name + ' (' + value.zipcode + ')');
                        if ($('#FallbackCityInput').val() === value.name) {
                            newOpt.attr('selected', 'selected');
                        }
                        $('#CitiesBinds').append(newOpt);
                    });
                }
                else {
                    $('#CitiesBinds')
                        .append($('<option>')
                            .text('Aucune ville trouvée'));

                    $('#FallbackCityInputDiv').children('.c-alert').remove();
                    $('#FallbackCityInputDiv').prepend($('<div>', {class: 'c-alert'})
                        .text('Code postal non trouvé, veuillez entrer le nom de la ville que vous recherchez.'));
                    $('#FallbackCityInputDiv').removeClass('u-hide');
                    $('#ZipcodeInputDiv')
                        .addClass('u-hide')
                        .html(null);
                }
            });
        }
    });

    var timeout = null;

    $('#FallbackCityInput').keyup(function() {
        if (timeout !== null) {
            clearTimeout(timeout);
        }

        timeout = setTimeout(function() {
            timeout = null;

            var params = {
                name: $('#FallbackCityInput').val()
            };

            $.get('/api/cities', params, function(response) {
                $('#CitiesBinds').html(null);

                if (response.length > 0) {
                    $('#CitiesBinds')
                        .attr('disabled', null);
                    $.each(response, function(index, value) {
                        $('#CitiesBinds')
                            .append($('<option>', {value: value.name})
                                .text(value.name + ' (' + value.zipcode + ')'));
                    });
                }
            });
        }, 400);
    });

    $(document).ready(function() {
        $('#ZipcodeInput').keyup();
    });
}
