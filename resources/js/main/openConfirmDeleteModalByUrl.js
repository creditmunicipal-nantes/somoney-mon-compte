const urlParams = new URLSearchParams(window.location.search);
const leaveService = urlParams.get('leaveService');
if (leaveService) {
    $('[data-target="#confirmDelete"]').click();
}

