import 'air-datepicker/src/js/air-datepicker';
import 'air-datepicker/dist/js/i18n/datepicker.fr';

// configuration *******************************************************************************************************
const moment = require('moment');
const baseConfig = {
    language: 'fr',
    navTitles: {days: 'MM yyyy'},
    position: 'top left'
};

// date picker *********************************************************************************************************
const selectDate = (datePicker, $datePicker) => {
    const filledDate = moment($datePicker.val(), 'DD/MM/YYYY');
    if (filledDate.isValid()) {
        const instance = datePicker.data('datepicker');
        const dateObject = filledDate.toDate();
        instance.selectDate(dateObject);
        instance.date = dateObject;
    }
};
window.triggerDatePickerElementsDetection = () => {
    const $datePickers = $('.date-picker');
    if ($datePickers.length) {
        _.each($datePickers, (item) => {
            const $datePicker = $(item);
            const datePicker = $datePicker.datepicker({
                ...baseConfig,
                ...{
                    dateFormat: 'dd/mm/yyyy'
                },
                onShow: () => selectDate(datePicker, $datePicker)
            });
        });
    }
};
triggerDatePickerElementsDetection();
