// creditMethodChange.js

export function creditMethodChange() {
    const creditFormHtml = $('#credit-form');
    const inputType = $('#credit-form input[name=type]');

    $('.btn-payment-method').click(event => {
        const $btn = $(event.currentTarget);
        inputType.val($btn.data('paymentType'));
        creditFormHtml.submit();
    });
}
