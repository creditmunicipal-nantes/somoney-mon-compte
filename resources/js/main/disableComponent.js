export function disableComponent(selector) {

	$(selector).click(function() {
		$(selector).attr('disabled', 'true');
	});

}
