<?php

return [
    'groups' => [
        'all'          => 'Tous',
        'individual'   => 'Particuliers',
        'professional' => 'Professionnels',
        'prelogin'     => 'Page de connexion',
        'credit-page'  => 'Page de crédit',
    ],
];
