<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'accepted' => ':attribute doit être accepté.',
    'active_url' => ':attribute is not a valid URL.',
    'after' => ':attribute doit être postérieure à :date.',
    'after_or_equal' => ':attribute doit être identique ou postérieure à :date.',
    'alpha' => ':attribute may only contain letters.',
    'alpha_dash' => ':attribute may only contain letters, numbers, and dashes.',
    'alpha_num' => ':attribute may only contain letters and numbers.',
    'array' => ':attribute must be an array.',
    'before' => ':attribute doit être avant :date.',
    'before_or_equal' => ':attribute doit être :date ou avant.',
    'between' => [
        'numeric' => ':attribute must be between :min and :max.',
        'file' => ':attribute must be between :min and :max kilobytes.',
        'string' => ':attribute must be between :min and :max characters.',
        'array' => ':attribute must have between :min and :max items.',
    ],
    'boolean' => ':attribute doit être un booléen.',
    'be_major' => 'Vous devez avoir :age ans minimum.',
    'confirmed' => ':attribute et sa confirmation diffèrent.',
    'date' => ':attribute n\'est pas une date valide.',
    'date_format' => ':attribute n\'est pas au format :format.',
    'different' => ':attribute and :other must be different.',
    'digits' => ':attribute must be :digits digits.',
    'digits_between' => ':attribute must be between :min and :max digits.',
    'dimensions' => ':attribute has invalid image dimensions.',
    'distinct' => ':attribute field has a duplicate value.',
    'email' => ':attribute doit être une adresse email valide.',
    'exists' => 'selected :attribute is invalid.',
    'file' => ':attribute doit être un fichier.',
    'filled' => ':attribute field must have a value.',
    'gt' => [
        'numeric' => ':attribute doit être supérieure à :value.',
        'file' => 'La taille du fichier de :attribute doit être supérieure à :value kilo-octets.',
        'string' => ':attribute doit contenir plus de :value caractères.',
        'array' => 'Le tableau :attribute doit contenir plus de :value éléments.',
    ],
    'gte' => [
        'numeric' => ':attribute doit être supérieure ou égale à :value.',
        'file' => 'La taille du fichier de :attribute doit être supérieure ou égale à :value kilo-octets.',
        'string' => ':attribute doit contenir au moins :value caractères.',
        'array' => 'Le tableau :attribute doit contenir au moins :value éléments.',
    ],
    'image' => ':attribute doit être une image.',
    'in' => 'selected :attribute is invalid.',
    'in_array' => ':attribute field does not exist in :other.',
    'integer' => ':attribute must be an integer.',
    'ip' => ':attribute must be a valid IP address.',
    'json' => ':attribute must be a valid JSON string.',
    'max' => [
        'numeric' => ':attribute ne doit pas dépasser :max.',
        'file' => ':attribute ne doit pas dépasser :max ko.',
        'string' => ':attribute ne doit pas dépasser :max caractères.',
        'array' => ':attribute ne doit pas avoir plus de :max éléments.',
    ],
    'mimes' => ':attribute doit être au format: :values.',
    'mimetypes' => ':attribute must be a file of type: :values.',
    'min' => [
        'numeric' => ':attribute doit être supérieur à :min.',
        'file' => ':attribute doit faire au moins :min ko.',
        'string' => ':attribute doit faire au moins :min caractères.',
        'array' => ':attribute doit contenir au moins :min éléments.',
    ],
    'not_in' => 'selected :attribute is invalid.',
    'numeric' => ':attribute doit être un nombre.',
    'phone' => ':attribute est incorrecte (ex: 02 40 60 80 10).',
    'present' => ':attribute field must be present.',
    'regex' => ':attribute n\'est pas valide.',
    'required' => ':attribute est obligatoire.',
    'required_if' => ':attribute est requis lorsque :other est :value.',
    'required_unless' => ':attribute field is required unless :other is in :values.',
    'required_with' => ':attribute field is required when :values is present.',
    'required_with_all' => ':attribute field is required when :values is present.',
    'required_without' => ':attribute est requis lorsque :values n\'est pas présent.',
    'required_without_all' => ':attribute field is required when none of :values are present.',
    'same' => ':attribute and :other must match.',
    'size' => [
        'numeric' => ':attribute doit être :size.',
        'file' => ':attribute doit être de :size kilo-octets.',
        'string' => ':attribute doit être de :size caractères.',
        'array' => ':attribute doit contenir :size éléments.',
    ],
    'string' => ':attribute doit être une chaîne de caractères.',
    'timezone' => ':attribute must be a valid zone.',
    'unique' => ':attribute est déjà utilisé.',
    'unique_cyclos_email' => ':attribute est déjà utilisé.',
    'uploaded' => ':attribute failed to upload.',
    'url' => ':attribute n\'est pas une url valide.',
    //cyclos
    'cyclos' => [
        'timeBetweenPaymentsNotMet' => 'Le temps avec le paiement précédent est trop court',
        'insufficientBalance' => 'Le solde de votre compte est insuffisant pour effectuer l\'opération',
        'transfer' => [
            'min_occurrence' => 'Le virement permanent doit comporter minimum 2 échéances',
            'first_occurrence' => 'La date de la première échéances ne peut être aujourd\'hui',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
        'addressLine1' => [
            'manual_address_need_city_gmap' => 'La saisie manuel d\'une adresse nécessite la saisie de la ville ou commune sur Google Map',
        ],
        'zip' => [
            'required_without' => 'La ville ou commune saisie sur Google Map ne possède pas de code postal, veuillez le renseigner dans la saisie manuelle',
        ],
        'gmap.city' => [
            'city_gmap_need_manual_address' => 'Vous devez saisir une adresse manuellement si vous choisissez une ville ou commune sur Google Map',
        ],
        'banknotes' => [
            'amount' => [
                'return' => [
                    'max' => ':user ne possède pas :value :money en billets',
                ],
            ],
        ],
        'password' => [
            'regex' => 'Le mot de passe contient un ou plusieurs caractères invalides',
        ],

    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */
    'attributes' => [
        'amount' => 'Le montant',
        'birthday' => 'La date de naissance',
        'category' => 'La catégorie',
        'cgu' => 'Les conditions général d\'utilisation',
        'description' => 'La description',
        'email' => 'L\'email',
        'employeenumber' => 'Type de structure et nombre de salariés',
        'end_date' => 'La date de fin',
        'first_name' => 'Le prénom',
        'firstname' => 'Le prénom',
        'idattachment' => 'La pièce d\'identité',
        'images' => 'L\'image',
        'individual_email' => 'L\'email du particulier',
        'last_name' => 'Le nom',
        'lastname' => 'Le nom',
        'login' => 'L\'identifiant',
        'mailpro' => 'L\'email de la structure',
        'message' => 'Le message',
        'name' => 'Le nom',
        'number' => 'Le numéro',
        'old_password' => 'L\'ancien mot de passe',
        'password' => 'Le mot de passe',
        'phone' => 'Le numéro de téléphone',
        'professional_email' => 'Le nom de l\'entreprise',
        'profil' => 'Le type de destinataire',
        'proname' => 'La raison sociale',
        'start_date' => 'La date de début',
        'street' => 'La rue',
        'title' => 'Le libellé',
        'transfer_title' => 'Le libellé',
        'address' => 'L\'adresse',
        'address_gmap' => 'L\'adresse sur Google Map',
        'addressLine2' => 'Le complément d\'adresse',
        'acceptAgreement' => 'Accepter les conditions générales d\'utilisation',
        'gender' => 'La civilité',
        'gmap.addressLine1' => 'L\'adresse sur Google Map',
        'gmap.zip' => 'Le code postal sur Google Map',
        'gmap.city' => 'La ville sur Google Map',
        'gmap.latitude' => 'La latitude sur Google Map',
        'gmap.longitude' => 'La longitude sur Google Map',
        'addressLine1' => 'L\'adresse',
        'zip' => 'Le code postal',
        'city' => 'La ville',
        'user.customValues.aboutMe' => 'La description',
        'user.customValues.birthday' => 'La date de naissance',
        'user.customValues.facebookurl' => 'L\'url facebook',
        'user.customValues.gender' => 'Le genre',
        'user.customValues.keyword' => 'Les mots clés',
        'user.customValues.mailpro' => 'L\'email de la structure',
        'user.customValues.proname' => 'La raison sociale',
        'user.customValues.twitterurl' => 'L\'url twitter',
        'user.customValues.website' => 'Le site web',
        'user.customValues.firstname' => 'Prénom',
        'user.customValues.lastname' => 'Nom',
        'user.customValues.optinnewsletter' => 'Newsletter',
        'user.customValues.commercialinfo' => 'Recevoir de informations commercial des adhérents professionnels',
        'user.email' => 'L\'email',
        'user.name' => 'Le nom',
        'user.phone' => 'Le téléphone',
        'user.profile' => 'La photo de profil',
        'duration_month' => 'Durée en mois',
        'frequency' => 'Fréquence',
        'date' => 'La date',
        'professional' => 'Professionnel',
        'individual' => 'Particulier',
        'ribattachment' => 'Le RIB',
        'organisationattachment' => 'Le document relatif à la structure',
        'siren' => 'Le siren',
        'q' => 'La recherche',
        'filters' => 'Les filtres',
        'filters.dateBegin' => 'Le filtre de date de début',
        'filters.dateEnd' => 'Le filtre de date de fin',
        'banknotes' => 'Les billets',
        'user' => 'L\'utilisateur',
        'type' => 'Le type',
        'stock' => 'Le stock',
        'date_end' => 'Date de fin',
        'category_id' => 'Catégorie',
        'sdg_id' => 'Objectif',
        'content' => 'Le contenu',
        'send_date' => 'La date d\'envoi',
        'hour' => 'L\'heure',
        'now' => 'maintenant',
        'newsletter_image' => 'L\'image de la newsletter',
        'counter' => 'Nombre d\'achats',
        'counter_as_price' => 'Type de nombre d\'achats',
        'discount' => 'Réduction',
        'discount_as_percent' => 'Type de réduction',
        'groups_type' => 'Type d\'utilisateurs bénéficiaire',
        'membershipsonantaise' => 'l\'adhésion à l\'association',
        'company' => 'Entreprise',
    ],
];
