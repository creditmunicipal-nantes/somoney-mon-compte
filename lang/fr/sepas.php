<?php

return [

    'status' => [

        'label' => [

            'waiting'       => 'En Attente',
            'not-confirmed' => 'À confirmer (Utilisateur)',
            'validation'    => 'À Valider (Administrateur)',
            'done'          => 'Réalisé',
            'in-progress'   => 'En cours',
            'ended'         => 'Terminé',
            'canceled'      => 'Annulé',

        ],

    ],

];
