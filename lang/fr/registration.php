<?php

return [
    'steps' => [
        'account'    => 'Identifiant et Mot de passe',
        'personal'   => 'Information personnel',
        'society'    => 'Information société',
        'address'    => 'Adresse',
        'files'      => 'Documents',
        'cgu'        => 'CGU',
        'success'    => 'Validation adresse e-mail',
        'validation' => 'Adresse E-mail validé',
        'credit'     => 'Crédit son compte',
        'on-cyclos'  => 'Sur Cyclos',
    ],
];
