<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use App\Http\Controllers\Api\CityController;
use App\Http\Controllers\Api\Cyclos\AdsController;
use App\Http\Controllers\Api\Cyclos\CyclosUserController;
use App\Http\Controllers\Api\DataStudio\ExportsController;
use App\Http\Controllers\Api\InseeController;
use App\Http\Controllers\Api\SdgsController;
use App\Http\Controllers\Api\Statistics\OperationController;
use App\Http\Controllers\Api\Statistics\StatsUserController;
use App\Http\Controllers\Api\SystemPayController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\Webhooks\CEVController;
use App\Http\Controllers\Api\Webhooks\NotificationPaymentController;
use App\Http\Controllers\Api\Webhooks\WebhooksController;

Route::get('/cities', [CityController::class, 'index']);
Route::get('/users', [UserController::class, 'search']);
Route::get('/user', [UserController::class, 'findOne']);
Route::get('/professional-users', [UserController::class, 'searchProfessionals']);
Route::get('/individual-users', [UserController::class, 'searchIndividuals']);
Route::get('/sdgs', [SdgsController::class, 'search']);
Route::group([
    'prefix' => 'webhooks',
    'as' => 'webhooks.',
], function () {
    Route::get('', [WebhooksController::class, 'index']);
    Route::get('payment', [WebhooksController::class, 'payment']);
    Route::post('payment', [CEVController::class, 'store'])->name('payment');
    Route::group([
        'prefix' => 'notification',
        'as' => 'notification.',
        'middleware' => 'cyclosAuth',
    ], function () {
        Route::group([
            'prefix' => 'payment',
            'as' => 'payment.',
        ], function () {
            Route::post('mobile', [NotificationPaymentController::class, 'mobile'])->name('mobile');
        });
    });
});

Route::group([
    'prefix' => 'insee',
    'middleware' => 'registerAvailable:professional.siren',
], function () {
    Route::post('search', [InseeController::class, 'search'])->middleware('cors');
    Route::get('details/{siren}', [InseeController::class, 'details'])->middleware('cors');
});

//cyclos api to provide cyclos data
Route::group([
    'prefix' => 'cyclos',
    'namespace' => 'Cyclos',
    'as' => 'cyclos.',
], function () {
    Route::get('users', [CyclosUserController::class, 'index'])
        ->name('index');
    Route::get('users/categories', [CyclosUserController::class, 'categories']);
    Route::get('users/{userId}', [CyclosUserController::class, 'show']);
    Route::get('ads', [AdsController::class, 'index']);
    Route::get('ads/categories', [AdsController::class, 'categories']);
    Route::get('ads/{adId}', [AdsController::class, 'show']);
});

Route::group([
    'prefix' => 'statistics',
    'namespace' => 'Statistics',
], function () {
    Route::get('operations', [OperationController::class, 'index']);
    Route::get('users-count', [StatsUserController::class, 'usersCount']);
    Route::get('registers', [StatsUserController::class, 'registers']);
});

Route::group([
    'prefix' => 'exports',
    'as' => 'exports.',
], function () {
    Route::group([
        'prefix' => 'data-studio',
        'namespace' => 'DataStudio',
        'as' => 'data_studio.',
        'middleware' => 'dataStudioAuth',
    ], function () {
        Route::get('operations', [ExportsController::class, 'operations'])->name('operations');
        Route::get('members', [ExportsController::class, 'members'])->name('members');
    });
});

// SystemPay
Route::post('system-pay/callback', [SystemPayController::class, 'callback'])
    ->name('system_pay.callback');
