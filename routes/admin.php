<?php

use App\Http\Controllers\Admin\AdsController;
use App\Http\Controllers\Admin\CEVController;
use App\Http\Controllers\Admin\CountersController;
use App\Http\Controllers\Admin\DataStudio\ExportsController;
use App\Http\Controllers\Admin\LeavingUsersController;
use App\Http\Controllers\Admin\LegalDocumentController;
use App\Http\Controllers\Admin\MembershipsController;
use App\Http\Controllers\Admin\NewUsersController;
use App\Http\Controllers\Admin\NotificationController;
use App\Http\Controllers\Admin\Operations\HistoricController;
use App\Http\Controllers\Admin\Operations\ManualCreditController;
use App\Http\Controllers\Admin\Operations\ReconversionController;
use App\Http\Controllers\Admin\Operations\SepaController;
use App\Http\Controllers\Admin\PageController;
use App\Http\Controllers\Admin\SdgsController;
use App\Http\Controllers\Admin\UserController;

Route::get('', [PageController::class, 'home'])->name('home');

Route::group([
    'as' => 'users.',
], function () {
    Route::get('utilisateurs', [UserController::class, 'index'])->name('index');

    Route::group([
        'prefix' => 'utilisateur',
    ], function () {
        Route::get('export/xls', [UserController::class, 'membersExport'])->name('xls');
        Route::get('{id}', [UserController::class, 'show'])->name('show');
        Route::get('{id}/edition', [UserController::class, 'edit'])->name('edit');
        Route::post('{id}/mettre-a-jour', [UserController::class, 'update'])->name('update');
        Route::delete('{userId}', [UserController::class, 'delete'])->name('delete');
        Route::get('{id}/statut-email/rafraichir', [UserController::class, 'emailStatusRefresh'])
            ->name('email-status.refresh')
            ->middleware('serviceAvailable:email-status');

        Route::group([
            'prefix' => 'bons-plans',
            'as' => 'ads.',
        ], function () {
            Route::group([
                'prefix' => '/{userId}',
            ], function () {
                Route::get('/', [AdsController::class, 'index'])->name('index');
                Route::get('/ajouter', [AdsController::class, 'create'])->name('create');
                Route::post('/enregistrer', [AdsController::class, 'store'])->name('store');
                Route::get('/editer/{adId}', [AdsController::class, 'edit'])->name('edit');
                Route::post('/editer/{adId}', [AdsController::class, 'update'])->name('update');
            });
            Route::post('/supprimer/{adId}', [AdsController::class, 'delete'])->name('delete');
            Route::post('/supprimer-image/{imageId}', [AdsController::class, 'deleteImage'])->name('deleteImage');
        });
    });

    Route::group([
        'prefix' => 'nouveaux',
        'as' => 'new.',
    ], function () {
        Route::group([
            'prefix' => 'email',
            'as' => 'email.',
            'middleware' => 'serviceAvailable:email-status',
        ], function () {
            Route::get('{userType}/rafraichir', [NewUsersController::class, 'emailRefresh'])
                ->name('refresh')
                ->where('userType', '(professionnel|particulier)');
            Route::post('{
            localUser}/renvoyer', [NewUsersController::class, 'emailResend'])->name('resend');
        });
        Route::get('{userType}', [NewUsersController::class, 'index'])
            ->name('index')
            ->where('userType', '(professionnel|particulier)');
        Route::put('{userId}', [NewUsersController::class, 'valid'])->name('valid');
        Route::get('{localUser}/voir', [NewUsersController::class, 'show'])->name('show');
        Route::delete('{localUser}/supprimer/{confirm?}', [NewUsersController::class, 'destroy'])->name('destroy');
        Route::post('force-validation/{userType}/{localUser}/{type}', [NewUsersController::class, 'forceValidation'])
            ->name('forceValidation')
            ->where('userType', '(professionnel|particulier)')
            ->middleware('serviceAvailable:admin-valid-new');
    });

    Route::group([
        'prefix' => 'quittant',
        'as' => 'leaving.',
    ], function () {
        Route::get('', [LeavingUsersController::class, 'index'])->name('index');
        Route::delete('{userId}/supprimer', [LeavingUsersController::class, 'destroy'])->name('destroy');
        Route::post('{userId}/annuler', [LeavingUsersController::class, 'cancel'])->name('cancel');
    });
});

Route::group(['prefix' => 'operations', 'as' => 'operations.'], function () {
    Route::get('', [HistoricController::class, 'index'])->name('historic');
    Route::get('exporter', [HistoricController::class, 'export'])->name('historic.export');

    Route::group([
        'prefix' => 'crediter/utilisateur',
        'as' => 'user-credit.',
        'middleware' => 'serviceAvailable:admin-credit',
    ], function () {
        Route::get('', [ManualCreditController::class, 'index'])->name('index');
        Route::post('{user}', [ManualCreditController::class, 'store'])->name('store');
    });

    Route::group([
        'prefix' => 'reconvertir/utilisateur',
        'as' => 'reconversion-user.',
        'middleware' => 'serviceAvailable:reconversion-user',
    ], function () {
        Route::get('', [ReconversionController::class, 'index'])->name('index');
        Route::post('{user}', [ReconversionController::class, 'store'])->name('store');
    });

    Route::group([
        'middleware' => 'serviceAvailable:sepa-payment',
    ], function () {
        Route::get('sepas', [SepaController::class, 'sepas'])->name('sepas');
        Route::group(['prefix' => 'sepa/{sepa}', 'as' => 'sepa.'], function () {
            Route::put('update/start-date', [SepaController::class, 'updateStartDate'])->name('updateStartDate');
            Route::get('done', [SepaController::class, 'done'])->name('done');
            Route::get('active', [SepaController::class, 'active'])->name('active');
            Route::get('canceled', [SepaController::class, 'canceled'])->name('canceled');
            Route::get('stopped', [SepaController::class, 'stopped'])->name('stopped');
        });
        Route::get('sepa/xls', [SepaController::class, 'exportXls'])->name('sepa.xls');
    });
});

if (config('somoney.available.services.counters')) {
    Route::get('comptoirs', [CountersController::class, 'index'])->name('counters');
    Route::group([
        'prefix' => 'comptoir',
        'as' => 'counter.',
    ], function () {
        Route::get('creer', [CountersController::class, 'create'])->name('create');
        Route::post('enregistrer', [CountersController::class, 'store'])->name('store');
        Route::get('{counter}/editer', [CountersController::class, 'edit'])->name('edit');
        Route::put('{counter}/mettre-a-jour', [CountersController::class, 'update'])->name('update');
        Route::delete('{counter}/supprimer', [CountersController::class, 'destroy'])->name('destroy');
        Route::get('exporter', [CountersController::class, 'export'])->name('export');
    });
}

Route::group([
    'prefix' => 'adhesions',
    'as' => 'memberships.',
    'middleware' => 'serviceAvailable:membership',
], function () {
    Route::get('', [MembershipsController::class, 'index'])->name('index');
    Route::get('details/{userId}', [MembershipsController::class, 'show'])->name('show');
    Route::get('valider/adhesion/{membershipId}', [MembershipsController::class, 'validateMembership'])
        ->name('validate');
    Route::get('operations', [MembershipsController::class, 'operations'])->name('operations');
    Route::group([
        'prefix' => 'exporter',
        'as' => 'export.',
    ], function () {
        Route::get('membres', [MembershipsController::class, 'exportMemberships'])->name('members');
        Route::get('operations', [MembershipsController::class, 'exportOperations'])->name('operations');
    });
});

Route::group(['prefix' => 'cev', 'as' => 'cev.'], function () {
    Route::get('', [CEVController::class, 'index'])->name('index');
});

Route::resource('notification', NotificationController::class, [
    'only' => ['index', 'store', 'destroy'],
]);

Route::get('legal_documents/{filename}', [LegalDocumentController::class, 'show'])
    ->where('filename', '(.+)')
    ->name('legal_documents');

Route::group([
    'prefix' => 'exports',
    'as' => 'exports.',
], function () {
    Route::group([
        'prefix' => 'data-studio',
        'namespace' => 'DataStudio',
        'as' => 'data_studio.',
    ], function () {
        Route::get('operations', [ExportsController::class, 'operations'])->name('operations');
        Route::get('members', [ExportsController::class, 'members'])->name('members');
    });
});

// SDGs
Route::group([
    'prefix' => 'ODD',
    'as' => 'sdgs.',
    'middleware' => ['serviceAvailable:sdgs'],
], function () {
    Route::get('', [SdgsController::class, 'index'])->name('index');
    Route::get('{sdgCategory}/creer', [SdgsController::class, 'create'])->name('create');
    Route::post('{sdgCategory}/enregistrer', [SdgsController::class, 'store'])->name('store');
    Route::get('{sdg}/editer', [SdgsController::class, 'edit'])->name('edit');
    Route::put('{sdg}/mettre-a-jour', [SdgsController::class, 'update'])->name('update');
    Route::delete('{sdg}/supprimer', [SdgsController::class, 'destroy'])->name('destroy');
    Route::get('{sdg}/professionnels', [SdgsController::class, 'professionals'])->name('professionals');
    Route::post('export-pdf', [SdgsController::class, 'exportPdf'])->name('exportPdf');
    Route::post('export-xls', [SdgsController::class, 'exportXls'])->name('exportXls');
});
