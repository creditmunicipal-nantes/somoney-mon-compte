<?php

use App\Http\Controllers\Main\AuthController;
use App\Http\Controllers\Main\Counter\BanknotesController;
use App\Http\Controllers\Main\DocumentsController;
use App\Http\Controllers\Main\PageController;
use App\Http\Controllers\Main\PasswordController;
use App\Http\Controllers\Main\SdgsProfessionalController;
use App\Http\Controllers\Main\SeoController;

Route::get('/favoris', [PageController::class, 'favorite'])
    ->name('favorite');
Route::get('/404', [PageController::class, 'errorNotFound'])
    ->name('error');
Route::get('robots.txt', [SeoController::class, 'robots'])
    ->name('main.seo.robots');
Route::get('sitemap.xml', [SeoController::class, 'sitemap'])
    ->name('main.seo.sitemap');

/**
 * Authentication
 */
Route::group(['as' => 'auth.'], function () {
    Route::get('connexion', [AuthController::class, 'getLogin'])
        ->name('login')
        ->middleware('guest');
    Route::post('login', [AuthController::class, 'postLogin'])
        ->name('postLogin')
        ->middleware('guest');
    Route::get('connecte', [AuthController::class, 'logged'])
        ->name('logged')
        ->middleware('auth');
    Route::get('logout', [AuthController::class, 'logout'])
        ->name('logout')
        ->middleware('auth');
    Route::get('validation/email/{userId}', [AuthController::class, 'validationEmail'])
        ->name('validation.email')
        ->middleware('guest');
    Route::get('validate/email/{emailValidationToken}', [AuthController::class, 'validateEmail'])
        ->name('validate.email');
});

/**
 * Password change
 */
Route::group([
    'as' => 'password.',
    'prefix' => 'mot-de-passe',
], function () {
    Route::prefix('{type}')->where([
        'type', '^(' . config('auth.passwords.reset.expired') . ')|(' . config('auth.passwords.reset.forgot') . ')$',
    ])->group(function () {
        Route::get('formulaire', [PasswordController::class, 'request'])
            ->name('request');
        Route::post('formulaire', [PasswordController::class, 'requestStore']);
    });

    Route::get('changement/{resetToken}', [PasswordController::class, 'change'])
        ->name('change');
    Route::post('changement/{resetToken}', [PasswordController::class, 'changeStore']);
});

Route::get('/nouveau-mot-de-passe-et-cgu/{token}', [PasswordController::class, 'newPasswordAndCGU'])
    ->name('password.changeNewCGU');
Route::post('/nouveau-mot-de-passe-et-cgu/{token}', [PasswordController::class, 'storeNewPasswordAndCGU'])
    ->name('password.sendChangeNewCGU');

// authenticate
Route::group([
    'middleware' => ['auth'],
], function () {
    // Counter
    Route::group([
        'prefix' => 'comptoir',
        'as' => 'counter.',
        'namespace' => 'Counter',
        'middleware' => ['serviceAvailable:counters', 'isOperatorOfCounter'],
    ], function () {
        Route::get('{counter}', [BanknotesController::class, 'show'])
            ->name('show');
        Route::post('{counter}/transfert', [BanknotesController::class, 'transfer'])
            ->name('transfer');
        Route::post('{counter}/stock', [BanknotesController::class, 'stock'])
            ->name('stock');
    });

    // SDGs
    Route::group([
        'prefix' => 'ODD',
        'as' => 'sdgs.',
        'middleware' => ['serviceAvailable:sdgs', 'restrictToProfessional'],
    ], function () {
        Route::group([
            'as' => 'professional.',
            'middleware' => ['restrictToProfessional'],
        ], function () {
            Route::get('', [SdgsProfessionalController::class, 'index'])
                ->name('index');
            Route::get('categorie/{sdgCategory}', [SdgsProfessionalController::class, 'show'])
                ->name('show');
            Route::post('export-pdf', [SdgsProfessionalController::class, 'exportPdf'])
                ->name('exportPdf');
            Route::get('commencer', [SdgsProfessionalController::class, 'begin'])
                ->name('begin');
            Route::post('commencer', [SdgsProfessionalController::class, 'store'])
                ->name('store');
            Route::put('{sdg}/abandonner', [SdgsProfessionalController::class, 'finish'])
                ->name('finish');
            Route::put('{sdg}/atteint', [SdgsProfessionalController::class, 'stop'])
                ->name('stop');
        });
    });

    Route::get('legal_documents/{filename}', [DocumentsController::class, 'redirect'])
        ->where('filename', '(.+)');
});
