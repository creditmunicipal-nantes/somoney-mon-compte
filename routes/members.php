<?php

use App\Http\Controllers\Main\AccountController;
use App\Http\Controllers\Main\AdsController;
use App\Http\Controllers\Main\Contact\ContactAdviserController;
use App\Http\Controllers\Main\Contact\SponsorController;
use App\Http\Controllers\Main\FavoritesController;
use App\Http\Controllers\Main\FidelityController;
use App\Http\Controllers\Main\LemonWayController;
use App\Http\Controllers\Main\MembershipsRenewController;
use App\Http\Controllers\Main\NewsletterController;
use App\Http\Controllers\Main\Operations\CreditAutomaticController;
use App\Http\Controllers\Main\Operations\CreditController;
use App\Http\Controllers\Main\Operations\HistoricController;
use App\Http\Controllers\Main\Operations\PaymentRequestController;
use App\Http\Controllers\Main\Operations\TransferController;
use App\Http\Controllers\Main\PageController;
use App\Http\Controllers\Main\SystemPayController;
use App\Http\Middleware\VerifyCsrfToken;

// Lemonway
Route::group(['prefix' => 'lemonway', 'as' => 'lemonway.'], function () {
    Route::get('success/{context}', [LemonWayController::class, 'success'])->name('success');
    Route::get('cancel/{context}', [LemonWayController::class, 'cancel'])->name('cancel');
    Route::get('error/{context}', [LemonWayController::class, 'error'])->name('error');
});

// SystemPay
Route::group(['prefix' => 'system-pay', 'as' => 'system_pay.'], function () {
    Route::get('form/{initPayment}', [SystemPayController::class, 'form'])->name('form');
    Route::post('retour', [SystemPayController::class, 'feedback'])
        ->name('feedback')
        ->withoutMiddleware([VerifyCsrfToken::class]);
});

// authenticate
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', [PageController::class, 'home'])->name('home');
    Route::group(['middleware' => ['onlyMembers']], function () {
        // Operations
        Route::group(['prefix' => 'operations', 'as' => 'operations.', 'namespace' => 'Operations'], function () {
            // Historic
            Route::group(['prefix' => 'historique', 'as' => 'historic.'], function () {
                Route::get('', [HistoricController::class, 'index'])->name('index');
                // Exports
                Route::group(['prefix' => 'export', 'as' => 'export.'], function () {
                    Route::get('pdf', [HistoricController::class, 'generateExportPdf'])->name('pdf');
                    Route::get('xls', [HistoricController::class, 'generateExportXls'])->name('xls');
                });
            });

            // Credit
            Route::group([
                'prefix' => 'crediter',
                'as' => 'credit.',
                'middleware' => ['isLock', 'serviceAvailable:show-credit'],
            ], function () {
                Route::get('', [CreditController::class, 'index'])->name('index');
                Route::post('', [CreditController::class, 'store'])->name('store');
                Route::group(['prefix' => 'paiement', 'as' => 'payment.'], function () {
                    Route::get('carte-bancaire', [CreditController::class, 'paymentBankCard'])->name('bank-card');
                    Route::group([
                        'middleware' => ['restrictToProfessional', 'serviceAvailable:sepa-payment'],
                        'prefix' => 'sepa',
                        'as' => 'sepa.',
                    ], function () {
                        Route::get('', [CreditController::class, 'sepa'])->name('index');
                        Route::post('', [CreditController::class, 'sepaStore'])->name('store');
                    });
                });
                Route::get('valide', [CreditController::class, 'success'])->name('success');
                Route::group([
                    'prefix' => 'automatique',
                    'as' => 'automatic.',
                    'middleware' => ['individualNeedIdDocument', 'serviceAvailable:sepa-payment'],
                ], function () {
                    Route::get('', [CreditAutomaticController::class, 'index'])->name('index');
                    Route::post('', [CreditAutomaticController::class, 'store'])->name('store');
                    Route::get('{sepa}/confirmer', [CreditAutomaticController::class, 'confirm'])->name('confirm');
                    Route::get('{sepa}/annuler', [CreditAutomaticController::class, 'cancel'])->name('cancel');
                });
            });

            // Transfer
            Route::group(['prefix' => 'virement', 'as' => 'transfer.', 'middleware' => 'isLock'], function () {
                Route::get('', [TransferController::class, 'index'])->name('index');
                Route::post('', [TransferController::class, 'store'])->name('store');
                Route::get('recurrents', [TransferController::class, 'recurrentTransfer'])
                    ->name('recurrent-transfer.index');
            });

            // Payment Request
            Route::group(['prefix' => 'demande-paiement', 'as' => 'request.'], function () {
                Route::get('', [PaymentRequestController::class, 'index'])->name('index');
                Route::post('', [PaymentRequestController::class, 'store'])->name('store');
            });
        });

        // Dashboard
        Route::group(['as' => 'dashboard.'], function () {
            Route::get('mes-favoris', [FavoritesController::class, 'index'])->name('favorites.index');
            Route::post('ajouter-favoris', [FavoritesController::class, 'store'])->name('favorites.store');
            Route::delete('supprimer-favoris/{favoriteId}', [FavoritesController::class, 'delete'])
                ->name('favorites.delete');
        });

        // Account
        Route::group(['as' => 'account.', 'prefix' => 'mon-compte'], function () {
            Route::get('mes-informations', [AccountController::class, 'get'])->name('information');
            Route::put('/professional/mes-informations', [AccountController::class, 'update'])
                ->name('professional.information.put');
            Route::put('/individual/mes-informations', [AccountController::class, 'update'])
                ->name('individual.information.put');
            Route::get('/mes-alertes', [PageController::class, 'accountAlert'])->name('alert');
            Route::get('/widget-badge', [PageController::class, 'accountWidget'])->name('widget');
            Route::get('/supprimer-son-compte', [AccountController::class, 'delete'])->name('delete');
            Route::post('/modifier-mon-mot-de-passe', [AccountController::class, 'storeNewPassword'])
                ->name('password.change');
        });

        // Advertisements
        Route::group(['prefix' => 'bons-plans', 'as' => 'ads.'], function () {
            Route::get('', [AdsController::class, 'index'])->name('index');
            Route::get('/ajouter', [AdsController::class, 'create'])->name('create');
            Route::post('/ajouter', [AdsController::class, 'store'])->name('store');
            Route::get('/editer/{adId}', [AdsController::class, 'edit'])->name('edit');
            Route::post('/editer/{adId}', [AdsController::class, 'update'])->name('update');
            Route::post('/supprimer/{adId}', [AdsController::class, 'delete'])->name('delete');
            Route::post('/supprimerImage/{imageId}', [AdsController::class, 'deleteImage'])->name('deleteImage');
        });

        // Contact
        Route::group(['as' => 'contact.'], function () {
            Route::get('contact', [ContactAdviserController::class, 'show'])->name('show');
            Route::post('contact-adviser-individual', [ContactAdviserController::class, 'storeIndividual'])
                ->name('adviser.individual');
            Route::post('contact-adviser-professional', [ContactAdviserController::class, 'storeProfessional'])
                ->name('adviser.professional');
        });

        // Sponsor
        Route::group(['as' => 'sponsor'], function () {
            Route::get('parrainage', [SponsorController::class, 'show'])->name('');
            Route::post('parrainage', [SponsorController::class, 'store'])->name('.sendMail');
        });

        // Fidelity
        Route::group([
            'as' => 'fidelity.',
            'middleware' => ['serviceAvailable:fidelity', 'restrictToProfessional'],
        ], function () {
            Route::get('fidelite', [FidelityController::class, 'show'])->name('show');
            Route::post('fidelite/mettre-a-jour', [FidelityController::class, 'update'])->name('update');
            Route::get('fidelite/supprimer/{fidelity}', [FidelityController::class, 'delete'])->name('delete');
        });

        Route::group([
            'as' => 'newsletter.',
            'middleware' => ['serviceAvailable:newsletters', 'restrictToProfessional'],
        ], function () {
            Route::get('newsletter', [NewsletterController::class, 'index'])->name('index');
            Route::get('newsletter/creer', [NewsletterController::class, 'create'])->name('create');
            Route::post('newsletter/enregistrer', [NewsletterController::class, 'store'])->name('store');
            Route::get('newsletter/editer/{newsletter?}', [NewsletterController::class, 'edit'])->name('edit');
            Route::put('newsletter/mettre-a-jour/{newsletter}', [NewsletterController::class, 'update'])
                ->name('update');
            Route::delete('newsletter/supprimer/{newsletter}', [NewsletterController::class, 'delete'])->name('delete');
        });

        Route::get('newsletter/unsubscribe', [NewsletterController::class, 'unsubscribe'])
            ->name('newsletter.unsubscribe');

        // Membership
        Route::group([
            'as' => 'memberships.renew.',
            'prefix' => 'adhesion/renouvellement/{membershipRenewToken}',
            'middleware' => 'serviceAvailable:membership',
        ], function () {
            Route::get('/', [MembershipsRenewController::class, 'index'])->name('index');
            Route::post('paiement', [MembershipsRenewController::class, 'payment'])->name('payment');
        });
    });
});
