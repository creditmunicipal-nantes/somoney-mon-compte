<?php

use App\Http\Controllers\Main\PageController;
use App\Http\Controllers\Main\Register\IndividualMembershipsController;
use App\Http\Controllers\Main\Register\IndividualRegistrationController;
use App\Http\Controllers\Main\Register\ProfessionalMembershipsController;
use App\Http\Controllers\Main\Register\ProfessionalRegistrationController;

Route::group([
    'prefix' => 'inscription',
    'as' => 'register',
    'middleware' => [
        'serviceAvailable:register',
        'sentryContext',
    ],
], function () {
    Route::get('', [PageController::class, 'register']);
    // part
    Route::group([
        'prefix' => 'particulier',
        'as' => '.individual.',
    ], function () {
        Route::group([
            'prefix' => 'informations',
            'as' => 'informations.',
        ], function () {
            Route::get('compte', [IndividualRegistrationController::class, 'account'])
                ->name('account');
            Route::post('compte', [IndividualRegistrationController::class, 'storeAccount'])
                ->name('account.post');
            Route::get('personnelles', [IndividualRegistrationController::class, 'informations'])
                ->name('personal')
                ->middleware('individualRegisterRedirect');
            Route::post('personnelles', [IndividualRegistrationController::class, 'postInformations'])
                ->name('personal.post');
            Route::get('adresse', [IndividualRegistrationController::class, 'address'])
                ->name('address')
                ->middleware('individualRegisterRedirect');
            Route::post('adresse', [IndividualRegistrationController::class, 'postAddress'])
                ->name('address.post');
        });
        Route::get('fichiers', [IndividualRegistrationController::class, 'files'])
            ->name('files')
            ->middleware('individualRegisterRedirect');
        Route::post('fichiers', [IndividualRegistrationController::class, 'postFiles'])
            ->name('files.post');
        Route::get('cgu', [IndividualRegistrationController::class, 'cgu'])
            ->name('cgu')
            ->middleware('individualRegisterRedirect');
        Route::post('cgu', [IndividualRegistrationController::class, 'postCgu'])
            ->name('cgu.post');
        Route::get('finalisation', [IndividualRegistrationController::class, 'success'])
            ->name('success')
            ->middleware('individualRegisterRedirect');
        Route::group([
            'prefix' => 'credit',
            'as' => 'credit.',
            'middleware' => 'serviceAvailable:membership',
        ], function () {
            Route::get('adherer', [IndividualMembershipsController::class, 'membership'])
                ->name('membership');
            Route::post('adherer', [IndividualMembershipsController::class, 'postMembership'])
                ->name('membership.post');
            Route::get('don', [IndividualMembershipsController::class, 'donate'])
                ->name('donate')
                ->middleware('individualMembershipRegisterRedirect', 'registerAvailable:individual.donate');
            Route::post('don', [IndividualMembershipsController::class, 'postDonate'])
                ->name('donate.post')
                ->middleware('registerAvailable:individual.donate');
            Route::get('paiement', [IndividualMembershipsController::class, 'payment'])
                ->name('payment')
                ->middleware('individualMembershipRegisterRedirect');
            Route::get('paiement/succes', [IndividualMembershipsController::class, 'paid'])
                ->name('paid');
        });
        Route::get('{email}/reprise', [IndividualRegistrationController::class, 'resumeRegistration'])
            ->name('email.resume')
            ->where(['email' => '^.+\@.+\..+$']);
        Route::get('email/validation/{emailValidationToken}', [IndividualRegistrationController::class, 'validation'])
            ->name('validation');
        Route::get(
            'reprise/{resumeRegistrationToken}',
            [IndividualRegistrationController::class, 'resumeRegistrationFromEmail']
        )->name('resume');
        Route::get(
            'need-membership/{needMembershipToken}',
            [IndividualRegistrationController::class, 'resumeNeedMembership']
        )
            ->name('need_membership');

        Route::get('echec', [IndividualRegistrationController::class, 'failCreate'])
            ->name('fail-create');
    });
    // pro
    Route::group([
        'prefix' => 'professionnel',
        'as' => '.professional.',
    ], function () {
        Route::group([
            'prefix' => 'informations',
            'as' => 'informations.',
        ], function () {
            Route::get('compte', [ProfessionalRegistrationController::class, 'account'])
                ->name('account')
                ->middleware('professionalRegisterRedirect');
            Route::post('compte', [ProfessionalRegistrationController::class, 'storeAccount'])
                ->name('account.post');
            Route::get('societe', [ProfessionalRegistrationController::class, 'informations'])
                ->name('society')
                ->middleware('professionalRegisterRedirect');
            Route::post('societe', [ProfessionalRegistrationController::class, 'postInformations'])
                ->name('society.post');
            Route::get('adresse', [ProfessionalRegistrationController::class, 'address'])
                ->name('address')
                ->middleware('professionalRegisterRedirect');
            Route::post('adresse', [ProfessionalRegistrationController::class, 'postAddress'])
                ->name('address.post');
        });
        Route::get('activite', [ProfessionalRegistrationController::class, 'activity'])
            ->name('activity')
            ->middleware('professionalRegisterRedirect');
        Route::post('activite', [ProfessionalRegistrationController::class, 'postActivity'])
            ->name('activity.post');
        Route::group([
            'middleware' => 'registerAvailable:professional.justification_documents',
        ], function () {
            Route::get('fichiers', [ProfessionalRegistrationController::class, 'files'])
                ->name('files')
                ->middleware('professionalRegisterRedirect');
            Route::post('fichiers', [ProfessionalRegistrationController::class, 'postFiles'])
                ->name('files.post');
        });
        Route::get('cgu', [ProfessionalRegistrationController::class, 'cgu'])
            ->name('cgu')
            ->middleware('professionalRegisterRedirect');
        Route::post('cgu', [ProfessionalRegistrationController::class, 'postCgu'])
            ->name('cgu.post');
        Route::get('finalisation', [ProfessionalRegistrationController::class, 'success'])
            ->name('success')
            ->middleware('professionalRegisterRedirect');
        Route::group([
            'prefix' => 'credit',
            'as' => 'credit.',
            'middleware' => 'serviceAvailable:membership',
        ], function () {
            Route::get('adherer', [ProfessionalMembershipsController::class, 'membership'])
                ->name('membership');
            Route::post('adherer', [ProfessionalMembershipsController::class, 'postMembership'])
                ->name('membership.post');
            Route::get('don', [ProfessionalMembershipsController::class, 'donate'])
                ->name('donate')
                ->middleware('professionalMembershipRegisterRedirect', 'registerAvailable:professional.donate');
            Route::post('don', [ProfessionalMembershipsController::class, 'postDonate'])
                ->name('donate.post')
                ->middleware('registerAvailable:professional.donate');
            Route::get('paiement', [ProfessionalMembershipsController::class, 'payment'])
                ->name('payment')
                ->middleware('professionalMembershipRegisterRedirect');
            Route::get('paiement/succes', [ProfessionalMembershipsController::class, 'paid'])
                ->name('paid');
        });
        Route::get('{email}/reprise', [ProfessionalRegistrationController::class, 'resumeRegistration'])
            ->name('email.resume')
            ->where(['email' => '^\w+@\w+\.\w+$']);
        Route::get(
            'email/validation/{emailValidationToken}',
            [ProfessionalRegistrationController::class, 'validation']
        )->name('validation');
        Route::get(
            'reprise/{resumeRegistrationToken}',
            [ProfessionalRegistrationController::class, 'resumeRegistrationFromEmail']
        )->name('resume');

        Route::get(
            'need-membership/{needMembershipToken}',
            [ProfessionalRegistrationController::class, 'resumeNeedMembership']
        )
            ->name('need_membership');

        Route::get('echec', [ProfessionalRegistrationController::class, 'failCreate'])
            ->name('fail-create');
    });
});
