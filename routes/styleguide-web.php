<?php

use App\Http\Controllers\Styleguide\PageController;

Route::get('', [PageController::class, 'home'])->name('styleguide.home');
