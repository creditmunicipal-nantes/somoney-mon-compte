<?php

return [
    'url' => env('INSEE_URL', 'https://api.insee.fr'),
    'api' => [
        'key' => env('INSEE_API_KEY'),
        'secret' => env('INSEE_SECRET_KEY'),
    ],
    'values' => [
        'number_units' => [
            'NN' => '4036693602914443112',
            //NN : Unité non employeuse (pas de salarié au cours de l'année de référence et pas d'effectif au 31/12)
            '00' => '4036693602914443112',
            //00 : 0 salarié (n'ayant pas d'effectif au 31/12 mais ayant employé des salariés au cours de l'année de référence)
            '01' => '4036693603182878568',
            //01 : 1 ou 2 salariés
            '02' => '4036693602377572200',
            //02 : 3 à 5 salariés
            '03' => '4036693602646007656',
            //03 : 6 à 9 salariés
            '11' => '4036693601840701288',
            //11 : 10 à 19 salariés
            '12' => '4036693602109136744',
            //12 : 20 à 49 salariés
            '21' => '4036693597008863080',
            //21 : 50 à 99 salariés
            '22' => '4036693597277298536',
            //22 : 100 à 199 salariés
            '31' => '4036693597277298536',
            //31 : 200 à 249 salariés
            '32' => '4036693597277298536',
            //32 : 250 à 499 salariés
            '41' => '4036693597277298536',
            //41 : 500 à 999 salariés
            '42' => '4036693597277298536',
            //42 : 1 000 à 1 999 salariés
            '51' => '4036693597277298536',
            //51 : 2 000 à 4 999 salariés
            '52' => '4036693597277298536',
            //52 : 5 000 à 9 999 salariés
            '53' => '4036693597277298536',
            //53 : 10 000 salariés et plus
        ],
        'way_type' => [
            'ALL' => 'Allée',
            'AV' => 'Avenue',
            'BD' => 'Boulevard',
            'CAR' => 'Carrefour',
            'CHE' => 'Chemin',
            'CHS' => 'Chaussée',
            'CITE' => 'Cité',
            'COR' => 'Corniche',
            'CRS' => 'Cours',
            'DOM' => 'Domaine',
            'DSC' => 'Descente',
            'ECA' => 'Ecart',
            'ESP' => 'Esplanade',
            'FG' => 'Faubourg',
            'GR' => 'Grande Rue',
            'HAM' => 'Hameau',
            'HLE' => 'Halle',
            'IMP' => 'Impasse',
            'LD' => 'Lieu dit',
            'LOT' => 'Lotissement',
            'MAR' => 'Marché',
            'MTE' => 'Montée',
            'PAS' => 'Passage',
            'PL' => 'Place',
            'PLN' => 'Plaine',
            'PLT' => 'Plateau',
            'PRO' => 'Promenade',
            'PRV' => 'Parvis',
            'QUA' => 'Quartier',
            'QUAI' => 'Quai',
            'RES' => 'Résidence',
            'RLE' => 'Ruelle',
            'ROC' => 'Rocade',
            'RPT' => 'Rond Point',
            'RTE' => 'Route',
            'RUE' => 'Rue',
            'SEN' => 'Sente - Sentier',
            'SQ' => 'Square',
            'TPL' => 'Terre-plein',
            'TRA' => 'Traverse',
            'VLA' => 'Villa',
            'VLGE' => 'Village',
            ' ' => '',
        ],

    ],
];