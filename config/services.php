<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
        'scheme' => 'api',
    ],
    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],
    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],
    'cyclos' => [
        'uri' => env('CYCLOS_URI', 'http://localhost'),
        'money' => env('CYCLOS_MONEY', 'somoney'),
        'user' => env('CYCLOS_USER', 'webservice@somoney.pro'),
        'password' => env('CYCLOS_PASSWORD', '1234'),
    ],
    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],
    'google' => [
        'analytics' => [
            'key' => env('GOOGLE_ANALYTICS'),
        ],
        'map' => [
            'api_key' => env('GOOGLE_API_KEY'),
        ],
        'geocoding' => [
            'api_key' => env('GOOGLE_SERVER_API_KEY', env('GOOGLE_API_KEY')),
            'url' => env('GOOGLE_GEOCODING_URL', 'https://maps.googleapis.com'),
        ],
        'data_studio' => [
            'api' => [
                'key' => env('DATA_STUDIO_AUTH_KEY'),
            ],
        ],
    ],
];
