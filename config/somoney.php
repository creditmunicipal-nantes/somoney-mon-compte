<?php

return [
    'domain' => [
        'landing' => env('DOMAIN_LANDING', env('DOMAIN')),
        'main' => env('DOMAIN_MAIN', 'moncompte.' . env('DOMAIN')),
        'styleguide' => env('DOMAIN_STYLEGUIDE', 'styleguide.' . env('DOMAIN')),
        'directory' => env('DOMAIN_DIRECTORY', 'annuaire.' . env('DOMAIN')),
    ],
    'mail' => [
        'account' => env('MAIL_ACCOUNT'),
        'adviser-individual' => env('MAIL_ADVISER_INDIVIDUAL'),
        'adviser-professional' => env('MAIL_ADVISER_PROFESSIONAL'),
        'weekly-report-contact' => env('MAIL_REPORT'),
    ],
    'payment-mode' => env('PAYMENT_MODE', null),
    'credit' => [
        'payment-types' => [
            'sepa' => 'sepa',
            'bank-card' => 'cb',
        ],
    ],
    'canals' => [
        'pos' => 'Point de vente',
        'webServices,main' => 'Site Web',
        'mobile' => 'Application mobile',
    ],
    'transfer' => [
        'types' => [
            'mobile',
        ],
    ],
    'form_constraints' => [
        'file' => [
            'maxSize' => 20,
            'extensions' => [
                'pdf', 'jpeg', 'bmp', 'gif', 'tiff', 'png',
            ],
        ],
        'image' => [
            'maxSize' => 10,
            'extensions' => [
                'jpeg', 'bmp', 'gif', 'tiff', 'png',
            ],
        ],
    ]
];
