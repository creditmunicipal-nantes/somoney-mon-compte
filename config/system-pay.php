<?php

use App\Models\Transaction;

return [

    'username' => env('SYSTEM_PAY_USERNAME'),
    'password' => env('SYSTEM_PAY_PASSWORD'),
    'endpoint' => env('SYSTEM_PAY_ENDPOINT'),
    'public_key' => env('SYSTEM_PAY_PUBLIC_KEY'),
    'sha_key' => env('SYSTEM_PAY_SHA_KEY'),

    'status' => [
        'PAID' => Transaction::STATUS_SUCCESS,
        'RUNNING' => Transaction::STATUS_PROCESSING,
        'UNPAID' => Transaction::STATUS_FAILED,
    ],

    'contracts' => [
        'memberships' => env('SYSTEM_PAY_CONTRACT_MEMBERSHIPS'),
        'credit' => env('SYSTEM_PAY_CONTRACT_CREDIT'),
    ],
];
