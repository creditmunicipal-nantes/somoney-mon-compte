<?php

return [

    'status' => [

        'icon' => [

            'waiting' => 'far fa-clock',
            'done' => 'far fa-check-circle',
            'in-progress' => 'fas fa-spinner',
            'ended' => 'far fa-calendar-check',
            'canceled' => 'fas fa-ban',
            'not-confirmed' => 'fas fa-user-clock',

        ],

    ],

];
