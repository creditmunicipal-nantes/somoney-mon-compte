<?php

return [
    'groups' => [
        'all',
        'individual',
        'professional',
        'prelogin',
        'credit-page',
    ],
];
