<?php

return [
    'user' => [
        'attributes' => [
            'name',
            'group',
            'email',
            'addresses',
            'mobilePhones',
            'landLinePhones',
            'contactInfos',
            'passwords',
            'images',
            'asMember',
            'customValues',
            'acceptAgreement',
        ],
    ],
    'api' => [
        'authorize-key' => env('CYCLOS_AUTHORIZE_KEY'),
    ],
    'internal-names' => [
        'groups' => [
            'professional' => 'professional',
            'individual' => 'individual',
        ],
        'admin' => 'admin',
        'genders' => [
            'man' => 'm',
            'female' => 'f',
        ],
        'qr_code' => 'codeqr',
    ],
];
