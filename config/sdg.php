<?php

$list = [
    [
        "id" => 1,
        "label" => "Éradication de la pauvreté",
        "slug" => "eradication-pauvrete",
        "color" => "#e41214",
    ],
    [
        "id" => 2,
        "label" => "Sécurité alimentaire et agriculture durable",
        "slug" => "securite-alimentaire-agriculture-durable",
        "color" => "#f4a400",
    ],
    [
        "id" => 3,
        "label" => "Santé et bien-être",
        "slug" => "sante-bien-etre",
        "color" => "#1f9e3a",
    ],
    [
        "id" => 4,
        "label" => "Éducation de qualité",
        "slug" => "education-qualite",
        "color" => "#e20817",
    ],
    [
        "id" => 5,
        "label" => "Égalité entre les femmes et les hommes",
        "slug" => "egalite-femmes-hommes",
        "color" => "#e74a10",
    ],
    [
        "id" => 6,
        "label" => "Gestion durable de l'eau pour tous",
        "slug" => "gestion-durable-eau-pour-tous",
        "color" => "#00a2c9",
    ],
    [
        "id" => 7,
        "label" => "Énergies propres et d'un coût abordable",
        "slug" => "energies-propres-et-d-un-cout-abordable",
        "color" => "#fbbf00",
    ],
    [
        "id" => 8,
        "label" => "Travail décent et croissance durable",
        "slug" => "travail-decent-croissance-durable",
        "color" => "#c7133c",
    ],
    [
        "id" => 9,
        "label" => "Infrastructures résilientes et innovation",
        "slug" => "infrastructures-resilientes-innovation",
        "color" => "#ec6804",
    ],
    [
        "id" => 10,
        "label" => "Réduction des inégalités",
        "slug" => "reduction-inegalites",
        "color" => "#e4004a",
    ],
    [
        "id" => 11,
        "label" => "Villes et communautés durables",
        "slug" => "villes-communautes-durables",
        "color" => "#f39402",
    ],
    [
        "id" => 12,
        "label" => "Consommation et production responsables",
        "slug" => "consommation-production-responsables",
        "color" => "#da8d08",
    ],
    [
        "id" => 13,
        "label" => "Lutte contre les changements climatiques",
        "slug" => "lutte-contre-changements-climatiques",
        "color" => "#147f3f",
    ],
    [
        "id" => 14,
        "label" => "Vie aquatique marine",
        "slug" => "vie-aquatique-marine",
        "color" => "#0099dd",
    ],
    [
        "id" => 15,
        "label" => "Vie terrestre",
        "slug" => "vie-terrestre",
        "color" => "#21a537",
    ],
    [
        "id" => 16,
        "label" => "Paix, justice et institutions efficaces",
        "slug" => "paix-justice-institutions-efficaces",
        "color" => "#016ca1",
    ],
    [
        "id" => 17,
        "label" => "Partenariats pour la réalisation des objectifs",
        "slug" => "partenariats-realisation-objectifs",
        "color" => "#016ca1",
    ],
];

$sdgCategories = [
    'id_raw' => [],
    'slug_raw' => [],
];

foreach ($list as $elem) {
    $sdgCategories['id_raw'][data_get($elem, 'id')] = $elem;
    $sdgCategories['slug_raw'][data_get($elem, 'slug')] = $elem;
}

return [
    'categories' => $sdgCategories,
];