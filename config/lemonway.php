<?php

return [
    'directkit' => [
        'url' => env('LW_DIRECTKIT_URL'),
    ],
    'directkit_json2' => env('LW_DIRECTKIT_JSON2_URL'),
    'webkit' => [
        'url' => env('LW_WEBKIT_URL'),
    ],
    'login' => env('LW_LOGIN'),
    'password' => env('LW_PASSWORD'),
    'lang' => env('LW_LANG', 'fr'),
    'debug' => env('LW_DEBUG', false),
    'default_expiration_duration' => env('LW_EXPIRATION', '1 year'),
    'ssl_verification' => env('LW_SSL_VERIFICATION', true),
    'xmlns' => env('LW_XMLNS', 'Service_mb_xml'),
    'mode' => env('LW_MODE', 'marketplace'),
    'vad_account' => env('LW_VAD_ACCOUNT'),
    'balance_min' => 200,
    'iban_id' => env('LW_IBAN_ID'),
    'money_out' => [
        'max' => env('LW_MONEY_OUT_MAX', 1000),
    ],

    'status' => [
        'success' => 0,
        'pending' => 4,
        'error' => 6,
        'wait_validation' => 16,
    ],
];
