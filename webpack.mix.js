let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .copy('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/fonts/fontawesome')
    .copy('custom-data/current/assets/fonts/**', 'public/assets/fonts')
    .copy('resources/img', 'public/assets/img')
    .copy('resources/img/email', 'public/assets/img/email')
    .copy('resources/img/icons', 'public/assets/img/icons')
    .copy('resources/img/sdgs', 'public/assets/img/sdgs')
    .copy('custom-data/current/assets/img', 'public/assets/img')
    .copy('resources/scss/images', 'public/assets/css/images')
    .copy('resources/css/**', 'public/assets/css')
    .copy('node_modules/html5shiv/dist/html5shiv.min.js', 'public/assets/js/somoney-ieSupport.js')

    // JS **************************************************************************************************************
    .js('resources/js/main/app.js', 'public/assets/js/somoney-main.js')
    .js('resources/js/styleguide/app.js', 'public/assets/js/somoney-styleguide.js')

    // SASS ************************************************************************************************************
    .sass('resources/scss/main.scss', 'public/assets/css/somoney-main.css')
    .sass('resources/scss/views/styleguide.scss', 'public/assets/css/somoney-styleguide.css')
    .sass('resources/scss/email/main.scss', 'public/assets/css/somoney-email.css')
    .sass('resources/scss/iframe/main.scss', 'public/assets/css/somoney-iframe.css')

    // Config **********************************************************************************************************
    .options({
        processCssUrls: false,
        cssNano: {discardComments: {removeAll: true}}
    })
    .autoload({
        lodash: ['_'],
        lozad: ['lozad', 'window.lozad'],
        jquery: ['$', 'jQuery', 'window.jQuery']
    })
    .extract(['lodash', 'jquery', 'lozad'])
    .sourceMaps();

if (mix.inProduction()) {
    mix.version().disableNotifications();
}
